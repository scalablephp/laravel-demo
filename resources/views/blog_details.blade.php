@extends('layouts.app')
@section('title', @$blog->name.' | Articles')
@section('banner')
<?php
$src = url("public/basicfront/img/banner_default.jpg");
if (!empty(@$blog->banner_image_url)) {
    $src = Storage::disk('azure')->url(@$blog->banner_image_url);
}
/*?>
<section class="parallax-window" data-parallax="scroll" data-image-src="{{ $src }}" data-natural-width="1400" data-natural-height="400">
    <div class="parallax-content-2">
        <div class="animated fadeInDown">
            <h1>Article</h1>
        </div>
    </div>
</section>
<?php */?>
<!-- End hero -->
@endsection

@section('content')
<!--<div id="position">
    <div class="container">
        <ul>
            <li><a href="{{ url("/") }}">Home</a></li>
            <li><a href="{{ url("/blogs") }}">Articles</a></li>
            <li>{{ @$blog->name }}</li>
        </ul>
    </div>
</div>-->
<!-- End position -->
<section class="page-title">
            <div class="container">
                <h1 class="page-title__title">{{ @$blog->name }}</h1>
                <ul class="single-post__list">
                    <li class="single-post__list-item">
                        <i class="la la-calendar-check-o"></i>
                        <span>{{date('F j, Y', strtotime(@$blog->created_at))}}</span>
                    </li>
                    <!--<li class="single-post__list-item">
                        <i class="la la-comments"></i>
                        <a href="#">2 Comments</a>
                    </li>-->
                </ul>
            </div>
        </section>

<!-- End container -->
<section class="blog-page">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8">
                        <div class="blog-page__box">
                                    
                            <!-- single-post module -->
                            <div class="single-post">

                                <img class="single-post__image" src="{{ Storage::disk('azure')->url(@$blog->banner_image_url) }}" alt="">

                                

                               

                                {!! html_entity_decode(@$blog->description) !!}

                                <div class="row">
                                    <!--<div class="col-md-6">
                                        <p class="single-post__tags">
                                            <i class="la la-tag"></i>
                                            <a href="#">Restaurants,</a>
                                            <a href="#">Travel</a>
                                        </p>
                                    </div>-->
                                    <div class="col-md-6">
                                        <p class="single-post__share-list">
                                            <i class="la la-share-alt"></i>
                                            <span>Share:</span>
                                            <a class="twitter" href="https://twitter.com/intent/tweet?url={{urlencode(Request::url())}}"><i class="fa fa-twitter"></i></a>
                                            <a class="facebook" href="https://www.facebook.com/sharer/sharer.php?u={{Request::url()}}"><i class="fa fa-facebook"></i></a>
                                            <!--<a class="pinterest" href="#"><i class="fa fa-pinterest"></i></a>-->
                                        </p>
                                    </div>
                                </div>

                                <!--<h3 class="single-post__line-title">
                                    Author
                                </h3>

                                <!-- author post box -->
                               <!-- <div class="author-post-box">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="author-post-box__content">
                                                <a class="author-post-box__image" href="#"><img src="upload/avatar1.jpg" alt=""></a>
                                                <h3 class="author-post-box__title">
                                                    <a href="#">Matt Smith</a>
                                                    <span class="author-post-box__location">
                                                        New York
                                                    </span>
                                                </h3>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <a class="author-post-box__btn follow-btn" href="#">
                                                <i class="la la-eye"></i>
                                                Follow
                                            </a>
                                        </div>
                                    </div>
                                </div>  -->                            
                                <!-- end author post box -->

                                <!-- other-posts -->
                               <!-- <div class="other-posts">
                                    <a class="other-posts__prev" href="#">
                                        <i class="la la-angle-left"></i>
                                        <div class="other-posts__prev-box">
                                            <span class="other-posts__desc">Previous Post</span>
                                            <h2 class="other-posts__title">
                                                Donec consectetuer ligula <br>vulputate sem tristique.
                                            </h2>
                                        </div>
                                    </a>
                                    <a class="other-posts__next" href="#">
                                        <i class="la la-angle-right"></i>
                                        <div class="other-posts__next-box">
                                            <span class="other-posts__desc">Next Post</span>
                                            <h2 class="other-posts__title">
                                                Ut aliquam sollicitudin leo.
                                            </h2>
                                        </div>
                                    </a>
                                </div>-->
                                <!-- end other-posts -->

                               <!-- <h3 class="single-post__line-title">
                                    2 Comments
                                </h3>-->

                                <!-- comments -->
                                <!--<div class="comments">
                                    <ul class="comments__list">
                                        <li class="comments__list-item">
                                            <img class="comments__list-item-image" src="upload/avatar2.jpg" alt="">
                                            <div class="comments__list-item-content">
                                                <h3 class="comments__list-item-title">
                                                    Philip W
                                                </h3>
                                                <span class="comments__list-item-location">
                                                    Ormskirk, United Kingdom
                                                </span>
                                                <span class="comments__list-item-date">
                                                    Posted October 7, 2018
                                                </span>
                                                <h3 class="comments__list-item-title">
                                                    Good Service but..
                                                </h3>
                                                <p class="comments__list-item-description">
                                                    Phasellus hendrerit. Pellentesque aliquet nibh nec urna. In nisi neque, aliquet vel, dapibus id, mattis vel, nisi. Sed pretium, ligula sollicitudin laoreet viverra, tortor libero sodales leo, eget blandit nunc tortor eu nibh. Nullam mollis. Ut justo. Suspendisse potenti.
                                                </p>
                                                <a class="comments__list-item-reply" href="#">
                                                    <i class="la la-mail-forward"></i>
                                                    Reply
                                                </a>
                                            </div>
                                        </li>
                                        <li class="comments__list-item">
                                            <img class="comments__list-item-image" src="upload/avatar2.jpg" alt="">
                                            <div class="comments__list-item-content">
                                                <h3 class="comments__list-item-title">
                                                    Philip W
                                                </h3>
                                                <span class="comments__list-item-location">
                                                    Ormskirk, United Kingdom
                                                </span>
                                                <span class="comments__list-item-date">
                                                    Posted October 7, 2018
                                                </span>
                                                <h3 class="comments__list-item-title">
                                                    Good Service but..
                                                </h3>
                                                <p class="comments__list-item-description">
                                                    Phasellus hendrerit. Pellentesque aliquet nibh nec urna. In nisi neque, aliquet vel, dapibus id, mattis vel, nisi. Sed pretium, ligula sollicitudin laoreet viverra, tortor libero sodales leo, eget blandit nunc tortor eu nibh. Nullam mollis. Ut justo. Suspendisse potenti.
                                                </p>
                                                <a class="comments__list-item-reply" href="#">
                                                    <i class="la la-mail-forward"></i>
                                                    Reply
                                                </a>
                                            </div>
                                        </li>
                                    </ul>
                                </div>-->
                                <!-- end comments -->

                                <!-- Contact form module -->
                                <!--<form class="contact-form" id="comment-form">
                                    <h2 class="contact-form__title">
                                        Write a Comment
                                    </h2>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <input class="contact-form__input-text" type="text" name="name" id="name" placeholder="Name:" />
                                        </div>
                                        <div class="col-md-6">
                                            <input class="contact-form__input-text" type="text" name="mail" id="mail" placeholder="Email:" />
                                        </div>
                                    </div>
                                    <textarea class="contact-form__textarea" name="comment" id="comment" placeholder="Comment"></textarea>
                                    <input class="contact-form__submit" type="submit" name="submit-contact" id="submit_contact" value="Send Comment" />
                                </form>-->
                                <!-- End Contact form module -->

                            </div>
                            <!-- End single-post module -->

                        </div>
                    </div>
                    <div class="col-lg-4">

                        <!-- sidebar -->
                        <div class="sidebar">

                            <div class="sidebar__widget sidebar__widget-category">
                                <h2 class="sidebar__widget-title">
                                    Categories
                                </h2>
                                <ul class="sidebar__category-list">
                                   <!-- <li><a href="#">Coffee</a></li>
                                    <li><a href="#">Travel</a></li>
                                    <li><a href="#">Restaurant</a></li>
                                    <li><a href="#">Art &amp; Culture</a></li>
                                    <li><a href="#">Hotels</a></li>
                                    <li><a href="#">Sport</a></li>
                                    <li><a href="#">Uncategorized</a></li>-->
                                </ul>
                            </div>

                            <div class="sidebar__widget sidebar__widget-tags">
                                <h2 class="sidebar__widget-title">
                                    Popular Tags
                                </h2>
                                <ul class="sidebar__tags-list">
                                    <li><a href="#">None</a></li>
                                    <!--<li><a href="#">Cafe</a></li>
                                    <li><a href="#">Restaurant</a></li>
                                    <li><a href="#">food</a></li>
                                    <li><a href="#">Romantic</a></li>
                                    <li><a href="#">Cozy</a></li>
                                    <li><a href="#">Sea Food</a></li>
                                    <li><a href="#">Healthy Food</a></li>
                                    <li><a href="#">Pasta</a></li>-->
                                </ul>
                            </div>

                            <!--<div class="sidebar__widget sidebar__widget-instagram">
                                <h2 class="sidebar__widget-title">
                                    Instagram
                                </h2>
                                <ul class="sidebar__instagram-list">
                                    <li><a href="#"><img src="upload/inst1.jpg" alt=""></a></li>
                                    <li><a href="#"><img src="upload/inst2.jpg" alt=""></a></li>
                                    <li><a href="#"><img src="upload/inst3.jpg" alt=""></a></li>
                                    <li><a href="#"><img src="upload/inst4.jpg" alt=""></a></li>
                                    <li><a href="#"><img src="upload/inst5.jpg" alt=""></a></li>
                                    <li><a href="#"><img src="upload/inst6.jpg" alt=""></a></li>
                                    <li><a href="#"><img src="upload/inst7.jpg" alt=""></a></li>
                                    <li><a href="#"><img src="upload/inst8.jpg" alt=""></a></li>
                                </ul>
                            </div>-->

                            <div class="sidebar__widget sidebar__widget-popular">
                                <h2 class="sidebar__widget-title">
                                    Popular Post
                                </h2>
                               <!-- <ul class="sidebar__popular-list">
                                    <li>
                                        <img src="upload/th1.jpg" alt="">
                                        <div class="sidebar__popular-list-cont">
                                            <h2 class="sidebar__popular-list-title">
                                                <a href="single-post.html">Nunc dignissim risus id metus.</a>
                                            </h2>
                                            <span class="sidebar__popular-list-desc">Oct 20, 2018, 0 Comments</span>
                                        </div>
                                    </li>
                                    <li>
                                        <img src="upload/th2.jpg" alt="">
                                        <div class="sidebar__popular-list-cont">
                                            <h2 class="sidebar__popular-list-title">
                                                <a href="single-post.html">Nunc dignissim risus id metus.</a>
                                            </h2>
                                            <span class="sidebar__popular-list-desc">Oct 20, 2018, 0 Comments</span>
                                        </div>
                                    </li>
                                    <li>
                                        <img src="upload/th3.jpg" alt="">
                                        <div class="sidebar__popular-list-cont">
                                            <h2 class="sidebar__popular-list-title">
                                                <a href="single-post.html">Nunc dignissim risus id metus.</a>
                                            </h2>
                                            <span class="sidebar__popular-list-desc">Oct 20, 2018, 0 Comments</span>
                                        </div>
                                    </li>
                                    <li>
                                        <img src="upload/th4.jpg" alt="">
                                        <div class="sidebar__popular-list-cont">
                                            <h2 class="sidebar__popular-list-title">
                                                <a href="single-post.html">Nunc dignissim risus id metus.</a>
                                            </h2>
                                            <span class="sidebar__popular-list-desc">Oct 20, 2018, 0 Comments</span>
                                        </div>
                                    </li>
                                </ul>-->
                            </div>

                            <!--<div class="sidebar__widget sidebar__advertise">
                                <span class="sidebar__advertise-title">
                                    advertising box
                                </span>
                                <img src="upload/add.jpg" alt="">
                            </div>-->

                        </div>
                        <!-- End sidebar -->

                    </div>
                </div>
            </div>
        </section>
@endsection
@section('footer')
<script type="text/javascript" src="{{ url("/public/basicfront/js/experience.js") }}"></script>
@endsection