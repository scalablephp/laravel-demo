@extends('admin.layouts.admin')
@section('title', 'Edit Service')
@section('content')
<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
@section('page-heading')
<h3 class="text-themecolor">Edit Service</h3>
@endsection
@section('page-breadcrumb')
<li class="breadcrumb-item"><a href="{{ url('bbadmin/services') }}">Service</a></li>
<li class="breadcrumb-item active">Edit Service</li>
@endsection
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <form id="frmService" action="{{ url("bbadmin/service/store") }}" method="post" enctype="multipart/form-data" novalidate>
                        {{ csrf_field() }}
                        <input type="hidden" name="id" id="id" value="<?php echo $eservice->id; ?>" />
                        <div class="form-group">
                            <h5>Name <span class="text-danger">*</span></h5>
                            <div class="controls">
                                <input type="text" id="name" name="name" class="form-control" value="<?php echo $eservice->name; ?>" required data-validation-required-message="This field is required" /> </div>
                            <div class="form-control-feedback"><small>The name is how it appear on the site.</small></div>
                        </div>
                        <div class="form-group">
                            <h5>Slug <span class="text-danger">*</span></h5>
                            <div class="controls">
                                <input type="text" id="slug" name="slug" class="form-control" value="<?php echo $eservice->slug; ?>" required data-validation-required-message="This field is required" /> </div>
                            <div class="form-control-feedback"><small>The "slug" is the URL-friendly version of the name. It is usually all lowercase and contains only letters, numbers, and hyphens.</small></div>
                        </div>
                        <div class="form-group">
                            <h5>Keywords</h5>
                            <div class="controls">
                                <textarea id="keywords" name="keywords" class="form-control"><?php echo $eservice->keywords; ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4">
                                    <h5>Parent Service</h5>
                                    <div class="controls">
                                        <select name="parent" id="parent" class="form-control">
                                            <option value="0" <?php echo ($eservice->parent == 0) ? "selected" : ""; ?>>None</option>
                                            <?php
                                            if (@$services) {
                                                foreach (@$services as $service) {
                                                    ?>
                                                    <option class="<?php echo ($service->type == 1) ? "service-type" : "service-type"; ?>" value="<?php echo $service->id; ?>" <?php echo ($eservice->parent == $service->id) ? "selected" : ""; ?>><?php echo $service->name; ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <h5>Display on Homepage</h5>
                                    <div class="controls">
                                        <select name="display_on_home" id="display_on_home" class="form-control">
                                            <option value="0" <?php echo ($eservice->display_on_home == 0) ? "selected" : ""; ?>>No</option>
                                            <option value="1" <?php echo ($eservice->display_on_home == 1) ? "selected" : ""; ?>>Yes</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <h5>Service Image</h5>
                            <div class="controls">
                                <input type="file" name="service_image" class="form-control" />
                                @if($eservice->image_url)
                                <div class="col-md-3 m-t-10" id="cat_img_container">
                                    <div class="card">
                                        <a href="{{ Storage::disk('azure')->url($eservice->image_url) }}" target="_blank">
                                            <img class="card-img-top img-responsive" src="{{ Storage::disk('azure')->url($eservice->image_url) }}" alt="{{ $eservice->image_title }}">
                                        </a>
                                        <div class="card-body">
                                            <a id="img_delete" href="{{ url('bbadmin/service/delete_image') }}" data-id="{{ $eservice->id }}" class="btn btn-danger">Remove</a>
                                        </div>
                                    </div>
                                </div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <h5>Service Banner Image</h5>
                            <div class="controls">
                                <input type="file" name="service_banner_image" class="form-control" />
                                @if($eservice->banner_image_url)
                                <div class="col-md-3 m-t-10" id="cat_img_banner_container">
                                    <div class="card">
                                        <a href="{{ Storage::disk('azure')->url($eservice->banner_image_url) }}" target="_blank">
                                            <img class="card-img-top img-responsive" src="{{ Storage::disk('azure')->url($eservice->banner_image_url) }}" alt="{{ $eservice->banner_image_title }}">
                                        </a>
                                        <div class="card-body">
                                            <a id="img_banner_delete" href="{{ url('bbadmin/service/delete_banner_image') }}" data-id="{{ $eservice->id }}" class="btn btn-danger">Remove</a>
                                        </div>
                                    </div>
                                </div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <h5>Description</h5>
                            <div class="controls">
                                <textarea name="description" id="description" class="form-control" rows="10"><?php echo $eservice->description; ?></textarea>
                            </div>
                        </div>
                        <div class="text-xs-right">
                            <button type="submit" class="btn btn-info">Submit</button>
                            <button type="reset" class="btn btn-inverse">Cancel</button>
                            <a href="{{ url('bbadmin/services') }}" class="btn btn-primary">Back</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
@endsection
@section('footer')
<script src="{{ asset('public/admin/js/service-create.js') }}"></script>
@endsection
