@extends('admin.layouts.admin')
@section('title', 'Services')
@section('content')
<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
@section('page-heading')
<h3 class="text-themecolor">Services</h3>
@endsection
@section('page-breadcrumb')
<li class="breadcrumb-item active">Services</li>
@endsection
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    @if(Session::has('flash_message'))
                    <div class="container">      
                        <div class="alert alert-success">
                            <em> {!! session('flash_message') !!}</em>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                        </div>
                    </div>
                    @endif 
                    @if(Session::has('flash_error_message'))
                    <div class="container">      
                        <div class="alert alert-danger">
                            <em> {!! session('flash_error_message') !!}</em>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                        </div>
                    </div>
                    @endif 
                    <div class="text-right">
                        <a href="{{ url('bbadmin/service/create')}}" class="btn btn-info btn-rounded">Add New Service</a>
                    </div>
                    <div class="table-responsive">
                        <table id="tblService" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Slug</th>
                                    <th>Parent</th>
                                    <th>Description</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (@$services) {
                                    foreach ($services as $parent => $service) {
                                        if (@$service) {
                                            ?>
                                            <tr>
                                                <td><a href="#"><?php echo $service['name']; ?></a></td>
                                                <td><?php echo $service['slug']; ?></td>
                                                <td>-</td>
                                                <td><?php echo $service['description']; ?></td>
                                                <td>
                                                    <div class="btn-group">
                                                        <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            Action
                                                        </button>
                                                        <div class="dropdown-menu" x-placement="bottom-start">
                                                            <a class="dropdown-item" href="<?php echo url('bbadmin/service/edit/' . $service['id']); ?>">Edit</a>
                                                            <div class="dropdown-divider"></div>
                                                            <a class="dropdown-item text-danger cat-delete" href="#" data-rel="<?php echo $service['id']; ?>" >Delete</a>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                        if (@$services[$parent]['subservice']) {
                                            foreach ($services[$parent]['subservice'] as $service) {
                                                if (@$service) {
                                                    ?>
                                                    <tr>
                                                        <td><a href="#"><?php echo $service['name']; ?></a></td>
                                                        <td><?php echo $service['slug']; ?></td>
                                                        <td><?php echo $services[$parent]['name']; ?></td>
                                                        <td><?php echo $service['description']; ?></td>
                                                        <td>
                                                            <div class="btn-group">
                                                                <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                    Action
                                                                </button>
                                                                <div class="dropdown-menu" x-placement="bottom-start">
                                                                    <a class="dropdown-item" href="<?php echo url('bbadmin/service/edit/' . $service['id']); ?>">Edit</a>
                                                                    <div class="dropdown-divider"></div>
                                                                    <a class="dropdown-item text-danger cat-delete" href="#" data-rel="<?php echo $service['id']; ?>" >Delete</a>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                }
                                            }
                                        }
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<form id="frmService" name="frmService" action="{{ url('bbadmin/service/destroy') }}" method="post">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" name="id" id="id" value="" />
</form>  
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
@endsection
@section('footer')
<script src="{{ asset('public/admin/js/service.js') }}"></script>
@endsection
