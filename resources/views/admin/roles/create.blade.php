@extends('admin.layouts.admin')

@section('title', '| Roles')

@section('page-heading')
    <h3 class="text-themecolor">Roles</h3>
@endsection

@section('page-breadcrumb')
    <li class="breadcrumb-item active">Roles</li>
@endsection

@section('content')
    <div class="container-fluid">
        <div class='col-lg-12 col-lg-offset-4'>

            <h1><i class='fa fa-key'></i> Add Role</h1>
            <hr>
            {{-- @include ('errors.list') --}}
            {!! Form::label('name', 'Name*', ['class' => 'control-label']) !!}
            {!! Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
            <p class="help-block"></p>
            @if($errors->has('name'))
                <p class="help-block">
                    {{ $errors->first('name') }}
                </p>
            @endif

            {!! Form::label('permission', 'Permissions', ['class' => 'control-label']) !!}
            {!! Form::select('permission[]', $permissions, old('permission'), ['class' => 'form-control select2', 'multiple' => 'multiple']) !!}
            <p class="help-block"></p>
            @if($errors->has('permission'))
                <p class="help-block">
                    {{ $errors->first('permission') }}
                </p>
            @endif

            {!! Form::submit('Save', ['class' => 'btn btn-danger']) !!}
            {!! Form::close() !!}
        </div>
    </div>
@endsection