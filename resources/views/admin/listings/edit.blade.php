@extends('admin.layouts.admin')
@section('title', 'Edit Listings')

@section('head')
<link href="{{ asset('public/admin/plugins/dropzone-master/dist/min/dropzone.min.css') }}" rel="stylesheet" />
<link href="{{ asset('public/admin/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.css') }}" rel="stylesheet" />
@endsection

@section('content')
<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
@section('page-heading')
<h3 class="text-themecolor">Edit Listing</h3>
@endsection
@section('page-breadcrumb')
<li class="breadcrumb-item"><a href="{{ url('bbadmin/listings') }}">Listings</a></li>
<li class="breadcrumb-item active">Edit Listing</li>
@endsection
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <form id="frmCenter" action="{{ url("bbadmin/listing/store") }}" method="post" enctype="multipart/form-data" novalidate>
                        {{ csrf_field() }}
                        <input type="hidden" name="id" id="id" value="<?php echo @$elisting->id; ?>" />
                        <h3 class="card-title">Name & Type</h3>
                        <hr />
                        <div class="form-group">
                            <h5>Name of Spa/Salon <span class="text-danger">*</span></h5>
                            <div class="controls">
                                <input type="text" id="name" name="name" class="form-control" required data-validation-required-message="This field is required" value="{{ $elisting->name }}" /> 
                            </div>
                            <div class="form-control-feedback"><small>The name is how it appear on the site.</small></div>
                        </div>
                        <div class="form-group">
                            <h5>Slug <span class="text-danger">*</span></h5>
                            <div class="controls">
                                <input type="text" id="slug" name="slug" class="form-control" required data-validation-required-message="This field is required" value="{{ $elisting->slug }}" /> </div>
                            <div class="form-control-feedback"><small>The "slug" is the URL-friendly version of the name. It is usually all lowercase and contains only letters, numbers, and hyphens.</small></div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <h5>Type of Spa</h5>
                                    <div class="controls">
                                        <select id="type_of_institution" name="type_of_institution" class="form-control select2" style="width: 100%" data-placeholder="">
                                            <option value="">Select</option>
                                            <?php
                                            if (@$types) {
                                                foreach (@$types as $type) {
                                                    ?>
                                                    <option value="<?php echo $type->id; ?>" {{ ($type->id == $elisting->type_of_institution) ? "selected" :"" }} ><?php echo $type->name; ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <h5>Banner Image</h5>
                            <div class="controls">
                                <input type="file" name="banner_image" id="banner_image" class="form-control" />
                            </div>
                            <div class="form-control-feedback"><small>Image size (750 X 500)</small></div>
                            @if($elisting->banner_image_url)
                            <div class="row" id="center_img_container">
                                <div class="col-md-3 m-t-10">
                                    <div class="card">
                                        <a href="{{ Storage::disk('azure')->url($elisting->banner_image_url) }}" target="_blank">
                                            <img class="card-img-top img-responsive" src="{{ Storage::disk('azure')->url($elisting->banner_image_url) }}" />
                                        </a>
                                        <div class="card-body">
                                            <a id="img_delete" href="{{ url('bbadmin/listing/delete_image') }}" data-id="{{ $elisting->id }}" class="btn btn-danger">Remove</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif
                        </div>
                        <h3 class="card-title">SEO</h3>
                        <hr />
                        <div class="form-group">
                            <h5>Meta Title</h5>
                            <div class="controls">
                                <input type="text" id="meta_title" name="meta_title" class="form-control" value="{{ $elisting->meta_title }}" required data-validation-required-message="This field is required" /> 
                            </div>
                        </div>
                        <div class="form-group">
                            <h5>Meta Keywords</h5>
                            <div class="controls">
                                <textarea id="meta_keywords" name="meta_keywords" class="form-control">{{ $elisting->meta_keywords }}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <h5>Meta Description</h5>
                            <div class="controls">
                                <textarea id="meta_description" name="meta_description" class="form-control">{{ $elisting->meta_description }}</textarea>
                            </div>
                        </div>
                        <h3 class="card-title">Address & Location</h3>
                        <hr />
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3">
                                    <h5>Country</h5>
                                    <div class="controls">
                                        <select id="country" name="country" class="form-control" required data-validation-required-message="This field is required" /> 
                                            <option value="">Select</option>
                                            @if($detinations)
                                            @foreach($detinations as $destination)
                                            <option value="{{ @$destination->id }}" {{ (@$destination->id == $elisting->country) ? "selected" :"" }}>{{ @$destination->name }}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <h5>State</h5>
                                    <div class="controls">
                                        <select id="state" name="state" class="form-control">
                                            <option value="{{ $elisting->state }}">{{ @$state->name }}</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <h5>City/Town</h5>
                                    <div class="controls">
                                        <select id="city" name="city" class="form-control" required data-validation-required-message="This field is required" /> 
                                            <option value="{{ $elisting->city }}">{{ @$city->name }}</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <h5>Zipcode/Pincode</h5>
                                    <div class="controls">
                                        <input type="text" id="zipcode" name="zipcode" class="form-control" value="{{ $elisting->zipcode }}" />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4">
                                    <h5>Address</h5>
                                    <div class="controls">
                                        <input type="text" id="address" name="address" class="form-control" value="{{ $elisting->address }}" />
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <h5>Area</h5>
                                    <div class="controls">
                                        <input type="text" id="area" name="area" class="form-control" value="{{ $elisting->area }}" />
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <h5>Landmark</h5>
                                    <div class="controls">
                                        <input type="text" id="landmark" name="landmark" class="form-control" value="{{ $elisting->landmark }}" />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-8">
                                    <h5>Nearest Public Transport</h5>
                                    <div class="controls">
                                        <input type="text" id="nearest_public_transport" name="nearest_public_transport" class="form-control" value="{{ $elisting->nearest_public_transport }}" />
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <h5>Latitude</h5>
                                    <div class="controls">
                                        <input type="text" id="latitude" name="latitude" class="form-control" value="{{ $elisting->latitude }}" />
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <h5>Longitude</h5>
                                    <div class="controls">
                                        <input type="text" id="longitude" name="longitude" class="form-control" value="{{ $elisting->longitude }}" />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <h5>How to get here (provided by spa)</h5>
                            <div class="controls">
                                <textarea id="how_to_get_there" name="how_to_get_there" class="textarea_editor form-control" rows="10" placeholder="Enter text ...">{{ $elisting->how_to_get_there }}</textarea>
                            </div>
                        </div>

                        <h3 class="card-title">Contact Details</h3>
                        <hr />
                        <div class="form-group">
                            <h5>Email</h5>
                            <div class="controls">
                                <input type="text" id="email" name="email" class="form-control form-control-tag" data-role="tagsinput" value="{{ $elisting->email }}" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4">
                                    <h5>Landline</h5>
                                    <div class="controls">
                                        <input type="text" id="landline" name="landline" class="form-control" value="{{ $elisting->landline }}" />
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <h5>Mobile</h5>
                                    <div class="controls">
                                        <input type="text" id="mobile" name="mobile" class="form-control" value="{{ $elisting->mobile }}" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3">
                                    <h5>Website</h5>
                                    <div class="controls">
                                        <input type="text" id="website" name="website" class="form-control" value="{{ $elisting->website }}" />
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <h5>Facebook URL</h5>
                                    <div class="controls">
                                        <input type="text" id="facebook_url" name="facebook_url" class="form-control" value="{{ $elisting->facebook_url }}" />
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <h5>Instagram URL</h5>
                                    <div class="controls">
                                        <input type="text" id="instagram_url" name="instagram_url" class="form-control" value="{{ $elisting->instagram_url }}" />
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <h5>Youtube Channel</h5>
                                    <div class="controls">
                                        <input type="text" id="youtube_channel" name="youtube_channel" class="form-control" value="{{ $elisting->youtube_channel }}" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <h3 class="card-title">About Spa/Salon</h3>
                        <hr />
                        <div class="form-group alert alert-info">
                            <div class="row">
                                <div class="col-md-12">
                                    <h5>Hours of Operation</h5>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <h5>Day</h5>
                                </div>
                                <div class="col-md-2">
                                    <h5>Opening Hour</h5>
                                </div>
                                <div class="col-md-2">
                                    <h5>Closing Hour</h5>
                                </div>
                            </div>
                            <hr />
                            <?php
                            $opening_hours = (!empty(@$elisting->opening_hours)) ? @unserialize(@$elisting->opening_hours) : array();
                            ?>
                            @php $day = array("monday","tuesday","wednesday","thursday","friday","saturday","sunday") @endphp
                            @foreach($day as $dy)
                            <?php
                            $open = "";
                            $close = "";
                            if (sizeof(@$opening_hours) > 0 && @$opening_hours) {
                                if (!empty(@$opening_hours->forDay($dy))) {
                                    $objOpen = explode("-", @$opening_hours->forDay($dy));
                                    $open = @$objOpen[0];
                                    $close = @$objOpen[1];
                                }
                            }
                            ?>
                            <div class="row">
                                <div class="col-md-3">
                                    <h5>{{ ucfirst($dy) }}</h5>
                                </div>
                                <div class="col-md-2">
                                    <div class="controls">
                                        <input type="text" id="opening_hours_opentime_{{ $dy }}" name="opening_hours[{{$dy}}][opentime]" class="form-control timepicker" value="{{ $open }}" />
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="controls">
                                        <input type="text" id="opening_hours_closetime_{{$dy}}" name="opening_hours[{{$dy}}][closetime]" class="form-control timepicker" value="{{ $close }}" />
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        <div class="form-group">
                            <h5>About Us</h5>
                            <div class="controls">
                                <textarea id="about_us" name="about_us" class="textarea_editor form-control" rows="10" placeholder="Enter text ...">{{ $elisting->about_us }}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4">
                                    <h5>Parking</h5>
                                    <div class="controls">
                                        <select id="parking" name="parking" class="form-control">
                                            <option value="">Select</option>
                                            <option value="Yes" {{ ($elisting->parking == "Yes") ? "selected" :"" }}>Yes</option>
                                            <option value="No" {{ ($elisting->parking == "No") ? "selected" :"" }}>No</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <h5>Spa Area</h5>
                                    <div class="controls">
                                        <input type="text" id="spa_area" name="spa_area" class="form-control" value="{{ $elisting->spa_area }}" />
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <h5>Gender</h5>
                                    <div class="controls">
                                        <select id="gender" name="gender" class="form-control" style="width: 100%" data-placeholder="">
                                            <option value="">Select</option>
                                            <option value="Male" {{ ($elisting->gender == "Male") ? "selected" :"" }}>Male</option>
                                            <option value="Female" {{ ($elisting->gender == "Female") ? "selected" :"" }}>Female</option>
                                            <option value="Unisex" {{ ($elisting->gender == "Unisex") ? "selected" :"" }}>Unisex</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <h5>Image gallery</h5>
                            <div class="controls">
                                <div id="image_gallery" class="dropzone">
                                    <div class="dz-message text-center">
                                        Upload Images (Click or Drag file here)
                                    </div>
                                    <input name="image_galleries" type="file" multiple style="display:none;" />
                                </div>
                                <input type="hidden" id="image_gallery_ids" name="image_gallery_ids" value="" />
                                <input type="hidden" id="dropzoneurl" value="{{ url("bbadmin/listing/upload_gallery_image") }}" />
                                <div class="row">
                                    @if(@$imagegalleries)
                                    @foreach(@$imagegalleries as $gallery)
                                    <div class="col-md-3 m-t-10" id="img-{{ $gallery->id }}">
                                        <div class="card">
                                            <a href="{{ Storage::disk('azure')->url($gallery->image_url) }}" target="_blank">
                                                <img class="card-img-top img-responsive" src="{{ Storage::disk('azure')->url($gallery->image_url) }}" alt="{{ $gallery->image_title }}">
                                            </a>
                                            <div class="card-body">
                                                <a id="gallery_img_delete" href="{{ url('bbadmin/listing/delete_gallery_image') }}" data-id="{{ $gallery->id }}" class="btn btn-danger">Remove</a>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <h5>Video</h5>
                            <div class="controls">
                                <?php $videos = (!empty($elisting->video)) ? explode("||", $elisting->video) : array(); ?>
                                <ol>
                                    <li>
                                        <input type="text" id="video[]" name="video[]" class="form-control" value="{{ @$videos[0] }}" />
                                    </li>
                                    <li>
                                        <input type="text" id="video[]" name="video[]" class="form-control" value="{{ @$videos[1] }}" />
                                    </li>
                                    <li>
                                        <input type="text" id="video[]" name="video[]" class="form-control" value="{{ @$videos[2] }}" />
                                    </li>
                                    <li>
                                        <input type="text" id="video[]" name="video[]" class="form-control" value="{{ @$videos[3] }}" />
                                    </li>
                                </ol>
                            </div>
                        </div>

                        <div class="form-group">
                            <h5>Awards</h5>
                            <div class="controls">
                                <?php $eawards = (!empty($elisting->awards)) ? explode("||", $elisting->awards) : array(); ?>
                                <select id="awards[]" name="awards[]" class="form-control select2-multiple form-control-select2" multiple="multiple" data-placeholder="">
                                    <option value="">Select</option>
                                    @if($awards)
                                    @foreach($awards as $award)
                                    <option value="{{ $award->id }}" {{ in_array($award->id, $eawards) ? "selected" : "" }}>{{ $award->name }}</option>
                                    @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>

                        <h3 class="card-title">Offered Service</h3>
                        <hr />
                        @if(@$services)
                        @foreach(@$services as $service)
                        <div class="form-group">
                            <h5>{{ $service['name'] }}</h5>
                            <div class="controls">
                                <select id="service[]" name="service[]" class="form-control select2-multiple form-control-select2" multiple="multiple" data-placeholder="">
                                    <option value="">Select</option>
                                    @if(isset($service['subservice']))
                                    @foreach($service['subservice'] as $subid=>$subservice)
                                    <option value="{{ @$subid }}" <?php echo (in_array(@$subid, @$listing_services)) ? "selected" : ""; ?>>{{ $subservice }}</option>
                                    @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        @endforeach
                        @endif
                        <input type="hidden" id="hdn_lst_ser_id" name="hdn_lst_ser_id" value="<?php echo implode("||", @$listing_services); ?>" />
                        <h3 class="card-title">Accommodation (If any)</h3>
                        <hr />
                        <div class="form-group">
                            <h5>Accommodation Overview</h5>
                            <div class="controls">
                                <textarea id="accommodation" name="accommodation" class="textarea_editor form-control" rows="10" placeholder="Enter text ...">{{ $elisting->accommodation }}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <h5>Accommodation Banner Image</h5>
                            <div class="controls">
                                <input type="file" name="accommodation_banner_image" id="accommodation_banner_image" class="form-control" value="{{ $elisting->accommodation_banner_image }}" />
                                @if(@$elisting->accommodation_banner_image_url)
                                <div class="row" id="exp_acm_img_container">
                                    <div class="col-md-3 m-t-10">
                                        <div class="card">
                                            <a href="{{ Storage::disk('azure')->url(@$elisting->accommodation_banner_image_url) }}" target="_blank">
                                                <img class="card-img-top img-responsive" src="{{ Storage::disk('azure')->url(@$elisting->accommodation_banner_image_url) }}" />
                                            </a>
                                            <div class="card-body">
                                                <a id="img_acm_delete" href="{{ url('bbadmin/listing/delete_accomodation_image') }}" data-id="{{ @$elisting->id }}" class="btn btn-danger">Remove</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endif
                            </div>
                            <div class="form-control-feedback"><small>Image size (750 X 500)</small></div>
                        </div>
                        <div class="form-group">
                            <h5>Accommodation Image Gallery</h5>
                            <div class="controls">
                                <div id="acm_image_gallery" class="dropzone">
                                    <div class="dz-message text-center">
                                        Upload Images (Click or Drag file here)
                                    </div>
                                    <input name="accommodation_image_gallery" type="file" multiple style="display:none;" />
                                </div>
                                <input type="hidden" id="accommodation_image_gallery_ids" name="accommodation_image_gallery_ids" value="" />
                                <input type="hidden" id="acmdropzoneurl" value="{{ url("bbadmin/listing/upload_gallery_image") }}" />
                                <div class="row">
                                    @if(@$accomodationgalleries)
                                    @foreach(@$accomodationgalleries as $gallery)
                                    <div class="col-md-3 m-t-10" id="img-acm-{{ $gallery->id }}">
                                        <div class="card">
                                            <a href="{{ Storage::disk('azure')->url($gallery->image_url) }}" target="_blank">
                                                <img class="card-img-top img-responsive" src="{{ Storage::disk('azure')->url($gallery->image_url) }}" alt="{{ $gallery->image_title }}">
                                            </a>
                                            <div class="card-body">
                                                <a id="acm_gallery_img_delete" href="{{ url('bbadmin/listing/delete_accomodation_gallery_image') }}" data-id="{{ $gallery->id }}" class="btn btn-danger acm_gallery_img_delete">Remove</a>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>

                        <h3 class="card-title">Food (If any)</h3>
                        <hr />
                        <div class="form-group">
                            <h5>Food</h5>
                            <div class="controls">
                                <textarea id="food" name="food" class="textarea_editor form-control" rows="10" placeholder="Enter text ...">{{ $elisting->food }}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <h5>Food Banner Image</h5>
                            <div class="controls">
                                <input type="file" name="food_banner_image" id="food_banner_image" class="form-control" />
                                @if(@$elisting->food_banner_image_url)
                                <div class="row" id="exp_food_img_container">
                                    <div class="col-md-3 m-t-10">
                                        <div class="card">
                                            <a href="{{ Storage::disk('azure')->url(@$elisting->food_banner_image_url) }}" target="_blank">
                                                <img class="card-img-top img-responsive" src="{{ Storage::disk('azure')->url(@$elisting->food_banner_image_url) }}" />
                                            </a>
                                            <div class="card-body">
                                                <a id="img_food_delete" href="{{ url('bbadmin/listing/delete_food_image') }}" data-id="{{ @$elisting->id }}" class="btn btn-danger">Remove</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endif
                            </div>
                            <div class="form-control-feedback"><small>Image size (750 X 500)</small></div>
                        </div>
                        <div class="form-group">
                            <h5>Food Image Gallery</h5>
                            <div class="controls">
                                <div id="food_image_gallery" class="dropzone">
                                    <div class="dz-message text-center">
                                        Upload Images (Click or Drag file here)
                                    </div>
                                    <input name="food_image_galleries" type="file" multiple style="display:none;" />
                                </div>
                                <input type="hidden" id="food_image_gallery_ids" name="food_image_gallery_ids" value="" />
                                <input type="hidden" id="fooddropzoneurl" value="{{ url("bbadmin/listing/upload_gallery_image") }}" />
                                <div class="row">
                                    @if(@$foodimagegalleries)
                                    @foreach(@$foodimagegalleries as $gallery)
                                    <div class="col-md-3 m-t-10" id="img-food-{{ $gallery->id }}">
                                        <div class="card">
                                            <a href="{{ Storage::disk('azure')->url($gallery->image_url) }}" target="_blank">
                                                <img class="card-img-top img-responsive" src="{{ Storage::disk('azure')->url($gallery->image_url) }}" alt="{{ $gallery->image_title }}">
                                            </a>
                                            <div class="card-body">
                                                <a id="food_gallery_img_delete" href="{{ url('bbadmin/listing/delete_food_gallery_image') }}" data-id="{{ $gallery->id }}" class="btn btn-danger food_gallery_img_delete">Remove</a>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <h5>Currency</h5>
                                    <div class="controls">
                                        <select id="currency" name="currency" class="form-control select2" style="width: 100%" data-placeholder="Select">
                                            @foreach(\App\Http\Helpers\CommonHelper::get_currency() as $currency)
                                            <option value="{{ $currency }}" {{ @$elisting->currency == $currency ? "selected" :"" }}>{{ $currency }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <h3 class="card-title">Menu</h3>
                        <div class="form-group">
                            <h5>Menu Image Gallery</h5>
                            <div class="controls">
                                <div id="menu_image_gallery" class="dropzone">
                                    <div class="dz-message text-center">
                                        Upload Images (Click or Drag file here)
                                    </div>
                                    <input name="menu_image_galleries" type="file" multiple style="display:none;" />
                                </div>
                                <input type="hidden" id="menu_image_gallery_ids" name="menu_image_gallery_ids" value="" />
                                <input type="hidden" id="menudropzoneurl" value="{{ url("bbadmin/listing/upload_gallery_image") }}" />
                                <div class="row">
                                    @if(@$menuimagegalleries)
                                    @foreach(@$menuimagegalleries as $gallery)
                                    <div class="col-md-3 m-t-10" id="img-menu-{{ $gallery->id }}">
                                        <div class="card">
                                            <a href="{{ Storage::disk('azure')->url($gallery->image_url) }}" target="_blank">
                                                <img class="card-img-top img-responsive" src="{{ Storage::disk('azure')->url($gallery->image_url) }}" alt="{{ $gallery->image_title }}">
                                            </a>
                                            <div class="card-body">
                                                <a id="menu_gallery_img_delete" href="{{ url('bbadmin/listing/delete_menu_gallery_image') }}" data-id="{{ $gallery->id }}" class="btn btn-danger menu_gallery_img_delete">Remove</a>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <?php $m = 1; ?>
                            @foreach($listing_menu as $objmenu)
                            <li class="nav-item"> <a class="nav-link {{ $m == 1 ? "active" : "" }} show" data-toggle="tab" href="#menu_{{ $m }}" role="tab" aria-selected="true"><span class="hidden-sm-up"><i class="ti-home"></i></span> <span class="hidden-xs-down">Menu {{ $m }}</span></a> </li>
                            <?php $m++; ?>
                            @endforeach
                            @for($i=$m;$i<=3;$i++)
                            <li class="nav-item"> <a class="nav-link {{ $i == 1 ? "active" : "" }} show" data-toggle="tab" href="#menu_{{ $i }}" role="tab" aria-selected="true"><span class="hidden-sm-up"><i class="ti-home"></i></span> <span class="hidden-xs-down">Menu {{ $i }}</span></a> </li>
                            @endfor
                        </ul>
                        <!-- Tab panes -->

                        <div class="tab-content tabcontent-border form-group">
                            <?php $m = 1; ?>
                            @foreach($listing_menu as $objmenu)
                            <div class="tab-pane p-20 {{ $m == 1 ? "active" : "" }} show" id="menu_{{ $m }}" role="tabpanel">
                                <div class="row">
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <h5>Title</h5>
                                            <div class="controls">
                                                <input type="text" id="menu_title[]" name="menu_title[]" class="form-control" value="{{ $objmenu->menu_title }}" />
                                                <input type="hidden" id="menu_id[]" name="menu_id[]" class="form-control" value="{{ $objmenu->id }}" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <h5>Classification</h5>
                                            <div class="controls">
                                                <select id="classification[]" name="classification[]" class="form-control">
                                                    <option value="Packages" {{ ($objmenu->classification == "Packages") ? "selected='selected'" : "" }}>Packages</option>
                                                    <option value="Treatments" {{ ($objmenu->classification == "Treatments") ? "selected='selected'" : "" }}>Treatments</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <h5>Price</h5>
                                            <div class="controls">
                                                <input type="text" id="menu_price[]" name="menu_price[]" class="form-control" value="{{ $objmenu->menu_price }}" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <h5>Duration</h5>
                                            <div class="controls">
                                                <div class="input-group">
                                                    <input type="text" id="duration[]" name="duration[]" class="form-control" value="{{ $objmenu->duration }}" />
                                                    <span class="input-group-addon">mins</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <h5>Description</h5>
                                    <div class="controls">
                                        <textarea id="menu_description[]" name="menu_description[]" class="textarea_editor form-control" placeholder="Enter text ...">{{ $objmenu->menu_description }}</textarea>
                                    </div>
                                </div>
                            </div>
                            <?php $m++; ?>
                            @endforeach
                            @for($i=$m;$i<=3;$i++)
                            <div class="tab-pane p-20 {{ $i == 1 ? "active" : "" }} show" id="menu_{{ $i }}" role="tabpanel">
                                <div class="row">
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <h5>Title</h5>
                                            <div class="controls">
                                                <input type="text" id="menu_title[]" name="menu_title[]" class="form-control" />
                                                <input type="hidden" id="menu_id[]" name="menu_id[]" class="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <h5>Classification</h5>
                                            <div class="controls">
                                                <select id="classification[]" name="classification[]" class="form-control">
                                                    <option value="Packages">Packages</option>
                                                    <option value="Treatments">Treatments</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <h5>Price</h5>
                                            <div class="controls">
                                                <input type="text" id="menu_price[]" name="menu_price[]" class="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <h5>Duration</h5>
                                            <div class="controls">
                                                <div class="input-group">
                                                    <input type="text" id="duration[]" name="duration[]" class="form-control" />
                                                    <span class="input-group-addon">mins</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <h5>Description</h5>
                                    <div class="controls">
                                        <textarea id="menu_description[]" name="menu_description[]" class="textarea_editor form-control" placeholder="Enter text ..."></textarea>
                                    </div>
                                </div>
                            </div>
                            @endfor
                        </div>                        
                        <input type="hidden" id="hdn_menu_id" name="hdn_menu_id" value="<?php //echo implode("||", @$listing_menu);    ?>" />
                        <h3 class="card-title">Payment Options</h3>
                        <hr />
                        <div class="form-group">
                            <h5>Payment Options</h5>
                            <div class="controls">
                                <select id="payment_options[]" name="payment_options[]" class="form-control select2-multiple form-control-select2" multiple="multiple" data-placeholder="">
                                    <option value="">Select</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3">
                                    <h5>Status</h5>
                                    <div class="controls">
                                        <select id="is_draft" name="is_draft" class="form-control select2 form-control-select2">
                                            <option value="0" {{ ($elisting->is_draft == 0) ? "selected":"" }}>Publish</option>
                                            <option value="1" {{ ($elisting->is_draft == 1) ? "selected":"" }}>Draft</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <h5>Display on Homepage</h5>
                                    <div class="controls">
                                        <select name="display_on_home" id="display_on_home" class="form-control select2 form-control-select2">
                                            <option value="0" {{ ($elisting->display_on_home == 0) ? "selected":"" }}>No</option>
                                            <option value="1" {{ ($elisting->display_on_home == 1) ? "selected":"" }}>Yes</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <h5>Featured Listing</h5>
                                    <div class="controls">
                                        <select name="is_featured" id="is_featured" class="form-control select2 form-control-select2">
                                            <option value="0" {{ ($elisting->is_featured == 0) ? "selected":"" }}>No</option>
                                            <option value="1"{{ ($elisting->is_featured == 1) ? "selected":"" }}>Yes</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="text-xs-right">
                            <button type="submit" class="btn btn-info">Submit</button>
                            <button type="reset" class="btn btn-inverse">Cancel</button>
                            <a href="{{ url('bbadmin/listings') }}" class="btn btn-primary">Back</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Page Content -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
@endsection
@section('footer')
<script src="{{ asset('public/admin/plugins/dropzone-master/dist/min/dropzone.min.js') }}"></script>
<script src="{{ asset('public/admin/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js') }}"></script>
<script src="{{ asset('public/admin/js/listing-create.js') }}"></script>
@endsection
