@extends('admin.layouts.admin')

@section('title', '| Create Permission')

@section('page-heading')
    <h3 class="text-themecolor">Permissions Edit</h3>
@endsection

@section('page-breadcrumb')
    <li class="breadcrumb-item active">Permissions</li>
@endsection


@section('content')
    <div class="container-fluid">
        <div class='col-lg-4 col-lg-offset-4'>
            {{-- @include ('errors.list') --}}
            <h1><i class='fa fa-key'></i> Add Permission</h1><br>
            {{ Form::open(array('route' => 'permissions.store')) }}
            <div class="form-group">
                {{ Form::label('name', 'Name') }}
                {{ Form::text('name', '', array('class' => 'form-control')) }}
            </div>
            <br>
            {{ Form::submit('Add', array('class' => 'btn btn-primary')) }}
            {{ Form::close() }}
        </div>
    </div>
@endsection