@extends('admin.layouts.admin')

@section('title', '| Permissions')

@section('page-heading')
    <h3 class="text-themecolor">Permissions</h3>
@endsection

@section('page-breadcrumb')
    <li class="breadcrumb-item active">Permissions</li>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="col-lg-12 col-lg-offset-1">
            <h1><i class="fa fa-key"></i>Available Permissions
                <a href="{{ route('users.index') }}" class="btn btn-default pull-right">Users</a>
                <a href="{{ route('roles.index') }}" class="btn btn-default pull-right">Roles</a></h1>
            <hr>
            <div class="table-responsive">
                <table class="table table-bordered table-striped">

                    <thead>
                    <tr>
                        <th>Permissions</th>
                        <th>Operation</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($permissions as $permission)
                        <tr>
                            <td>{{ $permission->name }}</td>
                            <td>
                                <a href="{{ route('permissions.edit',[$permission->id]) }}"
                                   class="btn btn-info pull-left" style="margin-right: 3px;">Edit</a>

                                {!! Form::open(['method' => 'DELETE', 'route' => ['permissions.destroy', $permission->id] ]) !!}
                                {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                                {!! Form::close() !!}

                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

            <a href="{{ route('permissions.create') }}" class="btn btn-success">Add Permission</a>

        </div>
    </div>

@endsection