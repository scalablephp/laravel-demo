@extends('admin.layouts.admin')
@section('title', 'Edit Type')
@section('content')
<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
@section('page-heading')
<h3 class="text-themecolor">Edit Type</h3>
@endsection
@section('page-breadcrumb')
<li class="breadcrumb-item"><a href="{{ url('bbadmin/type') }}">Categories</a></li>
<li class="breadcrumb-item active">Edit Type</li>
@endsection
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <form id="frmType" action="{{ url("bbadmin/type/store") }}" method="post" enctype="multipart/form-data" novalidate>
                        {{ csrf_field() }}
                        <input type="hidden" name="id" id="id" value="<?php echo $etype->id; ?>" />
                        <div class="form-group">
                            <h5>Name <span class="text-danger">*</span></h5>
                            <div class="controls">
                                <input type="text" id="name" name="name" class="form-control" value="<?php echo $etype->name; ?>" required data-validation-required-message="This field is required" /> </div>
                            <div class="form-control-feedback"><small>The name is how it appear on the site.</small></div>
                        </div>
                        <div class="form-group">
                            <h5>Slug <span class="text-danger">*</span></h5>
                            <div class="controls">
                                <input type="text" id="slug" name="slug" class="form-control" value="<?php echo $etype->slug; ?>" required data-validation-required-message="This field is required" /> </div>
                            <div class="form-control-feedback"><small>The "slug" is the URL-friendly version of the name. It is usually all lowercase and contains only letters, numbers, and hyphens.</small></div>
                        </div>
                        <div class="form-group">
                            <h5>Keywords</h5>
                            <div class="controls">
                                <textarea id="keywords" name="keywords" class="form-control"><?php echo $etype->keywords; ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4">
                                    <h5>Parent Type</h5>
                                    <div class="controls">
                                        <select name="parent" id="parent" class="form-control">
                                            <option value="0" <?php echo ($etype->parent == 0) ? "selected" : ""; ?>>None</option>
                                            <?php
                                            if (@$types) {
                                                foreach ($types as $type) {
                                                    ?>
                                                    <option class="<?php echo ($type->type == 1) ? "destination-type" : "type-type"; ?>" value="<?php echo $type->id; ?>" <?php echo ($etype->parent == $type->id) ? "selected" : ""; ?>><?php echo $type->name; ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <h5>Display on Homepage</h5>
                                    <div class="controls">
                                        <select name="display_on_home" id="display_on_home" class="form-control">
                                            <option value="0" <?php echo ($etype->display_on_home == 0) ? "selected" : ""; ?>>No</option>
                                            <option value="1" <?php echo ($etype->display_on_home == 1) ? "selected" : ""; ?>>Yes</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <h5>Type Image</h5>
                            <div class="controls">
                                <input type="file" name="type_image" class="form-control" />
                                @if($etype->image_url)
                                <div class="col-md-3 m-t-10" id="cat_img_container">
                                    <div class="card">
                                        <a href="{{ Storage::disk('azure')->url($etype->image_url) }}" target="_blank">
                                            <img class="card-img-top img-responsive" src="{{ Storage::disk('azure')->url($etype->image_url) }}" alt="{{ $etype->image_title }}">
                                        </a>
                                        <div class="card-body">
                                            <a id="img_delete" href="{{ url('bbadmin/type/delete_image') }}" data-id="{{ $etype->id }}" class="btn btn-danger">Remove</a>
                                        </div>
                                    </div>
                                </div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <h5>Type Banner Image</h5>
                            <div class="controls">
                                <input type="file" name="type_banner_image" class="form-control" />
                                @if($etype->banner_image_url)
                                <div class="col-md-3 m-t-10" id="cat_img_banner_container">
                                    <div class="card">
                                        <a href="{{ Storage::disk('azure')->url($etype->banner_image_url) }}" target="_blank">
                                            <img class="card-img-top img-responsive" src="{{ Storage::disk('azure')->url($etype->banner_image_url) }}" alt="{{ $etype->banner_image_title }}">
                                        </a>
                                        <div class="card-body">
                                            <a id="img_banner_delete" href="{{ url('bbadmin/type/delete_banner_image') }}" data-id="{{ $etype->id }}" class="btn btn-danger">Remove</a>
                                        </div>
                                    </div>
                                </div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <h5>Description</h5>
                            <div class="controls">
                                <textarea name="description" id="description" class="form-control" rows="10"><?php echo $etype->description; ?></textarea>
                            </div>
                        </div>
                        <div class="text-xs-right">
                            <button type="submit" class="btn btn-info">Submit</button>
                            <button type="reset" class="btn btn-inverse">Cancel</button>
                            <a href="{{ url('bbadmin/type') }}" class="btn btn-primary">Back</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
@endsection
@section('footer')
<script src="{{ asset('public/admin/js/type-create.js') }}"></script>
@endsection
