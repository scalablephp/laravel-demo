@extends('admin.layouts.admin')
@section('title', 'Edit Review')
@section('content')
<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
@section('page-heading')
<h3 class="text-themecolor">Edit Review</h3>
@endsection
@section('page-breadcrumb')
<li class="breadcrumb-item"><a href="{{ url('bbadmin/reviews') }}">Reviews</a></li>
<li class="breadcrumb-item active">Edit Review</li>
@endsection
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <form id="frmCategory" action="{{ url("bbadmin/review/store") }}" method="post" enctype="multipart/form-data" novalidate>
                        {{ csrf_field() }}
                        <input type="hidden" name="id" id="id" value="<?php echo $ecomment->id; ?>" />
                        <div class="form-group">
                            <h5>Listing</h5>
                            <div class="controls">
                                <a href='{{ url("/spa/".$ecomment->slug) }}' target="_blank">{{ $ecomment->name }}</a>
                            </div>
                        </div>
                        <div class="form-group">
                            <h5>Author</h5>
                            <div class="controls">
                                {{ $ecomment->comment_author }}
                            </div>
                        </div>
                        <div class="form-group">
                            <h5>Email</h5>
                            <div class="controls">
                                {{ $ecomment->comment_author_email }}
                            </div>
                        </div>
                        <div class="form-group">
                            <h5>Review</h5>
                            <div class="controls">
                                {{ $ecomment->comment_content }}
                            </div>
                        </div>
                        <div class="form-group">
                            <h5>Review Approved</h5>
                            <div class="controls">
                                <div class="radio radio-primary">
                                    <input type="radio" id="comment_approved_yes" name="comment_approved" value="1" {{ ($ecomment->comment_approved == 1) ? 'checked' : "" }} />
                                    <label for="comment_approved_yes">Yes</label>
                                    <input type="radio" id="comment_approved_no" name="comment_approved" value="0" {{ ($ecomment->comment_approved == 0) ? 'checked' : "" }} />
                                    <label for="comment_approved_no">No</label>
                                </div>
                            </div>
                        </div>
                        <div class="text-xs-right">
                            <button type="submit" class="btn btn-info">Submit</button>
                            <button type="reset" class="btn btn-inverse">Cancel</button>
                            <a href="{{ url('bbadmin/reviews') }}" class="btn btn-primary">Back</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
@endsection
@section('footer')
<script src="{{ asset('public/admin/js/category-create.js') }}"></script>
@endsection
