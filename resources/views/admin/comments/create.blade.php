@extends('admin.layouts.admin')
@section('title', 'Create Review')
@section('content')
<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
@section('page-heading')
<h3 class="text-themecolor">Create Review</h3>
@endsection
@section('page-breadcrumb')
<li class="breadcrumb-item"><a href="{{ url('bbadmin/reviews') }}">Reviews</a></li>
<li class="breadcrumb-item active">Create Review</li>
@endsection
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <form id="frmComment" action="{{ url("bbadmin/review/store") }}" method="post" enctype="multipart/form-data" novalidate>
                        {{ csrf_field() }}
                        <input type="hidden" name="comment_author_email" id="comment_author_email" value="{{ Auth::user()->email }}" />
                        <div class="form-group">
                            <h5>Listing <span class="text-danger">*</span></h5>
                            <div class="controls">
                                <select name="listing_id" id="listing_id" class="form-control" required data-validation-required-message="This field is required">
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <h5>Review</h5>
                            <div class="controls">
                                <textarea name="comment_content" id="comment_content" class="form-control required" required data-validation-required-message="This field is required" rows="10"></textarea>
                            </div>
                        </div>
                        <div class="text-xs-right">
                            <button type="submit" class="btn btn-info">Submit</button>
                            <button type="reset" class="btn btn-inverse">Cancel</button>
                            <a href="{{ url('bbadmin/reviews') }}" class="btn btn-primary">Back</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
@endsection
@section('footer')
<script src="{{ asset('public/admin/js/comment-create.js') }}"></script>
@endsection
