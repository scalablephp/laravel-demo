 <link href="{{ asset('public/spa-owner-dashboard/vendors/switchery/dist/switchery.min.css') }}" rel="stylesheet" />
<div class="listing-detail__reviews element-waypoint" id="tips-reviews-box">
                            <h2 class="listing-detail__content-title">
                                Quick Search
                                
                            </h2>
                            <div class="listing-detail__reviews-box">
                                
                            <!-- Contact form module -->
                    <form class="add-comment custom-form contact-form" id="frmsearch" action="/spa/filter" method="get" novalidate="novalidate" name="frmsearch">
                                   
                    <div>
                        <label class="label" for="city">Near Me</label>
                        
                          <input type="checkbox" name="nearme" class="js-switch">
                       
                          <input type="hidden" name="distance" id="nearmeval" value="100" disabled="disabled" />
                       
                    </div>
                    
                                   
                       @if($destinations)
                    <div style="display:{{ (!empty(@$srchcountry)) ? 'none' : '' }}">
                        <label class="label" for="country">Country</label>
                        <select id="country" name="country" class="explore__form-input select2-multiple">
                            <option value="0">Select Country</option>
                            @foreach($destinations as $country=>$destination)
                            <option value="{{ $country }}" {{ @$srchdestination->id ==  $country ? 'selected=selected' : '' }} >{{ $destination[0]->countryname."(".$destination['total_listings'].")" }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div>
                        <label class="label" for="city">City</label>
                        <select id="city" name="city" class="explore__form-input select2-multiple">
                            <option value="0">Select City</option>
                            @foreach($destinations as $country=>$destination)
                            @foreach(@$destination as $key=>$objCity)
                            @if(is_int($key))
                            <option value="{{ $objCity->id }}"
                                    {{ @$srchdestination->id ==  $objCity->id ? 'selected=selected' : '' }} data-rel="{{ $objCity->country }}">{{ $objCity->name }}
                                    ({{ @$objCity->total_listings }})
                        </option>
                        @endif
                        @endforeach
                        @endforeach
                    </select>
                </div>
                @endif
                      
                        <div>
                            <label class="label" for="type_of_spa">Type of Spa</label>
                                <select class="explore__form-input select2-multiple" name="type_of_spa"
                                        id="type_of_spa">
                                    <option value="0">Select Type of Spa</option>
                                    @if($types)
                                    @foreach ($types as $spaType)
                                    <option value="{{ $spaType['id'] }}" {{ old('type_of_spa') == $spaType['id'] ? 'selected=selected' : '' }} >{{ $spaType['name']."(".$spaType['listings_count'].")" }}</option>
                                    @endforeach
                                    @endif
                                </select>
                        </div>
                        <div>
                            <label class="label" for="distance">Within Distance</label>
                                <select id="distance" name="distance" class="explore__form-input select2-multiple">
                                <option value="" {{ old('distance') ==  '' ? 'selected=selected' : '' }}>
                                Any distance
                                </option>
                                <option value="50" {{ old('distance') ==  '50' ? 'selected=selected' : '' }}>
                                50 miles radius
                                </option>
                                <option value="30" {{ old('distance') ==  '30' ? 'selected=selected' : '' }}>
                                30 miles radius
                                </option>
                                <option value="15" {{ old('distance') ==  '15' ? 'selected=selected' : '' }}>
                                15 miles radius
                                </option>
                                <option value="10" {{ old('distance') ==  '10' ? 'selected=selected' : '' }}>
                                10 miles radius
                                </option>
                                </select>
                        </div>
                        <div>
                            <a href="#" class="advanced-search-toggle">
                            Show Advance Search Options
                            <i class="fa fa-angle-down" aria-hidden="true"></i>
                            </a>
                        </div>
                        <div class="explore__search-form-advanced" style="display:none;">
                        @if(@$services)
                        <label class="label">Services Offered</label>
                        <div class="panel-group" id="accordion2" role="tablist" aria-multiselectable="true">
                            @foreach($services as $service)
                            <div class="panel">
                                <div class="panel-heading" role="tab" id="headingSER{{ $service->id }}">
                                    <h4 class="panel-title">
                                        <a role="button" data-toggle="collapse"
                                           data-parent="#accordion2"
                                           href="#collapseSER{{ $service->id }}" aria-expanded="true"
                                           aria-controls="collapseSER{{ $service->id }}">
                                            <i class="more-less fa fa-plus"></i>
                                            {{ $service->name }}
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseSER{{ $service->id }}" class="panel-collapse collapse"
                                     role="tabpanel" aria-labelledby="headingSER{{ $service->id }}">
                                    <div class="panel-body">
                                        <ul class="explore__form-checkbox-list fullwidth explore__form-checkbox-list-side">
                                            @foreach($service->subServices()->get() as $subService)
                                            <li>
                                                <input class="explore__input-checkbox"
                                                       type="checkbox" name="services[]"
                                                       value="{{ $subService->id }}"
                                                       id="services{{$subService->id}}" {{  (collect(old('services'))->contains($subService->id))  ? 'checked=checked':'' }} >
                                                <span class="explore__checkbox-style"></span>
                                                <label class="explore__checkbox-text"
                                                       for="services{{$subService->id}}">{{$subService->name." (".@$subService->listings()->count().")" }}</label>
                                            </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        @endif
        </div>            
                                   
                                 
                                    <input class="contact-form__submit" type="submit" name="submit" id="submit_enquiry" value="Search">
                                </form>
   
                                <!-- End Contact form module -->

                            </div>

                        </div>

<script type="text/javascript" src="{{ asset('public/front/js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('public/spa-owner-dashboard/vendors/switchery/dist/switchery.min.js') }}"></script>
<script src="{{ asset('public/spa-owner-dashboard/build/js/custom.js') }}"></script>
<script src="{{ asset('public/front/js/location.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
           var openAdvancedBtn = $('a.advanced-search-toggle'),
        advancedBox = $('.explore__search-form-advanced');

        openAdvancedBtn.on('click', function(event) {
        event.preventDefault();

        advancedBox.slideToggle();

    });
       
        $('#frmsearch input').on('change', function() {
           //alert($('input[name=nearme]:checked', '#frmsearch').val());
            if($('input[name=nearme]:checked', '#frmsearch').val() == 'on') {
                $("#country").prop('disabled', true);
                $("#city").prop('disabled', true);
                $("#type_of_spa").prop('disabled', true);
                $("#distance").prop('disabled', true);
                $("#nearmeval").prop('disabled', false);
                getLocation();
            } else {
                $("#country").prop('disabled', false);
                $("#city").prop('disabled', false);
                $("#type_of_spa").prop('disabled', false);
                $("#distance").prop('disabled', false);
                $("#nearmeval").prop('disabled', true);

            }
      });

            
        });

    var options = {
    enableHighAccuracy: true,
    timeout: 5000,
    maximumAge: 0
};

function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition, error, options);
    } else {
        console.log("Geolocation is not supported by this browser.");
    }
}

function showPosition(position) {
    console.log("Latitude: " + position.coords.latitude + "<br>Longitude: " + position.coords.longitude);
    var container = document.getElementById('frmsearch');

    var input2 = document.createElement("input");
    input2.type = "hidden";
    input2.name = "longitude";
    input2.value = position.coords.longitude;
    input2.className = "longitude"; // set the CSS class
    container.insertBefore(input2, container.firstChild); // put it into the DOM

    var input1 = document.createElement("input");
    input1.type = "hidden";
    input1.name = "latitude";
    input1.value = position.coords.latitude;
    input1.className = "latitude";
    container.insertBefore(input1, container.firstChild); // put it into the DOM


}

function error(err) {
    console.warn(err.message);
}

</script>