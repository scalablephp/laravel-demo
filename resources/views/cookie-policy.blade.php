@extends('layouts.front')
@section('title', 'Cookie Policy')
<!-- Meta Info Start-->
@section('meta_title', "Cookie Policy")
@section('description', "Cookie Policy page for BalanceBoat, a booking platform for retreats and professional courses in Yoga and Ayurveda")
@section('keywords', "BalanceBoat, Booking platform, Yoga teacher Training booking website, Ayurveda packages booking, BalanceBoat Cookie Policy page, Cookie Policy page")
<!-- Meta Info End -->
@section('banner')
<section id="hero" class="subtitle">
    <div class="intro_title">
        <div class="container">
            <div class="row  text-left">
                <div class="col-md-7 sp_txt" style="verticle-align:bottom; height:60vh;max-height:400px">

                </div>
            </div>
        </div>

    </div>
</section>
<!-- End hero -->
@endsection
@section('content')
<main>
    <div id="position">
        <div class="container">
            <ul>
                <li><a href="{{ url("/") }}">Home</a></li>
                <li>Cookie Policy</li>
            </ul>
        </div>
    </div>
    <!-- End Position -->

    <div class="collapse" id="collapseMap">
        <div id="map" class="map">test</div>
    </div>
    <!-- End Map -->

    <div class="container margin_60">
        <div class="row">
            <div class="col-md-12">
                <h2 class="text-center">Cookie Policy</h2>
                <div class="row">
                    <div class="col-md-12">
                        <p>Last updated: 31st Jan 2018</p>
                        <p>Thank you for using BalanceBoat!</p>
                        <p>BalanceBoat uses cookies and similar technologies to help provide, protect, and improve the BalanceBoat Platform. This policy explains how and why we use these technologies and the choices you have.</p>
                        <p>
                            A cookie is a small data file that is transferred to your device (e.g. your phone or your computer). For example, a cookie could allow us to recognize your browser, while another could store your preferences. There are two types of cookies used on the BalanceBoat Platform: (1) “session cookies” and (2) “persistent cookies.” Session cookies normally expire when you close your browser, while persistent cookies remain on your device after you close your browser, and can be used again the next time you access the BalanceBoat Platform. 
                        </p>
                        <h3>Why do we use these technologies</h3>
                        <p>BalanceBoat uses these technologies for the following reasons</p>
                        <ul>
                            <li><p>To enable the use and access the BalanceBoat Platform and the Payment Services.</p></li>
                            <li><p>To better understand how visitors navigate through and interact with the BalanceBoat Platform and to improve the BalanceBoat Platform.</p></li>
                            <li><p>To serve you tailored advertising (on third party websites).</p></li>
                            <li><p>To show you content that is more relevant to visitors.</p></li>
                            <li><p>To monitor and analyze the performance, operation, and effectiveness of the BalanceBoat  Platform.</p></li>
                            <li><p>To enforce legal agreements that govern use of the BalanceBoat Platform.</p></li>
                            <li><p>For fraud detection and prevention, and investigations.</p></li>
                            <li><p>For purposes of our own customer support, analytics, research, product development, and regulatory compliance.</p></li>
                        </ul>
                        <h3>Third Parties</h3>
                        <p>We may also allow certain business partners to place these technologies on the BalanceBoat Platform. These partners use these technologies to (1) help us analyze how visitors use the BalanceBoat Platform, such as by noting the third party services from which you arrived, (2) help us detect or prevent fraud or conduct risk assessments, or (3) collect information about visitors activities on the BalanceBoat Platform, other sites, and/or the ads you have clicked on. For example, to help us better understand how people use the  BalanceBoat Platform, we work with a number of analytics partners, including Google Analytics. To prevent Google Analytics from using your information for analytics, you may install the Google Analytics Opt-Out Browser by <a href="http://tools.google.com/dlpage/gaoptout" target="_blank">clicking here.</a></p>
                        <p>Third parties may also use such tracking technologies to serve ads that they believe are most likely to be of interest to you and measure the effectiveness of their ads both on the  BalanceBoat  Platform and on other websites and online services. Targeting and advertising cookies we use may include Google, and other advertising networks and services we use from time to time. For more information about targeting and advertising cookies and how you can opt out, you can visit the <a href="http://www.networkadvertising.org/choices/" target="_blank">Network Advertising Initiative’s opt-out page</a>, the <a href="http://www.aboutads.info/choices/" target="_blank">Digital Advertising Alliance’s opt-out page</a>, or <a href="http://youronlinechoices.eu/">http://youronlinechoices.eu</a>. To opt out of Google Analytics for display advertising or customize Google display network ads, you can visit the <a href="https://www.google.com/settings/ads" target="_blank">Google Ads Settings</a> page. To the extent advertising technology is integrated into the  BalanceBoat Platform and you opt-out of tailored advertising, you may still receive advertising content. In that case, the advertising content will just not be tailored to your interests. Also, we do not control any of these opt-out links and are not responsible for the availability or accuracy of these mechanisms.</p>
                        <h3>Third Party Social Plugins</h3>
                        <p>
                            The BalanceBoat Platform may use social plugins provided and operated by third parties, such as Facebook’s Like Button. As a result of this, you may send to the third party the information that you are viewing on a certain part of the BalanceBoat Platform. If you are not logged into your account with the third party, then the third party may not know your identity. If you are logged in to your account with the third party, then the third party may be able to link information or actions about your interactions with the BalanceBoat Platform to your account with them. Please refer to the third party’s privacy policies to learn more about its data practices.
                        </p>
                        <h3>CHANGES TO POLICY</h3>
                        <p>We reserve the right to modify this cookie policy at any time, so please review it frequently. Changes and clarifications will take effect immediately upon their posting on the website. If we make material changes to this policy, we will notify you here that it has been updated, so that you are aware of what information we collect, how we use it, and under what circumstances, if any, we use and/or disclose it.</p>
                        <h3>CONSENT</h3>
                        <p>By using this site, you represent that you are at least the age of majority in your state or province of residence, or that you are the age of majority in your state or province of residence and you have given us your consent to allow any of your minor dependents to use this site.</p>
                    </div>
                </div>
            </div>
        </div>
        <!--End row -->        
    </div>
    <!--End container -->
    <div id="overlay"></div>
    <!-- Mask on input focus -->
</main>
<!-- End section -->
@endsection
@section('footer')
@endsection