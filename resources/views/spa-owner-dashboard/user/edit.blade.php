@extends('spa-owner-dashboard.layouts.app')
@section('page-heading')
    <h3 class="text-themecolor">Edit User</h3>
@endsection
@section('head-style')

@endsection
@section('content')
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    @include ('errors.list') {{-- Including error file --}}
                   
                     <form id="frmCategory" action="{{ route("lstC.updateprofile") }}" method="post"
                              enctype="multipart/form-data" novalidate>
                            {{ csrf_field() }}
                            <input type="hidden" name="user_id" id="user_id" value="{{ $user->id }}"/>
                    <div class="row">
                        <div class="col-md-12">
                            <h4>Personal Details</h4>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                    <h5>First Name <span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <input type="text" id="first_name" name="first_name" value="{{ $user->first_name }}" class="form-control" required data-validation-required-message="This field is required" />
                                    </div>                           
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <h5>Last Name</h5>
                                <div class="controls">
                                    <input type="text" id="last_name" name="last_name" value="{{ $user->last_name }}" class="form-control" />
                                </div>                           
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <h5>Email <span class="text-danger">*</span></h5>
                                <div class="controls">
                                    <input type="email" id="email" name="email" value="{{ $user->email }}" class="form-control required" required data-validation-required-message="This field is required" />
                                </div>                            
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <h5>Phone No </h5>
                                <div class="controls">
                                <input class="form-control required number" name="phone_number" id="phone_number" type="tel" value="{{ $user->phone_number }}" required="" />
                                </div>
                            </div>

                        </div>
                    </div>
                   <div class="row">
                        <div class="col-md-12">
                                <h4>Update Password</h4>
                            </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                {{ Form::label('password', 'Password') }}<br>
                                {{ Form::password('password', array('class' => 'form-control')) }}
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                {{ Form::label('password', 'Confirm Password') }}<br>
                                {{ Form::password('password_confirmation', array('class' => 'form-control')) }}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <h4>Edit address</h4>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <label>Street address</label>
                                <input class="form-control" name="street_address" id="street_address" type="text" value="{{ $user->street_address }}" />
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <label>City/Town</label>
                                <input class="form-control" name="city" id="city" type="text" value="{{ $user->city }}" />
                            </div>
                        </div>
                    </div>
                    <!-- End row -->
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <label>Zip code</label>
                                <input class="form-control" name="zipcode" id="zipcode" type="text" value="{{ $user->zipcode }}" />
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <label>Country</label>
                                <select id="country" class="form-control" name="country">
                                    <option value="">Select</option>
                                    <option value="IN" {{ Auth::user()->country == "IN" ? "selected" : "" }}>India</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <!--<div class="row">
                        <div class="col-md-12">
                            <h4>Upload profile photo</h4>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <label>Street address</label>
                                <input type="file" name="profile_image_url" id="profile_image_url" />
                            </div>
                        </div>
                        
                    </div>-->
                    
                    <div class="text-xs-right">
                        <button type="submit" class="btn btn-info">Submit</button>
                        
                        <a href="{{ url('spa-owner-dashboard/dashboard') }}" class="btn btn-primary">Back</a>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Page Content -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
@endsection
@section('footer-script')
    <script src="{{ asset('public/spa-owner-dashboard/vendors/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/fastclick/lib/fastclick.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/nprogress/nprogress.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/iCheck/icheck.min.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/moment/min/moment.min.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/jquery.hotkeys/jquery.hotkeys.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/google-code-prettify/src/prettify.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/jquery.tagsinput/src/jquery.tagsinput.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/switchery/dist/switchery.min.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/parsleyjs/dist/parsley.min.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/autosize/dist/autosize.min.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/starrr/dist/starrr.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/build/js/custom.js') }}"></script>
@endsection