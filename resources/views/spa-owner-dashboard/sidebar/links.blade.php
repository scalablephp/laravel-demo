<div class="menu_section">
    <h3>Manage Listings</h3>
    <ul class="nav side-menu">
        <li><a><i class="fa fa-home"></i> Listing <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
                <li><a href="{{ route('spd.list') }}">Listings</a></li>
                <li><a href="{{ route('lstC.index') }}">Comments</a></li>
            </ul>
        </li>
    </ul>
</div>