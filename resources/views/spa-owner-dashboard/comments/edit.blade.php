@extends('spa-owner-dashboard.layouts.app')
@section('page-heading')
    <h3 class="text-themecolor">Comment Details</h3>
@endsection
@section('head-style')

@endsection
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <form id="frmCategory" action="{{ route("lstC.store") }}" method="post"
                              enctype="multipart/form-data" novalidate>
                            {{ csrf_field() }}
                            <input type="hidden" name="id" id="id" value="{{ $listingComment->id }}"/>
                            <div class="form-group row">
                                <label for="staticEmail" class="col-md-2 col-sm-2 col-form-label">Listing</label>
                                <div class="col-md-10 col-sm-10 controls">
                                    <a href='{{ route('spd.edit', [$listingComment->listing_id]) }}'
                                       target="_blank">{{ $listingComment->name }}</a>
                                </div>
                            </div>
                            <hr/>
                            <div class="form-group row">
                                <label for="staticEmail" class="col-md-2 col-sm-2 col-form-label">Author</label>
                                <div class="col-md-10 col-sm-10 controls">
                                    {{ $listingComment->comment_author }}
                                </div>
                            </div>
                            <hr/>
                            <div class="form-group row">
                                <label for="staticEmail" class="col-md-2 col-sm-2 col-form-label">Email</label>
                                <div class="col-md-10 col-sm-10 controls">
                                    {{ $listingComment->comment_author_email }}
                                </div>
                            </div>
                            <hr/>
                            <div class="form-group row">
                                <label for="staticEmail" class="col-md-2 col-sm-2 col-form-label">Comment</label>
                                <div class="col-md-10 col-sm-10 controls">
                                    {{ $listingComment->comment_content }}
                                </div>
                            </div>
                            <hr/>
                            <div class="form-group row">
                                <label for="staticEmail" class="col-md-2 col-sm-2 col-form-label">Comment
                                    Approved</label>
                                <div class="col-md-10 col-sm-10 controls">
                                    <div class="radio radio-primary">
                                        <input type="checkbox" name="comment_approved" class="js-switch"
                                                {{ ($listingComment->comment_approved == 1) ? 'checked' : "" }}
                                        />
                                        Approved
                                    </div>
                                </div>
                            </div>
                            <div class="text-xs-right row">
                               <!-- <button type="submit" class="btn btn-info">Submit</button>
                                <button type="reset" class="btn btn-inverse">Cancel</button>-->
                                <a href="{{ route('lstC.index') }}" class="btn btn-primary">Back</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
@endsection
@section('footer-script')
    <script src="{{ asset('public/spa-owner-dashboard/vendors/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/fastclick/lib/fastclick.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/nprogress/nprogress.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/iCheck/icheck.min.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/moment/min/moment.min.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/jquery.hotkeys/jquery.hotkeys.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/google-code-prettify/src/prettify.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/jquery.tagsinput/src/jquery.tagsinput.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/switchery/dist/switchery.min.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/parsleyjs/dist/parsley.min.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/autosize/dist/autosize.min.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/starrr/dist/starrr.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/build/js/custom.js') }}"></script>
@endsection