@extends('spa-owner-dashboard.layouts.app')
@section('page-heading')
    <h3 class="text-themecolor">Create Comment</h3>
@endsection
@section('head-style')

@endsection
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <form id="frmComment" action="{{ route('lstC.store') }}" method="post"
                              enctype="multipart/form-data" novalidate>
                            {{ csrf_field() }}
                            <input type="hidden" name="comment_author_email" id="comment_author_email"
                                   value="{{ Auth::user()->email }}"/>
                            <div class="form-group">
                                <h5>Listing <span class="text-danger">*</span></h5>
                                <div class="controls">
                                    <select name="listing_id" id="listing_id" class="select2_single form-control"
                                            required
                                            data-validation-required-message="This field is required">
                                    </select>
                                </div>
                                @if ($errors->has('listing_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('listing_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <h5>Comment</h5>
                                <div class="controls">
                                    <textarea name="comment_content" id="comment_content" class="form-control required"
                                              required data-validation-required-message="This field is required"
                                              rows="10"></textarea>
                                </div>
                            </div>
                            <div class="text-xs-right">
                                <button type="submit" class="btn btn-info">Submit</button>
                                <button type="reset" class="btn btn-inverse">Cancel</button>
                                <a href="{{ route('lstC.index') }}" class="btn btn-primary">Back</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
@endsection
@section('footer-script')
    <script type="text/javascript">ADMIN_URL = '<?php echo url("/spa-owner-dashboard/"); ?>';</script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/fastclick/lib/fastclick.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/nprogress/nprogress.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/iCheck/icheck.min.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/moment/min/moment.min.js') }}"></script>
    <script src="{{ asset('public/admin/js/validation.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/jquery.hotkeys/jquery.hotkeys.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/google-code-prettify/src/prettify.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/jquery.tagsinput/src/jquery.tagsinput.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/switchery/dist/switchery.min.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/parsleyjs/dist/parsley.min.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/autosize/dist/autosize.min.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/starrr/dist/starrr.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/build/js/custom.js') }}"></script>
    <script src="{{ asset('public/admin/js/comment-create.js') }}"></script>
@endsection