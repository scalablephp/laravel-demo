@extends('spa-owner-dashboard.layouts.app')

@section('page-heading')
    <h3 class="text-themecolor">Comments</h3>
@endsection

@section('content')
    <!--<div class="text-right">
        <a href="{{route('lstC.create')}}" class="btn btn-info btn-rounded">Add New Comments</a>
    </div>-->
    <table id="datatable-responsive"
           class="table table-striped table-bordered dt-responsive nowrap"
           cellspacing="0"
           width="100%">
        <thead>
        <tr>
            <th>Listing</th>
            <th>Author</th>
            <th>Email</th>
            <th>Date</th>
            <th>Detail</th>
        </tr>
        </thead>
        <tbody>
        @foreach($ListingComments as $listingComment)
            <tr>
                <td>{{ $listingComment->name }}</td>
                <td>{{ $listingComment->comment_author }}</td>
                <td>{{ $listingComment->comment_author_email }}</td>
                <td>{{ $listingComment->comment_date }}</td>
                <td>
                    <div class="btn-group">
                        <a href="{{ route('lstC.edit', [$listingComment->id]) }}" class = "btn btn-info">Detail</a>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <form id="frmListings" name="frmListings" action="{{ url('spa-owner-dashboard/listing/destroy') }}" method="post">
        {{ csrf_field() }}
        <input type="hidden" name="id" id="id" value=""/>
    </form>
@endsection
@section('footer-script')
    <script src="{{ asset('public/spa-owner-dashboard/vendors/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/datatables.net-buttons/js/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script src="{{ asset('public/admin/plugins/select2/dist/js/select2.full.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/datatables.net-scroller/js/dataTables.scroller.min.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/jszip/dist/jszip.min.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/pdfmake/build/pdfmake.min.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/pdfmake/build/vfs_fonts.js') }}"></script>
    <script src="{{ asset('public/admin/plugins/sweetalert/sweetalert.min.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/build/js/custom.js') }}"></script>
@endsection
