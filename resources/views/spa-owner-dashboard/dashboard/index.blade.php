@extends('spa-owner-dashboard.layouts.app')

@section('page-heading')
    <h3 class="text-themecolor">Dashboard</h3>
@endsection

@section('content')

    <div class="row top_tiles">
        <!--<div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="tile-stats">
                <div class="icon"><i class="fa fa-check-square-o"></i></div>
                <div class="count">{{ $listings}}</div>
                <h3>Listings Added</h3>
                
            </div>
        </div>
        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="tile-stats">
                <div class="icon"><i class="fa fa-comments-o"></i></div>
                <div class="count">{{ $ListingComments }}</div>
                <h3>Comments</h3>
                
            </div>
        </div>
       <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="tile-stats">
                <div class="icon"><i class="fa fa-sort-amount-desc"></i></div>
                <div class="count">{{ $ListingHits }}</div>
                <h3>Visitors</h3>
                <p>Lorem ipsum psdea itgum rixt.</p>
            </div>
        </div>-->
        <div class="row tile_count">
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i> Total Listing</span>
              <div class="count">{{ $listings}}</div>
              <!--<span class="count_bottom"><i class="green">4% </i> From last Week</span>-->
            </div>
           <!-- <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-clock-o"></i> Average Time</span>
              <div class="count">123.50</div>
              <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>3% </i> From last Week</span>
            </div>-->
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i> Total Comments</span>
              <div class="count green">{{ $ListingComments }}</div>
              <!--<span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>34% </i> From last Week</span>-->
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i> Total Visitors</span>
              <div class="count">{{ $ListingHits }}</div>
              <!--<span class="count_bottom"><i class="red"><i class="fa fa-sort-desc"></i>12% </i> From last Week</span>-->
            </div>
           <!-- <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i> Total Collections</span>
              <div class="count">2,315</div>
              <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>34% </i> From last Week</span>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i> Total Connections</span>
              <div class="count">7,325</div>
              <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>34% </i> From last Week</span>
            </div>
          </div>-->
       <!--  <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="tile-stats">
                <div class="icon"><i class="fa fa-check-square-o"></i></div>
                <div class="count">179</div>
                <h3>New Sign ups</h3>
                <p>Lorem ipsum psdea itgum rixt.</p>
            </div>
        </div>-->
        <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="dashboard_graph">

                <div class="row x_title">
                  <div class="col-md-6">
                    <h3>User Traffic <small></small></h3>
                  </div>
                  <div class="col-md-6">
                    <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
                      <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                      <span>April 19, 2019 - May 18, 2019</span> <b class="caret"></b>
                    </div>
                  </div>
                </div>

                <div class="col-md-9 col-sm-9 col-xs-12" id="chartarea">
                   <canvas id="myChart" width="400" height="100"></canvas>
    
   <canvas class="flot-base" width="1164" height="280" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 1164px; height: 280px;"></canvas>
   <div class="flot-text" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; font-size: smaller; color: rgb(84, 84, 84);">
     
      
     
   </div>
   <canvas class="flot-overlay" width="1164" height="280" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 1164px; height: 280px;"></canvas>
</div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12 bg-white">
                  <div class="x_title">
                    <h2>Top Listings</h2>
                    <div class="clearfix"></div>
                  </div>

                  <div class="col-md-12 col-sm-12 col-xs-6">
                     @foreach($getLast6Hits as $hits)
                     <div>
                      <p>{{$hits->name}}</p>
                      <div class="">
                        <div class="progress progress_sm" style="width: 76%;">
                          <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="80" aria-valuenow="79" style="width: 80%;"></div>
                        </div>
                      </div>
                    </div>
                      @endforeach 
                   
                    
                  </div>
                  <div class="col-md-12 col-sm-12 col-xs-6">
                    
                  </div>

                </div>

                <div class="clearfix"></div>
              </div>
            </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="x_panel tile fixed_height_320">
                <div class="x_title">
                  <h2>Top Performer listings</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                   
                    <!--<li><a class="close-link"><i class="fa fa-close"></i></a>-->
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <h4>App Usage across versions</h4>
                  @foreach($getTopListingHits as $hits)
                       <div class="widget_summary">
                    <div class="w_left w_25">
                      <span>{{$hits->name}}</span>
                    </div>
                    <div class="w_center w_55">
                      <div class="progress">
                        <div class="progress-bar bg-green" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 66%;">
                          <span class="sr-only">60% Complete</span>
                        </div>
                      </div>
                    </div>
                    <div class="w_right w_20">
                      <span>{{$hits->Day_count}} Visitors</span>
                    </div>
                    <div class="clearfix"></div>
                  </div>

                  @endforeach 
                 

                </div>
              </div>
        </div>

        <div class="col-md-4">
            <div class="x_panel">
                <div class="x_title">
                  <h2>Recent Listing Reviews</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                   
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <div class="dashboard-widget-content">

                    <ul class="list-unstyled timeline widget">
                      @foreach($recentListingComments as $listingComments)
                        <li>
                        <div class="block">
                          <div class="block_content">
                            <h2 class="title">
                             <a>{{$listingComments->name}}</a>
                             </h2>
                            <div class="byline">
                              <span>{{ \Carbon\Carbon::parse($listingComments->comment_date)->format('F j, Y, g:i a')}}</span> by <a>{{$listingComments->comment_author}}</a>
                            </div>
                            <p class="excerpt">{{$listingComments->comment_content}}
                            </p>
                          </div>
                        </div>
                      </li>

                      @endforeach 
                      
                    </ul>
                  </div>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Top Profiles <small>Sessions</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Settings 1</a>
                                </li>
                                <li><a href="#">Settings 2</a>
                                </li>
                            </ul>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <article class="media event">
                        <a class="pull-left date">
                            <p class="month">April</p>
                            <p class="day">23</p>
                        </a>
                        <div class="media-body">
                            <a class="title" href="#">Item One Title</a>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                        </div>
                    </article>
                    <article class="media event">
                        <a class="pull-left date">
                            <p class="month">April</p>
                            <p class="day">23</p>
                        </a>
                        <div class="media-body">
                            <a class="title" href="#">Item Two Title</a>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                        </div>
                    </article>
                    <article class="media event">
                        <a class="pull-left date">
                            <p class="month">April</p>
                            <p class="day">23</p>
                        </a>
                        <div class="media-body">
                            <a class="title" href="#">Item Two Title</a>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                        </div>
                    </article>
                    <article class="media event">
                        <a class="pull-left date">
                            <p class="month">April</p>
                            <p class="day">23</p>
                        </a>
                        <div class="media-body">
                            <a class="title" href="#">Item Two Title</a>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                        </div>
                    </article>
                    <article class="media event">
                        <a class="pull-left date">
                            <p class="month">April</p>
                            <p class="day">23</p>
                        </a>
                        <div class="media-body">
                            <a class="title" href="#">Item Three Title</a>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                        </div>
                    </article>
                </div>
            </div>
        </div>
    </div>
   


@endsection

@section('footer-script')

    <script src="{{ asset('public/spa-owner-dashboard/vendors/Chart.js/dist/Chart.min.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/jquery-sparkline/dist/jquery.sparkline.min.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/Flot/jquery.flot.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/Flot/jquery.flot.pie.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/Flot/jquery.flot.time.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/Flot/jquery.flot.stack.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/Flot/jquery.flot.resize.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/flot.orderbars/js/jquery.flot.orderBars.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/flot-spline/js/jquery.flot.spline.min.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/flot.curvedlines/curvedLines.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/DateJS/build/date.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/moment/min/moment.min.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/build/js/custom.js') }}"></script>
    
    <script>

var ctx = document.getElementById('myChart').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'line',
    data: {
        labels: [
        @foreach($getLast6Hits as $hits)
        '{{$hits->day}}',
        @endforeach 
        ],
        datasets: [{
           label: '# Last 7 days visitors',

            data: {{$graphArr}},
            backgroundColor: [
                'rgba(0, 0, 0, 0.1)',
            ],
            borderColor: [
                'rgba(38, 185, 154, 0.38)',
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
}); 

function updateChart(resultString, graphdata)
{

  $("#myChart").remove();

  $("#chartarea").append('<canvas id="myChart" width="1179" height="294" style=""></canvas>');
  var ctx = document.getElementById('myChart').getContext('2d');
  var myChart = new Chart(ctx, {
    type: 'line',
    data: {
        labels: [
         
        ],
        datasets: [{
           label: '# Last 7 days visitors',

            data: [],
            backgroundColor: [
                'rgba(0, 0, 0, 0.1)',
            ],
            borderColor: [
                'rgba(38, 185, 154, 0.38)',
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
}); 

  var array = resultString.split(",");
  $.each(array,function(i){
     myChart.data.labels.push(array[i]);
  });
  var array1 = graphdata.split(",");
  $.each(array1,function(i){
     myChart.data.datasets[0].data.push(array1[i]);
  });

  myChart.update();

}
  $('#reportrange').on('hide.daterangepicker', function () {
        //alert("nnn");
    }); 
  $('#reportrange').on('apply.daterangepicker', function (ev, picker) {

    var startDate = picker.startDate.format('MMMM D, YYYY');
    var endDate = picker.endDate.format('MMMM D, YYYY');

      $.ajax({
        url: "/spa-owner-dashboard/dashboard/updategraph",
        method: "POST",
        data: {
        startDate : startDate,
        endDate :endDate ,
       _token:     '{{ csrf_token() }}'
      },
        success: function (result) {
           // alert(result); return false;
           if(result) {
                           
var obj = jQuery.parseJSON(result);
var graphLabel = obj.graphlabel;
var graphdata = obj.graphdata;

//var input = "label1, label2, label3, label4, label5";
//var resultString = '\'' + graphLabel.split(',').join('\',\'') + '\'';
//var resultString = '['+resultString+']';

 updateChart(graphLabel, graphdata);
 return false;
var ctx = document.getElementById('myChart').getContext('2d');

var myChart = new Chart(ctx, {
    type: 'line',
    data: {

        labels: ['2019-05-18','2019-04-30'],
        datasets: [{
           label: '# Last 7 days visitors',

            data: [8,1],
            backgroundColor: [
                'rgba(0, 0, 0, 0.1)',
            ],
            borderColor: [
                'rgba(38, 185, 154, 0.38)',
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
}); 
                               
                                
                           } 
                            
                        }
                    });
    });

    </script>
@endsection


        <!-- page content -->
        