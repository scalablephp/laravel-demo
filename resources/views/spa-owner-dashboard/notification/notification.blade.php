@if(Session::has('flash_message'))
    <div class="container">
        <div class="alert alert-success">
            <em> {!! session('flash_message') !!}</em>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
        </div>
    </div>
@endif
@if(Session::has('flash_error_message'))
    <div class="container">
        <div class="alert alert-danger">
            <em> {!! session('flash_error_message') !!}</em>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
        </div>
    </div>
@endif