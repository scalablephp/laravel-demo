@extends('spa-owner-dashboard.layouts.app')
@section('page-heading')
    <h3 class="text-themecolor">Create Listings</h3>
@endsection
@section('head-style')
    <link href="{{ asset('public/admin/plugins/dropzone-master/dist/min/dropzone.min.css') }}" rel="stylesheet"/>
    <link href="{{ asset('public/admin/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.css') }}" rel="stylesheet"/>
@endsection
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <form id="frmCenter" action="{{ action('SpaOwnerDashboard\ListingController@store') }}"
                          method="post" enctype="multipart/form-data" novalidate>
                        {{ csrf_field() }}
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <fieldset class="col-md-12">
                                    <legend>Name & Type</legend>
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <h5>Name of Spa/Salon <span class="text-danger">*</span></h5>
                                                <div class="controls">
                                                    <input type="text"
                                                           id="name"
                                                           name="name"
                                                           class="form-control" required
                                                           data-validation-required-message="This field is required"/>
                                                </div>
                                                <div class="form-control-feedback">
                                                    <small>The name is how it appear on the site.</small>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <h5>Slug <span class="text-danger">*</span></h5>
                                                <div class="controls">
                                                    <input type="text" id="slug" name="slug" class="form-control"
                                                           required
                                                           data-validation-required-message="This field is required"/>
                                                </div>
                                                <div class="form-control-feedback">
                                                    <small>
                                                        The "slug" is the URL-friendly version of the name. It is
                                                        usually all lowercase
                                                        and contains only letters, numbers, and hyphens.
                                                    </small>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <h5>Type of Spa</h5>
                                                        <div class="controls">
                                                            <select id="type_of_institution" name="type_of_institution"
                                                                    class="form-control select2_single select2" style="width: 100%"
                                                                    data-placeholder="">
                                                                <option value="">Select</option>
                                                                @if(@$types)
                                                                    @foreach (@$types as $type)
                                                                        <option value="{{ $type->id }}">{{ $type->name }}</option>
                                                                    @endforeach
                                                                @endif
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6"></div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <h5>Banner Image</h5>
                                                <div class="controls">
                                                    <input type="file" name="banner_image" id="banner_image"
                                                           class="form-control"/>
                                                </div>
                                                <div class="form-control-feedback">
                                                    <small>Image size (750 X 500)</small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <fieldset class="col-md-12">
                                    <legend>SEO</legend>
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <h5>Meta Title</h5>
                                                <div class="controls">
                                                    <input type="text" id="meta_title" name="meta_title"
                                                           class="form-control" value=""
                                                           required
                                                           data-validation-required-message="This field is required"/>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <h5>Meta Keywords</h5>
                                                <div class="controls">
                                 <textarea id="meta_keywords" name="meta_keywords"
                                           class="form-control"></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <h5>Meta Description</h5>
                                                <div class="controls">
                                 <textarea id="meta_description" name="meta_description"
                                           class="form-control"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <fieldset class="col-md-12">
                                    <legend>Address & Location</legend>
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <h5>Country</h5>
                                                        <div class="controls">
                                                            <select id="country" name="country" class="select2_single form-control"
                                                                    required
                                                                    data-validation-required-message="This field is required">
                                                                <option value="">Select</option>
                                                                @if($detinations)
                                                                    @foreach($detinations as $destination)
                                                                        <option value="{{ @$destination->id }}">{{ @$destination->name }}</option>
                                                                    @endforeach
                                                                @endif
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <h5>State</h5>
                                                        <div class="controls">
                                                            <select id="state" name="state" class="form-control">
                                                                <option value="">Select</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <h5>City/Town</h5>
                                                        <div class="controls">
                                                            <select id="city" name="city" class="form-control required"
                                                                    data-validation-required-message="This field is required">
                                                                <option value="">Select</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <h5>Zipcode/Pincode</h5>
                                                        <div class="controls">
                                                            <input type="text" id="zipcode" name="zipcode"
                                                                   class="form-control"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <h5>Address</h5>
                                                        <div class="controls">
                                                            <input type="text" id="address" name="address"
                                                                   class="form-control"/>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <h5>Area</h5>
                                                        <div class="controls">
                                                            <input type="text" id="area" name="area"
                                                                   class="form-control"/>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <h5>Landmark</h5>
                                                        <div class="controls">
                                                            <input type="text" id="landmark" name="landmark"
                                                                   class="form-control"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-8">
                                                        <h5>Nearest Public Transport</h5>
                                                        <div class="controls">
                                                            <input type="text" id="nearest_public_transport"
                                                                   name="nearest_public_transport"
                                                                   class="form-control"/>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <h5>Latitude</h5>
                                                        <div class="controls">
                                                            <input type="text" id="latitude" name="latitude"
                                                                   class="form-control"/>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <h5>Longitude</h5>
                                                        <div class="controls">
                                                            <input type="text" id="longitude" name="longitude"
                                                                   class="form-control"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <h5>How to get here (provided by spa)</h5>
                                                <div class="controls">
                                 <textarea id="how_to_get_there" name="how_to_get_there"
                                           class="textarea_editor form-control" rows="10"
                                           placeholder="Enter text ..."></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <fieldset class="col-md-12">
                                    <legend>Contact Details</legend>
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <h5>Email</h5>
                                                <div class="controls">
                                                    <input type="text" id="email" name="email"
                                                           class="form-control form-control-tag"
                                                           data-role="tagsinput"/>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <h5>Landline</h5>
                                                        <div class="controls">
                                                            <input type="text" id="landline" name="landline"
                                                                   class="form-control"/>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <h5>Mobile</h5>
                                                        <div class="controls">
                                                            <input type="text" id="mobile" name="mobile"
                                                                   class="form-control"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <h5>Website</h5>
                                                        <div class="controls">
                                                            <input type="text" id="website" name="website"
                                                                   class="form-control"/>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <h5>Facebook URL</h5>
                                                        <div class="controls">
                                                            <input type="text" id="facebook_url" name="facebook_url"
                                                                   class="form-control"/>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <h5>Instagram URL</h5>
                                                        <div class="controls">
                                                            <input type="text" id="instagram_url" name="instagram_url"
                                                                   class="form-control"/>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <h5>Youtube Channel</h5>
                                                        <div class="controls">
                                                            <input type="text" id="youtube_channel"
                                                                   name="youtube_channel"
                                                                   class="form-control"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <fieldset class="col-md-12">
                                    <legend>About Spa/Salon</legend>
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="form-group alert alert-info">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <h5>Hours of Operation</h5>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <h5>Day</h5>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <h5>Opening Hour</h5>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <h5>Closing Hour</h5>
                                                    </div>
                                                </div>
                                                <hr/>
                                                @php $day = array("monday","tuesday","wednesday","thursday","friday","saturday","sunday") @endphp
                                                @foreach ($day as $dy)
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <h5>{{ ucfirst($dy) }}</h5>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <div class="controls">
                                                                <input type="text" id="opening_hours_opentime_{{ $dy }}"
                                                                       name="opening_hours[{{$dy}}][opentime]"
                                                                       class="form-control timepicker"/>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <div class="controls">
                                                                <input type="text" id="opening_hours_closetime_{{$dy}}"
                                                                       name="opening_hours[{{$dy}}][closetime]"
                                                                       class="form-control timepicker"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                            <div class="form-group">
                                                <h5>About Us</h5>
                                                <div class="controls">
                                 <textarea id="about_us" name="about_us" class="textarea_editor form-control" rows="10"
                                           placeholder="Enter text ..."></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <h5>Parking</h5>
                                                        <div class="controls">
                                                            <select id="parking" name="parking" class="select2_single form-control">
                                                                <option value="">Select</option>
                                                                <option value="Yes">Yes</option>
                                                                <option value="No">No</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <h5>Spa Area</h5>
                                                        <div class="controls">
                                                            <input type="text" id="spa_area" name="spa_area"
                                                                   class="form-control"/>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <h5>Gender</h5>
                                                        <div class="controls">
                                                            <select id="gender" name="gender" class="select2_single form-control"
                                                                    style="width: 100%"
                                                                    data-placeholder="">
                                                                <option value="">Select</option>
                                                                <option value="Male">Male</option>
                                                                <option value="Female">Female</option>
                                                                <option value="Unisex">Unisex</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <h5>Image gallery</h5>
                                                <div class="controls">
                                                    <div id="image_gallery" class="dropzone">
                                                        <div class="dz-message text-center">
                                                            Upload Images (Click or Drag file here)
                                                        </div>
                                                        <input name="image_galleries" type="file" multiple
                                                               style="display:none;"/>
                                                    </div>
                                                    <input type="hidden" id="image_gallery_ids" name="image_gallery_ids"
                                                           value=""/>
                                                    <input type="hidden" id="dropzoneurl"
                                                           value="{{ url("spa-owner-dashboard/listing/upload_gallery_image") }}"/>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <h5>Video</h5>
                                                <div class="controls">
                                                    <ol>
                                                        <li>
                                                            <input type="text" id="video[]" name="video[]"
                                                                   class="form-control"/>
                                                        </li>
                                                        <li>
                                                            <input type="text" id="video[]" name="video[]"
                                                                   class="form-control"/>
                                                        </li>
                                                        <li>
                                                            <input type="text" id="video[]" name="video[]"
                                                                   class="form-control"/>
                                                        </li>
                                                        <li>
                                                            <input type="text" id="video[]" name="video[]"
                                                                   class="form-control"/>
                                                        </li>
                                                    </ol>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <h5>Awards</h5>
                                                <div class="controls">
                                                    <select id="awards[]" name="awards[]"
                                                            class="form-control select2_multiple select2-multiple form-control-select2"
                                                            multiple="multiple"
                                                            data-placeholder="">
                                                        <option value="">Select</option>
                                                        @if($awards)
                                                            @foreach($awards as $award)
                                                                <option value="{{ $award->id }}">{{ $award->name }}</option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <fieldset class="col-md-12">
                                    <legend>Offered Service</legend>
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            @if(@$services)
                                                @foreach(@$services as $service)
                                                    <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                                        <h5>{{ $service['name'] }}</h5>
                                                        <div class="controls">
                                                            <select id="service[]" name="service[]"
                                                                    class="form-control select2-multiple select2_multiple form-control-select2"
                                                                    multiple="multiple" data-placeholder="">
                                                                <option value="">Select</option>
                                                                @if(isset($service['subservice']))
                                                                    @foreach($service['subservice'] as $subid=>$subservice)
                                                                        <option value="{{ @$subid }}">{{ $subservice }}</option>
                                                                    @endforeach
                                                                @endif
                                                            </select>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            @endif
                                        </div>
                                    </div>
                                </fieldset>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <fieldset class="col-md-12">
                                    <legend>Accommodation (If any)</legend>
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <h5>Accommodation Overview</h5>
                                                <div class="controls">
                                 <textarea id="accommodation" name="accommodation"
                                           class="textarea_editor form-control"
                                           rows="10" placeholder="Enter text ..."></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <h5>Accommodation Banner Image</h5>
                                                <div class="controls">
                                                    <input type="file" name="accommodation_banner_image"
                                                           id="accommodation_banner_image"
                                                           class="form-control"/>
                                                </div>
                                                <div class="form-control-feedback">
                                                    <small>Image size (750 X 500)</small>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <h5>Accommodation Image Gallery</h5>
                                                <div class="controls">
                                                    <div id="acm_image_gallery" class="dropzone">
                                                        <div class="dz-message text-center">
                                                            Upload Images (Click or Drag file here)
                                                        </div>
                                                        <input name="accommodation_image_gallery" type="file" multiple
                                                               style="display:none;"/>
                                                    </div>
                                                    <input type="hidden" id="accommodation_image_gallery_ids"
                                                           name="accommodation_image_gallery_ids" value=""/>
                                                    <input type="hidden" id="acmdropzoneurl"
                                                           value="{{ url("spa-owner-dashboard/listing/upload_gallery_image") }}"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <fieldset class="col-md-12">
                                    <legend>Food (If any)</legend>
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <h5>Food</h5>
                                                <div class="controls">
                                 <textarea id="food" name="food" class="textarea_editor form-control" rows="10"
                                           placeholder="Enter text ..."></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <h5>Foog Banner Image</h5>
                                                <div class="controls">
                                                    <input type="file" name="food_banner_image" id="food_banner_image"
                                                           class="form-control"/>
                                                </div>
                                                <div class="form-control-feedback">
                                                    <small>Image size (750 X 500)</small>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <h5>Food Image Gallery</h5>
                                                <div class="controls">
                                                    <div id="food_image_gallery" class="dropzone">
                                                        <div class="dz-message text-center">
                                                            Upload Images (Click or Drag file here)
                                                        </div>
                                                        <input name="food_image_galleries" type="file" multiple
                                                               style="display:none;"/>
                                                    </div>
                                                    <input type="hidden" id="food_image_gallery_ids"
                                                           name="food_image_gallery_ids"
                                                           value=""/>
                                                    <input type="hidden" id="fooddropzoneurl"
                                                           value="{{ url("spa-owner-dashboard/listing/upload_gallery_image") }}"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-body">

                                <fieldset class="col-md-12">
                                    <legend>Currency</legend>

                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <div class="controls">
                                                    <select id="currency" name="currency" class="form-control select2_single select2"
                                                            style="width: 100%" data-placeholder="Select">
                                                        @foreach(\App\Http\Helpers\CommonHelper::get_currency() as $currency)
                                                            <option value="{{ $currency }}">{{ $currency }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </fieldset>

                                <div class="clearfix"></div>
                            </div>

                        </div>
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <fieldset class="col-md-12">
                                    <legend>Menu</legend>
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <h5>Menu Image Gallery</h5>
                                                <div class="controls">
                                                    <div id="menu_image_gallery" class="dropzone">
                                                        <div class="dz-message text-center">
                                                            Upload Images (Click or Drag file here)
                                                        </div>
                                                        <input name="menu_image_galleries" type="file" multiple
                                                               style="display:none;"/>
                                                    </div>
                                                    <input type="hidden" id="menu_image_gallery_ids"
                                                           name="menu_image_gallery_ids"
                                                           value=""/>
                                                    <input type="hidden" id="menudropzoneurl"
                                                           value="{{ url("spa-owner-dashboard/listing/upload_gallery_image") }}"/>
                                                </div>
                                            </div>
                                            <!-- Nav tabs -->
                                            <ul class="nav nav-tabs" role="tablist">
                                                <li class="nav-item">
                                                    <a class="nav-link active show" data-toggle="tab" href="#menu_1"
                                                       role="tab" aria-selected="true">
                                                        <span class="hidden-sm-up"><i
                                                                    class="ti-home"></i></span> <span
                                                                class="hidden-xs-down">Menu 1</span></a></li>
                                                <li class="nav-item"><a class="nav-link show" data-toggle="tab"
                                                                        href="#menu_2" role="tab"
                                                                        aria-selected="false"><span
                                                                class="hidden-sm-up"><i
                                                                    class="ti-user"></i></span> <span
                                                                class="hidden-xs-down">Menu 2</span></a></li>
                                                <li class="nav-item"><a class="nav-link show" data-toggle="tab"
                                                                        href="#menu_3" role="tab"
                                                                        aria-selected="false"><span
                                                                class="hidden-sm-up"><i
                                                                    class="ti-email"></i></span> <span
                                                                class="hidden-xs-down">Menu 3</span></a>
                                                </li>
                                            </ul>
                                            <!-- Tab panes -->
                                            <div class="tab-content tabcontent-border form-group">
                                                <div class="tab-pane p-20 active show1" id="menu_1" role="tabpanel">
                                                    <div class="row">
                                                        <div class="col-md-9">
                                                            <div class="form-group">
                                                                <h5>Title</h5>
                                                                <div class="controls">
                                                                    <input type="text" id="menu_title[]"
                                                                           name="menu_title[]"
                                                                           class="form-control"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <h5>Classification</h5>
                                                                <div class="controls">
                                                                    <select id="classification[]"
                                                                            name="classification[]"
                                                                            class="form-control">
                                                                        <option value="Packages">Packages</option>
                                                                        <option value="Treatments">Treatments</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <h5>Price</h5>
                                                                <div class="controls">
                                                                    <input type="text" id="menu_price[]"
                                                                           name="menu_price[]"
                                                                           class="form-control"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <h5>Duration</h5>
                                                                <div class="controls">
                                                                    <div class="input-group">
                                                                        <input type="text" id="duration[]"
                                                                               name="duration[]"
                                                                               class="form-control"/>
                                                                        <span class="input-group-addon">mins</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <h5>Description</h5>
                                                        <div class="controls">
                                       <textarea id="menu_description[]" name="menu_description[]"
                                                 class="textarea_editor form-control"
                                                 placeholder="Enter text ..."></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane p-20 show1" id="menu_2" role="tabpanel">
                                                    <div class="row">
                                                        <div class="col-md-9">
                                                            <div class="form-group">
                                                                <h5>Title</h5>
                                                                <div class="controls">
                                                                    <input type="text" id="menu_title[]"
                                                                           name="menu_title[]"
                                                                           class="form-control"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <h5>Price</h5>
                                                                <div class="controls">
                                                                    <input type="text" id="menu_price[]"
                                                                           name="menu_price[]"
                                                                           class="form-control"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <h5>Description</h5>
                                                        <div class="controls">
                                       <textarea id="menu_description[]" name="menu_description[]"
                                                 class="textarea_editor form-control"
                                                 placeholder="Enter text ..."></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane p-20 show1" id="menu_3" role="tabpanel">
                                                    <div class="row">
                                                        <div class="col-md-9">
                                                            <div class="form-group">
                                                                <h5>Title</h5>
                                                                <div class="controls">
                                                                    <input type="text" id="menu_title[]"
                                                                           name="menu_title[]"
                                                                           class="form-control"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <h5>Price</h5>
                                                                <div class="controls">
                                                                    <input type="text" id="menu_price[]"
                                                                           name="menu_price[]"
                                                                           class="form-control"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <h5>Description</h5>
                                                        <div class="controls">
                                       <textarea id="menu_description[]" name="menu_description[]"
                                                 class="textarea_editor form-control"
                                                 placeholder="Enter text ..."></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <fieldset class="col-md-12">
                                    <legend>Payment Options</legend>
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <h5>Payment Options</h5>
                                                <div class="controls">
                                                    <select id="payment_options[]" name="payment_options[]"
                                                            class="form-control select2_multiple form-control-select2"
                                                            multiple="multiple"
                                                            data-placeholder="">
                                                        <option value="">Select</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <h5>Status</h5>
                                                        <div class="controls">
                                                            <select id="is_draft" name="is_draft"
                                                                    class="form-control select2_single select2 form-control-select2">
                                                                <option value="1" selected="">Draft</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <h5>Display on Homepage</h5>
                                                        <div class="controls">
                                                            <select name="display_on_home" id="display_on_home"
                                                                    class="form-control select2_single select2 form-control-select2">
                                                                <option value="0" selected="">No</option>
                                                                <option value="1">Yes</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <h5>Featured Listing</h5>
                                                        <div class="controls">
                                                            <select name="is_featured" id="is_featured"
                                                                    class="form-control select2_single select2 form-control-select2">
                                                                <option value="0" selected="">No</option>
                                                                <option value="1">Yes</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="text-xs-right">
                                                <button type="submit" class="btn btn-info">Submit</button>
                                                <button type="reset" class="btn btn-inverse">Cancel</button>
                                                <a href="{{ url('spa-owner-dashboard/listings') }}" class="btn btn-primary">Back</a>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer-script')
    <script type="text/javascript">ADMIN_URL = '<?php echo url("/spa-owner-dashboard/"); ?>';</script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/moment/min/moment.min.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('public/admin/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/jquery.hotkeys/jquery.hotkeys.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/google-code-prettify/src/prettify.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/jquery.tagsinput/src/jquery.tagsinput.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/switchery/dist/switchery.min.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script src="{{ asset('public/admin/plugins/select2/dist/js/select2.full.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('public/admin/js/validation.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/parsleyjs/dist/parsley.min.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/autosize/dist/autosize.min.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/starrr/dist/starrr.js') }}"></script>
    <script src="{{ asset('public/admin/plugins/dropzone-master/dist/min/dropzone.min.js') }}"></script>
    <script src="{{ asset('public/admin/plugins/tinymce/tinymce.min.js') }}"></script>
    <script src="{{ asset('public/admin/js/listing-create.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/build/js/custom.js') }}"></script>
@endsection