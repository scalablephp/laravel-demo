@extends('spa-owner-dashboard.layouts.app')

@section('page-heading')
    <h3 class="text-themecolor">Listings</h3>
@endsection

@section('content')
    <div class="text-right">
        <a href="{{route('spd.create')}}" class="btn btn-info btn-rounded">Add New Listing</a>
    </div>
    <table id="datatable-responsive"
           class="table table-striped table-bordered dt-responsive nowrap"
           cellspacing="0"
           width="100%">
        <thead>
        <tr>
            <th>#</th>
            <th>Name</th>
            <th>Slug</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        @foreach($listings as $listing)
            <tr>
                <td>{{ $listing->id }}</td>
                <td>{{ $listing->name }}</td>
                <td>{{ $listing->slug }}</td>
                <td>
                    <div class="btn-group">
                        <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                            Action
                        </button>
                        <div class="dropdown-menu" x-placement="bottom-start">
                            <a class="dropdown-item" href="{{ route('spd.edit', [$listing->id]) }}">Edit</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item text-danger listing-delete" href="#"
                               data-rel="<?php echo $listing->id; ?>">Delete</a>
                        </div>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <form id="frmListings" name="frmListings" action="{{ url('spa-owner-dashboard/listing/destroy') }}" method="post">
        {{ csrf_field() }}
        <input type="hidden" name="id" id="id" value=""/>
    </form>
@endsection
@section('footer-script')
    <script src="{{ asset('public/spa-owner-dashboard/vendors/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/datatables.net-buttons/js/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script src="{{ asset('public/admin/plugins/select2/dist/js/select2.full.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/datatables.net-scroller/js/dataTables.scroller.min.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/jszip/dist/jszip.min.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/pdfmake/build/pdfmake.min.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/vendors/pdfmake/build/vfs_fonts.js') }}"></script>
    <script src="{{ asset('public/admin/plugins/sweetalert/sweetalert.min.js') }}"></script>
    <script src="{{ asset('public/spa-owner-dashboard/build/js/custom.js') }}"></script>
@endsection
