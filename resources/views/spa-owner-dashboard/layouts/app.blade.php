<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('public/admin/assets/images/favicon.png') }}">
    <title>{{ config('app.name', 'Balanceseek') }}</title>
    <link href="{{ asset('public/spa-owner-dashboard/vendors/bootstrap/dist/css/bootstrap.min.css') }}"
          rel="stylesheet">
    <link href="{{ asset('public/spa-owner-dashboard/vendors/font-awesome/css/font-awesome.min.css') }}"
          rel="stylesheet">
    <link href="{{ asset('public/spa-owner-dashboard/vendors/nprogress/nprogress.css') }}" rel="stylesheet">
    <link href="{{ asset('public/spa-owner-dashboard/vendors/iCheck/skins/flat/green.css') }}" rel="stylesheet">
    <link href="{{ asset('public/spa-owner-dashboard/vendors/google-code-prettify/bin/prettify.min.css') }}"
          rel="stylesheet">
    <link href="{{ asset('public/admin/plugins/sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('public/spa-owner-dashboard/vendors/select2/dist/css/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('public/spa-owner-dashboard/vendors/switchery/dist/switchery.min.css') }}" rel="stylesheet">
    <link href="{{ asset('public/spa-owner-dashboard/vendors/starrr/dist/starrr.css') }}" rel="stylesheet">
    <link href="{{ asset('public/spa-owner-dashboard/vendors/bootstrap-daterangepicker/daterangepicker.css') }}"
          rel="stylesheet">
    <link href="{{ asset('public/admin/plugins/select2/dist/css/select2.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('public/admin/plugins/dropzone-master/dist/min/dropzone.min.css') }}" rel="stylesheet"/>
    <link href="{{ asset('public/spa-owner-dashboard/build/css/custom.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('public/admin/plugins/html5-editor/bootstrap-wysihtml5.css') }}"/>
    @yield('head-style')
</head>
<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <div class="navbar nav_title" style="border: 0;">
                    <a href="{{ route('spd.dashboard') }}" class="site_title"><i class="fa fa-paw"></i> <span>Balance Seek</span></a>
                </div>
                <div class="clearfix"></div>
                <br/>
                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                    @include('spa-owner-dashboard.sidebar.links')
                </div>
                <!-- /sidebar menu -->
            </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
            <div class="nav_menu">
                <nav>
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>

                    <ul class="nav navbar-nav navbar-right">
                        <li class="">
                            <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown"
                               aria-expanded="false">
                                @if(Auth::user()->first_name)
                                    {{ Auth::user()->first_name }} &nbsp; <span class=" fa fa-angle-down"></span>
                                @endif
                            </a>
                            <ul class="dropdown-menu dropdown-usermenu pull-right">
                                <li><a href="{{ route('spa.owner.account') }}"><i class="fa fa-user" aria-hidden="true"></i>
                                        Update Profile</a></li>
                                <li><a href="{{ route('spa.owner.logout') }}"><i class="fa fa-sign-out pull-right"></i>
                                        Log Out</a></li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
        <!-- /top navigation -->
        <!-- page content -->
        <div class="right_col" role="main">
            @yield('page-heading')
            @include('spa-owner-dashboard.notification.notification')
            @yield('content')
        </div>
        <!-- /page content -->
        <!-- footer content -->
        <footer>
            @yield('footer')
            <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
    </div>
</div>
<script src="{{ asset('public/spa-owner-dashboard/vendors/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('public/spa-owner-dashboard/vendors/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('public/spa-owner-dashboard/vendors/fastclick/lib/fastclick.js') }}"></script>
<script src="{{ asset('public/spa-owner-dashboard/vendors/nprogress/nprogress.js') }}"></script>
<script src="{{ asset('public/spa-owner-dashboard/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js') }}"></script>
<script src="{{ asset('public/spa-owner-dashboard/vendors/iCheck/icheck.min.js') }}"></script>
@yield('footer-script')
</body>
</html>
