@extends('layouts.app')
@section('content')

    <!-- discover-module section================================================== -->
    <section class="discover">
        <div class="container">
            <!--p class="discover__description">
                Find out perfect place to hangout in your city.
            </p>

            <h1 class="discover__title places-tab">
                Discover great places.
            </h1-->
            <ul class="discover__list">
                <li class="discover__list-item">
                    <a class="place active-list" href="#"><i class="la la-compass" aria-hidden="true"></i>Places</a>
                </li>
            </ul>
            <form id="discover_form" class="discover__form" action="{{ url('/search') }}"
            " method="get">
            <!--<input class="discover__form-input" type="text" name="place" id="place" placeholder="What are you looking for?" />-->
            <select class="discover__form-input js-example-basic-multiple" name="destination">
                <option value="">Select Location</option>
                <?php /* @foreach($searchLocations as $location)
                @if($location->parent == 0)
                <optgroup label="{{$location->name}}">
                @foreach($location->state()->get() as $state)
                @foreach($state->city()->get() as $city)
                <option value="{{$city->id}}">{{@$city->name}}({{ @$city->listings()->count() }})</option>
                @endforeach
                @endforeach
                </optgroup>
                @endif
                @endforeach */ ?>
                @foreach($searchLocations as $location)
                    <optgroup label="{{ @$location[0]->countryname }}">
                        @foreach($location as $city)
                            <option value="{{$city->id}}">{{@$city->name}}({{ @$city->total_listings }})</option>
                        @endforeach
                    </optgroup>
                @endforeach
            </select>
            <select class="discover__form-input js-example-basic-multiple" name="service">
                <option value="">Select Services</option>
                {{--<option value="">Select Services</option>--}}
                {{--<option value="1">Massage</option>--}}
                {{--<option value="2">Facial</option>--}}
                {{--<option value="7">Body Scrubs</option>--}}
                {{--<option value="8">Body Wraps</option>--}}
                {{--<option value="123">Pedicure</option>--}}
                {{--<option value="124">Manicure</option>--}}
                @foreach($serviceListingCount as $item)
                    <option value="{{ $item['ID'] }}">{{ $item['Name'] }} ({{ $item['Count'] }})</option>
                @endforeach
            </select>
            <button class="btn-default btn-default-red" type="submit"><i class="fa fa-search" aria-hidden="true"></i>
                Search
            </button>
            </form>
        </div>
    </section>
    <!-- End discover-module section -->

    <!-- top-experience-block================================================== -->
    <section class="top-experience">
        <div class="container">
            <!-- section-header module -->
            <div class="section-header">
                <h1 class="section-header__title white-style">
                    Top-rated Destination
                </h1>
                <p class="section-header__description">
                    Check out the highest rated Destinations on BalanceSeek
                </p>
            </div>
            <!-- end section-header module -->
            <div class="top-experience__box owl-wrapper">
                <div class="owl-carousel" data-num="3">
                    @if(@$destinations)
                        @foreach(@$destinations as $objDestination)
                            <div class="item">
                                <!-- place-gal module -->
                                <div class="place-gal">
                                    <?php
                                    $src = url("public/front/upload/top1.jpg");
                                    if (! empty(@$objDestination->image_url)) {
                                    $src = Storage::disk('azure')->url(@$objDestination->image_url);
                                    $file_headers = @get_headers($src);
                                    if (!$file_headers || strstr($file_headers[0], "HTTP/1.1 404")) {
                                    $src = url("public/front/upload/top1.jpg");
                                    }
                                    }
                                    ?>
                                    <a href="{{ url('search/destination/'.$objDestination->slug) }}"><img
                                                class="place-gal__image" src="{{ $src }}" alt=""/></a>
                                    <div class="place-gal__content">
                                        <h2 class="place-gal__title">
                                            <a href="{{ url('search/destination/'.$objDestination->slug) }}">
                                                <img src="{{ url("public/front/upload/".$objDestination->slug."-flag.png") }}"
                                                     alt=""/>
                                                {{ $objDestination->name }}
                                            </a>
                                        </h2>
                                        <ul class="place-gal__list">
                                            <li class="place-gal__list-item">
                                                <a href="javascript:void(0);">
                                                    <?php
                                                    $param['where'] = array("parent" => $objDestination->id);
                                                    $cityCount = \App\Destination::get_data($param)->count();
                                                    echo $cityCount . '  Cities';
                                                    ?>
                                                </a>
                                            </li>
                                            <li class="place-gal__list-item">
                                                <a href="{{ url('search/destination/'.$objDestination->slug) }}">{{ @$objDestination->listings()->count() }}
                                                    Listing</a>
                                            </li>
                                        </ul>
                                        <a class="btn-default"
                                           href="{{ url('search/destination/'.$objDestination->slug) }}">
                                            <i class="fa fa-caret-right" aria-hidden="true"></i>
                                            Explore
                                        </a>
                                    </div>
                                </div>
                                <!-- end place-gal module -->
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </section>
    <!-- End top-experience-block -->

    <!-- services-block================================================== -->
    <section class="services">
        <div class="container">
            <!-- section-header module -->
            <div class="section-header">
                <h1 class="section-header__title">
                    Find Your Services
                </h1>
                <p class="section-header__description"></p>
            </div>
            <!-- end section-header module -->
            <div class="services__box">
                <div class="row">
                    @if(@$services)
                        @foreach(@$services as $service)

                            @if($service->display_on_home == 1)
                                <div class="col-xl-2 col-md-4 col-sm-6">
                                    <!-- services-post2 module -->
                                    <?php
                                    if ($service->id == 123) {
                                        $href = url("/search/service/foot/" . $service->slug);
                                    } else if ($service->id == 124) {
                                        $href = url("/search/service/hands/" . $service->slug);
                                    } else {
                                        $href = url("/search/service/" . $service->slug);
                                    }
                                    ?>
                                    <div class="services-post">
                                        <div class="services-post__content">
                                            <!--a href="{{ $href }}"-->
                                                <i class="la la-check-circle" aria-hidden="true"></i>
                                                <h2 class="services-post__title">
                                                    {{ $service->name }}
                                                </h2>
                                                <p class="services-post__location">
                                                    @if($service->id == 123)
                                                        {{ $serviceListingCount[$service->id]['Count'] + $serviceListingCount[114]['Count'] }}
                                                        Locations
                                                    @elseif($service->id == 124)
                                                        {{ $serviceListingCount[$service->id]['Count'] + $serviceListingCount[111]['Count'] }}
                                                        Locations
                                                    @else
                                                        {{ $serviceListingCount[$service->id]['Count'] }} Locations
                                                    @endif
                                                </p>
                                            <!--/a-->
                                        </div>
                                    </div>
                                    <!-- end services-post2 module -->
                                </div>
                            @endif
                        @endforeach
                    @endif
                </div>
            </div>

        </div>
    </section>
    <!-- End services-block -->

    <!-- trending-places-block
            ================================================== -->
    <section class="trending-places">
        <div class="container">

            <!-- section-header module -->
            <div class="section-header">
                <h1 class="section-header__title">
                    Top Trending Places
                </h1>
                <p class="section-header__description"></p>
            </div>
            <!-- end section-header module -->

            <div class="trending-places__box owl-wrapper">
                <div class="owl-carousel" data-num="5">
                    @if(@$listings)
                        @foreach(@$listings as $spa)
                            <div class="item">
                                <!-- place-post module -->
                                <div class="place-post">
                                    <div class="place-post__gal-box">
                                        <?php
                                        $src = url("public/front/upload/1.jpg");
                                        if ( ! empty(@$spa->banner_image_url)) {
                                        $src = Storage::disk('azure')->url(@$spa->banner_image_url);
                                        $file_headers = @get_headers($src);
                                        if (!$file_headers || strstr($file_headers[0], "HTTP/1.1 404")) {
                                        $src = url("public/front/upload/1.jpg");
                                        }
                                        }
                                        ?>
                                        <a href="{{ url('spa/'.@$spa->slug) }}">
										<img class="place-post__image" src="{{ $src }}" alt=""/></a>
                                        <span class="place-post__rating">{{ @$spa->rating }}</span>
                                    </div>
                                    <div class="place-post__content">
                                        <h2 class="place-post__title">
                                            <a href="{{ url("spa/".@$spa->slug)}}">{{ @$spa->name}}</a>
                                        </h2>
                                        <p class="place-post__info">
                                            {{ @$spa->type_of_spa }}
											
											 <span class="view-count">
												<i class="la la-eye"></i>
												<span>{{ $spa->visitors()->sum('hits') }}</span>
											</span>											
                                        </p>
                                        <p class="place-post__address">
                                            <i class="fa fa-map-marker" aria-hidden="true"></i>
                                            {{ ($spa->area) ? $spa->area.", " :"" }}
                                            @if(!empty(@$spa->cityname))
                                                {{ @$spa->cityname.", " }}
                                            @endif
                                            @if(!empty(@$spa->countryname))
                                                {{ @$spa->countryname }}
                                            @endif
											
											
                                        </p>
                                       
                                    </div>
                                </div>
                                <!-- end place-post module -->
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
        <!--         <div class="center-button">
                    <a class="text-btn" href="{{ url("spa") }}">View All Places <span>({{ @$listingcount }})</span></a>
                </div> -->
        </div>
    </section>
    <!-- End trending-places-block -->

    <!-- banner-block
            ================================================== -->
    <section class="banner">
        <div class="container">
            <div class="row">
                <div class="col-md-8">

                    <!-- section-header module -->
                    <div class="section-header">
                        <h1 class="section-header__title white-style">
                            Add Your Business
                        </h1>
                        <p class="section-header__description white-style">
                            If you are looking for more customers then LIST YOUR SPA on BalanceSeek and get verified
                            leads and more walk in customers.
                        </p>
                    </div>
                    <!-- end section-header module -->

                </div>
                <div class="col-md-4">
                    <a class="btn-default" href="{{ url('/add-your-spa') }}">
                        <i class="fa fa-check-circle-o" aria-hidden="true"></i>
                        Register Now
                    </a>
                </div>
            </div>
        </div>
    </section>
    <!-- End banner-block -->

    <!-- statistic-block================================================== -->
    <section class="statistic">
        <div class="container">
            <div class="statistic__box">
                <div class="row">
                    <div class="col-lg-4">
                        <!-- statistic-post module -->
                        <div class="statistic-post">
                        <span class="statistic-post__icon statistic-post__icon-primary">
                            <i class="la la-hotel" aria-hidden="true"></i>
                        </span>
                            <div class="statistic-post__content">
                                <h1 class="statistic-post__title">
                                    <span class="timer" data-from="0"
                                          data-to="{{ @$listingcount }}">{{ @$listingcount }}</span> +
                                </h1>
                                <p class="statistic-post__description">
                                    Spa Listed
                                </p>
                            </div>
                        </div>
                        <!-- end statistic-post module -->
                    </div>
                    <div class="col-lg-4">
                        <!-- statistic-post module -->
                        <div class="statistic-post">
                        <span class="statistic-post__icon statistic-post__icon-primary">
                            <i class="la la-group" aria-hidden="true"></i>
                        </span>
                            <div class="statistic-post__content">
                                <h1 class="statistic-post__title">
                                    <span class="timer" data-from="0" data-to="9000">9000</span> +
                                </h1>
                                <p class="statistic-post__description">
                                    Website Visitors
                                </p>
                            </div>
                        </div>
                        <!-- end statistic-post module -->
                    </div>
                    <div class="col-lg-4">
                        <!-- statistic-post module -->
                        <div class="statistic-post">
                        <span class="statistic-post__icon statistic-post__icon-primary">
                            <i class="la la-map-marker" aria-hidden="true"></i>
                        </span>
                            <div class="statistic-post__content">
                                <h1 class="statistic-post__title">
                                    <span class="timer" data-from="0"
                                          data-to="{{ @$citycount }}">{{ @$citycount }}</span>
                                </h1>
                                <p class="statistic-post__description">
                                    Cities Listed
                                </p>
                            </div>
                        </div>
                        <!-- end statistic-post module -->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End statistic-block -->
@endsection

@section('footer')
    <script src="{{ asset('public/front/js/location.js') }}"></script>
@endsection


