@extends('layouts.front')
@section('title', @$center->name)

<!-- Meta Info Start-->
@section('meta_title', @$center->name)
<?php if (!empty(@$center->about_center)) { ?> 
    @section('description', strip_tags(@$center->about_center))
<?php } ?>
<?php if (!empty(@$center->keywords)) { ?> 
    @section('keywords', strip_tags(@$center->keywords))
<?php } ?>
<?php if (!empty(@$center->banner_image_url)) { ?> 
    @section('image', Storage::disk('azure')->url(@$center->banner_image_url))
<?php } ?>
<!-- Meta Info End -->

@section('head')
<link href="{{ asset('public/basicfront/css/slider-pro.min.css') }}" rel="stylesheet" />
@endsection    
@section('banner')
<section class="parallax-window" data-parallax="scroll" data-image-src="{{ Storage::disk('azure')->url(@$center->banner_image_url) }}" data-natural-width="1280" data-natural-height="780">
    <div class="parallax-content-2">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-sm-8">
                    <h1>{{ @$center->name }}</h1>
                    <span>{{ @$center->location }}</span>
                </div>
                <div class="col-md-4 col-sm-4 text-right">
                    @if(sizeof(@$imagegalleries)>0)  
                    <a href="javascript:void(0);" class="btn_1 view_image">View Images</a>
                    <div class="carousel magnific-gallery">
                        @foreach(@$imagegalleries as $gallery)
                        <div class="item hidden">
                            <a href="{{ Storage::disk('azure')->url($gallery->image_url) }}">
                                <img src="{{ Storage::disk('azure')->url($gallery->image_url) }}" alt="{{ $gallery->image_title }}" />
                            </a>
                        </div>
                        @endforeach           
                    </div>
                    @endif             
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End section -->
@endsection
@section('content')
<main>
    <div id="position">
        <div class="container">
            <ul>
                <li><a href="{{ url("/") }}">Home</a></li>
                <li><a href="#">Centers</a></li>
                <li>{{ @$center->name }}</li>
            </ul>
        </div>
    </div>
    <!-- End Position -->

    <div class="collapse" id="collapseMap">
        <div id="map" class="map">test</div>
    </div>
    <!-- End Map -->

    <div class="container margin_30">
        <div class="row">
            <div class="col-md-8" id="single_tour_desc">
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="nomargin_top">{{ @$center->name }}</h2>
                        <p class="text-italic"><i class="icon-location-2"></i>{{ @$center->address_of_center }}</p><br>
                    </div>
                </div>
                <?php
                if (@$center->about_center) {
                    ?>
                    <div class="row">
                        <div class="col-md-12">
                            <h3>About</h3>
                            {!! html_entity_decode(@$center->about_center) !!}
                        </div>
                    </div>
                    <hr>
                    <?php
                }
                if (@$center->center_type) {
                    ?>
                    <div class="row">
                        <div class="col-md-12">
                            <h3>Type of Center</h3>
                            <p><button class="btn_1 medium font18 bgcolor-btn-success">{{ @$center_type }}</button></p>
                        </div>
                    </div>
                    <hr>
                <?php } ?>

                <?php
                if (@$center->center_highlights) {
                    ?>
                    <div class="row">
                        <div class="col-md-12">
                            <h3>Highlights of {{ @$center->name }}</h3>
                            <ul class="list_ok">
                                <?php
                                foreach (explode(",", @$center->center_highlights) as $center_highlight) {
                                    ?>
                                    <li>{{ $center_highlight }}</li>
                                    <?php
                                }
                                ?>
                            </ul>
                        </div>
                    </div>
                    <hr>
                <?php } ?>

                <?php
                if (@$center->center_features) {
                    ?>
                    <div class="row">
                        <div class="col-md-12">
                            <h3>Features</h3>
                            <ul class="row list">
                                <?php
                                foreach (explode(",", @$center->center_features) as $center_feature) {
                                    ?>
                                    <li class="col-md-6">{{ $center_feature }}</li>
                                    <?php
                                }
                                ?>
                            </ul>
                        </div>
                    </div>
                    <hr>
                <?php } ?>

                @if ((!empty(@$center->accomodation_banner_image_url)) or (sizeof(@$accomodationimagegalleries) > 0) or (@$center->accomodation_overview))    
                <div class="row">
                    <div class="col-md-12">
                        <h3>Accommodation Overview</h3>
                        @if ((@$center->accomodation_banner_image_url) or (@$accomodationimagegalleries))
                        <div id="Img_carousel" class="slider-pro">
                            <div class="sp-slides">
                                @if(@$center->accomodation_banner_image_url)
                                <div class="sp-slide">
                                    <img alt="Image" class="sp-image" src="css/images/blank.gif" data-src="{{ Storage::disk('azure')->url(@$center->accomodation_banner_image_url) }}" />
                                </div>
                                @endif
                                @if(sizeof(@$accomodationimagegalleries)>0)
                                @foreach(@$accomodationimagegalleries as $accomodationimagegallery)
                                <div class="sp-slide">
                                    <img alt="Image" class="sp-image" src="css/images/blank.gif" data-src="{{ Storage::disk('azure')->url(@$accomodationimagegallery->image_url) }}" />
                                </div>
                                @endforeach
                                @endif                                                                    
                            </div>
                            <div class="sp-thumbnails">
                                @if(@$center->accomodation_banner_image_url)
                                <img alt="Image" class="sp-thumbnail" src="{{ Storage::disk('azure')->url(@$center->accomodation_banner_image_url) }}" />                                    
                                @endif
                                @if(sizeof(@$accomodationimagegalleries)>0)
                                @foreach(@$accomodationimagegalleries as $accomodationimagegallery)
                                <img alt="Image" class="sp-thumbnail" data-src="{{ Storage::disk('azure')->url(@$accomodationimagegallery->image_url) }}" />
                                @endforeach
                                @endif
                            </div>
                        </div>
                        @endif
                        <br />
                        {!! @$center->accomodation_overview !!}                            
                    </div>
                </div>
                <hr>
                @endif

                <?php if (@$center->how_to_get_there) { ?>
                    <div class="row">
                        <div class="col-md-12">
                            <h3>How to get there</h3>
                            {!! html_entity_decode(@$center->how_to_get_there) !!}
                        </div>
                    </div>
                    <hr>
                <?php } ?>

                <?php if (@$center->things_to_do_around_the_center) { ?>
                    <div class="row">
                        <div class="col-md-12">
                            <h3>Things to do around</h3>
                            <ul>
                                <?php
                                foreach (explode(",", @$center->things_to_do_around_the_center) as $things_to_do_around_the_center) {
                                    ?>
                                    <li>{{ @$things_to_do_around_the_center }}</li>
                                    <?php
                                }
                                ?>
                            </ul>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <!--End  single_tour_desc-->

            <aside class="col-md-4">
                <?php if (sizeof(@$center_specialities) > 0) { ?>
                    <div class="box_style_1 expose">
                        <h3 class="inner">- Center Speciality -</h3>
                        <div class="row">
                            <div class="col-md-12">
                                <ul class="list_ok">
                                    <?php
                                    foreach (@$center_specialities as $speciality) {
                                        ?>
                                        <li>{{ @$speciality->name }}</li>
                                        <?php
                                    }
                                    ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                <!--/box_style_1 -->

                <div class="box_style_1 expose">
                    <h3 class="inner">- Center Overviews -</h3>
                    <div class="row">
                        <div class="col-md-12">
                            <?php
                            if (@$center->year_of_foundation) {
                                ?>
                                Founded in {{ @$center->year_of_foundation }}
                                <?php
                            }
                            ?>
                            <?php if (@$center->certificate_id) { ?>
                                <h4>Certiﬁcations</h4>
                                <?php if (sizeof(@$certificates) > 0) { ?>
                                    <div class="row">
                                        <?php
                                        foreach (@$certificates as $certificate) {
                                            if (in_array($certificate->id, explode("||", $center->certificate_id))) {
                                                ?>
                                                <div class="col-md-4">
                                                    <img src="{{ Storage::disk('azure')->url(@$certificate->image_url) }}" alt="{{ $certificate->name }}" title="{{ $certificate->name }}" class="img-responsive" />
                                                </div>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </div>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                        <?php if (sizeof(@$experience_teachers) > 0) { ?>
                            <div class="col-md-12">
                                <hr>
                                <h4>Teachers</h4>
                                <?php
                                if (@$experience_teachers) {
                                    foreach (@$experience_teachers as $teacher) {
                                        ?>
                                        <div class="review_strip_single">
                                            <?php
                                            $src = url("/public/basicfront/img/teacher_thumb.jpg");
                                            if (@$teacher->profile_image_url) {
                                                $src = Storage::disk('azure')->url($teacher->profile_image_url);
                                            }
                                            ?>
                                            <img src="{{ $src }}" alt="" title="{{ $teacher->name }}" class="img-responsive img-circle"  style="width: 80px; height: 80px;" />
                                            <h4>
                                                {{ @$teacher->name }}
                                                <br><br>
                                                <?php
                                                if (@$teacher->certificate_id) {
                                                    if (sizeof(@$certificates) > 0) {
                                                        ?>
                                                        <small>
                                                            <?php
                                                            foreach (@$certificates as $certificate) {
                                                                if (in_array($certificate->id, explode("||", $teacher->certificate_id))) {
                                                                    ?>
                                                                    {{ $certificate->name.", " }}
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                        </small>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </h4>
                                            {{ @$teacher->short_description }}
                                            <p class="text-right"><a href="{{ url("teacher/".@$teacher->slug) }}" class="text-pink">View Complete Profile</a></p>
                                        </div>
                                        <?php
                                    }
                                }
                                ?>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </aside>
        </div>
        <!--End row -->

        @if(sizeof(@$center_experiences) > 0)
        <section>
            <div class="container">
                <div class="main_title">
                    <h2>Experiences Offered</h2>
                    <p><i>{{ $center->name }} offers the following</i> </p>
                </div>
                <div class="row add_bottom_45">
                    @foreach (@$center_experiences as $experience) 
                    @include('content-experience', (array)$experience)
                    @endforeach
                </div>
            </div>
            <!-- End container -->
        </section>
        <!-- End section -->
        @endif

    </div>
    <!--End container -->
    <div id="overlay"></div>
    <!-- Mask on input focus -->
</main>
<!-- End main -->

@endsection
@section('footer')
<script src="{{ asset('public/basicfront/js/center-detail.js') }}"></script>
<script src="{{ asset('public/basicfront/js/jquery.sliderPro.min.js') }}"></script>
<script type="text/javascript">
$(document).ready(function ($) {
    $('#Img_carousel').sliderPro({
        width: 960,
        height: 500,
        fade: true,
        arrows: true,
        buttons: false,
        fullScreen: true,
        smallSize: 500,
        startSlide: 0,
        mediumSize: 1000,
        largeSize: 3000,
        thumbnailArrows: true,
        autoplay: false
    });
});
</script>
@endsection