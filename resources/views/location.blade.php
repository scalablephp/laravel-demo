@extends('layouts.front')
@section('title', @$destination->name)
<!-- Meta Info Start-->
@section('meta_title', @$destination->name)
<?php if (!empty(@$destination->description)) { ?> 
    @section('description', @$destination->description)
<?php } ?>
<?php if (!empty(@$destination->keywords)) { ?> 
    @section('keywords', @$destination->keywords)
<?php } ?>
<?php if (!empty(@$destination->banner_image_url)) { ?> 
    @section('image', Storage::disk('azure')->url(@$destination->banner_image_url))
<?php } ?>
<?php
$qs = "";
if (!empty(request()->getQueryString())) {
    $qs = "?" . request()->getQueryString();
}
?>
<!-- Meta Info End -->
@section('banner')
<section id="hero" style="<?php if (!empty(@$destination->banner_image_url)) { ?> background-image: url('{{ Storage::disk('azure')->url(@$destination->banner_image_url) }}'); <?php } ?>">
    <div class="intro_title">
        <h3 class="animated fadeInDown">{{ @$destination->name }}</h3>
        <p class="animated fadeInDown"></p>
    </div>
    @include('layouts.searchbar')
    <!-- /search_bar-->
</section>
<!-- End hero -->
@endsection
@section('content')

@if(count($categories) > 0 && !empty($categories))
<section class="container padding_t_30 margin_b_30">
    <div class="main_title">
        <h2>Experiences Categories</h2>
    </div>
    <div class="row">
        <?php $i = 1; ?>  
        @foreach ($categories as $category)        
        <div class="col-md-4 col-sm-6 wow zoomIn" data-wow-delay="0.1s">
            <div class="tour_container category_hover">
                <div class="img_container">
                    <a href="{{ url()->current()."?category=".$category->slug.$qs }}">
                        @if($category->image_url)
                        <img src="{{ Storage::disk('azure')->url($category->image_url) }}" class="img-responsive" style="height:170px" alt="{{ $category->image_title }}">
                        @endif
                        <div class="short_info"> {{ $category->name }} </div>
                    </a>
                </div>
            </div>
            <!-- End box tour -->
        </div>
        <!-- End col-md-4 -->
        <?php $i++; ?>
        @endforeach
    </div>
    <!-- End row -->
</section>
<!-- End section -->
@endif
<section class="white_bg">
    <div class="container margin_60">
        <div class="main_title">
            <h2>Popular Experiences</h2>
            <p><i>Checkout some of the most popular experiences on BalanceBoat</i></p>
        </div>
        <div class="row  ">
            <div class="col-lg-12 col-md-12">
                {{-- Loop thru experiences --}}                            
                @foreach (@$experiences as $experience) 
                @include('content-experience', (array)$experience)
                @endforeach
                @if(count(@$experiences) > 100)
                <p class="text-right add_bottom_10">
                    <a href="{{ url("experiences")}}" class="button_intro">VIEW ALL EXPERIENCES >> </a>
                </p>
                @endif
                <!--End strip -->
            </div>
        </div>
        @if(count($subdestinations) > 1 && !empty($subdestinations))
        <div class="row">
            <div class="col-md-12">
                <div class="main_title">
                    <h2>{{ $destination->name }} Popular <span>Destinations</span></h2>
                    <p>{{ $destination->description }}</p>
                </div>
            </div>  
            @foreach ($subdestinations as $val_catesDestinations_top)
            <div class="col-md-3 col-sm-6 wow zoomIn" data-wow-delay="0.1s">
                <div class="tour_container tour_container1">
                    <div class="img_container">
                        <a href = "{{ url("/location/".$destination->slug."/".$val_catesDestinations_top->slug).$qs }}">
                            @if($val_catesDestinations_top->image_url)
                            <img src = "{{ Storage::disk('azure')->url($val_catesDestinations_top->image_url) }}" class = "img-responsive" alt = "{{ $val_catesDestinations_top->image_title }}" style = "max-height:173px;" />
                            @endif
                        </a>
                    </div>
                </div>
                <!--End box tour -->
                <h4 class = "text-center"><a href = "{{ url("/location/".$destination->slug."/".$val_catesDestinations_top->slug) }}">{{ $val_catesDestinations_top->name
                        }}</a></h4>
            </div>
            <!--End col-md-4 -->
            @endforeach
        </div>
        @endif
        <!--End row -->
    </div>
    <!--End container -->
</section>
<!--End section -->
@endsection