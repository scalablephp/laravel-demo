@extends('layouts.front')
@section('title')
Search Spa
@endsection
@section('content')
<div class="row">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1779.8278545103099!2d80.94270626612528!3d26.85090082974657!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x399bfd09d1fe94f7%3A0xb5776fe0ed3449d0!2sAura+Day+Spa!5e0!3m2!1sen!2sin!4v1546478844431" width="1670" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
    <div class="container">
        <a class="btn-light cntr"> <i class="fas fa-map-marker-alt"></i> Spa Near Me</a> <a class="btn btmrght">Map View</a>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-2 sidbr">
            <form action="{{ url("search") }}" id="frmSearch" method="get" role="search">
                {{ csrf_field() }}
                <div class="gndr">
                    <h3>Gender</h3>
                    <label>
                        <input type="radio" name="gender" value="" id="gender_0" <?php echo (@$gender == "") ? "checked" : ""; ?>>
                        Unisex
                    </label>
                    <br>
                    <label>
                        <input type="radio" name="gender" value="Male" id="gender_1" <?php echo (@$gender == "Male") ? "checked" : ""; ?>>
                        Male
                    </label>
                    <br>
                    <label>
                        <input type="radio" name="gender" value="Female" id="gender_2" <?php echo (@$gender == "Female") ? "checked" : ""; ?>>
                        Female
                    </label>
                </div>
                <div>
                    <h3>Type of Spa </h3>
                    <div class="">
                        @if(@$type_of_spas)
                        @foreach(@$type_of_spas as $objType)
                        <label>
                            <input type="checkbox" name="spatype" value="{{ $objType->id }}" id="spatype_0">
                            {{ $objType->name }}
                        </label>
                        <br>
                        @endforeach
                        @endif
                    </div>
                </div>

                <div>
                    <h3>Services</h3>
                    <div class="">
                        @if(@$services)
                        @foreach(@$services as $objService)
                        <label>
                            <input type="checkbox" name="services[]" value="{{ $objService->id }}" id="services_{{ $objService->id }}" />
                            {{ $objService->name }}
                        </label>
                        <br>
                        @endforeach
                        @endif
                    </div>
                </div>
            </form>
        </div>
        <div class="col-md-10">
            <div class="container">
                @foreach($spas as $spa)
                <div class="row revlst">
                    <div class="col-md-3"> 
                        <img src="{{ url("public/basicfront/images/slider1.jpg") }}" /> 
                    </div>
                    <div class="col-md-9">
                        <div class="clearfix"></div>
                        <div class="gogmap">
                            <a href="{{ url("spa/".$spa->slug)}}"> <h3>{{ $spa->name}}</h3></a>
                            <p>{{ $spa->address}}</p>
                            <div class="rtng"> <span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star"></span> <span class="fa fa-star"></span> </div>
                            <p>Deep Tissue, Swedish, Hot Stone, Back Massage, Aromatherapy, Hand/Foot, Couples, Thai, Balinese massage, Anti-aging, Waxing... </p>
                        </div>
                    </div>

                </div>
                @endforeach
                <nav aria-label="...">
                    <ul class="pagination">
                        {{$spas->appends(@$querystring)->links('vendor.pagination.bootstrap-4')}}
                    </ul>
                </nav> 
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="footer-top">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <h5> SEO</h5>
                <ul>
                    <li><a href="#">Seo</a></li>
                    <li><a href="#">Seo</a></li>
                    <li><a href="#">Seo</a></li>
                    <li><a href="#">Seo</a></li>
                </ul>
            </div>
            <div class="col-md-3">
                <h5> SEO</h5>
                <ul>
                    <li><a href="#">Seo</a></li>
                    <li><a href="#">Seo</a></li>
                    <li><a href="#">Seo</a></li>
                    <li><a href="#">Seo</a></li>
                </ul>
            </div>
            <div class="col-md-3">
                <h5> SEO</h5>
                <ul>
                    <li><a href="#">Seo</a></li>
                    <li><a href="#">Seo</a></li>
                    <li><a href="#">Seo</a></li>
                    <li><a href="#">Seo</a></li>
                </ul>
            </div>
            <div class="col-md-3">
                <h5> SEO</h5>
                <ul>
                    <li><a href="#">Seo</a></li>
                    <li><a href="#">Seo</a></li>
                    <li><a href="#">Seo</a></li>
                    <li><a href="#">Seo</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
@endsection 
@section('footer')
<script>
    $(document).ready(function () {
        $("#frmSearch input").on("change", function () {
            $("#frmSearch").submit();
        });
        $("#owl-tpcrsl").owlCarousel({
            autoPlay: 3000,
            singleItem: true,
            navigation: true
        });
    });

    $(document).ready(function () {
        $("#owl-menu").owlCarousel({
            autoPlay: 3000,
            navigation: true
        });
    });

    $(document).ready(function () {
        $("#owl-acmdn").owlCarousel({
            autoPlay: 3000,
            navigation: true
        });
    });

    $(document).ready(function () {
        $("#owl-food").owlCarousel({
            autoPlay: 3000,
            navigation: true
        });
    });


    $(document).ready(function () {
        $("#owl-span").owlCarousel({
            autoPlay: 3000,
            navigation: true
        });
    });
    $(document).ready(function () {
        $("#owl-spapop").owlCarousel({
            autoPlay: 3000,
            navigation: true
        });
    });
</script>
@endsection
