@extends('layouts.front')
@section('title', 'Terms and Conditions')
<!-- Meta Info Start-->
@section('meta_title', "Terms and Conditions")
@section('description', "Terms and conditions page for BalanceBoat  booking platform for yoga teacher training and ayurveda packages")
@section('keywords', "BalanceBoat, Booking platform, Yoga teacher Training booking website, Ayurveda packages booking, BalanceBoat Terms and conditions page, terms and conditions, T&C page")
<!-- Meta Info End -->
@section('content')
<main>
    <div id="position">
        <div class="container">
            <ul>
                <li><a href="{{ url("/") }}">Home</a></li>
                <li>Terms and Conditions</li>
            </ul>
        </div>
    </div>
    <!-- End Position -->

    <div class="collapse" id="collapseMap">
        <div id="map" class="map">test</div>
    </div>
    <!-- End Map -->

    <div class="container margin_60">
        <div class="row">
            <div class="col-md-12">
                <h2 class="text-center">Terms and Conditions</h2>
                <div class="row">
                    <div class="col-md-12">
                        <h3>Who operates this website?</h3>
                        <p>This website is operated and maintained by BalanceBoat Bookings Pvt. Ltd. a registered company in India. You can contact us by email (<a href="mailto:zen@balanceboat.com">zen@balanceboat.com</a>).</p>
                        <p>Bookings made through the platform are subject to the Terms and Conditions of BalanceBoat Bookings Pvt. Ltd. as well as the Terms and Conditions of the Supplier/Organizer of the experience.</p>
                        <h3>General</h3>
                        <p>The terms and conditions ("Terms and Conditions") are applicable to every use made of the platform as developed by BalanceBoat Booking Pvt. Ltd (to be referred as ‘BalanceBoat’ from here on). www.balaceboat.com is a website (to be referred as ‘platform’ from here on) where you can find, compare and book health, fitness and other travel experiences.</p>
                        <p>Use of platform after amendment/supplement of the Terms and conditions means unconditional agreement to the amended/supplemented Terms and conditions. If one does not wish to accept 
                            the amendments and/or supplements, one must cease using the platform.</p>
                        <p>BalanceBoat maintains the platform to the best of its ability. The use of the platform, it functionalities and other characteristics is per “as is” and “as available” at that given moment in time. BalanceBoat without being liable to you is entitled to make procedural and technical alterations and/or improvements to the platform at all times.</p>
                        <p>Each and every use of the Platform is for your own risk and responsibility.</p>
                        <h3>Platform & its usage</h3>
                        <p>
                            All offers published on the platform by organizer/organizers ("Organizers") are referred as experiences (“Experiences”), submit requests ("Requests") with the Organizers, make a reservation ("Reservation") for such Experience. All reservations made through the platform form a binding agreement between you and the Organizer and are subject to approval and change by the organizer. Requests and Inquiries are however not binding.
                        </p>
                        <p>
                            Experiences are subject to the Terms and Conditions of the Supplier/Organizer of the experience. We strongly advise you to carefully read the Organizer’s terms and conditions, cancellation policies since they entail legal obligations. BalanceBoat shall never become a party to an agreement between an Organizer and you. BalanceBoat does not accept any responsibility whatsoever for decisions made by you based on the content published on the platform unless stated otherwise in these Terms and conditions.
                        </p>
                        <p>
                            The successful execution of a reservation depends on correct data being provided in a timely manner, correct amount being deposited, acknowledgement from the organizer and the availability none of these are under the control of BalanceBoat, hence you and the Organizer are solely responsible of a successful reservation.
                        </p>
                        <p>
                            While using the platform the following are not permitted
                        <ul>
                            <li>Accessing the platform from a device that contains viruses, Trojan horses, worms, bots or other malicious software that can alter, damage, disable, infect or delete the platform or it basic elements or make it unavailable or inaccessible;</li>                                    
                            <li>Manual or automated "crawling" or scraping of any content on the Platform;</li>
                            <li>Circumventing security measures to technically limit the plaform</li>
                        </ul>
                        </p>
                        <h3>Payment and Prices</h3>
                        <p>There is no charge levied in order to access the platform. The fee for the experience and its updation is done by the organizers and BalanceBoat has no say hence does not bear any responsibility in this regard. Please take note that many a times the organizers would request only a deposit to block your seat the rest would be payable at the centre. Different organizers handle taxes differently in some cases it is inclusive and in some cases it is not.</p>
                        <p>The platform allows you to change the displayed currency on demand such converted prices are indicative only. At the time of booking the experience the price is converted in real time to the selected and supported currency.</p>
                        <p>Before making reservation payment please read the terms and conditions and the cancellation policy of the organizer. BalanceBoat has no influence over the terms and conditions and the cancellation policy set forth by the organizer.</p>
                        <h3>IP Rights</h3>
                        <p>Copyrights, Trade mark rights, Patent rights, Design rights, Trade name rights, Database rights, and rights due to affiliation, as well as rights to knowhow ("IP Rights"), are owned by BalanceBoat, its licensors or the Organizers of experience published on the platform. IP right are non transferable and require explicit permission for use. You are solely granted a right to use the platform if you act in accordance with the terms and conditions.</p>
                        <p>All content that you upload/share like comments, review and more you grant BalanceBoat a worldwide, royalty-free, non-exclusive, sub-licensable and transferable right to reproduce and make available on the platform, including the right to use (parts of) above mentioned content, for promotional purposes and other services in connection with the platform.</p>
                        <p>All the content that you share on platform or via email or via social media you represent and warrant that you have all rights to grant the licenses as mentioned in the above paragraph.</p>
                        <h3>Damages and Liability</h3>
                        <p>Liability attributed to default, unlawful act or any other ground shall not exceed the amount of Rs 5000 in a given year per event wherein a sequence of events is regarded at one event. </p>
                        <p>
                            Direct damages are defined and limited to 
                        <ul>
                            <li>Damages to property;</li>                                    
                            <li>Reasonable expenses incurred in order to prevent direct damages expect from an event and</li>
                            <li>reasonable expenses incurred in in determining the cause of the damage.</li>
                        </ul>
                        </p>
                        <p>BalanceBoat is excluded from partake of any liability for damages other than direct damage, including but not limited to indirect loss, consequential loss, loss and/or damage of data or content, loss of profit and loss of revenue, loss of savings, reduced goodwill, damage by business interruption and damage as a result of claims from third parties.</p>
                        <p>If the booking or reservation of the experience was not made via BalanceBoat or via the platform, BalanceBoat shares no liability about the booking or the entire experience.</p>
                        <h3>Warranties and indemnifications</h3>
                        <p>BalanceBoat is a booking platform that provides the technology and the interface to book experience, share reviews and interact with the organizers. Though BalanceBoat has Doctors and certified teachers in the team, BalanceBoat does not claim to be a medical professional, health advisor, doctor, fitness consultant or dietician and strongly suggests everyone to consult a medical professional when they deem fit.</p>
                        <p>BalanceBoat will make every effort possible to provide you assistance in case there is an issue with the booking made via the platform and in case of any dispute between you and the organizer of the experience, we don’t however influence the terms and conditions, booking policy and refund policy set forward by the organizer. The sole responsibility of the reservation and cancellation rests on you and the organizer of the selected experience. </p>
                        <h3>Questionable content</h3>
                        <p>BalanceBoat is not responsible for the content made available on the platform, it however is obliged to remove or block access to questionable/unlawful content after receiving a notification in writing or via email with sufficient proof. In case a person or organization considers content attached to their experience to be questionable/unlawful they can have it removed after providing a court order from a competent court in India.</p>
                        <p>We highly respect protect the privacy of those who report questionable/unlawful content All personal data received as part of the notification will always be processed in accordance with the applicable privacy legislation/law.</p>
                        <h3>Applicable law and competent court</h3>
                        <p>The Terms and conditions and the use of the platform are governed by Indian law. Any and all disputes arising from or related any agreement between parties will be brought before the competent court in India.</p>
                        <p>BalanceBoat reserves the right to change, amend or update any part of this terms and conditions at anytime with prior intimation, though we do send out an email to all registered members informing them of the changes, amendments or updates to the terms and conditions.</p>
                    </div>
                </div>
            </div>
        </div>
        <!--End row -->        
    </div>
    <!--End container -->
    <div id="overlay"></div>
    <!-- Mask on input focus -->
</main>
<!-- End section -->
@endsection
@section('footer')
@endsection