@extends('layouts.app')
@section('title', @$spa->meta_title)
<!-- Meta Info Start-->
@section('meta_title', @$spa->meta_title)
@if(!empty(@$spa->meta_description))
@section('description', strip_tags(@$spa->meta_description))
@endif
@if(!empty(@$spa->meta_keywords))
@section('keywords', strip_tags(@$spa->meta_keywords))
@endif
@if(!empty(@$spa->banner_image_url))
@section('image', Storage::disk('azure')->url(str_replace(' ', '%20',stripslashes(@$spa->banner_image_url))))
@endif
<!-- Meta Info End -->
@section('content')
<!-- listing-detail ================================================== -->
<section class="listing-detail" id="top">
    <div class="listing-detail__gal">
        <div class="listing-detail__gal-box">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-6 col-12">
                        <h1 class="listing-detail__title-black">{{ @$spa->name }}</h1>
                    </div>
                    <div class="col-md-6 col-12">
                        <div class="listing-detail__buttons">
                            <a class="btn-default btn-default-red navigate-btn" href="#tips-reviews-box">
                                <i class="la la-pencil" aria-hidden="true"></i>
                                Write a Review
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if(sizeof(@$imagegalleries)>0)
    <div class="listing-detail__gallery-inner owl-wrapper">
        <div class="owl-carousel" data-num="4">
            @foreach(@$imagegalleries as $gallery)
            <div class="item">
                <img src="{{ Storage::disk('azure')->url(stripslashes($gallery->image_url)) }}" alt="">
            </div>
            @endforeach
        </div>
    </div>
    @endif

    <div class="listing-detail__scroll-menu-box scroller-menu">
        <div class="container">
            <ul class="listing-detail__scroll-menu navigate-section">
                <li><a class="active" href="#top">Top</a></li>
                <li><a class="active" href="#about-box">About <span>{{ @$spa->name }}</span></a></li>
                @if(sizeof(@$parentServices)>0)
                <li><a href="#service-box">Services</a></li>
                @endif
                @if(sizeof(@$menuimagegalleries)>0 || sizeof(@$menus) > 0)
                <li><a href="#menu-box">Spa Menu</a></li>
                @endif
                <li><a href="#tips-reviews-box"><span>{{ @$spa->name }}</span> Reviews</a></li>
            </ul>
        </div>
    </div>

    <div class="listing-detail__content">
        <div class="container">
            <div class="row spa_detail">
                <div class="col-lg-8">
                    <div class="listing-detail__content-box">
                        <!--Spa Map Location-->
                        <div class="listing-detail__map">
                            <div class="listing-detail__location">
                                @if(!empty(@$spa->latitude) && !empty(@$spa->longitude))
                                {{--<div id="mapSingle" data-latitude="{{ @$spa->latitude }}"--}}
                                {{--data-longitude="{{ @$spa->longitude }}" data-map-icon="la la-cutlery"></div>--}}
                            <div id="map__Single"></div>
                            @endif
                        </div>
                    </div>

                    <!--Spa web connect-->
                    <div class="listing-detail__connect">
                        <ul class="sidebar__listing-list social__icons">
                            <li>
                                <a class='btn-default btn-default-red' href='tel:"{{ @$spa->mobile }}"' >{!! (!empty(@$spa->mobile)) ? "<i class='fa fa-phone'></i>" : "N/A" !!}</a>
                            </li>
                            <li>
                                @if(!empty(@$spa->email))
                                <button type="submit" data-path="{{url('spainquiry')}}"
                                        class="btn-default btn-default-red navigate-btn load-ajax-modal mobile-sticky"
                                        data-toggle="modal" data-target="#dynamic-modal" id="enquirypath">Send Inquiry
                                </button>
                                @endif
                            </li>
                            <li>
                                <a class='btn-default btn-default-red' href='{{ \App\Http\Helpers\CommonHelper::addhttp(@$spa->website) }} '>{!! 		(!empty(@$spa->website)) ?
                                    "WWW" : "N/A" !!}
                                </a>
                            </li>
                            <li id="distance__calculator">
                                <a class='btn-default' href="#">
                                    Navigate
                                </a>
                            </li>
                            <!--<p id="distance1" style="display:block"></p>-->
                        </ul>
                    </div>

                    <!-- Spa Locations -->		
                    <div class="listing-detail__locations">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-4">										
                                        <h2 class="listing-detail__content-title">Locations</h2>
                                    </div>
                                    <div class="col-md-8">												
                                               <strong id="distance"></strong>
                                        <p class="listing-detail__content-description">{{ @$spa->address }}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-4">										
                                        <h2 class="listing-detail__content-title">Opening Hours</h2>
                                    </div>
                                    <div class="col-md-8">		
                                        <ul class="opening-hours">
                                            <?php
                                            $opening_hours = (!empty(@$spa->opening_hours)) ? @unserialize(@$spa->opening_hours) : array();
                                            $open = "09:00";
                                            $close = "18:00";
                                            if (!empty(@$opening_hours)) {
                                                ?>
                                                @foreach(@$opening_hours->forWeek() as $key=>$opHrWeek)
                                                <?php
                                                if (!empty(@$opHrWeek)) {
                                                    $objOpen = explode("-", @$opHrWeek);
                                                    if (@$objOpen[0])
                                                        $open = @$objOpen[0];
                                                    if (@$objOpen[1])
                                                        $close = @$objOpen[1];
                                                }
                                                ?>
                                                <li>
                                                    <label>{{ ucfirst($key) }}</label>
                                                    <span>{{ $open }} - {{ $close }}</span>
                                                </li>
                                                @endforeach
                                            <?php } else { ?>
                                                <ul class="opening-hours">
                                                    <li>
                                                        <label>Monday</label>
                                                        <span>09:00 - 18:00</span>
                                                    </li>
                                                    <li>
                                                        <label>Tuesday</label>
                                                        <span>09:00 - 18:00</span>
                                                    </li>
                                                    <li>
                                                        <label>Wednesday</label>
                                                        <span>09:00 - 18:00</span>
                                                    </li>
                                                    <li>
                                                        <label>Thursday</label>
                                                        <span>09:00 - 18:00</span>
                                                    </li>
                                                    <li>
                                                        <label>Friday</label>
                                                        <span>09:00 - 18:00</span>
                                                    </li>
                                                    <li>
                                                        <label>Saturday</label>
                                                        <span>09:00 - 18:00</span>
                                                    </li>
                                                    <li>
                                                        <label>Sunday</label>
                                                        <span>Closed</span>
                                                    </li>
                                                </ul>
                                            <?php }
                                            ?>	
                                        </ul>
                                    </div>
                                </div>
                            </div>			 

                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-4">										
                                        <h2 class="listing-detail__content-title">Contact</h2>
                                    </div>
                                    <div class="col-md-8">		
                                        <ul class="opening-hours">
                                            <li>
                                                +91-8044802000, +91-9999835852, +91-8686130601
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- overview box -->
                    <div class="listing-detail__overview element-waypoint" id="about-box">
                        <h2 class="listing-detail__content-title heading-text">About {{ @$spa->name }}</h2>
                        <div class="listing-detail__content-description hide-content">{!! $spa->about_us !!}
                            <ul class="social__icons">
                                @if(@$spa->facebook_url)
                                <li>
                                    {!! (!empty(@$spa->facebook_url)) ? "<a href='".@$spa->facebook_url."'><i class='fa fa-facebook-official'></i>Facebook</a>" : "N/A" !!}
                                </li>
                                @endif
                                @if(@$spa->instagram_url)
                                <li>
                                    {!! (!empty(@$spa->instagram_url)) ? "<a href='".@$spa->instagram_url."'><i class='fa fa-instagram'></i>Instagam</a>" : "N/A" !!}
                                </li>
                                @endif
                            </ul>
                        </div>
                        <a href="javascript:void(0)" class="morelink">(-) View More</a>	
                    </div>



                </div>

                @if(!empty(@$spa->awards))
                <div class="element-waypoint">
                    <h2 class="listing-detail__content-title">
                        Awards
                    </h2>
                    <div class="listing-detail__content-description">
                        @foreach(@$awards as $objAward)
                        <p>{{ @$objAward->name }}</p>
                        @endforeach
                    </div>
                </div>
                @endif

                <div class="listing-detail__overview element-waypoint">
                    @if(@$spa->parking)
                    <div class="row">
                        <div class="col-md-2"><h2 class="listing-detail__content-title">Parking</h2>
                        </div>
                        <div class="col-md-10">
                            <p class="listing-detail__content-description">
                                {{ $spa->parking }}
                            </p>
                        </div>
                    </div>
                    @endif

                    @if(@$spa->spa_area)
                    <div class="row">
                        <div class="col-md-2"><h2 class="listing-detail__content-title">Area</h2></div>
                        <div class="col-md-10">
                            <p class="listing-detail__content-description">
                                {{ $spa->spa_area }}
                            </p>
                        </div>
                    </div>
                    @endif

                    @if(@$spa->gender)
                    <div class="row">
                        <div class="col-md-2"><h2 class="listing-detail__content-title">Gender</h2>
                        </div>
                        <div class="col-md-10">
                            <p class="listing-detail__content-description">
                                {{ $spa->gender }}
                            </p>
                        </div>
                    </div>
                    @endif
                </div>

                @if(@$spa->nearest_public_transport)
                <div class="listing-detail__overview element-waypoint">
                    <h2 class="listing-detail__content-title">Nearest Public Transport</h2>
                    <p class="listing-detail__content-description">
                        {!! $spa->nearest_public_transport !!}
                    </p>
                </div>
                @endif

                @if(@$spa->how_to_get_there)
                <div class="listing-detail__overview element-waypoint">
                    <h2 class="listing-detail__content-title">How to get here</h2>
                    <p class="listing-detail__content-description">
                        {!! $spa->how_to_get_there !!}
                    </p>
                </div>
                @endif

                @if(@$spa->accommodation)
                <div class="listing-detail__overview element-waypoint">
                    <h2 class="listing-detail__content-title">Accommodation Overview</h2>
                    <p class="listing-detail__content-description">
                        {!! $spa->accommodation !!}
                    </p>
                </div>
                @endif

                <!-- gallery-box -->
                @if(sizeof(@$accomodationimagegalleries)>0)
                <div class="listing-detail__gallery element-waypoint">
                    <h2 class="listing-detail__content-title">Accommodation Gallery</h2>
                    <div class="listing-detail__gallery-inner owl-wrapper">
                        <div class="owl-carousel" data-num="4">
                            @foreach(@$accomodationimagegalleries as $gallery)
                            <div class="item">
                                <img src="{{ Storage::disk('azure')->url($gallery->image_url) }}"
                                     alt=""/>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                @endif

                @if(@$spa->food)
                <div class="listing-detail__overview element-waypoint">
                    <h2 class="listing-detail__content-title">Food Detail</h2>
                    <p class="listing-detail__content-description">
                        {!! $spa->food !!}
                    </p>
                </div>
                @endif

                <!-- gallery-box -->
                @if(sizeof(@$foodimagegalleries)>0)
                <div class="listing-detail__gallery element-waypoint">
                    <h2 class="listing-detail__content-title">Food Gallery</h2>
                    <div class="listing-detail__gallery-inner owl-wrapper">
                        <div class="owl-carousel" data-num="4">
                            @foreach(@$foodimagegalleries as $gallery)
                            <div class="item">
                                <img src="{{ Storage::disk('azure')->url($gallery->image_url) }}"
                                     alt=""/>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                @endif

                @if(sizeof(@$parentServices)>0)
                <div class=" listing-detail__overview element-waypoint" id="service-box">
                    <h2 class="listing-detail__content-title heading-text">Services by {{ @$spa->name }}</h2>


                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        @foreach(@$parentServices as $parentService)
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion"
                                       href="#collapse{{ @$parentService->id }}"
                                       aria-expanded="true" aria-controls="collapseOne">
                                        <i class="more-less fa fa-plus"></i>
                                        {{ @$parentService->name }}
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse{{ @$parentService->id }}"
                                 class="panel-collapse collapse" role="tabpanel"
                                 aria-labelledby="headingOne">
                                <div class="panel-body">
                                    <div class="row">
                                        @foreach(@$services[$parentService->id] as $service)
                                        <div class="col-md-4">
                                            <p class="checklist">
                                                <i class="fa fa-check"></i> <?php
                                                echo @
                                                $service->name;
                                                ?>
                                            </p>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div><!-- panel-group -->
                </div>
                @endif

                @if(sizeof(@$menuimagegalleries)>0 || sizeof(@$menus) > 0)
                <div id="menu-box" class="listing-detail__gallery element-waypoint">
                    <h2 class="listing-detail__content-title heading-text">{{ @$spa->name }} Menu</h2>
                    <div class="listing-detail__content-description">
                        @if(sizeof(@$menuimagegalleries)>0)
                        <div class="listing-detail__gallery-inner owl-wrapper">
                            <div class="owl-carousel" data-num="4">
                                @foreach(@$menuimagegalleries as $gallery)
                                <div class="item">
                                    <img src="{{ Storage::disk('azure')->url($gallery->image_url) }}"
                                         alt="">
                                </div>
                                @endforeach
                            </div>
                        </div>
                        <br/>
                        @endif
                    </div>
                </div>

                @if(sizeof(@$menus) > 0)
                <div class="listing-detail__overview element-waypoint treatment hide-content">
                    @foreach(@$menus as $classification=>$cmenu)
                    <h2 class="listing-detail__content-title">{{ $classification }}</h2>
                    @foreach(@$cmenu as $menu)
                    <div class="row">
                        <div class="col-md-8 col-7">
                            <span class="listing-detail__content-title">{{ @$menu->menu_title }}</span>
                            <div class="listing-detail__content-description">
                                {!! @$menu->menu_description !!}
                            </div>
                        </div>
                        <div class="col-md-4 col-5" align="right">
                            <span class="menu-price">{{ @$menu->menu_price }}</span>
                            <?php /* {{ \App\Http\Helpers\CommonHelper::get_currency_rate(@$menu->menu_price, $spa->currency) }} */ ?>
                            @if(@$menu->duration > 0)
                            &nbsp;({{ @$menu->duration }} mins.)
                            @endif
                        </div>
                    </div>
                    <hr class="hr" style="margin: 0.5rem 0;"/>
                    @endforeach
                    <br/>
                    @endforeach
                    <div class="text-info"><strong>Note:</strong> Prices are subject to 21%
                        government tax and service charge
                    </div>
                </div>
                <a href="javascript:void(0)" class="morelink">(-) View More</a>
                @endif
                @endif

                <?php
                /* $videos = (!empty(@$spa->video)) ? explode("||", @$spa->video) : array();
                  if (sizeof($videos) > 0) {
                  ?>
                  <div class="listing-detail__overview element-waypoint">
                  <h2 class="listing-detail__content-title">Videos</h2>
                  <div class="listing-detail__content-description">
                  <div class="row">
                  <?php
                  foreach ($videos as $video) {
                  if (!empty($video)) {
                  ?>
                  <div class="col-md-4">

                  <iframe width="100%" height="auto" src="{{ \App\Http\Helpers\CommonHelper::addhttp($video) }}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                  </div>
                  <?php
                  }
                  }
                  ?>
                  </div>
                  </div>
                  </div>
                  <?php
                  } */
                ?>


            </div>

            <div class="col-lg-4">
                <div class="sidebar">

                    <div class="sidebar__widget sidebar__widget-listing-details">
                        <h2 class="sidebar__widget-title heading-text">
                            Spas near By
                        </h2>

                    <div id="spa_nearby">
                        

                    </div>


                        <div class="explore__advertise">
                            <span class="explore__advertise-title">
                                advertising box
                            </span>
                            <img src="http://nunforest.com/triptip-demo/upload/add2.jpg" alt="">
                        </div>

                        <div class="explore__advertise">
                            <span class="explore__advertise-title">
                                advertising box
                            </span>
                            <img src="http://nunforest.com/triptip-demo/upload/add.jpg" alt="">
                        </div>

                    </div>

                </div>
            </div>

            <div class="col-md-12">
                <!-- tips & reviews-box -->
                <div class="listing-detail__reviews element-waypoint" id="tips-reviews-box">
                    <h2 class="listing-detail__content-title heading-text">
                        {{ @$spa->name }} Reviews

                        <a href="#tips-reviews-box" class="btn-default btn-default-red navigate-btn">Write a
                            Review</a>
                    </h2>
                    <div class="listing-detail__reviews-box">
                        @if(sizeof(@$comments)>0)

                        <!-- reviews list -->
                        <ul class="reviews-list">
                            @foreach(@$comments as $comment)
                            <li class="reviews-list__item">
                                <div class="reviews-list__item-box">
                                    <div class="reviews-list__item-content">
                                        <h3 class="reviews-list__item-title">
                                            {{ @$comment->comment_author }}
                                        </h3>
                                        <p class="reviews-list__item-date">
                                            Posted {{ \Carbon\Carbon::parse(@$comment->comment_date)->format("F d, Y") }}
                                            <span class="reviews-list__item-rating solid-rat">{{ @$comment->rating }}</span>
                                        </p>
                                        <p class="reviews-list__item-description">
                                            {{ @$comment->comment_content }}
                                        </p>
                                        <a class="reviews-list__item-reply" href="#">
                                            <i class="la la-mail-forward"></i>
                                            Reply
                                        </a>
                                    </div>
                                </div>
                            </li>
                            @endforeach

                        </ul>
                        <!-- reviews-list -->
                        @endif

                        <!-- Contact form module -->
                        <form class="add-comment custom-form contact-form" id="frmComment" method="post">
                            <input type="hidden" name="listing_id" id="listing_id" value="{{ @$spa->id }}"/>
                            {{ csrf_field() }}
                            <h2 class="contact-form__title">
                                Rate us and Write a Review
                            </h2>
                            <div class="row">
                                <div class="col-md-6">
                                    <p class="contact-form__rate">
                                        Your rating for this listing:
                                    </p>
                                    <p class="contact-form__rate-bx">
                                        <i class="la la-star"></i>
                                        <i class="la la-star"></i>
                                        <i class="la la-star"></i>
                                        <i class="la la-star"></i>
                                        <i class="la la-star"></i>
                                    </p>
                                    <p class="contact-form__rate-bx-show">
                                        <span class="rate-actual">0</span> / 5
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <input class="contact-form__input-text required" type="text" name="name"
                                           id="name" placeholder="Name:"/>
                                </div>
                                <div class="col-md-6">
                                    <input class="contact-form__input-text required email" type="email"
                                           name="email" id="email" placeholder="Email:"/>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <input class="contact-form__input-text" type="text" name="city"
                                           id="city" placeholder="City:"/>
                                </div>
                                <div class="col-md-6">
                                    <input class="contact-form__input-text" type="text"
                                           name="country" id="country" placeholder="Country:"/>
                                </div>
                            </div>

                            <textarea class="contact-form__textarea" name="comment" id="comment"
                                      placeholder="Your Review"></textarea>
                            <div class="row">
                                <div class="col-md-12"></div>
                                <div class="form-group col-md-4">
                                    <label for="ReCaptcha">Recaptcha:</label>
                                    {!! NoCaptcha::renderJs() !!}
                                    {!! NoCaptcha::display() !!}

                                </div>
                            </div>
                            <input class="contact-form__submit" type="submit" name="submit-contact"
                                   id="submit_contact" value="Submit Review"/>
                        </form>

                        <!-- End Contact form module -->

                    </div>

                </div>
            </div>

        </div>
    </div>
    <div class="modal fade" id="dynamic-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body"></div>
            </div>
        </div>
    </div>
</section>
<!-- End listing-detail -->
@endsection
@section('footer')
<script type="text/javascript" src="{{ asset('public/front/js/jquery.validate.min.js') }}"></script>
<script type="text/javascript">
$(document).ready(function () {
    $("#frmComment").validate();
    $("#frmComment").on("submit", function () {
        if ($("#frmComment").valid()) {
            $.ajax({
                url: APP_URL + "/savecomment",
                method: "POST",
                data: $("#frmComment").serialize() + "&rating=" + ($(".la.la-star.selected").length),
                success: function (response) {
                    if (response) {
                        $("#submit_contact").after(' <div class="alert alert-danger" align="left"><br />' + response + '</div>');
                        setTimeout(function () {
                            $('.alert-danger').fadeOut();
                        }, 1500);
                    } else {
                        $("#frmComment")[0].reset();
                        $("#submit_contact").after('<div class="text-success" align="left"><br />Comment has been saved successfully!</div>');
                    }
                }
            });
        }
        return false;
    });
    function toggleIcon(e) {
        $(e.target).prev('.panel-heading').find(".more-less").toggleClass('fa-plus fa-minus');
    }

    $('.panel-group').on('hidden.bs.collapse', toggleIcon);
    $('.panel-group').on('shown.bs.collapse', toggleIcon);
});</script>
@if(!empty(@$spa->latitude) && !empty(@$spa->longitude))
<script src="https://unpkg.com/leaflet@1.0.1/dist/leaflet.js"></script>
<link href="https://unpkg.com/leaflet@1.0.1/dist/leaflet.css" rel="stylesheet"/>
<script type="text/javascript">
// Where you want to render the map.
var element = document.getElementById('map__Single');
// Height has to be set. You can do this in CSS too.
element.style = 'height:300px;';
// Create Leaflet map on map element.
var map = L.map(element);
// Add OSM tile leayer to the Leaflet map.
L.tileLayer('https://{s}.tile.osm.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);
// Target's GPS coordinates.
var target = L.latLng('{{$spa->latitude}}', '{{$spa->longitude}}');
// Set map's center to target with zoom 14.
map.setView(target, 14);
// Place a marker on the same location.
L.marker(target).addTo(map);
</script>
<script type="text/javascript">
    var options = {
        enableHighAccuracy: true,
        timeout: 5000,
        maximumAge: 0
    };
    function getLocation() {
        getnearbySpa('28.493032399999997', '77.0938803');
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition, error, options);
        } else {
            console.log("Geolocation is not supported by this browser.");
        }
    }

    function showPosition(position) {
        var markerFrom = L.circleMarker([position.coords.latitude, position.coords.longitude], {
            color: "#F00",
            radius: 10
        });
        var markerTo = L.circleMarker([<?php echo $spa->latitude; ?>, <?php echo $spa->longitude; ?>], {
            color: "#4AFF00",
            radius: 10
        });
        var from = markerFrom.getLatLng();
        var to = markerTo.getLatLng();
        markerFrom.bindPopup('Delhi ' + (from).toString());
        markerTo.bindPopup('Mumbai ' + (to).toString());
        map.addLayer(markerTo);
        map.addLayer(markerFrom);
        getDistance(from, to);
        getnearbySpa(from, to);
    }

    function error(err) {
        console.warn(err.message);
    }

   function getDistance(from, to) {
        var container = document.getElementById('distance');
         container.innerHTML = ("Distance From Current Location - " + (from.distanceTo(to)).toFixed(0) / 1000) + ' km';
    } 

    function getnearbySpa(from, to) {
        $.ajax({
            type: 'GET',
            url: '/nearbyspa/'+to+'/'+from,
            success: function (result) {
                 // alert(result);return false;
                $('#spa_nearby').html(result);
            }
        });

    }
    

    getLocation();
    $("#distance__calculator").click(function () {
        $("#distance").slideToggle();
        return false;
    });
    $('.load-ajax-modal').click(function () {
        $.ajax({
            type: 'GET',
            url: $('#enquirypath').data('path'),
            success: function (result) {
                //  alert(result);
                $('#dynamic-modal div.modal-body').html(result);
            }
        });
    });

</script>
@endif
@endsection
