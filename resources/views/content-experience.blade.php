<div class="strip_all_tour_list wow fadeIn" data-wow-delay="0.1s">
    <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-4">
            <div class="img_list">
                <a href="{{ url("/experience/".$experience->slug) }}">
                    <img data-src="{{ Storage::disk('azure')->url($experience->banner_image_url) }}" alt="{{ $experience->banner_image_title }}" class="img-responsive lazy"> 
                </a>
            </div>
        </div>
        <div class="clearfix visible-xs-block"></div>
        <div class="col-lg-6 col-md-6 col-sm-6">
            <div class="tour_list_desc">
                <a href="{{ url("/experience/".$experience->slug) }}"><h3>{{ $experience->name }}</h3></a>
                <h5 class="hidden-xs hidden-sm">
                    <i class="fa fa-calender"></i> 
                    @if(@$experience->recurring_type == "Daily")
                    Available all year round
                    @else
                    {{ @$experience->available_month }}
                    @endif
                </h5>
                <?php if (@$experience->experience_summary) { ?>
                    <p>
                    <ul class="list_ok hidden-xs hidden-sm">
                        <?php
                        $lmt = 0;
                        foreach (explode(",", @$experience->experience_summary) as $experience_summary) {
                            if ($lmt < 4) {
                                ?>
                                <li>{{ @$experience_summary }}</li>
                                <?php
                            }
                            $lmt++;
                        }
                        ?>
                    </ul>
                    </p>
                <?php } ?>
            </div>
        </div>
        <div class="col-lg-2 col-md-2 col-sm-2">
            <div class="price_list">
                    <small class="hidden-xs hidden-sm">From:</small>
                    <span class="hidden-xs hidden-sm">
                        <?php
                        $discount = 0;
                        $pay = @$experience->room_price;
                        if ((!empty(@$experience->eirly_bird_before_days)) && (!empty(@$experience->eirly_bird_discount)) && (@$experience->eirly_bird_discount > 0)) {
                            if (@$experience->eirly_bird_discount_type == "amt") {
                                $discount += @$experience->eirly_bird_discount;
                            } else {
                                $discount = (@$experience->room_price * @$experience->eirly_bird_discount) / 100;
                            }
                        }

                        if ((!empty(@$experience->offer_start_date)) && (!empty(@$experience->offer_discount)) && (@$experience->offer_discount > 0)) {
                            $now = \Carbon\Carbon::parse(date("Y-m-d"))->format("Y-m-d");
                            if ((\Carbon\Carbon::parse(@$experience->offer_start_date)->format("Y-m-d") <= $now) && (\Carbon\Carbon::parse(@$experience->offer_end_date)->format("Y-m-d") >= $now)) {
                                if (@$experience->offer_discount_type == "amt") {
                                    $discount += @$experience->offer_discount;
                                } else {
                                    $discount += (@$experience->room_price * @$experience->offer_discount) / 100;
                                }
                            }
                        }


                        if (!empty(@$discount)) {
                            ?>
                            <del class="text-default">
                                {{ \App\Http\Helpers\CommonHelper::get_currency_rate((@$pay), @$experience->accomodation_currency) }}
                            </del>
                            <?php
                        }
                        ?>

                        {{ \App\Http\Helpers\CommonHelper::get_currency_rate(@$pay - $discount, @$experience->accomodation_currency) }}
                    </span>
                    <?php
                    if (!empty(@$experience->duration)) {
                        ?>
                        <p class="hidden-xs hidden-sm"><br /></p>
                        <small class="hidden-xs hidden-sm">For:</small>
                        <span class="hidden-xs hidden-sm text-days">
                            <?php
                            echo @$experience->duration;
                            ?>
                        </span>
                        <p class="hidden-xs hidden-sm"><br /></p>
                        <?php
                    }
                    ?>
                    <a href="{{ url("/experience/".$experience->slug) }}" class="btn btn-pink m-5">Check details</a>
                
            </div>
            @if (!empty(@$discount)) 
            <div class="badge_save">Save<strong>{{ round((@$discount/@$pay) * 100) }}%</strong></div>
            @endif
        </div>
    </div>
</div>