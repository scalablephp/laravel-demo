@extends('layouts.front')
@section('title', @$experience->name)

<!-- Meta Info Start-->
@section('meta_title', @$experience->name)
<?php if (!empty(@$experience->experience_overview)) { ?> 
    @section('description', strip_tags(@$experience->experience_overview))
<?php } ?>
<?php if (!empty(@$experience->keywords)) { ?> 
    @section('keywords', strip_tags(@$experience->keywords))
<?php } ?>
<?php if (!empty(@$experience->banner_image_url)) { ?> 
    @section('image', Storage::disk('azure')->url(@$experience->banner_image_url))
<?php } ?>
<!-- Meta Info End -->

@section('head')
<link href="{{ asset('public/basicfront/css/owl.carousel.css') }}" rel="stylesheet">
<link href="{{ asset('public/basicfront/css/owl.theme.css') }}" rel="stylesheet">
<link href="{{ asset('public/basicfront/css/slider-pro.min.css') }}" rel="stylesheet" />
@endsection    
@section('banner')
<section class="parallax-window" data-parallax="scroll" data-image-src="{{ Storage::disk('azure')->url(@$experience->banner_image_url) }}" data-natural-width="1280" data-natural-height="780">
    <div class="parallax-content-2">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-sm-8">
                    <h1>{{ @$experience->name }}</h1>
                    <span>{{ @$experience->location }}</span>
                </div>
                <div class="col-md-4 col-sm-4 text-right">
                    @if(sizeof(@$imagegalleries)>0) 
                    <a href="javascript:void(0);" class="btn_1 view_image">View Images</a>
                    <div class="carousel magnific-gallery">                                               
                        @foreach(@$imagegalleries as $gallery)
                        <div class="item hidden">
                            <a href="{{ Storage::disk('azure')->url($gallery->image_url) }}">
                                <img src="{{ Storage::disk('azure')->url($gallery->image_url) }}" alt="{{ $gallery->image_title }}" />
                            </a>
                        </div>
                        @endforeach            
                    </div>
                    @endif            
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End section -->
@endsection
@section('content')

<div id="position">
    <div class="container">
        <ul>
            <li><a href="{{ url("/") }}">Home</a></li>
            <li><a href="{{ url("/experiences") }}">Experiences</a></li>
            <li>{{ @$experience->name }}</li>
        </ul>
    </div>
</div>
<!-- End Position -->

<div class="container margin_30">
    <div class="row">
        <div class="col-md-8" id="single_tour_desc">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="nomargin_top">{{ @$experience->name }}</h2>
                    <?php
                    if (@$center->address_of_center) {
                        ?>
                        <p class="text-italic"><i class="icon-location-2"></i>{{ @$center->address_of_center }}</p>
                        <?php
                    }
                    ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h3>Experience Schedule</h3>
                    @if (\Carbon\Carbon::parse(@$experience->start_date_time)->subDays(2) >=  \Carbon\Carbon::now())
                        {{ (@$experience->start_date_time) ? \Carbon\Carbon::parse(@$experience->start_date_time)->format("d M") : "" }}
                        {{ (@$experience->end_date_time) ? " - ".\Carbon\Carbon::parse(@$experience->end_date_time)->format("d M Y") : "" }}
                         | 
                    @endif
                    @if (sizeof(@$experience_recurring_manually) > 0)
                    @foreach($experience_recurring_manually as $exp_recurr)
                        {{ (@$exp_recurr->start_date) ? \Carbon\Carbon::parse(@$exp_recurr->start_date)->format("d M") : "" }}
                        {{ (@$exp_recurr->end_date) ? " - ".\Carbon\Carbon::parse(@$exp_recurr->end_date)->format("d M Y") : "" }}
                         | 
                    @endforeach
                    @endif
                </div>
            </div>
            <hr>
            <?php
            if (@$experience->experience_overview) {
                ?>
                <div class="row">
                    <div class="col-md-12">
                        <h3>Experience Overview</h3>
                        {!! html_entity_decode(@$experience->experience_overview) !!}
                    </div>
                </div>
                <hr>
                <?php
            }
            if (@$experience->experience_highlights) {
                ?>
                <div class="row">
                    <div class="col-md-12">
                        <h3>Experience Highlights</h3>
                        <ul class="list_ok cls_new_list">
                            <?php
                            foreach (explode(",", @$experience->experience_highlights) as $experience_highlight) {
                                ?>
                                <li>{{ $experience_highlight }}</li>
                                <?php
                            }
                            ?>
                        </ul>
                    </div>

                </div>
                <hr>
            <?php } ?>
            <?php if (@$experience->batch_size) { ?>
                <div class="row">
                    <div class="col-md-12">
                        <h3>Group Size</h3>
                        <p>The maximum size of the group is {{ (@$experience->batch_size) ?: "NA" }}</p>
                    </div>
                </div>
                <hr>
            <?php } ?>
            <?php if (@$experience->styles_taught) { ?>
                <div class="row">
                    <div class="col-md-12">
                        <h3>Style</h3>
                        <div class="tags">
                            <?php
                            foreach (explode(",", @$experience->styles_taught) as $styles_taught) {
                                ?>
                                <span class="medium font18">{{ $styles_taught }}</span>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>
                <hr>
            <?php } ?>
            <?php if (@$experience->language_spoken) { ?>
                <div class="row">
                    <div class="col-md-12">
                        <h3>Language</h3>
                        <div class="tags">
                            <?php
                            foreach (explode("||", @$experience->language_spoken) as $language) {
                                ?>
                                <span class="medium font18">{{ @$language }}</span>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>
                <hr>
            <?php } ?>
            <?php if (@$experience->schedule) { ?>
                <div class="row">
                    <div class="col-md-12">
                        <h3>Schedule</h3>
                        {!! html_entity_decode(@$experience->schedule) !!}
                    </div>
                </div>
                <hr>
            <?php } ?>
            <?php if (@$center) { ?>
                <div class="row">
                    <div class="col-md-12">
                        <h3>About the center</h3>
                        <h4>{{ @$center->name }}</h4>
                        <?php if (@$center->year_of_foundation) { ?>
                            <p>Founded in {{ (@$center->year_of_foundation) ?: "N/A" }}</p>
                        <?php } ?>
                        {!! @$center->about_center !!}
                        <p><a href="{{ url("center/".@$center->slug) }}" class="text-pink">Visit Center’s Profile</a></p>
                    </div>
                </div>
                <hr>
                <?php if (@$center->accomodation_overview) { ?>    
                    <div class="row">
                        <div class="col-md-12">
                            <h3>Accommodation Overview</h3>
                            @if ((@$center->accomodation_banner_image_url) or (sizeof(@$accomodationimagegalleries)>0))
                            <div id="Img_carousel" class="slider-pro">
                                <div class="sp-slides">
                                    @if(@$center->accomodation_banner_image_url)
                                    <div class="sp-slide">
                                        <img alt="Image" class="sp-image" src="css/images/blank.gif" data-src="{{ Storage::disk('azure')->url(@$center->accomodation_banner_image_url) }}" />
                                    </div>
                                    @endif
                                    @if(@$accomodationimagegalleries)
                                    @foreach(@$accomodationimagegalleries as $accomodationimagegallery)
                                    <div class="sp-slide">
                                        <img alt="Image" class="sp-image" src="css/images/blank.gif" data-src="{{ Storage::disk('azure')->url(@$accomodationimagegallery->image_url) }}" />
                                    </div>
                                    @endforeach
                                    @endif                                                                    
                                </div>
                                <div class="sp-thumbnails">
                                    @if(@$center->accomodation_banner_image_url)
                                    <img alt="Image" class="sp-thumbnail" src="{{ Storage::disk('azure')->url(@$center->accomodation_banner_image_url) }}" />                                    
                                    @endif
                                    @if(@$accomodationimagegalleries)
                                    @foreach(@$accomodationimagegalleries as $accomodationimagegallery)
                                    <img alt="Image" class="sp-thumbnail" data-src="{{ Storage::disk('azure')->url(@$accomodationimagegallery->image_url) }}" />
                                    @endforeach
                                    @endif
                                </div>
                            </div>
                            <br />
                            @endif
                            {!! @$center->accomodation_overview !!}                            
                        </div>
                    </div>
                    <hr>
                <?php } ?>
            <?php } ?>
            <div class="row hidden">
                <div class="col-md-12">
                    <h3>Feature</h3>
                </div>
                <div class="col-md-4">
                    <h4>Activities</h4>
                    <ul>
                        <li>Gym</li>
                        <li>Hiking</li>
                        <li>Horsr riding</li>
                        <li>Hot tub / jacuzzi</li>
                        <li>Massage</li>
                        <li>Pilates</li>
                        <li>Raiki</li>
                        <li>Surfing</li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <h4>General</h4>
                    <ul>
                        <li>Balcony</li>
                        <li>Beach</li>
                        <li>Dining Area</li>
                        <li>Garden</li>
                        <li>Smoke-free poperty</li>
                        <li>Special menu request</li>
                        <li>Yoga shala</li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <h4>Services</h4>
                    <ul>
                        <li>Board rental</li>
                        <li>Free Wifi</li>
                        <li>Room Cleaning</li>
                        <li>Wirelesh internet</li>
                    </ul>
                </div>
            </div>
            <div class="row hidden">
                <div class="col-md-12">
                    <h3>Location Overview</h3>
                    <p>With an extensive beach along the cliffs, Praia da Aguda is wild and beautiful the access is for the adventurous ones but beauty and tranquility awaits at the end of the stairs. Along the coast, two kilometers each way, you will ﬁnd Magoito beach and Azenhas do Mar, a beautiful and awe-inspiring village perched on the cliffs. This location is ideal for long walks of exploration and close contact with nature´s beauty.</p>
                    <h5>Nearby places</h5>
                    <p>Azenhas do Mar - 15 minutes' walk </p>
                    <p>The beach - 10 minutes' walk</p>
                    <p>Sintra - 10 minutes' drive</p>
                </div>
            </div>
            <?php if (@$center->how_to_get_there) { ?>
                <div class="row">
                    <div class="col-md-12">
                        <h3>How to get there</h3>
                        {!! html_entity_decode(@$center->how_to_get_there) !!}
                    </div>
                </div>
                <hr>
            <?php } ?>                
            <?php if (@$experience->food_overview) { ?>
                <div class="row">
                    <div class="col-md-12">
                        <h3>Food Overview</h3>
                        @if ((!empty(@$experience->food_banner_image_url)) or (sizeof(@$foodimagegalleries) > 0) )
                        <div id="Img_carousel" class="slider-pro">
                            <div class="sp-slides">
                                @if(@$experience->food_banner_image_url)
                                <div class="sp-slide">
                                    <img alt="Image" class="sp-image" src="css/images/blank.gif" data-src="{{ Storage::disk('azure')->url(@$experience->food_banner_image_url) }}" />
                                </div>
                                @endif
                                @if(@$foodimagegalleries)
                                @foreach(@$foodimagegalleries as $foodimagegallery)
                                <div class="sp-slide">
                                    <img alt="Image" class="sp-image" src="css/images/blank.gif" data-src="{{ Storage::disk('azure')->url(@$foodimagegallery->image_url) }}" />
                                </div>
                                @endforeach
                                @endif                                                                    
                            </div>
                            <div class="sp-thumbnails">
                                @if(@$experience->food_banner_image_url)
                                <img alt="Image" class="sp-thumbnail" src="{{ Storage::disk('azure')->url(@$experience->food_banner_image_url) }}" />                                    
                                @endif
                                @if(@$foodimagegalleries)
                                @foreach(@$foodimagegalleries as $foodimagegallery)
                                <img alt="Image" class="sp-thumbnail" data-src="{{ Storage::disk('azure')->url(@$foodimagegallery->image_url) }}" />
                                @endforeach
                                @endif
                            </div>
                        </div>                        
                        <br />
                        @endif
                        {!! html_entity_decode(@$experience->food_overview) !!}
                    </div>
                </div>
                <hr>
            <?php } ?>

            <?php if (@$experience->what_is_included) { ?>
                <div class="row">
                    <div class="col-md-12">
                        <h3>What is Included</h3>
                        <div class="cls_new_list">
                            {!! html_entity_decode(@$experience->what_is_included) !!}
                        </div>
                    </div>
                </div>
                <hr>                
            <?php } ?>

            <?php if (@$experience->what_is_not_included) { ?>
                <div class="row">
                    <div class="col-md-12">
                        <h3>What is not Included </h3>
                        {!! html_entity_decode(@$experience->what_is_not_included) !!}
                    </div>
                </div>
                <hr>              
            <?php } ?>

            <?php if (@$experience->cancellation_policy) { ?>
                <div class="row">
                    <div class="col-md-12">
                        <h3>Cancellation Policy</h3>
                        {!! html_entity_decode(@$experience->cancellation_policy) !!}
                    </div>
                </div>
                <hr>
            <?php } ?>

            <?php if (@$center->things_to_do_around_the_center) { ?>
                <div class="row">
                    <div class="col-md-12">
                        <h3>Things to do around</h3>
                        <ul>
                            <?php
                            foreach (explode(",", @$center->things_to_do_around_the_center) as $things_to_do_around_the_center) {
                                ?>
                                <li>{{ @$things_to_do_around_the_center }}</li>
                                <?php
                            }
                            ?>
                        </ul>
                    </div>
                </div>
                <hr>
            <?php } ?>

            <div class="row">
                <div class="col-md-12">
                    <p class="text-left add_bottom_10">
                        <a href="#" class="button_intro"> BACK TO TOP</a>
                    </p>
                </div>
            </div>
        </div>
        <!--End  single_tour_desc-->

        <aside class="col-md-4">
            <?php if (@$experience->experience_summary) { ?>
                <div class="box_style_1 expose">
                    <h3 class="inner cls_new_h3">Experience Summary</h3>
                    <div class="row">
                        <div class="col-md-12">
                            <ul class="list_ok cls_new_list">
                                <?php
                                foreach (explode(",", @$experience->experience_summary) as $experience_summary) {
                                    ?>
                                    <li>{{ @$experience_summary }}</li>
                                    <?php
                                }
                                ?>
                            </ul>
                        </div>
                    </div>
                </div>
            <?php } ?>

            <div class="box_style_1">
                <h3 class="inner">Booking</h3>
                <?php $bkfrmAction = url("/reservation"); ?>
                @if(@$center->name == "AyurYoga Eco-Ashram Mysore India")
                <?php
                $bkfrmAction = url("/redirect-to-portal");
                ?>
                @endif
                <form id="frmBooking" name="frmBooking" action="{{ $bkfrmAction }}" method="POST">
                    <input type="hidden" id="hdn_experience_id" name="hdn_experience_id" value="{{ @$experience->id }}" />
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="text-right">
                                    <small>Price</small>
                                    <span class="price_list price_list-min"></span>
                                    @if(@$experience->duration)
                                    <small>For</small>
                                    <span class="text-days">
                                        <?php
                                        echo @$experience->duration;
                                        ?>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Select date:</label>
                                <?php if (@$experience_recurring->recurring_type == 'Daily') { ?>
                                    <input type="text" class="date-pick form-control" id="booking_date" name="booking_date" data-date-format="mm/dd/yyyy" />
                                <?php } else { ?>
                                    <select id="booking_date" class="form-control" name="booking_date">
                                        @if (\Carbon\Carbon::parse(@$experience->start_date_time)->subDays(2) >=  \Carbon\Carbon::now())
                                        <option value="{{ @$experience->start_date_time." - ". @$experience->end_date_time }}">
                                            {{ (@$experience->start_date_time) ? \Carbon\Carbon::parse(@$experience->start_date_time)->format("d M, Y") : "" }}
                                            {{ (@$experience->end_date_time) ? " - ".\Carbon\Carbon::parse(@$experience->end_date_time)->format("d M, Y") : "" }}
                                        </option>
                                        @endif

                                        @if (sizeof(@$experience_recurring_manually) > 0)
                                        @foreach($experience_recurring_manually as $exp_recurr)
                                        <option value="{{ @$exp_recurr->start_date." - ". @$exp_recurr->end_date}}">
                                            {{ (@$exp_recurr->start_date) ? \Carbon\Carbon::parse(@$exp_recurr->start_date)->format("d M, Y") : "" }}
                                            {{ (@$exp_recurr->end_date) ? " - ".\Carbon\Carbon::parse(@$exp_recurr->end_date)->format("d M, Y") : "" }}
                                        </option>
                                        @endforeach
                                        @endif
                                    </select>
                                <?php } ?>
                            </div>
                            <hr />
                            @if(sizeof(@$experience_accomodations) > 0)
                            @foreach(@$experience_accomodations as $experience_accomodation)
                            <div class="form-group">
                                <label>
                                    <input type="radio" id="exp_accomodation_id_{{ @$experience_accomodation->id }}" name="exp_accomodation_id" value="{{ @$experience_accomodation->id }}" {{ (@$experience_accomodation->accomodation_default == 1) ? "checked='checked'" : "" }}> 
                                    <a href="#mdlAccommodation{{ @$experience_accomodation->id }}"  data-toggle="modal"  class="text-info">{{ @$experience_accomodation->name }}</a>
                                </label>
                                <div class="pull-right" id="accomodation_price_{{ @$experience_accomodation->id }}">
                                    <?php
                                    $discount = 0;
                                    $pay = @$experience_accomodation->room_price;
                                    if ((!empty(@$experience->eirly_bird_before_days)) && (!empty(@$experience->eirly_bird_discount)) && (@$experience->eirly_bird_discount > 0)) {
                                        if (@$experience->eirly_bird_discount_type == "amt") {
                                            $discount += @$experience->eirly_bird_discount;
                                        } else {
                                            $discount = (@$pay * @$experience->eirly_bird_discount) / 100;
                                        }
                                    }

                                    if ((!empty(@$experience->offer_start_date)) && (!empty(@$experience->offer_discount)) && (@$experience->offer_discount > 0)) {
                                        $now = \Carbon\Carbon::parse(date("Y-m-d"))->format("Y-m-d");
                                        if ((\Carbon\Carbon::parse(@$experience->offer_start_date)->format("Y-m-d") <= $now) && (\Carbon\Carbon::parse(@$experience->offer_end_date)->format("Y-m-d") >= $now)) {
                                            if (@$experience->offer_discount_type == "amt") {
                                                $discount += @$experience->offer_discount;
                                            } else {
                                                $discount += (@$pay * @$experience->offer_discount) / 100;
                                            }
                                        }
                                    }


                                    if (!empty(@$discount)) {
                                        ?>
                                        <del class="text-default">
                                            {{ \App\Http\Helpers\CommonHelper::get_currency_rate((@$pay), @$experience_accomodation->currency) }}
                                        </del>
                                        <?php
                                    }
                                    ?>
                                    {{ \App\Http\Helpers\CommonHelper::get_currency_rate(@$pay - $discount, @$experience_accomodation->currency) }}
                                </div>
                                @if(@$experience_accomodation->banner_image_url)
                                <div class="text-center">    
                                    <a href="#mdlAccommodation{{ @$experience_accomodation->id }}"  data-toggle="modal"  class="text-info">
                                        <img src="{{ Storage::disk('azure')->url(@$experience_accomodation->banner_image_url) }}" alt="" class="img-responsive img-thumbnail" />
                                    </a>
                                </div>
                                @endif
                            </div>
                            @endforeach 
                            <div role="alert" class="form-group alert alert-info">
                                <strong>Want to book for 2 or more people?</strong>
                                <br>Contact <a href="mailto:zen@balanceboat.com" class="text-pink">zen@balanceboat.com</a> to get the best deal.
                            </div>
                            @endif
                            @if(@$experience->is_draft == 0 && @$experience->is_bookable == 1)
                            <div class="form-group">
                                <a href="javascript:void(0);" id="btnReserve{{ @$experience->id }}" class="btn_1 medium text-center" style="width:100%">Reserve</a>
                            </div>
                            @endif
                            <div class="form-group">
                                <a href="{{ url("/experience-inquiry/".@$experience->slug) }}" target="_blank" class="btn_full_outline medium text-center">Send Inquiry</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

            <div class="box_style_1 text-center">
                <img src="{{ asset('public/basicfront/img/cards.png') }}" alt="Cards" class="cards" />
            </div>

            <?php if (@$center->speciality) { ?>
                <div class="box_style_1 expose">
                    <h3 class="inner">Center Speciality</h3>
                    <div class="row">
                        <div class="col-md-12">
                            <ul class="list_ok">
                                <?php
                                foreach (explode(",", @$center->speciality) as $speciality) {
                                    ?>
                                    <li>{{ @$speciality }}</li>
                                    <?php
                                }
                                ?>
                            </ul>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <div class="box_style_1 expose">
                <h3 class="inner">Centre Overview</h3>
                <div class="row">
                    <div class="col-md-12">
                        <img src="{{ Storage::disk('azure')->url(@$center->banner_image_url) }}" alt="" class="img-responsive" />
                        <h3><a class="text-default" href="{{ url("center/".@$center->slug) }}">{{ @$center->name }}</a></h3>
                        <?php
                        if (@$center->address_of_center) {
                            ?>
                            <p>
                                <i class="icon-location-2"></i>{{ @$center->address_of_center }}
                            </p>
                            <hr />
                            <?php
                        }
                        ?>
                        <?php if (@$center->certificate_id) { ?>
                            <h4>Certiﬁcations</h4>
                            <?php if (@$certificates) { ?>
                                <div class="row">
                                    <?php
                                    foreach (@$certificates as $certificate) {
                                        if (in_array($certificate->id, explode("||", $center->certificate_id))) {
                                            ?>
                                            <div class="col-md-4">
                                                <img src="{{ Storage::disk('azure')->url(@$certificate->image_url) }}" alt="{{ $certificate->name }}" title="{{ $certificate->name }}" class="img-responsive" />
                                            </div>
                                            <?php
                                        }
                                    }
                                    ?>
                                </div>
                                <?php
                            }
                        }
                        ?>
                    </div>
                    <?php if (sizeof(@$experience_teachers) > 0) { ?>
                        <div class="col-md-12">
                            <hr>
                            <h4>Teachers</h4>
                            <?php
                            if (@$experience_teachers) {
                                foreach (@$experience_teachers as $teacher) {
                                    ?>
                                    <div class="review_strip_single">
                                        <?php
                                        $src = url("/public/basicfront/img/teacher_thumb.jpg");
                                        if (@$teacher->profile_image_url) {
                                            $src = Storage::disk('azure')->url($teacher->profile_image_url);
                                        }
                                        ?>
                                        <img src="{{ $src }}" alt="" title="{{ $teacher->name }}" class="img-responsive img-circle"  style="width: 80px; height: 80px;" />
                                        <h4>
                                            {{ @$teacher->name }}
                                            <br>
                                            <?php
                                            if (@$teacher->certificate_id) {
                                                if (sizeof(@$certificates) > 0) {
                                                    ?>
                                                    <i class="icon-certificate-2"></i>
                                                    <small>
                                                        <?php
                                                        foreach (@$certificates as $certificate) {
                                                            if (in_array($certificate->id, explode("||", $teacher->certificate_id))) {
                                                                ?>
                                                                {{ $certificate->name.", " }}
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                    </small>
                                                    <?php
                                                }
                                            } else {
                                                ?>
                                                <br />
                                            <?php }
                                            ?>
                                        </h4>
                                        {{ @$teacher->short_description }}
                                        <p class="text-right"><a href="{{ url("teacher/".@$teacher->slug) }}" class="text-pink">View Complete Profile</a></p>
                                    </div>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </aside>
    </div>
    <!--End row -->
</div>
<!--End container -->
<!-- End main -->

<!-- Modal Review -->
@if(sizeof(@$experience_accomodations) > 0)
@foreach(@$experience_accomodations as $experience_accomodation)
<div class="modal fade" id="mdlAccommodation{{$experience_accomodation->id}}" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">
                    {{ @$experience_accomodation->name }}
                    <span class="price_list price_list-min">
                        <?php
                        $discount = 0;
                        $pay = @$experience_accomodation->room_price;
                        if ((!empty(@$experience->eirly_bird_before_days)) && (!empty(@$experience->eirly_bird_discount)) && (@$experience->eirly_bird_discount > 0)) {
                            if (@$experience->eirly_bird_discount_type == "amt") {
                                $discount += @$experience->eirly_bird_discount;
                            } else {
                                $discount = (@$pay * @$experience->eirly_bird_discount) / 100;
                            }
                        }

                        if ((!empty(@$experience->offer_start_date)) && (!empty(@$experience->offer_discount)) && (@$experience->offer_discount > 0)) {
                            $now = \Carbon\Carbon::parse(date("Y-m-d"))->format("Y-m-d");
                            if ((\Carbon\Carbon::parse(@$experience->offer_start_date)->format("Y-m-d") <= $now) && (\Carbon\Carbon::parse(@$experience->offer_end_date)->format("Y-m-d") >= $now)) {
                                if (@$experience->offer_discount_type == "amt") {
                                    $discount += @$experience->offer_discount;
                                } else {
                                    $discount += (@$pay * @$experience->offer_discount) / 100;
                                }
                            }
                        }


                        if (!empty(@$discount)) {
                            ?>
                            <del class="text-default">
                                {{ \App\Http\Helpers\CommonHelper::get_currency_rate((@$pay), @$experience_accomodation->currency) }}
                            </del>
                            <?php
                        }
                        ?>
                        {{ \App\Http\Helpers\CommonHelper::get_currency_rate(@$pay - $discount, @$experience_accomodation->currency) }}
                    </span>
                </h4>
            </div>
            <div class="modal-body">                
                {!! html_entity_decode(@$experience_accomodation->description) !!}
                @if(@$accomodationimagegalleries)
                <div class="carousel magnific-gallery">
                    @if(@$accomodationimagegalleries)
                    @foreach(@$accomodationimagegalleries as $accomodationimagegallery)
                    @if ($accomodationimagegallery->accomodation_id == $experience_accomodation->id)
                    <div class="item">
                        <a href="{{ Storage::disk('azure')->url(@$accomodationimagegallery->image_url) }}"><img src="{{ Storage::disk('azure')->url(@$accomodationimagegallery->image_url) }}" alt="Image"></a>
                    </div>
                    @endif
                    @endforeach
                    @endif                    
                </div>                    
                @endif                 
            </div>
        </div>
    </div>
</div>
@endforeach
@endif
<!-- End modal review -->

<!-- Modal Review -->
<div class="modal fade" id="myReview" tabindex="-1" role="dialog" aria-labelledby="myReviewLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myReviewLabel">Write your review</h4>
            </div>
            <div class="modal-body">
                <div id="message-review">
                </div>
                <form method="post" action="assets/review_tour.php" name="review_tour" id="review_tour">
                    <input name="tour_name" id="tour_name" type="hidden" value="Paris Arch de Triomphe Tour">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input name="name_review" id="name_review" type="text" placeholder="Your name" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input name="lastname_review" id="lastname_review" type="text" placeholder="Your last name" class="form-control">
                            </div>
                        </div>
                    </div>
                    <!-- End row -->
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input name="email_review" id="email_review" type="email" placeholder="Your email" class="form-control">
                            </div>
                        </div>
                    </div>
                    <!-- End row -->
                    <hr>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Position</label>
                                <select class="form-control" name="position_review" id="position_review">
                                    <option value="">Please review</option>
                                    <option value="Low">Low</option>
                                    <option value="Sufficient">Sufficient</option>
                                    <option value="Good">Good</option>
                                    <option value="Excellent">Excellent</option>
                                    <option value="Superb">Super</option>
                                    <option value="Not rated">I don't know</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Tourist guide</label>
                                <select class="form-control" name="guide_review" id="guide_review">
                                    <option value="">Please review</option>
                                    <option value="Low">Low</option>
                                    <option value="Sufficient">Sufficient</option>
                                    <option value="Good">Good</option>
                                    <option value="Excellent">Excellent</option>
                                    <option value="Superb">Super</option>
                                    <option value="Not rated">I don't know</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <!-- End row -->
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Price</label>
                                <select class="form-control" name="price_review" id="price_review">
                                    <option value="">Please review</option>
                                    <option value="Low">Low</option>
                                    <option value="Sufficient">Sufficient</option>
                                    <option value="Good">Good</option>
                                    <option value="Excellent">Excellent</option>
                                    <option value="Superb">Super</option>
                                    <option value="Not rated">I don't know</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Quality</label>
                                <select class="form-control" name="quality_review" id="quality_review">
                                    <option value="">Please review</option>
                                    <option value="Low">Low</option>
                                    <option value="Sufficient">Sufficient</option>
                                    <option value="Good">Good</option>
                                    <option value="Excellent">Excellent</option>
                                    <option value="Superb">Super</option>
                                    <option value="Not rated">I don't know</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <!-- End row -->
                    <div class="form-group">
                        <textarea name="review_text" id="review_text" class="form-control" style="height:100px" placeholder="Write your review"></textarea>
                    </div>
                    <div class="form-group">
                        <input type="text" id="verify_review" class=" form-control" placeholder="Are you human? 3 + 1 =">
                    </div>
                    <input type="submit" value="Submit" class="btn_1" id="submit-review">
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End modal review -->

@endsection
@section('footer')
<script src="{{ asset('public/basicfront/js/experience-detail.js') }}"></script>
<script src="{{ asset('public/basicfront/js/jquery.sliderPro.min.js') }}"></script>
<!-- Carousel -->
<script src="{{ asset('public/basicfront/js/owl.carousel.min.js') }}"></script>
<script>
$(document).ready(function () {
    $(".carousel").owlCarousel({
        items: 4,
        autoplay: true
                //itemsDesktop: [1199, 3],
                //itemsDesktopSmall: [979, 3]
    });
});
</script>
<script type="text/javascript">
    $(document).ready(function ($) {
        $('.slider-pro').sliderPro({
            width: 960,
            height: 500,
            fade: true,
            arrows: true,
            buttons: false,
            fullScreen: true,
            smallSize: 500,
            startSlide: 0,
            mediumSize: 1000,
            largeSize: 3000,
            thumbnailArrows: true,
            autoplay: false
        });
    });
</script>
@endsection