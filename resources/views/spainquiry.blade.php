<div class="listing-detail__reviews element-waypoint" id="tips-reviews-box">
                            <h2 class="listing-detail__content-title">
                                Check availability and plan your trip
                                
                            </h2>
                            <div class="listing-detail__reviews-box">
                            
                            <!-- Contact form module -->
                                <form class="add-comment custom-form contact-form" id="frmInquiry" action="/test" method="post" novalidate="novalidate">
                                    <input type="hidden" name="listing_id" id="listing_id" value="{{ @$spa->id }}">
                                    {{ csrf_field() }}
                                    <h2 class="contact-form__title">
                                        Send your Inquiry
                                    </h2>
                                    
                                    <div class="row">
                                        <div class="col-md-6">
                            <input class="contact-form__input-text required" type="text" name="name" id="name" placeholder="First Name:">
                                        </div>
                                        <div class="col-md-6">
                                            <input class="contact-form__input-text required" type="text" name="lastname" id="lastname" placeholder="Last Name:">
                                        </div>
                                    </div>
                                    <div class="row">
                                       
                                        <div class="col-md-12">
                                            <input class="contact-form__input-text required email" type="email" name="email" id="email" placeholder="Email:">
                                        </div>
                                    </div>
                                    <div class="row">
                                       
                                        <div class="col-md-12">
                                            <input class="contact-form__input-text required" type="text" name="phone" id="phone" placeholder="Phone">
                                        </div>
                                    </div>
                                    <textarea class="contact-form__textarea" name="comment" id="comment" placeholder="Message"></textarea>
                                    <div class="row">
          <div class="col-md-12"></div>
          <div class="form-group col-md-4">
          <label for="ReCaptcha">Recaptcha:</label>
          {!! NoCaptcha::renderJs() !!}
          {!! NoCaptcha::display() !!}
          
            </div>
        </div>
                                    <input class="contact-form__submit" type="submit" name="submit-contact" id="submit_enquiry" value="Send Inquiry">
                                </form>
   
                                <!-- End Contact form module -->

                            </div>

                        </div>

<script type="text/javascript" src="{{ asset('public/front/js/jquery.validate.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
           var spaId = $("#listing_id",parent.document).val();
            $("#frmInquiry").validate()
            $("#frmInquiry").on("submit", function () {
                if ($("#frmInquiry").valid()) {
                    $.ajax({
                        url: APP_URL + "/store",
                        method: "POST",
                        data: $("#frmInquiry").serialize()+ "&spaid=" +spaId,
                        success: function (result) {
                           // alert(result);return false;
                           if(result) {
                             
                               $("#submit_enquiry").after(' <div class="alert alert-danger" align="left"><br />'+result+'</div>');
                                setTimeout(function(){
                                    $('.alert-danger').fadeOut();
                                }, 1500);
                           } else {
                                $("#frmInquiry")[0].reset();
                                $("#submit_enquiry").after('<div class="text-success" align="left"><br />Your Enqury  has been saved successfully!</div>');
                                setTimeout(function() {
                                $('#dynamic-modal').modal('toggle');
                                }, 3000);
                           }
                            
                        }
                    });
                }
                return false;
            });

            
        });
</script>