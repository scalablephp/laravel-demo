@extends('layouts.app')
@section('title', 'Disclaimer')
<!-- Meta Info Start-->
@section('meta_title', "Disclaimer")
@section('description', "Disclaimer page for BalanceSeek")
@section('keywords', "BalanceSeek, BalanceSeek disclaimer page, Disclaimer page")
<!-- Meta Info End -->
@section('content')
<!-- contact-page-block
                    ================================================== -->
<section class="contact-page">
    <div class="container">
        <h1 class="contact-page__title">
            Disclaimer
        </h1>
        <p class="contact-page__description">The information contained in the balanceSeek website is for general information purposes only. The information is provided by balanceSeek and while we endeavour to keep the information up to date and correct, we make no representations or warranties of any kind, express or implied, about the completeness, accuracy, reliability, suitability or availability with respect to the website or the information, products, services, or related graphics contained on the website for any purpose. Any reliance you place on such information is therefore strictly at ones own risk.</p>
        <p class="contact-page__description">
        In no event will we be liable for any loss or damage including without limitation, indirect or consequential loss or damage, or any loss or damage whatsoever arising from loss of data or profits arising out of, or in connection with, the use of this website.</p>
        <p class="contact-page__description">Through this website you are able to link to other websites which are not under the control of balanceSeek.com. We have no control over the nature, content and availability of those sites. The inclusion of any links does not necessarily imply a recommendation or endorse the views expressed within them.
        </p>
        <p class="contact-page__description">
        Every effort is made to keep the website up and running smoothly. However, balanceSeek takes no responsibility for, and will not be liable for, the website being temporarily unavailable due to technical issues beyond our control.
        </p>
    </div>
</section>
<!-- End contact-page-block -->


@endsection
@section('footer')
<script src="{{ asset('public/front/js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('public/front/js/contactus.js') }}"></script>
@endsection