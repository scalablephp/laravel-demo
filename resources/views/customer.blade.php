@extends('layouts.app')
@section('title', 'Customer Inquiry')
<!-- Meta Info Start-->
@section('meta_title', "Customer Inquiry")
@section('description', "Contact Us page for BalanceSeek, a booking platform for retreats and professional courses in Yoga and Ayurveda")
@section('keywords', "BalanceSeek, Booking platform, Yoga teacher Training booking website, Ayurveda packages booking, BalanceSeek contact us page, contact us page")
<!-- Meta Info End -->
@section('head')
<link href="{{asset('public/basicfront/css/contactus.css')}}" rel="stylesheet" />
@endsection
@section('content')
<!-- contact-page-block
                    ================================================== -->

<section class="contact-page">
    <div class="container">
        <h1 class="contact-page__title">
            @if($guestType =='2')
               Reply to Customer
            @else
                Reply to {{$listingId->name}}
            @endif
            
        </h1>
        <p class="contact-page__description"></p>
        <div class="row">
            <div class="col-lg-8 col-md-8">
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                @if(Session::has('flash_message'))
                <div class="alert alert-success">
                    <em> {!! session('flash_message') !!}</em>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                </div>
                @endif 
                @if(Session::has('flash_error_message'))
                <div class="alert alert-danger">
                    <em> {!! session('flash_error_message') !!}</em>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                </div>
                @endif 
                <!-- Contact form module -->
                <form id="frmMessage" name="frmContact" action="{{ url("/savemessage") }}" method="post" class="contact-form">
                <input type="hidden" name="conversationId" value="{{$conversationid}}"/>
                <input type="hidden" name="listingid" value="{{$listingId->listingid}}"/>
                <input type="hidden" name="message_type" value="{{$guestType}}"/>
                    {{ csrf_field() }}
                    
                   
                    <textarea class="contact-form__textarea required" required="" name="message" id="message" placeholder="Message"></textarea>
                    <br />
                    <input class="contact-form__submit" type="submit" name="submit-contact" id="submit_message" value="Send Message" />
                </form>
                <!-- End Contact form module -->
                @foreach($customerMessage as $message)  

                  @if($message->message_type =='2')
                        <div class="message-center ">
                        <div class="message-meta">
                            <span class="message-name">{{$listingId->name}}</span>
                            <span class="message-timeago">{{ \Carbon\Carbon::parse($message->created_at)->format('F j, Y, g:i a')}}</span></div>
                       <div class="message-content">
                          <p>{{$message->message}}</p>
                      </div>
                    </div>
                  @else
                       <div class="message-customer">
                        <div class="message-meta">
                            <span class="message-name">{{$message->name}} {{$message->lastname}}</span>
                            <span class="message-timeago">{{ \Carbon\Carbon::parse($message->created_at)->format('F j, Y, g:i a')}}</span></div>
                       <div class="message-content">
                          <p>{{$message->message}}</p>
                      </div>
                    </div>
                  @endif
                   
                @endforeach 
               
            </div>

            <div class="col-lg-3 offset-lg-1 col-md-4">

                <!-- contact-post-module -->
                <div class="contact-post">
					
					<div class="contact-post__content">
						<h2 class="contact-post__title">
							{{$listingId->name}}
						</h2>
						<p class="contact-post__description">
							{{$listingId->address}}
						</p>
					</div>
				</div>
                <div class="contact-post">
                    <i class="la la-envelope"></i>
                    <div class="contact-post__content">
                        <h2 class="contact-post__title">
                            Email:
                        </h2>
                        <p class="contact-post__description">
                            <a href="mailto:support@balanceseek.com">support@balanceseek.com</a>
                        </p>
                    </div>
                </div>
                <!-- End contact-post-module -->

            </div>

        </div>

    </div>
</section>
<!-- End contact-page-block -->

<!-- map block
                        ================================================== -->
<div class="contact-map"></div>
<!-- End map block -->

@endsection
@section('footer')
<script src="{{ asset('public/front/js/jquery.validate.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/front/js/jquery.validate.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            $("#frmMessage").validate()
            

            
        });
</script>
@endsection