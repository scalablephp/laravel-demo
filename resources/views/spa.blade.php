@extends('layouts.app')
@section('title', @$meta_title)
<!-- Meta Info Start-->
@if(!empty(@$meta_title))
@section('meta_title', @$meta_title)
@endif
@if(!empty(@$meta_description))
@section('description', strip_tags(@$meta_description))
@endif
@if(!empty(@$meta_keywords))
@section('keywords', strip_tags(@$meta_keywords))
@endif
@if(!empty(@$meta_image_url))
@section('image', Storage::disk('azure')->url(str_replace(' ', '%20',stripslashes(@$meta_image_url))))
@endif
<!-- Meta Info End -->
@section('content')
<style type="text/css">
    .select2-container--default .select2-selection--multiple .select2-selection__rendered, .select2-dropdown.select2-dropdown--above, .select2-container {
        width: 330px !important;
    }
</style>
<?php
$agent = new \Jenssegers\Agent\Agent();
?>
<!-- category-list block ================================================== -->
<section class="category-list">
    @if($agent->isPhone())
    <main class="cd-main-content is-fixed">
        <a href="javascript:void(0);" class="cd-filter-trigger">Filters</a>
        <div class="cd-filter">
            {!! Form::open(['url' => '/spa/filter', 'method'=>'get']) !!}
            <!--<div>
                <input class="explore__form-input" type="text" name="name" id="name" value="{{ @$srchplace }}"
                       placeholder="what are you looking for"/>
            </div>-->

            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                @if($types)
                <div class="panel">
                    <div class="panel-heading" role="tab" id="headingTOS">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion"
                               href="#collapseTOS" aria-expanded="true" aria-controls="collapseTOS">
                                <i class="more-less fa fa-plus"></i>
                                Type of SPA
                            </a>
                        </h4>
                    </div>
                    <div id="collapseTOS" class="panel-collapse collapse" role="tabpanel"
                         aria-labelledby="headingTOS">
                        <div class="panel-body">
                            <ul class="explore__form-checkbox-list fullwidth explore__form-checkbox-list-side">
                                @foreach ($types as $spaType)
                                <li>
                                    <input class="explore__input-checkbox" type="checkbox"
                                           name="type_of_spa[]" id="type_of_spa{{ $spaType['id'] }}"
                                           value="{{ $spaType['id'] }}" {{ old('type_of_spa') == $spaType['id'] ? 'checked=checked' : '' }} >
                                           <span class="explore__checkbox-style"></span>
                                    <label class="explore__checkbox-text"
                                           for="type_of_spa{{ $spaType['id'] }}">{{ $spaType['name']."(".$spaType['listings_count'].")" }}</label>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
                @endif
                @if($destinations)
                <div class="panel">
                    <div class="panel-heading" role="tab" id="headingCNT">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion"
                               href="#collapseCNT" aria-expanded="true" aria-controls="collapseCNT">
                                <i class="more-less fa fa-plus"></i>
                                Country
                            </a>
                        </h4>
                    </div>
                    <div id="collapseCNT" class="panel-collapse collapse" role="tabpanel"
                         aria-labelledby="headingCNT">
                        <div class="panel-body">
                            <ul class="explore__form-checkbox-list fullwidth explore__form-checkbox-list-side">
                                @foreach($destinations as $country=>$destination)
                                <li>
                                    <input class="explore__input-checkbox" type="checkbox"
                                           name="country[]" id="country{{ $country }}"
                                           value="{{ $country }}" {{ old('country') ==  $country ? 'selected=selected' : '' }} >
                                           <span class="explore__checkbox-style"></span>
                                    <label class="explore__checkbox-text"
                                           for="country{{ $country }}">{{ $destination[0]->countryname."(".$destination['total_listings'].")" }}</label>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="panel">
                    <div class="panel-heading" role="tab" id="headingCT">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion"
                               href="#collapseCT" aria-expanded="true" aria-controls="collapseCT">
                                <i class="more-less fa fa-plus"></i>
                                City
                            </a>
                        </h4>
                    </div>
                    <div id="collapseCT" class="panel-collapse collapse" role="tabpanel"
                         aria-labelledby="headingCT">
                        <div class="panel-body">
                            <ul class="explore__form-checkbox-list fullwidth explore__form-checkbox-list-side">
                                @foreach($destinations as $country=>$destination)
                                @foreach(@$destination as $key=>$objCity)
                                @if(is_int($key))
                                <li>
                                    <input class="explore__input-checkbox" type="checkbox"
                                           name="city[]" id="city{{ $objCity->id }}"
                                           value="{{ $objCity->id }}" {{ old('city') ==  $objCity->id ? 'selected=selected' : '' }} >
                                           <span class="explore__checkbox-style"></span>
                                    <label class="explore__checkbox-text"
                                           for="country{{ $objCity->id }}">{{ $objCity->name }}
                                        ({{ @$objCity->total_listings }})</label>
                                </li>
                                @endif
                                @endforeach
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
                @endif

                <div class="panel">
                    <div class="panel-heading" role="tab" id="headingDIST">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion"
                               href="#collapseDIST" aria-expanded="true" aria-controls="collapseCNT">
                                <i class="more-less fa fa-plus"></i>
                                Within Distance
                            </a>
                        </h4>
                    </div>
                    <div id="collapseDIST" class="panel-collapse collapse" role="tabpanel"
                         aria-labelledby="headingDIST">
                        <div class="panel-body">
                            <ul class="explore__form-checkbox-list explore__form-checkbox-list-side fullwidth">
                                <li>
                                    <input class="explore__input-radio" type="radio" name="distance"
                                           id="distance50"
                                           value="" {{ old('distance') ==  '50' ? 'selected=selected' : '' }} />
                                           <span class="explore__radio-style"></span>
                                    <label class="explore__checkbox-text" for="distance50">50 miles
                                        radius</label>
                                </li>
                                <li>
                                    <input class="explore__input-radio" type="radio" name="distance"
                                           id="distance30"
                                           value="" {{ old('distance') ==  '30' ? 'selected=selected' : '' }} />
                                           <span class="explore__radio-style"></span>
                                    <label class="explore__checkbox-text" for="distance30">30 miles
                                        radius</label>
                                </li>
                                <li>
                                    <input class="explore__input-radio" type="radio" name="distance"
                                           id="distance15"
                                           value="" {{ old('distance') ==  '15' ? 'selected=selected' : '' }} />
                                           <span class="explore__radio-style"></span>
                                    <label class="explore__checkbox-text" for="distance15">15 miles
                                        radius</label>
                                </li>
                                <li>
                                    <input class="explore__input-radio" type="radio" name="distance"
                                           id="distance10"
                                           value="" {{ old('distance') ==  '10' ? 'selected=selected' : '' }} />
                                           <span class="explore__radio-style"></span>
                                    <label class="explore__checkbox-text" for="distance10">10 miles
                                        radius</label>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div><!-- panel-group -->
            <h2 class="explore__form-desc">
                Advanced Search
                <a href="#" class="advanced-toggle">
                    more
                    <i class="fa fa-angle-down" aria-hidden="true"></i>
                </a>
            </h2>
            <div class="explore__form-advanced">
                @if(@$services)
                <div class="panel-group" id="accordion2" role="tablist" aria-multiselectable="true">
                    @foreach($services as $service)
                    <div class="panel">
                        <div class="panel-heading" role="tab" id="headingSER{{ $service->id }}">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion2"
                                   href="#collapseSER{{ $service->id }}" aria-expanded="true"
                                   aria-controls="collapseSER{{ $service->id }}">
                                    <i class="more-less fa fa-plus"></i>
                                    {{ $service->name }}
                                </a>
                            </h4>
                        </div>
                        <div id="collapseSER{{ $service->id }}" class="panel-collapse collapse"
                             role="tabpanel" aria-labelledby="headingSER{{ $service->id }}">
                            <div class="panel-body">
                                <ul class="explore__form-checkbox-list fullwidth explore__form-checkbox-list-side">
                                    @foreach($service->subServices()->get() as $subService)
                                    <li>
                                        <input class="explore__input-checkbox" type="checkbox"
                                               name="services[]"
                                               id="services{{$subService->id}}" {{  (collect(old('services'))->contains($subService->id))  ? 'checked=checked':'' }} >
                                        <span class="explore__checkbox-style"></span>
                                        <label class="explore__checkbox-text"
                                               for="services{{$subService->id}}">{{$subService->name." (".@$subService->listings()->count().")" }}</label>
                                    </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                @endif
            </div>


            <div class="form-group">
                <button class="form-control" type="submit"> Filter</button>
            </div>
            {!! Form::close() !!}

            <a href="#0" class="cd-close">Close</a>
        </div> <!-- cd-filter -->
    </main>
    @endif
    @if(@$cities)
    <div class="container">
        <h1>Find listing by Cities</h1>
    </div>
    <div class="category-list__box owl-wrapper">
        <div class="owl-carousel" data-num="5">
            @foreach(@$cities as $objCity)
            <div class="item">
                <!-- category-post module -->
                <a class="category-post__list-num"
                   href="{{ url('search/destination/'.$parent.'/'.@$objCity->slug) }}">
                    <div class="category-post">
                        <?php
                        $src = url("public/front/upload/12.jpg");
                        if (!empty(@$objCity->image_url)) {
                            $src = Storage::disk('azure')->url(stripslashes(@$objCity->image_url));
                        }
                        ?>
                        <img class="category-post__image" src="{{ $src }}" alt=""/>
                        <div class="category-post__content">
                            <h2 class="category-post__title">
                                {{ @$objCity->name }}
                            </h2>
                            {{ @$objCity->total_listings }} Listing
                        </div>
                    </div>
                </a>
                <!-- end category-post module -->
            </div>
            @endforeach
        </div>
    </div>
    @endif
    @if(@$childServices)
    <div class="container">
        <h1>Find listings by Sub Services</h1>
    </div>
    <div class="category-list__box owl-wrapper">
        <div class="owl-carousel" data-num="5">
            @foreach(@$childServices as $objService)
            <div class="item">
                <!-- category-post module -->
                <a class="category-post__list-num"
                   href="{{ url('search/service/'.$parent.'/'.@$objService->slug) }}">
                    <div class="category-post">
                        <?php
                        $src = url("public/front/upload/12.jpg");
                        if (!empty(@$objService->image_url)) {
                            $src = Storage::disk('azure')->url(stripslashes(@$objService->image_url));
                        }
                        ?>
                        <img class="category-post__image" src="{{ $src }}" alt=""/>
                        <div class="category-post__content">
                            <h2 class="category-post__title">
                                {{ @$objService->name }}
                            </h2>
                            {{ @$objService->listings()->count() }} Listing
                        </div>
                    </div>
                </a>
                <!-- end category-post module -->
            </div>
            @endforeach
        </div>
    </div>
    @endif
</section>
<!-- End category-list block -->


<!-- explore-module  ================================================== -->
<section class="explore">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div class="explore__box">

                    <form id="frmsortby" action="" method="get">
                        <div class="form-group form-inline pull-right">
                            <label class="label" for="sortby">Sort By:&nbsp;</label> 
                            <select id="sortby" name="sortby" class="form-control">
                                <option value="new" selected="" {{ (@$sortby == "new") ? "selected":"" }}>New</option>
                                <option value="popular" {{ (@$sortby == "popular") ? "selected":"" }}>Popular</option>
                            </select>
                        </div>
                    </form>
                    <div class="clearfix"></div>
                    @if(@$srchplace || @$srchservice || @$srchdestination)
                    <h2 class="explore__filter-title">
                        <span>Results For:</span> {{ @$srchplace." ".@$srchdestination->name." ".@$srchservice->name." (".@$spacount." results found)" }}
                        <!--a href="explore-fullwidth-map-list.html" class="active">
                        <i class="fa fa-list-ul" aria-hidden="true"></i>
                    </a>
                    <a href="explore-fullwidth-map.html">
                        <i class="fa fa-th" aria-hidden="true"></i>
                    </a-->
                    </h2>
                    @endif
                    @if(sizeof($spas)>0)
                    <div class="explore__wrap iso-call list-version">
                        @foreach($spas as $spa)
                        <div class="item">
                            <!-- place-post module -->
                            <div class="place-post list-style">
                                <div class="place-post__gal-box">
                                    <?php
                                    $src = url("public/front/upload/1.jpg");
                                    if (!empty(@$spa->banner_image_url)) {
                                        $src = Storage::disk('azure')->url(stripslashes(@
                                                        $spa->banner_image_url));
                                    }
                                    ?>
                                    <img class="place-post__image" src="{{ $src }}" alt=""/>
                                </div>
                                <div class="place-post__content">
                                    <h2 class="place-post__title">
                                        <a href="{{ url("spa/".$spa->slug)}}">{{ $spa->name}}</a>
                                    </h2>
                                    <p class="place-post__text">
                                        {!! \App\Http\Helpers\CommonHelper::excerpt($spa->about_us, 110) !!}
                                    </p>
                                    <p class="place-post__address">
                                        <i class="fa fa-map-marker" aria-hidden="true"></i>
                                        {{ ($spa->area) ? $spa->area.", " :"" }}
                                        @if(!empty(@$cityname))
                                        {{ @$cityname.", " }}
                                        @elseif(!empty(@$spa->cityname))
                                        {{ @$spa->cityname.", " }}
                                        @endif
                                        @if(!empty(@$countryname))
                                        {{ @$countryname }}
                                        @elseif(!empty(@$spa->countryname))
                                        {{ @$spa->countryname }}
                                        @endif
                                    </p>
                                    <p class="view-count">
                                        <i class="la la-eye"></i>
                                        <span>Viewed {{ $spa->visitors()->sum('hits') }}</span>
                                    </p>
                                </div>
                            </div>
                            <!-- end place-post module -->
                        </div>
                        @endforeach
                    </div>
                    <div class="center-button">
                        {{ @$spas->appends(@$querystring)->links('layouts.pagination') }}
                    </div>
                    @endif
                </div>
            </div>

            <div class="col-lg-4">
                @if($agent->isDesktop())
                <div class="explore__filter">
                    <h2 class="explore__form-title">
                        Filter Listings
                    </h2>
                    {!! Form::open(['url' => '/spa/filter', 'method'=>'get', 'id' => 'discover_form']) !!}

                    <!--<div>
                        <input class="explore__form-input" type="text" name="name" id="name"
                               value="{{ @$srchplace }}" placeholder="what are you looking for"/>
                    </div>-->

                    <div>
                        <label class="label" for="type_of_spa">Type of Spa</label>
                        <select class="explore__form-input select2-multiple" name="type_of_spa"
                                id="type_of_spa">
                            <option value="0">Select Type of Spa</option>
                            @if($types)
                            @foreach ($types as $spaType)
                            <option value="{{ $spaType['id'] }}" {{ old('type_of_spa') == $spaType['id'] ? 'selected=selected' : '' }} >{{ $spaType['name']."(".$spaType['listings_count'].")" }}</option>
                            @endforeach
                            @endif
                        </select>
                    </div>

                    @if($destinations)
                    <div style="display:{{ (!empty(@$srchcountry)) ? 'none' : '' }}">
                        <label class="label" for="country">Country</label>
                        <select id="country" name="country" class="explore__form-input select2-multiple">
                            <option value="0">Select Country</option>
                            @foreach($destinations as $country=>$destination)
                            <option value="{{ $country }}" {{ @$srchdestination->id ==  $country ? 'selected=selected' : '' }} >{{ $destination[0]->countryname."(".$destination['total_listings'].")" }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div>
                        <label class="label" for="city">City</label>
                        <select id="city" name="city" class="explore__form-input select2-multiple">
                            <option value="0">Select City</option>
                            @foreach($destinations as $country=>$destination)
                            @foreach(@$destination as $key=>$objCity)
                            @if(is_int($key))
                            <option value="{{ $objCity->id }}"
                                    {{ @$srchdestination->id ==  $objCity->id ? 'selected=selected' : '' }} data-rel="{{ $objCity->country }}">{{ $objCity->name }}
                                    ({{ @$objCity->total_listings }})
                        </option>
                        @endif
                        @endforeach
                        @endforeach
                    </select>
                </div>
                @endif

                <div>
                    <label class="label" for="distance">Within Distance</label>
                    <select id="distance" name="distance" class="explore__form-input select2-multiple">
                        <option value="" {{ old('distance') ==  '' ? 'selected=selected' : '' }}>
                                Any distance
                    </option>
                    <option value="50" {{ old('distance') ==  '50' ? 'selected=selected' : '' }}>
                            50 miles radius
                </option>
                <option value="30" {{ old('distance') ==  '30' ? 'selected=selected' : '' }}>
                        30 miles radius
            </option>
            <option value="15" {{ old('distance') ==  '15' ? 'selected=selected' : '' }}>
                    15 miles radius
        </option>
        <option value="10" {{ old('distance') ==  '10' ? 'selected=selected' : '' }}>
                10 miles radius
    </option>
</select>
</div>

<h2 class="explore__form-desc">
    Advanced Search
    <a href="#" class="advanced-toggle">
        more
        <i class="fa fa-angle-down" aria-hidden="true"></i>
    </a>
</h2>

<div class="explore__form-advanced">
    @if(@$services)
    <label class="label">Services Offered</label>
    <div class="panel-group" id="accordion2" role="tablist" aria-multiselectable="true">
        @foreach($services as $service)
        <div class="panel">
            <div class="panel-heading" role="tab" id="headingSER{{ $service->id }}">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse"
                       data-parent="#accordion2"
                       href="#collapseSER{{ $service->id }}" aria-expanded="true"
                       aria-controls="collapseSER{{ $service->id }}">
                        <i class="more-less fa fa-plus"></i>
                        {{ $service->name }}
                    </a>
                </h4>
            </div>
            <div id="collapseSER{{ $service->id }}" class="panel-collapse collapse"
                 role="tabpanel" aria-labelledby="headingSER{{ $service->id }}">
                <div class="panel-body">
                    <ul class="explore__form-checkbox-list fullwidth explore__form-checkbox-list-side">
                        @foreach($service->subServices()->get() as $subService)
                        <li>
                            <input class="explore__input-checkbox"
                                   type="checkbox" name="services[]"
                                   value="{{ $subService->id }}"
                                   id="services{{$subService->id}}" {{  (collect(old('services'))->contains($subService->id))  ? 'checked=checked':'' }} >
                            <span class="explore__checkbox-style"></span>
                            <label class="explore__checkbox-text"
                                   for="services{{$subService->id}}">{{$subService->name." (".@$subService->listings()->count().")" }}</label>
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
        @endforeach
    </div>
    @endif
</div>

<div class="form-group">
    <input id="position" type="hidden" name="position" value="">
    <button class="form-control" type="submit"> Filter</button>
</div>
{!! Form::close() !!}
</div>
</div>
@endif
</div>
</div>
</section>
<!-- End explore-module -->
@endsection
@section('footer')
<script type="text/javascript">
    $(document).ready(function () {
        $("#sortby").on("change", function () {
            $("#frmsortby").submit();
        });
    });
</script>
<script type="text/javascript">
    jQuery(document).ready(function ($) {
        //open/close lateral filter
        $('.cd-filter-trigger').on('click', function () {
            triggerFilter(true);
        });
        $('.cd-filter .cd-close').on('click', function () {
            triggerFilter(false);
        });

        function triggerFilter($bool) {
            var elementsToTrigger = $([$('.cd-filter-trigger'), $('.cd-filter'), $('.cd-tab-filter'), $('.cd-gallery')]);
            elementsToTrigger.each(function () {
                $(this).toggleClass('filter-is-visible', $bool);
            });
        }

        function toggleIcon(e) {
            $(e.target)
                    .prev('.panel-heading')
                    .find(".more-less")
                    .toggleClass('fa-plus fa-minus');
        }

        $('.panel-group').on('hidden.bs.collapse', toggleIcon);
        $('.panel-group').on('shown.bs.collapse', toggleIcon);
        /*$("select[name^=city] option").attr("disabled","disabled");
         $("select[name^=city]").trigger('change');
         $("select[name^=country]").on("change", function(){
         if ($(this).val() > 0 ) {
         $("select[name^=city] option[data-rel="+$(this).val()+"]").removeAttr('disabled');
         } else {
         $("select[name^=city] option").attr("disabled","disabled");
         }
         $("select[name^=city]").trigger('change');
         });*/
    });
</script>
<script src="{{ asset('public/front/js/location.js') }}"></script>
{{--<script type="text/javascript">--}}
{{--$('input[name^=distance], #distance').on('change', function () {--}}
{{--if ($('input[name^=distance], #distance').val() != '') {--}}
{{--if (navigator.geolocation) {--}}
{{--navigator.geolocation.getCurrentPosition(function (position) {--}}
{{--var geocoder = new google.maps.Geocoder;--}}
{{--var latlng = {lat: position.coords.latitude, lng: position.coords.longitude};--}}
{{--geocoder.geocode({'location': latlng}, function (results, status) {--}}
{{--if (status === 'OK') {--}}
{{--if (results[0]) {--}}
{{--var arrAddress = results[0].address_components;--}}
{{--var itemLocality = '';--}}
{{--$.each(arrAddress, function (i, address_component) {--}}
{{--//console.log(address_component.types[0]+':'+address_component.long_name);--}}
{{--if (address_component.types[0] == "locality") {--}}
{{--//console.log("town:"+address_component.long_name);--}}
{{--//$('#city').val(address_component.long_name);--}}
{{--$('#position').val(position.coords.latitude + ',' + position.coords.longitude);--}}
{{--}--}}
{{--});--}}
{{--}--}}
{{--}--}}
{{--});--}}
{{--});--}}
{{--}--}}
{{--} else {--}}
{{--$('#position').val('');--}}
{{--}--}}
{{--});--}}
{{--</script>--}}
@endsection
