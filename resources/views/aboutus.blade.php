@extends('layouts.app')

@section('title', 'About Us')

<!-- Meta Info Start-->
@section('meta_title', "About Us")
@section('description', "About Us page for Balanceseek")
@section('keywords', "Balanceseek")
<!-- Meta Info End -->

@section('content')

<!-- about-block
        ================================================== -->
<section class="about">
    <div class="container">
        <div class="about__box">

            <div class="about__box-line">
                <div class="row">
                    <div class="col-lg-8">

                        <!-- article-post module -->
                        <div class="article-post">
                            <span class="article-post__excerpt">
                                A few words about us
                            </span>
                            <h1 class="article-post__title">
                                BalanceSeek
                            </h1>
                            <p class="article-post__description">
                                BalanceSeek is one of a kind Spa promoting and listing platform focussing on Spa’s in the South Asian Market.Finding the perfect Spa has never been easy there are either too many of them around or none at all. Our mission is to help you relax, de-stress and de-toxify by helping you find the perfect Spa. We do this by bringing all the information that is reviews, contact info, spa menu and details about the spa team in one place. 
                            </p>
                        </div>
                        <!-- end article-post module -->

                    </div>
                    <div class="col-lg-4">

                        <div class="testimonial-wrapp owl-wrapper">
                            <h2>Why YOU should spa too?</h2>
                            <div class="owl-carousel" data-num="1">

                                <div class="item">

                                    <!-- testimonial-post module -->
                                    <div class="testimonial-post">

                                        <img class="testimonial-post__image testimonial-post__image-top" src="{{ url("public/front/upload/Sonya Friedman.jpeg") }}" alt="" />

                                        <div class="testimonial-post__content testimonial-post__content-bottom">

                                            <p class="testimonial-post__description">
                                                " The way you treat yourself sets the standard for others "
                                            </p>

                                            <h2 class="testimonial-post__title">
                                                Sonya Friedman
                                            </h2>

                                        </div>

                                    </div>
                                    <!-- end testimonial-post module -->

                                </div>

                                <div class="item">

                                    <!-- testimonial-post module -->
                                    <div class="testimonial-post">

                                        <img class="testimonial-post__image testimonial-post__image-top" src="{{ url("public/front/upload/Martina McBride.jpg") }}" alt="" />

                                        <div class="testimonial-post__content testimonial-post__content-bottom">

                                            <p class="testimonial-post__description">
                                                “I wish I could do an entire spa day”
                                            </p>

                                            <h2 class="testimonial-post__title">
                                                Martina McBride
                                            </h2>

                                        </div>

                                    </div>
                                    <!-- end testimonial-post module -->

                                </div>

                                <div class="item">

                                    <!-- testimonial-post module -->
                                    <div class="testimonial-post">

                                        <img class="testimonial-post__image testimonial-post__image-top" src="{{ url("public/front/upload/Jim Rohn.jpeg") }}" alt="" />

                                        <div class="testimonial-post__content testimonial-post__content-bottom">

                                            <p class="testimonial-post__description">
                                                “Take care of your body it’s the only place you have to live” 
                                            </p>

                                            <h2 class="testimonial-post__title">
                                                Jim Rohn
                                            </h2>

                                        </div>

                                    </div>
                                    <!-- end testimonial-post module -->

                                </div>

                                <div class="item">

                                    <!-- testimonial-post module -->
                                    <div class="testimonial-post">

                                        <img class="testimonial-post__image testimonial-post__image-top" src="{{ url("public/front/upload/Dr Howard Murad.jpg") }}" alt="" />

                                        <div class="testimonial-post__content testimonial-post__content-bottom">

                                            <p class="testimonial-post__description">
                                                “Aging is a fact of life. Looking your age is not.” 
                                            </p>

                                            <h2 class="testimonial-post__title">
                                                Dr Howard Murad
                                            </h2>

                                        </div>

                                    </div>
                                    <!-- end testimonial-post module -->

                                </div>

                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="about__statistic-line">
                <div class="row">

                    <div class="col-lg-4">
                        <!-- statistic-post module -->
                        <div class="statistic-post">
                            <span class="statistic-post__icon statistic-post__icon-primary">
                                <i class="la la-hotel" aria-hidden="true"></i>
                            </span>
                            <div class="statistic-post__content">
                                <h1 class="statistic-post__title statistic-post__title-white">
                                    @php
                                    @$listingcount = \App\Listings::select("listings.*", "type_of_spa.name as type_of_spa","destination.name as cityname","cnt.name as countryname")->join("destination","listings.city","=","destination.id")->join("destination as cnt","listings.country","=","cnt.id")
                                        ->leftJoin("type_of_spa", "type_of_spa.id", "=", "listings.type_of_institution")
                                        ->where("listings.is_draft", "0")
                                        ->groupBy("listings.id")
                                        ->orderBy("listings.total_score", "DESC")->get()->count();
                                    @endphp    
                                    <span class="timer" data-from="0" data-to="{{ @$listingcount }}">{{ @$listingcount }}</span> +
                                </h1>
                                <p class="statistic-post__description">
                                    Spa Listed
                                </p>
                            </div>
                        </div>
                        <!-- end statistic-post module -->
                    </div>
                    <div class="col-lg-4">
                        <!-- statistic-post module -->
                        <div class="statistic-post">
                            <span class="statistic-post__icon statistic-post__icon-primary">
                                <i class="la la-group" aria-hidden="true"></i>
                            </span>
                            <div class="statistic-post__content">
                                <h1 class="statistic-post__title statistic-post__title-white">
                                    <span class="timer" data-from="0" data-to="9000">9000</span> +
                                </h1>
                                <p class="statistic-post__description">
                                    Website Visitors
                                </p>
                            </div>
                        </div>
                        <!-- end statistic-post module -->
                    </div>
                    <div class="col-lg-4">
                        <!-- statistic-post module -->
                        <div class="statistic-post">
                            <span class="statistic-post__icon statistic-post__icon-primary">
                                <i class="la la-map-marker" aria-hidden="true"></i>
                            </span>
                            <div class="statistic-post__content">
                                <h1 class="statistic-post__title statistic-post__title-white">
                                    @php
                                    @$citycount = \App\Destination::join("listings","listings.city","=","destination.id")->groupBy("listings.city")->get()->count();
                                    @endphp
                                    <span class="timer" data-from="0" data-to="{{ @$citycount }}">{{ @$citycount }}</span>
                                </h1>
                                <p class="statistic-post__description">
                                    Cities Listed
                                </p>
                            </div>
                        </div>
                        <!-- end statistic-post module -->
                    </div>

                </div>
            </div>

        </div>
    </div>
</section>
<!-- End about-block -->

<!-- team-block
        ================================================== -->
<section class="team">
    <div class="container">

        <!-- section-header module -->
        <div class="section-header">
            <h1 class="section-header__title">
                Our Team
            </h1>
            <p class="section-header__description">
                We are a team comprising of an Ayurvedic doctor, calligraphist, marathon runners, swimmers, programmers, teachers and massage aficionados. Passion for wellness and technology is in our blood.
            </p>
        </div>
        <!-- end section-header module -->
    </div>
</section>
<!-- End team-block -->


<!-- Advisors-block
        ================================================== -->
<section class="team">
    <div class="container">

        <!-- section-header module -->
        <div class="section-header">
            <h1 class="section-header__title">
                Our Advisors & Investors
            </h1>
            <p class="section-header__description">
                The team of Balanceboat is extremely proud to count Abhilash K Ramesh the Director of India’s most famous Ayurvedic Brand “Kairali” as not only a mentor but also an early stage investor. 
            </p>
        </div>
        <!-- end section-header module -->
    </div>
</section>
<!-- End Advisors-block -->

@endsection
@section('footer')
@endsection
