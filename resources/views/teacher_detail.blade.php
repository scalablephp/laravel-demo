@extends('layouts.front')
@section('title', @$teacher->name)

<!-- Meta Info Start-->
@section('meta_title', @$teacher->name)
<?php if (!empty(@$teacher->complete_bio)) { ?> 
    @section('description', strip_tags(@$teacher->complete_bio))
<?php } ?>
<?php if (!empty(@$teacher->keywords)) { ?> 
    @section('keywords', strip_tags(@$teacher->keywords))
<?php } ?>
<?php if (!empty(@$teacher->profile_image_url)) { ?> 
    @section('image', Storage::disk('azure')->url(@$teacher->profile_image_url))
<?php } ?>
<!-- Meta Info End -->

@section('banner')
<?php
$src = url("public/basicfront/img/banner_default.jpg");
if (!empty(@$teacher->profile_image_url)) {
    $src = Storage::disk('azure')->url(@$teacher->profile_image_url);
}
?>
<section class="parallax-window" data-parallax="scroll" data-image-src="{{ $src }}" data-natural-width="1280" data-natural-height="780">
    <div class="parallax-content-2">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-sm-8">
                    <h1>{{ @$teacher->name }}</h1>
                    <span>{{ @$teacher->location }}</span>
                </div>
                <div class="col-md-4 col-sm-4">
                    <div id="price_single_main">
                        More Images
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End section -->
@endsection
@section('content')
<main>
    <div id="position">
        <div class="container">
            <ul>
                <li><a href="{{ url("/") }}">Home</a></li>
                <li>{{ @$teacher->name }}</li>
            </ul>
        </div>
    </div>
    <!-- End Position -->

    <div class="collapse" id="collapseMap">
        <div id="map" class="map">test</div>
    </div>
    <!-- End Map -->

    <div class="container margin_30">
        <div class="row">
            <div class="col-md-8" id="single_tour_desc">
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="nomargin_top">{{ @$teacher->name }}</h2>
                        <p>
                            <?php
                            if (@$teacher->certificate_id) {
                                if (sizeof(@$certificates) > 0) {
                                    ?>
                                    <i class="icon-certificate-2"></i>
                                    <small>
                                        <?php
                                        foreach (@$certificates as $certificate) {
                                            if (in_array($certificate->id, explode("||", $teacher->certificate_id))) {
                                                ?>
                                                {{ $certificate->name.", " }}
                                                <?php
                                            }
                                        }
                                        ?>
                                    </small>
                                    <?php
                                }
                            }
                            ?>
                        </p><br>
                    </div>
                </div>
                <?php
                if (@$teacher->complete_bio) {
                    ?>
                    <div class="row">
                        <div class="col-md-12">
                            <h3>{{ @$teacher->name }}’s Bio</h3>
                            <?php
                            if (@$teacher->teaching_since) {
                                ?>
                                <p>
                                    Teaching since {{ @$teacher->teaching_since }}
                                </p>
                                <?php
                            }
                            ?>
                            {!! html_entity_decode(@$teacher->complete_bio) !!}
                        </div>
                    </div>
                    <hr>
                    <?php
                }
                if (@$teacher->currently_working_with) {
                    ?>
                    <div class="row">
                        <div class="col-md-12">
                            <h3>Currently Working With</h3>
                            {!! html_entity_decode(@$teacher->currently_working_with) !!}
                        </div>

                    </div>
                <?php } ?>
            </div>
            <!--End  single_tour_desc-->

            <aside class="col-md-4">
                <?php if (@$teacher->expertise_id) { ?>
                    <div class="box_style_1 expose">
                        <h3 class="inner">- Expertise -</h3>
                        <?php if (@$specialities) { ?>
                            <div class="row">
                                <div class="col-md-12">
                                    <ul class="list_ok">
                                        <?php
                                        foreach (@$specialities as $speciality) {
                                            if (in_array($speciality->id, explode("||", @$teacher->expertise_id))) {
                                                ?>
                                                <li>{{ @$speciality->name }}</li>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </ul>
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                <?php } ?>

                <?php if (@$teacher->certificate_id) { ?>
                    <div class="box_style_1 expose">
                        <h3 class="inner">- Certiﬁcations -</h3>
                        <?php if (@$certificates) { ?>
                            <div class="row">
                                <?php
                                foreach (@$certificates as $certificate) {
                                    if (in_array($certificate->id, explode("||", @$teacher->certificate_id))) {
                                        ?>
                                        <div class="col-md-4">
                                            <img src="{{ Storage::disk('azure')->url(@$certificate->image_url) }}" alt="{{ $certificate->name }}" title="{{ $certificate->name }}" class="img-responsive" />
                                            <p>{{ $certificate->name }}</p>
                                        </div>
                                        <?php
                                    }
                                }
                                ?>
                            </div>
                        <?php }
                        ?>
                    </div>
                <?php } ?>
            </aside>
        </div>
        <!--End row -->
        <hr>
        @if(sizeof(@$teacher_experiences) > 0)
        <section>
            <div class="container">
                <div class="main_title">
                    <h2>Experiences Offered</h2>
                    <p><i>{{ $teacher->name }} offers the following</i> </p>
                </div>
                <div class="row add_bottom_45">
                    @foreach (@$teacher_experiences as $experience) 
                    @include('content-experience', (array)$experience)
                    @endforeach
                </div>
            </div>
            <!-- End container -->
        </section>
        <!-- End section -->
        @endif
    </div>
    <!--End container -->
    <div id="overlay"></div>
    <!-- Mask on input focus -->
</main>
<!-- End main -->

<!-- Modal Review -->
<div class="modal fade" id="myReview" tabindex="-1" role="dialog" aria-labelledby="myReviewLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myReviewLabel">Write your review</h4>
            </div>
            <div class="modal-body">
                <div id="message-review">
                </div>
                <form method="post" action="assets/review_tour.php" name="review_tour" id="review_tour">
                    <input name="tour_name" id="tour_name" type="hidden" value="Paris Arch de Triomphe Tour">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input name="name_review" id="name_review" type="text" placeholder="Your name" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input name="lastname_review" id="lastname_review" type="text" placeholder="Your last name" class="form-control">
                            </div>
                        </div>
                    </div>
                    <!-- End row -->
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input name="email_review" id="email_review" type="email" placeholder="Your email" class="form-control">
                            </div>
                        </div>
                    </div>
                    <!-- End row -->
                    <hr>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Position</label>
                                <select class="form-control" name="position_review" id="position_review">
                                    <option value="">Please review</option>
                                    <option value="Low">Low</option>
                                    <option value="Sufficient">Sufficient</option>
                                    <option value="Good">Good</option>
                                    <option value="Excellent">Excellent</option>
                                    <option value="Superb">Super</option>
                                    <option value="Not rated">I don't know</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Tourist guide</label>
                                <select class="form-control" name="guide_review" id="guide_review">
                                    <option value="">Please review</option>
                                    <option value="Low">Low</option>
                                    <option value="Sufficient">Sufficient</option>
                                    <option value="Good">Good</option>
                                    <option value="Excellent">Excellent</option>
                                    <option value="Superb">Super</option>
                                    <option value="Not rated">I don't know</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <!-- End row -->
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Price</label>
                                <select class="form-control" name="price_review" id="price_review">
                                    <option value="">Please review</option>
                                    <option value="Low">Low</option>
                                    <option value="Sufficient">Sufficient</option>
                                    <option value="Good">Good</option>
                                    <option value="Excellent">Excellent</option>
                                    <option value="Superb">Super</option>
                                    <option value="Not rated">I don't know</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Quality</label>
                                <select class="form-control" name="quality_review" id="quality_review">
                                    <option value="">Please review</option>
                                    <option value="Low">Low</option>
                                    <option value="Sufficient">Sufficient</option>
                                    <option value="Good">Good</option>
                                    <option value="Excellent">Excellent</option>
                                    <option value="Superb">Super</option>
                                    <option value="Not rated">I don't know</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <!-- End row -->
                    <div class="form-group">
                        <textarea name="review_text" id="review_text" class="form-control" style="height:100px" placeholder="Write your review"></textarea>
                    </div>
                    <div class="form-group">
                        <input type="text" id="verify_review" class=" form-control" placeholder="Are you human? 3 + 1 =">
                    </div>
                    <input type="submit" value="Submit" class="btn_1" id="submit-review">
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End modal review -->

@endsection
@section('footer')
@endsection