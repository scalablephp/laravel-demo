@extends('layouts.front')
@section('title', @$experience->name)
@section('head')
<link href="{{ asset('public/basicfront/css/owl.carousel.css') }}" rel="stylesheet">
<link href="{{ asset('public/basicfront/css/owl.theme.css') }}" rel="stylesheet">
@endsection
@section('content')

<!--div id="position">
    <div class="container">
        <ul>
            <li><a href="{{ url("/") }}">Home</a></li>
            <li><a href="{{ url("/experiences") }}">Experiences</a></li>
            <li>{{ @$experience->name }}</li>
        </ul>
    </div>
</div-->
<!-- End Position -->

<div class="collapse" id="collapseMap">
    <div id="map" class="map">test</div>
</div>
<!-- End Map -->

<div class="container margin_80">
    <div class="row">
        <div class="col-md-12">
            <h2>Reservation Details </h2>
        </div>
    </div>
    <section>
        <div class="container">
            <div class="row add_bottom_45">
                <div class="strip_all_tour_list wow fadeIn" data-wow-delay="0.1s">
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <div class="img_list">
                                <a href="{{ url("/experience/".$experience->slug) }}">
                                    <img src="{{ Storage::disk('azure')->url($experience->banner_image_url) }}" alt="{{ $experience->banner_image_title }}" class="img-responsive"> 
                                </a>
                            </div>
                        </div>
                        <div class="clearfix visible-xs-block"></div>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="tour_list_desc">
                                <a href="{{ url("/experience/".$experience->slug) }}"><h3>{{ $experience->name }}</h3></a>
                                @if(Session('experience_info')['exp_booking_date'])
                                <?php
                                $arexp_booking_date = explode(" - ", Session('experience_info')['exp_booking_date']);
                                $exp_booking_start = @$arexp_booking_date[0];
                                $exp_booking_end = @$arexp_booking_date[1];
                                ?>
                                <p>
                                    <i class="fa fa-calender"></i> 
                                    {{ (@$exp_booking_start) ? \Carbon\Carbon::parse(trim(@$exp_booking_start))->format("M d, Y") : "" }}
                                    {{ (@$exp_booking_end) ? " - ".\Carbon\Carbon::parse(trim(@$exp_booking_end))->format("M d, Y") : "" }}
                                </p>
                                @endif
                                <?php if (@$experience->experience_summary) { ?>
                                    <ul class="list_ok">
                                        <?php
                                        $lmt = 0;
                                        foreach (explode(",", @$experience->experience_summary) as $experience_summary) {
                                            if ($lmt < 4) {
                                                ?>
                                                <li>{{ @$experience_summary }}</li>
                                                <?php
                                            }
                                            $lmt++;
                                        }
                                        ?>
                                    </ul>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2">
                            <div class="price_list">
                                <div>
                                    <small>Price:</small>
                                    <span>
                                        <?php
                                        $discount = 0;
                                        $pay = @$experience_accomodation->room_price;
                                        if ((!empty(@$experience->eirly_bird_before_days)) && (!empty(@$experience->eirly_bird_discount)) && (@$experience->eirly_bird_discount > 0)) {
                                            if (@$experience->eirly_bird_discount_type == "amt") {
                                                $discount += @$experience->eirly_bird_discount;
                                            } else {
                                                $discount = (@$pay * @$experience->eirly_bird_discount) / 100;
                                            }
                                        }

                                        if ((!empty(@$experience->offer_start_date)) && (!empty(@$experience->offer_discount)) && (@$experience->offer_discount > 0)) {
                                            $now = \Carbon\Carbon::parse(date("Y-m-d"))->format("Y-m-d");
                                            if ((\Carbon\Carbon::parse(@$experience->offer_start_date)->format("Y-m-d") <= $now) && (\Carbon\Carbon::parse(@$experience->offer_end_date)->format("Y-m-d") >= $now)) {
                                                if (@$experience->offer_discount_type == "amt") {
                                                    $discount += @$experience->offer_discount;
                                                } else {
                                                    $discount += (@$pay * @$experience->offer_discount) / 100;
                                                }
                                            }
                                        }


                                        if (!empty(@$discount)) {
                                            ?>
                                            <del class="text-default">
                                                {{ \App\Http\Helpers\CommonHelper::get_currency_rate((@$pay), @$experience_accomodation->currency) }}
                                            </del>
                                            <?php
                                        }
                                        ?>
                                        {{ \App\Http\Helpers\CommonHelper::get_currency_rate(@$pay - $discount, @$experience_accomodation->currency) }}
                                    </span>
                                    <?php
                                    if (!empty(@$experience->duration)) {
                                        ?>
                                        <p><br /></p>
                                        <small>For:</small>
                                        <span class="text-days">
                                            <?php
                                            echo @$experience->duration;
                                            ?>
                                        </span>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End container -->
    </section>
    <!-- End section -->
    <div class="row">
        <aside class="col-md-4">
            <?php if (@$experience->what_is_included) { ?>
                <div class="box_style_1 expose">
                    <h3 class="inner">What is Included</h3>
                    <div class="row">
                        <div class="col-md-12">
                            {!! html_entity_decode(@$experience->what_is_included) !!}
                        </div>
                    </div>
                </div>
            <?php } ?>

            <?php if (@$experience->what_is_not_included) { ?>
                <div class="box_style_1 expose">
                    <h3 class="inner">What is not Included</h3>
                    <div class="row">
                        <div class="col-md-12">
                            {!! html_entity_decode(@$experience->what_is_not_included) !!}
                        </div>
                    </div>
                </div>            
            <?php } ?>

            <div class="box_style_1 expose">
                <h3 class="inner">Reservation details</h3>
                <div class="row">
                    <div class="col-md-12">
                        @if (@$experience_accomodation)
                        <div class="form-group">
                            <label>
                                <a href="#mdlAccommodation{{ @$experience_accomodation->id }}"  data-toggle="modal"  class="text-info">{{ @$experience_accomodation->name }}</a>
                            </label>
                            <div class="pull-right" id="accomodation_price_{{ @$experience_accomodation->id }}">
                                <?php
                                $discount = 0;
                                $pay = @$experience_accomodation->room_price;
                                if ((!empty(@$experience->eirly_bird_before_days)) && (!empty(@$experience->eirly_bird_discount)) && (@$experience->eirly_bird_discount > 0)) {
                                    if (@$experience->eirly_bird_discount_type == "amt") {
                                        $discount += @$experience->eirly_bird_discount;
                                    } else {
                                        $discount = (@$pay * @$experience->eirly_bird_discount) / 100;
                                    }
                                }

                                if ((!empty(@$experience->offer_start_date)) && (!empty(@$experience->offer_discount)) && (@$experience->offer_discount > 0)) {
                                    $now = \Carbon\Carbon::parse(date("Y-m-d"))->format("Y-m-d");
                                    if ((\Carbon\Carbon::parse(@$experience->offer_start_date)->format("Y-m-d") <= $now) && (\Carbon\Carbon::parse(@$experience->offer_end_date)->format("Y-m-d") >= $now)) {
                                        if (@$experience->offer_discount_type == "amt") {
                                            $discount += @$experience->offer_discount;
                                        } else {
                                            $discount += (@$pay * @$experience->offer_discount) / 100;
                                        }
                                    }
                                }


                                if (!empty(@$discount)) {
                                    ?>
                                    <del class="text-default">
                                        {{ \App\Http\Helpers\CommonHelper::get_currency_rate((@$pay), @$experience_accomodation->currency) }}
                                    </del>
                                    <?php
                                }
                                ?>
                                {{ \App\Http\Helpers\CommonHelper::get_currency_rate(@$pay - $discount, @$experience_accomodation->currency) }}
                            </div>
                            @if(@$experience_accomodation->banner_image_url)
                            <div class="text-center">    
                                <a href="#mdlAccommodation{{ @$experience_accomodation->id }}"  data-toggle="modal"  class="text-info">
                                    <img src="{{ Storage::disk('azure')->url(@$experience_accomodation->banner_image_url) }}" alt="" class="img-responsive img-thumbnail" />
                                </a>
                            </div>
                            @endif
                        </div>
                        <div class="form-group">
                            @if(@$experience_accomodation->description)
                            {!! @$experience_accomodation->description !!} 
                            @endif
                        </div>
                        @endif
                        <?php
                        $pay = $pay - $discount;
                        ?>
                        <table class="table table_summary">
                            <tbody>
                                <tr class="total">
                                    <th>Pay:</th>
                                    <td class="text-right">
                                        <?php
                                        // Calculate Commission
                                        $commission = ($pay * @$experience->commission) / 100;
                                        if (!empty(@$experience->deposit_policy)) {
                                            switch (@$experience->deposit_policy) {
                                                case "2" :
                                                    // If Fixed Amount
                                                    $deposit_amount = @$experience->deposit_amount;
                                                    $pay = @$experience->deposit_amount + $commission;
                                                    break;
                                                case "3" :
                                                    // If Percentage
                                                    $deposit_amount = (@$pay * @$experience->deposit_amount) / 100;
                                                    $pay = $commission + $deposit_amount;
                                                    break;
                                                default:
                                                    break;
                                            }
                                        }
                                        ?>
                                        {{ \App\Http\Helpers\CommonHelper::get_currency_rate(@$pay, @$experience_accomodation->currency) }}
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!--/box_style_1 -->

            <div class="box_style_1 expose">
                <h3 class="inner">Refund Details</h3>
                <ul class="list">
                    <?php
                    $refCond = "N/A";
                    if (!empty(@$experience->cancellation_policy_condition)) {
                        switch (@$experience->cancellation_policy_condition) {
                            case "1" :
                                // If Non Refundable
                                $refCond = "Non Refundable";
                                break;
                            case "2" :
                                // If Always refundable
                                $refCond = "Always refundable";
                                break;
                            case "3" :
                                // If Refundable before specified number of days before arrival date
                                if (@$experience->cancellation_policy_days) {
                                    $refCond = "Refundable before " . @$experience->cancellation_policy_days . " days before arrival date";
                                }
                                break;
                            default:
                                break;
                        }
                    }
                    if ($refCond) {
                        ?>
                        <li><?php echo $refCond; ?></li>
                        <?php
                    }
                    ?>
                </ul>
            </div>
        </aside>
        <div class="col-md-8" id="single_tour_desc" class="single_tour_desc1">
            <form id="frmReservation" name="frmReservation" method="post" action="{{ url("/reservation/store") }}">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-md-12">
                        @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                        <p class="text-right">Almost done! Just fill in the required info</p>
                        <h3>Enter your details</h3>
                        <div class="row">
                            <div class="col-md-6 col-sm-6">
                                <div class="form-group">
                                    <label>Arrival Date</label>
                                    <input type="text" class="form-control required date-pick" required="" id="arrival_date" name="arrival_date" value="{{ (@session('reservation_info')['arrival_date']) ? @session('reservation_info')['arrival_date'] : '' }}" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-sm-6">
                                <div class="form-group">
                                    <label>First name</label>
                                    <input type="text" class="form-control required" required="" id="firstname" name="firstname" value="{{ (@session('reservation_info')['firstname']) ? @session('reservation_info')['firstname'] : @explode(" ",@$user_info->name)[0] }}" />
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <div class="form-group">
                                    <label>Last name</label>
                                    <input type="text" class="form-control required" required="" id="lastname" name="lastname" value="{{ (@session('reservation_info')['lastname']) ? @session('reservation_info')['lastname'] : @explode(" ",@$user_info->name)[1] }}" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-sm-6">
                                <div class="form-group">
                                    <label>Email Address</label>
                                    <input type="email" id="email" name="email" class="form-control required email" required="" value="{{ (@session('reservation_info')['email']) ? @session('reservation_info')['email'] : @$user_info->email }}" />
                                    <small>Conﬁrmation email sent to this address</small>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <div class="form-group">
                                    <label>Confirm email address</label>
                                    <input type="email" id="email_confirmation" name="email_confirmation" class="form-control required email" required="" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-sm-6">
                                <div class="form-group">
                                    <label>Telephone number</label>
                                    <input type="tel" id="phone" name="phone" class="form-control" value="{{ @session('reservation_info')['phone'] }}" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Message for the experience organizer (optional)</label>
                                    <textarea id="message" name="message" class="form-control" rows="5">{{ @session('reservation_info')['message'] }}</textarea>
                                </div>
                            </div>
                        </div>
                        <a href="javascript:void(0);" id="btnReservation" class="btn_1 medium">Continue</a>
                    </div>
                </div>
            </form>
        </div>
        <!--End  single_tour_desc-->
    </div>
    @if(@$deposit_amount > 0)
    <div class="row">
        <div class="col-md-12">
            <div class="box_style_1">
                <h5><i class="icon-certificate"></i> Only pay {{ \App\Http\Helpers\CommonHelper::get_currency_rate(@$pay, @$experience_accomodation->currency) }} now to reserve your spot, Rest pay once you arrive.</h5>
            </div>
        </div>
    </div>
    @endif
    <!--End row -->
</div>
<!--End container -->
<!-- Modal Review -->
@if(@$experience_accomodation))
<div class="modal fade" id="mdlAccommodation{{$experience_accomodation->id}}" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">
                    {{ @$experience_accomodation->name }}
                    <span class="price_list price_list-min">{{ \App\Http\Helpers\CommonHelper::get_currency_rate(@$experience_accomodation->room_price, @$experience_accomodation->currency) }}</span>
                </h4>
            </div>
            <div class="modal-body">                
                {!! html_entity_decode(@$experience_accomodation->description) !!}
                @if(@$accomodationimagegalleries)
                <div class="carousel magnific-gallery">
                    @if(@$accomodationimagegalleries)
                    @foreach(@$accomodationimagegalleries as $accomodationimagegallery)
                    @if ($accomodationimagegallery->accomodation_id == $experience_accomodation->id)
                    <div class="item">
                        <a href="{{ Storage::disk('azure')->url(@$accomodationimagegallery->image_url) }}"><img src="{{ Storage::disk('azure')->url(@$accomodationimagegallery->image_url) }}" alt="Image"></a>
                    </div>
                    @endif
                    @endforeach
                    @endif                    
                </div>                    
                @endif                 
            </div>
        </div>
    </div>
</div>
@endif
<!-- End modal review -->
@endsection
@section('footer')
<script src="{{ asset('public/basicfront/js/reservation.js') }}"></script>
<!-- Carousel -->
<script src="{{ asset('public/basicfront/js/owl.carousel.min.js') }}"></script>
<script>
$(document).ready(function () {
    $(".carousel").owlCarousel({
        items: 4,
        autoplay: true
//itemsDesktop: [1199, 3],
//itemsDesktopSmall: [979, 3]
    });
});
</script>
@endsection