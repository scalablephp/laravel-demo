@extends('layouts.app')
@section('content')
<section class="sign">
    <div class="sign__area">
        <nav>
            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                <a class="nav-item nav-link " id="nav-sign-tab" data-toggle="tab" href="#nav-sign" role="tab" aria-controls="nav-sign" aria-selected="true">Sign in</a>
                <a class="nav-item nav-link active" id="nav-register-tab" data-toggle="tab" href="#nav-register" role="tab" aria-controls="nav-register" aria-selected="false">Register</a>
            </div>
        </nav>
        <div class="tab-content" id="nav-tabContent">
            <div class="tab-pane fade" id="nav-sign" role="tabpanel" aria-labelledby="nav-sign-tab">

                <!-- sign-form-module -->
                <form class="sign-form" id="loginform" role="form" method="POST" action="{{ url('login') }}">
                    {{ csrf_field() }}
                    @if ($errors->has('email'))
                    <span class="help-block">
                        <small>{{ $errors->first('email') }}</small>
                    </span><br />
                    @endif
                    <label class="sign-form__label {{ $errors->has('email') ? ' has-error' : '' }}" for="username">
                        Email address
                    </label>
                    <input class="sign-form__input-text" type="text" name="email" id="email" placeholder="Email" value="{{ old('email') }}" required />

                    <label class="sign-form__label {{ $errors->has('password') ? ' has-error' : '' }}" for="password">
                        Password:
                    </label>
                    <input class="sign-form__input-text" type="password" name="password" id="password" placeholder="Password" required="" />
                    @if ($errors->has('password'))
                    <span class="help-block">
                        <small>{{ $errors->first('password') }}</small>
                    </span>
                    @endif
                    <button  class="sign-form__submit" id="submit-loggin" type="submit">
                        <i class="fa fa-sign-in" aria-hidden="true"></i>
                        Login
                    </button>
                    <p class="sign-form__text">
                        or Login With
                    </p>
                    <ul class="sign-form__social">
                        <li><a href="#" class="facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                        <li><a href="#" class="google"><i class="fa fa-google" aria-hidden="true"></i></a></li>
                    </ul>
                </form>

                <!-- End sign-form-module -->

            </div>
            <div class="tab-pane fade show active" id="nav-register" role="tabpanel" aria-labelledby="nav-register-tab">


                <!-- sign-form-module -->
                <form class="sign-form" id="loginform" role="form" method="POST" action="{{ route('register') }}">
                    {{ csrf_field() }}
                    <label class="sign-form__label {{ $errors->has('first_name') ? 'has-error' : '' }}" for="first_name">
                        Firstname:
                    </label>
                    @if ($errors->has('first_name'))
                    <span class="help-block">
                        <small>{{ $errors->first('first_name') }}</small>
                    </span><br />
                    @endif
                    <input class="sign-form__input-text" type="text" name="first_name" id="first_name" value="{{ old('first_name') }}" required autofocus placeholder="First Name" />

                    <label class="sign-form__label {{ $errors->has('last_name') ? 'has-error' : '' }}" for="first_name">
                        Lastname:
                    </label>
                    @if ($errors->has('last_name'))
                    <span class="help-block">
                        <small>{{ $errors->first('last_name') }}</small>
                    </span><br />
                    @endif
                    <input class="sign-form__input-text" type="text" name="last_name" id="last_name" value="{{ old('last_name') }}" required autofocus placeholder="Last Name" />

                    <label class="sign-form__label {{ $errors->has('email') ? 'has-error' : '' }}" for="email">
                        Email address:
                    </label>
                    @if ($errors->has('email'))
                    <span class="help-block">
                        <small>{{ $errors->first('email') }}</small>
                    </span><br />
                    @endif
                    <input class="sign-form__input-text" type="text" name="email" id="email"  value="{{ old('email') }}" required placeholder="Enter your email" />

                    <label class="sign-form__label  {{ $errors->has('password') ? 'has-error' : '' }}" for="password">
                        Password:
                    </label>
                    @if ($errors->has('password'))
                    <span class="help-block">
                        <small>{{ $errors->first('password') }}</small>
                    </span><br />
                    @endif
                    <input id="password" type="password" class="sign-form__input-text" name="password" placeholder="Password" required  />


                    <label class="sign-form__label" for="password-confirm">
                        Confirm Password:
                    </label>
                    <input id="password-confirm" type="password" class="sign-form__input-text" placeholder="Confirm Password" name="password_confirmation" required />

                    <!--div class="sign-form__checkbox">
                        <input class="sign-form__input-checkbox" type="checkbox" name="rememb-check2" id="rememb-check2"/>
                        <span class="sign-form__checkbox-style"></span>
                        <span class="sign-form__checkbox-text">I've read and accept terms & conditions</span>
                    </div-->

                    <button  class="sign-form__submit" type="submit" id="submit_signup">
                        <i class="fa fa-sign-in" aria-hidden="true"></i>
                        Sign Up
                    </button>
                    <p class="sign-form__text">
                        or Sign Up With
                    </p>
                    <ul class="sign-form__social">
                        <li><a href="#" class="facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                        <li><a href="#" class="google"><i class="fa fa-google" aria-hidden="true"></i></a></li>
                    </ul>
                </form>

                <!-- End sign-form-module -->
            </div>
        </div>
    </div>
    <ul class="sign__slideshow">
        <li><span>Image 01</span></li>
        <li><span>Image 02</span></li>
        <li><span>Image 03</span></li>
    </ul>
</section>
<!-- End sign-block -->
@endsection
