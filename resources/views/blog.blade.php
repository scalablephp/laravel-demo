@extends('layouts.app')
@section('title', 'Articles')
@section('banner')
<section id="hero" class="lazy" data-src="{{ url("/public/basicfront/img/articles-page-banner-image.jpg") }}" style="background-position: top center;">
    <div class="intro_title">
        <h3 class="animated fadeInDown">Articles</h3>
    </div>
</section>
<!-- End hero -->
<?php ?>
@endsection

@section('content')
<!--div id="position">
    <div class="container">
        <ul>
            <li><a href="{{ url("/") }}">Home</a></li>
            <li>Articles</li>
        </ul>
    </div>
</div -->
<!-- End position -->
<section class="page-title">
            <div class="container">
                <h1 class="page-title__title">Blog</h1>
                <span class="page-title__description">News and stories from the team at Balanceseek</span>
            </div>
        </section>
<section class="blog-page">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8">
                        <div class="blog-page__box">
                        @if(@$blogs)
                        @php ($i = 0)

                        @foreach(@$blogs as $blog)
                     
                            @if($i % 2 == 0)
                            <div class="row">
                                
                            @endif
                            <div class="col-md-6">
                                    
                                    <div class="blog-post">

                                        <img class="blog-post__image" src="{{ Storage::disk('azure')->url(@$blog->banner_image_url) }}" alt="">

                                        <ul class="blog-post__list">
                                            <li class="blog-post__list-item">
                                                <i class="la la-calendar-check-o"></i>
                                                <span>{{date('F j, Y', strtotime(@$blog->created_at))}}</span>
                                            </li>
                                           <!-- <li class="blog-post__list-item">
                                                <i class="la la-comments"></i>
                                                <a href="#">1 Comment</a>
                                            </li>-->
                                        </ul>

                                        <h2 class="blog-post__title">
                                            <a href="{{ url("/blog/".@$blog->slug) }}">
                                                {{ @$blog->name }}
                                            </a>
                                        </h2>

                                        <p class="blog-post__description">
                                            {!! html_entity_decode(\App\Http\Helpers\CommonHelper::excerpt(strip_tags(@$blog->description), 400)) !!}...
                                        </p>

                                       <!-- <p class="blog-post__tags">
                                            <i class="la la-tag"></i>
                                            <a href="#">Travel,</a>
                                            <a href="#">Art &amp; Culture,</a>
                                            <a href="#">Hotels</a>
                                        </p>-->

                                    </div>

                                </div>
                           
                        @if($i % 2 != 0 || count($blogs) == ($i+1))
                           </div>
                                
                        @endif
                             @php ($i++)
                        @endforeach
                        @endif
 

                        </div>
                    </div>
                    <div class="col-lg-4">

                        <!-- sidebar -->
                        <div class="sidebar">

                            <div class="sidebar__widget sidebar__widget-category">
                                <h2 class="sidebar__widget-title">
                                    Categories
                                </h2>
                               <!-- <ul class="sidebar__category-list">
                                    <li><a href="#">Coffee</a></li>
                                    <li><a href="#">Travel</a></li>
                                    <li><a href="#">Restaurant</a></li>
                                    <li><a href="#">Art &amp; Culture</a></li>
                                    <li><a href="#">Hotels</a></li>
                                    <li><a href="#">Sport</a></li>
                                    <li><a href="#">Uncategorized</a></li>
                                </ul>-->
                            </div>

                            <div class="sidebar__widget sidebar__widget-tags">
                                <h2 class="sidebar__widget-title">
                                    Popular Tags
                                </h2>
                               <!-- <ul class="sidebar__tags-list">
                                    <li><a href="#">Cafe</a></li>
                                    <li><a href="#">Restaurant</a></li>
                                    <li><a href="#">food</a></li>
                                    <li><a href="#">Romantic</a></li>
                                    <li><a href="#">Cozy</a></li>
                                    <li><a href="#">Sea Food</a></li>
                                    <li><a href="#">Healthy Food</a></li>
                                    <li><a href="#">Pasta</a></li>
                                </ul>-->
                            </div>

                        <!--    <div class="sidebar__widget sidebar__widget-instagram">
                                <h2 class="sidebar__widget-title">
                                    Instagram
                                </h2>
                                <ul class="sidebar__instagram-list">
                                    <li><a href="#"><img src="upload/inst1.jpg" alt=""></a></li>
                                    <li><a href="#"><img src="upload/inst2.jpg" alt=""></a></li>
                                    <li><a href="#"><img src="upload/inst3.jpg" alt=""></a></li>
                                    <li><a href="#"><img src="upload/inst4.jpg" alt=""></a></li>
                                    <li><a href="#"><img src="upload/inst5.jpg" alt=""></a></li>
                                    <li><a href="#"><img src="upload/inst6.jpg" alt=""></a></li>
                                    <li><a href="#"><img src="upload/inst7.jpg" alt=""></a></li>
                                    <li><a href="#"><img src="upload/inst8.jpg" alt=""></a></li>
                                </ul>
                            </div>-->

                            <div class="sidebar__widget sidebar__widget-popular">
                                <h2 class="sidebar__widget-title">
                                    Popular Post
                                </h2>
                             <!--   <ul class="sidebar__popular-list">
                                    <li>
                                        <img src="upload/th1.jpg" alt="">
                                        <div class="sidebar__popular-list-cont">
                                            <h2 class="sidebar__popular-list-title">
                                                <a href="single-post.html">Nunc dignissim risus id metus.</a>
                                            </h2>
                                            <span class="sidebar__popular-list-desc">Oct 20, 2018, 0 Comments</span>
                                        </div>
                                    </li>
                                    <li>
                                        <img src="upload/th2.jpg" alt="">
                                        <div class="sidebar__popular-list-cont">
                                            <h2 class="sidebar__popular-list-title">
                                                <a href="single-post.html">Nunc dignissim risus id metus.</a>
                                            </h2>
                                            <span class="sidebar__popular-list-desc">Oct 20, 2018, 0 Comments</span>
                                        </div>
                                    </li>
                                    <li>
                                        <img src="upload/th3.jpg" alt="">
                                        <div class="sidebar__popular-list-cont">
                                            <h2 class="sidebar__popular-list-title">
                                                <a href="single-post.html">Nunc dignissim risus id metus.</a>
                                            </h2>
                                            <span class="sidebar__popular-list-desc">Oct 20, 2018, 0 Comments</span>
                                        </div>
                                    </li>
                                    <li>
                                        <img src="upload/th4.jpg" alt="">
                                        <div class="sidebar__popular-list-cont">
                                            <h2 class="sidebar__popular-list-title">
                                                <a href="single-post.html">Nunc dignissim risus id metus.</a>
                                            </h2>
                                            <span class="sidebar__popular-list-desc">Oct 20, 2018, 0 Comments</span>
                                        </div>
                                    </li>
                                </ul>-->
                            </div>

                         <!--   <div class="sidebar__widget sidebar__advertise">
                                <span class="sidebar__advertise-title">
                                    advertising box
                                </span>
                                <img src="upload/add.jpg" alt="">
                            </div>-->

                        </div>
                        <!-- End sidebar -->

                    </div>
                </div>
            </div>
        </section>
<!-- End container -->

@endsection
@section('footer')
<script type="text/javascript" src="{{ url("/public/basicfront/js/experience.js") }}"></script>
@endsection