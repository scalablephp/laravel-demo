<form id="frmSearchbar" name="frmSearchbar" action="{{ url("search-experiences") }}" method="get">
    <div id="search_bar_container">
        <div class="container">
            <div class="row">
                <div class="col-md-4 form-group top_search">
                    <i class="icon-location-1"></i>
                    <select class="form-control" id="sdestination" name="sdestination">                        
                        <option value="">Destinations</option>
                        @foreach(\App\Http\Helpers\CommonHelper::get_site_destinations() as $destination)
                        <option value="{{ $destination->id }}" {{ (@$sdest == $destination->id) ? "selected":"" }}>{{ $destination->name." (".@$destination->total.")" }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-3 form-group top_search">
                    <i class=" icon-magic"></i>
                    <select class="form-control" id="scategory" name="scategory">
                        <option value="">Category</option>
                        @foreach(\App\Http\Helpers\CommonHelper::get_site_categories() as $category)
                        <option value="{{ $category->id }}" {{ (@$scat == $category->id) ? "selected":"" }}>{{ $category->name." (".@$category->total.")" }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-3 form-group">
                    <input type="text" class="date-pick form-control" id="sexp_date" name="sexp_date" placeholder="Experience Start Date" data-date-format="mm/dd/yyyy" value="{{ (@$sedate) ? \Carbon\Carbon::parse(@$sedate)->format("m/d/Y") : "" }}" />
                </div>
                <div class="col-md-2 form-group text-left">
                    <button class="button_intro">GO</button>
                </div>
            </div>
        </div>
    </div>
</form>
<!-- /search_bar-->