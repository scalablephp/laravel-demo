<!doctype html>
<html>
    <head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-138289739-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag() {
                dataLayer.push(arguments);
            }
            gtag('js', new Date());

            gtag('config', 'UA-138289739-1');
        </script>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <link rel="icon" href="favicon.ico">
        <title>@yield('title', "Balanceseek")</title>
        <meta name=keywords content="@yield('keywords','')" />
        <meta name="description" content="@yield('description','')" />
        <meta itemprop=name content="BalanceSeek.com | @yield('meta_title','Balanceseek.com')" />
        <meta itemprop=description content="@yield('description','')" />
        <meta itemprop=image content="@yield('image',asset('public/basicfront/img/social-share-image-1.jpg'))" />
        <meta name=twitter:card content="summary" />
        <meta name=twitter:title content="BalanceSeek.com | @yield('meta_title','Balanceseek.com')" />
        <meta name=twitter:description content="@yield('description', '')" />
        <meta name=twitter:image:src content="@yield('image',asset('public/basicfront/img/social-share-image-1.jpg'))" />
        <meta property=og:title content="BalanceSeek.com | @yield('meta_title','Balanceseek.com')" />
        <meta property=og:description content="@yield('description', '')" />
        <meta property=og:image content="@yield('image',asset('public/basicfront/img/social-share-image-1.jpg'))" />
        <meta property="og:image:url" content="@yield('image',asset('public/basicfront/img/social-share-image-1.jpg'))" />
        <meta property="og:image:secure_url" content="@yield('image',asset('public/basicfront/img/social-share-image-1.jpg'))" />
        <meta property=og:url content="{{ url()->current() }}" />
        <meta property=og:site_name content="{{ url("/") }}" />
        <meta property=og:type content="website" />
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!-- CSS template -->
        <link href="{{ asset('public/front/css/triptip-assets.min.css') }}" rel="stylesheet" />
        <link href="{{ asset('public/front/css/style.css') }}" rel="stylesheet" />
        <link href="{{ asset('public/front/fonts/stylesheet.css') }}" rel="stylesheet" />
        <!-- Bootstrap Core CSS -->
        <!--link href="{{ asset('public/admin/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ asset('public/admin/plugins/select2/dist/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="{{ asset('public/admin/plugins/html5-editor/bootstrap-wysihtml5.css') }}" />
        <link href="{{ asset('public/admin/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" type="text/css" /-->
        @yield('head')
        <!-- Facebook Pixel Code -->
        <script>
            !function (f, b, e, v, n, t, s)
            {
                if (f.fbq)
                    return;
                n = f.fbq = function () {
                    n.callMethod ?
                            n.callMethod.apply(n, arguments) : n.queue.push(arguments)
                };
                if (!f._fbq)
                    f._fbq = n;
                n.push = n;
                n.loaded = !0;
                n.version = '2.0';
                n.queue = [];
                t = b.createElement(e);
                t.async = !0;
                t.src = v;
                s = b.getElementsByTagName(e)[0];
                s.parentNode.insertBefore(t, s)
            }(window, document, 'script',
                    'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '2378629815701686');
            fbq('track', 'PageView');
        </script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=2378629815701686&ev=PageView&noscript=1"
                   /></noscript>
    <!-- End Facebook Pixel Code -->
</head>
<body>
    <div class="modal fade" id="search-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
             aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body"></div>
                </div>
            </div>
    </div>
    <!-- Container -->
    <div id="container">
        <!-- Header
            ================================================== -->
        <?php
        $cls = " navbar-light bg-light";
        $clsheader = "white-header-style fullwidth-with-search";
        if (Route::getCurrentRoute()->uri() == '/') {
            $cls = " navbar-dark bg-dark";
            $clsheader = "";
        }
        ?>
        <header class="clearfix {{ $clsheader }}">
            <nav class="navbar navbar-expand-lg {{ $cls }}">
                <div class="container-fluid">
                    <a class="navbar-brand logo" href="{{ url("/") }}">
                        <img src="{{asset('public/front/images/BalanceSeek-Header-logo.png') }}" />
                    </a>
                    <a class="navbar-brand logo mob-logo" href="{{ url("/") }}">
                        <img src="{{asset('public/front/images/BalanceSeek-mobile-logo.png') }}" />
                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mr-auto">
                            <li>
                                <a class="active" href="{{ url("/") }}">Home</a>
                            </li>
                            <li><a href="{{ url("/about-us") }}">About Us</a></li>
                            <li><a href="{{ url("/contact-us") }}">Contact</a></li>
                            <li><a href="{{ url("/blog") }}">Blog</a></li>
                        </ul>
                        <ul class="navbar-nav ml-auto right-list">
                            <li><a data-path="{{url('/quicksearch')}}" class="navigate-btn load-searchajax-modal mobile-sticky" data-toggle="modal" data-target="#search-modal" id="open">Quick Search</a>
                                        </li>
                            @if(Auth::user())
                            <li><a href="{{ url("/logout") }}"><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i> Logout</a></li>
                            <li><a href="{{ url("/subscription") }}"><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i> My Account</a></li>
                            @else
                            <!--li><a href="{{ url("/login") }}"><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i> Sign In</a></li-->
                            @endif
                        </ul>
                        <a href="{{ url('/add-your-spa')  }}" class="add-list-btn btn-default"><i class="fa fa-plus" aria-hidden="true"></i> Add Your Spa</a>
                    </div>
                </div>
            </nav>
        </header>
        <!-- End Header -->

        @yield('content')
        @if(!in_array(Route::currentRouteName(),array("login","register")))
        <!-- footer block module
                ================================================== -->
        <footer class="footer">
            <div class="container">
                <div class="footer__up-part">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="footer__widget text-widget">
                                <h2 class="footer__widget-title">Balanceseek</h2>
                                <p class="footer__widget-description">
                                    BalanceSeek is a Spa Listing & Promoting Platform. Users can find trusted Spas and read authentic reviews. Spa Owners can get trusted leads & more walk in customers.
                                </p>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="footer__widget subscribe-widget">
                                <h2 class="footer__widget-title">
                                    Subscribe
                                </h2>
                                <p class="footer__widget-description">
                                    Be notified monthly with new Spa's, great offers and promotion & Spa of the month.
                                </p>
                                <form class="footer__subscribe-form">
                                    <input class="footer__subscribe-input" type="text" name="email-sub" id="email-sub" placeholder="Enter your Email" />
                                    <button class="footer__subscribe-button  footer__subscribe-button-primary" type="submit">
                                        <i class="la la-arrow-circle-o-right" aria-hidden="true"></i>
                                    </button>
                                </form>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="footer__widget text-widget">
                                <h2 class="footer__widget-title">
                                    Follow Us
                                </h2>
                                <p class="footer__widget-description">
                                    <a href="https://www.facebook.com/balanceseek/" target="_blank" style="color: #999999;">
                                        <strong>Facebook</strong>
                                    </a>
                                    <br />
                                    <a href="https://www.instagram.com/balance.seek/" target="_blank" style="color: #999999;">
                                        <strong>Instagram</strong>
                                    </a>
                                </p>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="footer__widget text-widget">
                                <h2 class="footer__widget-title">
                                    Contact Info
                                </h2>
                                <p class="footer__widget-description">
                                    D-130, Ambedkar Colony, Mehrauli, <br />
                                    New Delhi-110 030 <br>
                                    support@balanceseek.com
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer__down-part">
                    <div class="row">
                        <div class="col-md-7">
                            <p class="footer__copyright">
                                © Copyright 2019 - All Rights Reserved by Rudanti Wellness Services Pvt Ltd.
                            </p>
                        </div>
                        <div class="col-md-5">
                            <ul class="footer__social-list">
                                <li><a href="{{ url('disclaimer') }}">Disclaimer</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- End footer -->
        @endif
    </div>
    <!-- End Container -->
    <script type="text/javascript">APP_URL = '<?php echo url("/"); ?>';</script>
    <script src="{{ asset('public/front/js/jquery.min.js') }}"></script>
    <script src="{{ asset('public/front/js/jquery.migrate.js') }}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key={{ @config('services.google.BROWSER_KEY') }}"></script>
    <!--build:js js/triptip-plugins.min.js -->
    <script src="{{ asset('public/front/js/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('public/front/js/select2.min.js') }}"></script>
    <script src="{{ asset('public/front/js/jquery.imagesloaded.min.js') }}"></script>
    <script src="{{ asset('public/front/js/jquery.isotope.min.js') }}"></script>
    <script src="{{ asset('public/front/js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('public/front/js/jquery.waypoints.min.js') }}"></script>
    <script src="{{ asset('public/front/js/retina-1.1.0.min.js') }}"></script>
    <script src="{{ asset('public/front/js/jquery.appear.js') }}"></script>
    <script src="{{ asset('public/front/js/infobox.min.js') }}"></script>
    <script src="{{ asset('public/front/js/markerclusterer.js') }}"></script>
    <script src="{{ asset('public/front/js/maps.js') }}"></script>
    <!-- endbuild -->

    <script src="{{ asset('public/front/js/popper.js') }}"></script>
    <script src="{{ asset('public/front/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('public/front/js/jquery.countTo.js') }}"></script>
    <script src="{{ asset('public/front/js/script.js') }}"></script>
    <!-- Bootstrap Datepicker plugin -->
    <!--script src="{{ asset('public/admin/js/validation.js') }}"></script>
    <script src="{{ asset('public/admin/plugins/moment/min/moment.min.js') }}"></script>
    <script src="{{ asset('public/admin/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js') }}"></script-->
    <!-- wysuhtml5 Plugin JavaScript -->
    <!--script src="{{ asset('public/admin/plugins/tinymce/tinymce.min.js') }}"></script-->
    <script type="text/javascript">ADMIN_URL = '<?php echo url("/bbadmin/"); ?>';</script>
    <script type="text/javascript">
        $(document).ready(function () {
            var moretext = "(+) View More";
            var lesstext = "(-)View Less";
            $('.morelink').click(function () {
                $(this).prev().toggleClass('expanded');

                if ($(this).hasClass("less")) {
                    $(this).removeClass("less");
                    $(this).html(moretext);
                } else {
                    $(this).addClass("less");
                    $(this).html(lesstext);
                }

            });
        });
        $('.load-searchajax-modal').click(function () {

                $.ajax({
                    type: 'GET',
                    url: $('#open').data('path'),

                    success: function (result) {
                        
                        $('#search-modal div.modal-body').html(result);
                    }
                });
            });
    </script>
    @yield('footer')
</body>
</html>
