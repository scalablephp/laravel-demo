<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="favicon.ico">
        <title>@yield('title')</title>
        <link href="https://fonts.googleapis.com/css?family=Noto+Serif|Roboto" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

        <!-- Bootstrap core CSS -->
        <link href="{{ asset('public/basicfront/css/bootstrap.min.css') }}" rel="stylesheet" />

        <!-- Custom styles for this template -->
        <link href="{{ asset('public/basicfront/css/custom.css') }}" rel="stylesheet" />
        <link href="{{ asset('public/basicfront/css/owl.carousel.css') }}" rel="stylesheet" />
        <link href="{{ asset('public/basicfront/css/owl.theme.css') }}" rel="stylesheet" />
        @yield('css')
    </head>
    <body>
        <header>
            <div class="row">
                <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-light">
                    <div class="container">
                        <div class="col-md-3 col-6"> <a class="navbar-brand" href="#">BalanceSeek</a> </div>
                        <div class="col-md-9 col-6">
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>
                            <div class="collapse navbar-collapse" id="navbarCollapse">
                                <ul class="navbar-nav mr-auto">
                                    <li class="nav-item active"> <a class="nav-link btn btn-outline-primary" href="#"> kERALA, iNDIA </a> </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </nav>
            </div>
        </header>
        <div class="clearfix"></div>
        @yield('content')
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <h5> Need Help?</h5>
                        <ul>
                            <p>Address....</p>
                            <p>Telephone Number</p>
                            <p>Contact Email Address</p>
                        </ul>
                    </div>
                    <div class="col-md-3">
                        <h5> About</h5>
                        <ul>
                            <li><a href="#">About Us</a></li>
                            <li><a href="#">Contact Us</a></li>
                            <li><a href="#">Advertise With Us</a></li>
                            <li><a href="#">List Your Property</a></li>
                        </ul>
                    </div>
                    <div class="col-md-3">
                        <h5> Policy</h5>
                        <ul>
                            <li><a href="#">Cookie Policy </a></li>
                            <li><a href="#">Privacy Policy </a></li>
                            <li><a href="#">Terms & Conditions</a></li>
                        </ul>
                    </div>
                    <div class="col-md-3">
                        <h5> Social</h5>
                        <ul>
                            <li><a href="#">Facebook</a></li>
                            <li><a href="#">Twitter</a></li>
                            <li><a href="#">Instagram</a></li>
                            <li><a href="#">Youtube</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </footer>
        <div class="bottom-bar">
            <p><strong>©2018 balanceboat.com</strong> 
        </div>
        <script src="{{ asset('public/basicfront/js/jquery-1.9.1.min.js') }}"></script> 
        <script src="{{ asset('public/basicfront/js/owl.carousel.js') }}"></script> 
        @yield('footer')
    </body>
</html>
