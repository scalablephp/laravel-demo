<section id="hero" class="lazy">
    <div class="intro_title">
        <h3 class="animated fadeInDown">EXPERIENCE</h3>
        <p class="animated fadeInDown">The best of wellness, spirituality &amp; holistic health around the world </p>
    </div>
    <div id="search_bar_container">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-xs-6 form-group top_search">
                    <i class="icon-location-1"></i>
                    <select class="form-control ">
                        <option><i class="icon-location-1"></i>Destinations</option>
                    </select>
                </div>
                <div class="col-md-3 col-xs-6 form-group top_search">
                    <i class=" icon-magic"></i>
                    <select class="form-control">
                        <option>Experiences</option>
                    </select>
                </div>
                <div class="col-md-3 col-xs-6 form-group top_search">
                    <i class="icon-calendar"></i>
                    <select class="form-control">
                        <option>Arrival Date</option>
                    </select>
                </div>
                <div class="col-md-2 col-xs-6 form-group">
                    <button class="button_intro">GO</button>
                </div>
            </div>
        </div>
    </div>
    <!-- /search_bar-->
</section>
<!-- End hero -->