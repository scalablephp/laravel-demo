@extends('layouts.app')
@section('title', 'Add Your Spa')
<!-- Meta Info Start-->
@section('meta_title', "Contact Us")
@section('description', "Contact Us page for BalanceSeek, a booking platform for retreats and professional courses in Yoga and Ayurveda")
@section('keywords', "BalanceSeek, Booking platform, Yoga teacher Training booking website, Ayurveda packages booking, BalanceSeek contact us page, contact us page")
<!-- Meta Info End -->
@section('head')
<link href="{{asset('public/basicfront/css/contactus.css')}}" rel="stylesheet" />
@endsection
@section('content')
<!-- contact-page-block
                    ================================================== -->
<section class="contact-page">
    <div class="container">
        <h1 class="contact-page__title">
            Add Your Spa
        </h1>
        <p class="contact-page__description"></p>
        <div class="row">
            <div class="col-lg-8 col-md-8">
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                @if(Session::has('flash_message'))
                <div class="alert alert-success">
                    <em> {!! session('flash_message') !!}</em>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                </div>
                @endif 
                @if(Session::has('flash_error_message'))
                <div class="alert alert-danger">
                    <em> {!! session('flash_error_message') !!}</em>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                </div>
                @endif 
                <!-- Contact form module -->
                <form id="frmContact" name="frmContact" action="{{ url("/spa-listing-enquiry_submission") }}" method="post" class="contact-form">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-6">
                            <input class="contact-form__input-text required" required="" type="text" name="pname" id="pname" placeholder="Your Name:" />
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <input class="contact-form__input-text required" required="" type="text" name="spaname" id="spaname" placeholder="Spa Name:" />
                        </div>
                        <div class="col-md-6">
                            <input class="contact-form__input-text required" required="" type="text" name="weburl" id="weburl" placeholder="Website Address:" />
                        </div>                         
                    </div>


                            <input class="contact-form__input-text required" required="" type="text" name="spalocation" id="spalocation" placeholder="Spa Location:" />
                       
                    <div class="row">
                    <div class="col-md-6">        
                       <input class="contact-form__input-text" type="text" name="phone" id="phone" placeholder="Phone" />
                    </div>
                    <div class="col-md-6">        
                       <input class="contact-form__input-text" type="text" name="email" id="email" placeholder="email" />
                    </div>
                    </div>
                    <textarea class="contact-form__textarea required" required="" name="message" id="message" placeholder="Message" style="resize:none"></textarea>
                    <br />


                   <!-- <div class = "row">
                        <div class="col-md-6">
                            <div class="g-recaptcha" data-sitekey="{{ env('NOCAPTCHA_SITEKEY')}}"></div>
                            @if($errors->has('g-recaptcha-response'))
                                <span class="invalid-feedback" style="display:block">
                                    <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>-->
                    <div class="row">
          <div class="col-md-12"></div>
          <div class="form-group col-md-4">
          <label for="ReCaptcha">Recaptcha:</label>
          {!! NoCaptcha::renderJs() !!}
          {!! NoCaptcha::display() !!}
          
            </div>
        </div>
              
                    <input class="contact-form__submit" type="submit" name="submit-contact" id="submit_contact" value="Submit Message" />
                </form>
                <!-- End Contact form module -->

            </div>

            <div class="col-lg-3 offset-lg-1 col-md-4">

                <!-- contact-post-module -->
                <div class="contact-post">
					<i class="la la-map-marker"></i>
					<div class="contact-post__content">
						<h2 class="contact-post__title">
							Location:
						</h2>
						<p class="contact-post__description">
							D-130, Ambedkar Colony, Mehrauli,<br />New Delhi-110 030
						</p>
					</div>
				</div>
                <div class="contact-post">
                    <i class="la la-envelope"></i>
                    <div class="contact-post__content">
                        <h2 class="contact-post__title">
                            Email:
                        </h2>
                        <p class="contact-post__description">
                            <a href="mailto:support@balanceseek.com">support@balanceseek.com</a>
                        </p>
                    </div>
                </div>
                <!-- End contact-post-module -->

            </div>

        </div>

    </div>
</section>
<!-- End contact-page-block -->

<!-- map block
                        ================================================== -->
<div class="contact-map"><div id="mapSingle" data-latitude="28.504084" data-longitude="77.181602" data-map-icon="la la-map-marker"></div></div>
<!-- End map block -->

@endsection
@section('footer')
<script src="{{ asset('public/front/js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('public/front/js/contactus.js') }}"></script>


<script>
    $(document).ready(function() {
            // alert();
    });
</script>
@endsection