<!DOCTYPE html>
<html>
<head>
	<title></title>
	
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

</head>
<body>

<div class = "container">
	<div class = "row col-sm-12  p-3 mb-2 bg-dark text-white">
		Spa Listing Enquiry
	</div>
	<div class = "row col-sm-6">
		<p><strong>Person Name:</strong>    {{ $pname }}</p>
		<p><strong>Spa Name:</strong>		 {{ $spaname }}</p>
		<p><strong>Spa Location:</strong>   {{ $spalocation }}</p>
		<p><strong>Web Url:</strong>		 {{ $weburl }}</p>
		<p><strong>Phone:</strong>			 {{ $phone }}</p>
		<p><strong>Email:</strong>			 {{ $email }}</p>
		
	</div>

</div>



</body>
</html>