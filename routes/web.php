<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

//Route::get('/', function () {
//    return view('welcome');
//});
//Auth::routes();
//Route::get('/home', 'HomeController@index');
//
//Route::get('/', 'PostController@index')->name('home');
//Route::get('/', 'IndexController@index');

Route::get('/', 'IndexController@index');
Route::get('/home', 'IndexController@index');

// customer inquiry

Route::get('/customerinquiry/{conversationid}', 'CustomerController@inquiry');
Route::post('/savemessage', 'CustomerController@savemessage');


// Spa


Route::get('/spa', 'SpaController@index');
Route::post('/store', 'SpaController@storenquiry');
Route::get('/spa/loadDataAjax', 'SpaController@loadDataAjax');
Route::get('/spa/filter', 'SpaController@listFilter')->name('spa-filter');
Route::get('/spa/{slug}', 'SpaController@detail')->name('spa-detail')->middleware('visitor');
Route::get('/search', 'SearchController@search')->name('search');
Route::get('/quicksearch', 'SpaController@quicksearch')->name('quicksearch');
Route::get('/nearbyspa/{to}/{from}', 'SpaController@nearspa')->name('nearspa');

Route::get('/search/destination/{slug}', 'SearchController@searchByDestination');
Route::get('/search/destination/{slug}/{subslug}', 'SearchController@searchByDestination');
Route::get('/search/service/{slug}', 'SearchController@searchByService');
Route::get('/search/service/{slug}/{subslug}', 'SearchController@searchByService');
Route::get('/search/category/{slug}', 'SearchController@searchByTypeOfSpa');
Route::post('/savecomment', 'SpaController@savecomment');



Route::get('/festivals', function () {
    return View::make('festivals');
});

Route::get('/about-us', function () {
    return View::make('aboutus');
});

Route::any('/spainquiry', function () {
    return View::make('spainquiry');
});

//Route::get('/type/{cat}', 'CategoryController@index');
//Route::get('/type/{cat}/{subcat}', 'CategoryController@index');

Route::get('/location/{dest}', 'LocationController@index');
Route::get('/location/{dest}/{subdest}', 'LocationController@index');

Route::get('/experiences', 'ExperienceController@index');
Route::get('/experiences/loadDataAjax', 'ExperienceController@loadDataAjax');
Route::get('/experience/{slug}', 'ExperienceController@index');
Route::get('/experience-inquiry/{slug}', 'ExperienceController@inquiry');
Route::any('/search-experiences', 'ExperienceController@search');
Route::post('/get_ajax_exp_accomodation', 'ExperienceController@get_ajax_exp_accomodation');

Route::post('/redirect-to-portal', 'ExperienceController@redirect_to_portal');

Route::get('/centers', 'CenterController@index');
Route::get('/center/{slug}', 'CenterController@index');

Route::get('/teachers', 'TeacherController@index');
Route::get('/teacher/{slug}', 'TeacherController@index');

Route::get('/blog', 'BlogController@index');
Route::get('/blog/{slug}', 'BlogController@index');

// Subscription
Route::get('/subscription', 'SubscriptionController@create');
Route::post('subscription/store', 'SubscriptionController@store');
Route::post('/subscription/delete_image', 'SubscriptionController@delete_image');
Route::post('/subscription/upload_gallery_image', 'SubscriptionController@upload_gallery_image');
Route::post('/subscription/delete_gallery_image', 'SubscriptionController@delete_gallery_image');
Route::post('/subscription/delete_accomodation_image', 'SubscriptionController@delete_accomodation_image');
Route::post(
    '/subscription/upload_accomodation_gallery_image',
    'SubscriptionController@upload_accomodation_gallery_image'
);
Route::post(
    '/subscription/delete_accomodation_gallery_image',
    'SubscriptionController@delete_accomodation_gallery_image'
);
Route::post('/subscription/delete_food_image', 'SubscriptionController@delete_food_image');
Route::post('/subscription/delete_food_gallery_image', 'SubscriptionController@delete_food_gallery_image');
Route::post('/subscription/delete_menu_gallery_image', 'SubscriptionController@delete_menu_gallery_image');
Route::get('/subscription/getstate', 'SubscriptionController@getstate');
Route::get('/subscription/getcity', 'SubscriptionController@getcity');

Route::group(['middleware' => ['auth']], function () {

    // My Account
    Route::get('myaccount', 'BookingController@index');
    Route::get('booking', 'BookingController@booking');
    Route::get('booking/{id}', 'BookingController@booking');


    Route::any('/reservation', 'ReservationController@index');
    Route::post('/reservation/store', 'ReservationController@store');

    Route::get('/payment', 'PaymentController@index');
    Route::post('/payment/process', 'PaymentController@process');
    Route::post('/payment/response', 'PaymentController@response');
    Route::any('/payment/success', 'PaymentController@success');
    Route::any('/payment/cancel', 'PaymentController@cancel');

    /* User Routes */
    Route::post('/user/update-profile', 'UserController@update_profile');
    Route::post('/user/delete_image', 'UserController@delete_image');
});

Route::get('/register', 'Admin\Auth\RegisterController@showFrontRegistrationForm')->name('register');
Route::post('/login', 'Admin\Auth\LoginController@login');
Route::get('/login', 'Admin\Auth\LoginController@showFrontLoginForm')->name('login');
Route::any('/logout', 'Admin\Auth\LoginController@logout')->name('logout');

//Route::resource('roles', 'RoleController');
//Route::resource('permissions', 'PermissionController');
Route::resource('posts', 'PostController');

// Admin Routes
$this->get('bbadmin/login', 'Admin\Auth\LoginController@showLoginForm')->name('login');
$this->post('bbadmin/login', 'Admin\Auth\LoginController@login');
$this->get('bbadmin/logout', 'Admin\Auth\LoginController@logout')->name('logout');

//Spa Owner Routes
$this->get('spa_admin/login', 'Admin\Auth\LoginController@showSpaOwnerLoginForm')->name('spa.owner.login');
$this->post('spa_admin/login', 'Admin\Auth\LoginController@login')->name('spa.owner.post.login');
$this->post('spa_admin/register', 'Admin\Auth\RegisterController@registerSpaOwner')->name('spa.owner.post.register');

$this->get('spa_admin/logout', 'Admin\Auth\LoginController@logout')->name('spa.owner.logout');

// Registration Routes...
$this->get('bbadmin/register', 'Admin\Auth\RegisterController@showRegistrationForm')->name('register');
$this->post('bbadmin/register', 'Admin\Auth\RegisterController@register');

// Password Reset Routes...
$this->get('password/reset', 'Admin\Auth\ForgotPasswordController@showLinkRequestForm');
$this->post('password/email', 'Admin\Auth\ForgotPasswordController@sendResetLinkEmail');
$this->get('password/reset/{token}', 'Admin\Auth\ResetPasswordController@showResetForm');
$this->post('password/reset', 'Admin\Auth\ResetPasswordController@reset');

Route::get('/about-us', function () {
    return View::make('aboutus');
});

Route::get('/contact-us', function () {
    return View::make('contactus');
});

Route::get('/disclaimer', function () {
    return View::make('disclaimer');
});

Route::post('/send-register-your-business-email', 'EmailController@send_register_your_business_email');
Route::post('/send-contact-us-email', 'EmailController@send_contact_us_email');
Route::post('/send-inquiry-email', 'EmailController@send_inquiry_email');

Route::get('/help', function () {
    return View::make('help');
});

Route::get('/privacy-policy', function () {
    return View::make('privacy-policy');
});

Route::get('/cookie-policy', function () {
    return View::make('cookie-policy');
});

Route::get('/terms-and-conditions', function () {
    return View::make('terms-and-conditions');
});

Route::get('/add-your-spa', 'SpaController@add_your_spa');
Route::post('/spa-listing-enquiry_submission', 'SpaController@spa_listing_enquiry_submission');


Route::get('import_azure_listing_images', 'SpaController@import_azure_listing_images');
Route::get('import_azure_listing_menu_images', 'SpaController@import_azure_listing_menu_images');

Route::group(['prefix' => 'bbadmin', 'middleware' => ['auth', 'isAdmin']], function () {
    Route::get('/', 'Admin\IndexController@index');

    // Users Management
    Route::resource('/users', 'Admin\UserController');
    Route::get('/users/invitation/{uid}', 'Admin\UserController@invitation');

    Route::post('/users/store', 'Admin\UserController@store');

    // Type of Spa Management
    Route::get('/type', 'Admin\TypeController@index');
    Route::get('/type/create', 'Admin\TypeController@create');
    Route::get('/type/edit/{id}', 'Admin\TypeController@edit');
    Route::post('/type/store', 'Admin\TypeController@store');
    Route::post('/type/destroy', 'Admin\TypeController@destroy');
    Route::post('/type/delete_image', 'Admin\TypeController@delete_image');
    Route::post('/type/delete_banner_image', 'Admin\TypeController@delete_banner_image');

    // Destination Management
    Route::get('/destination', 'Admin\DestinationController@index');
    Route::get('/destination/create', 'Admin\DestinationController@create');
    Route::get('/destination/edit/{id}', 'Admin\DestinationController@edit');
    Route::post('/destination/store', 'Admin\DestinationController@store');
    Route::post('/destination/destroy', 'Admin\DestinationController@destroy');
    Route::post('/destination/delete_image', 'Admin\DestinationController@delete_image');
    Route::post('/destination/delete_banner_image', 'Admin\DestinationController@delete_banner_image');

    // Services Management
    Route::get('/services', 'Admin\ServiceController@index');
    Route::get('/service/create', 'Admin\ServiceController@create');
    Route::get('/service/edit/{id}', 'Admin\ServiceController@edit');
    Route::post('/service/store', 'Admin\ServiceController@store');
    Route::post('/service/destroy', 'Admin\ServiceController@destroy');
    Route::post('/service/delete_image', 'Admin\ServiceController@delete_image');
    Route::post('/service/delete_banner_image', 'Admin\ServiceController@delete_banner_image');

    // Awards Management
    Route::get('/awards', 'Admin\AwardController@index');
    Route::get('/award/create', 'Admin\AwardController@create');
    Route::get('/award/edit/{id}', 'Admin\AwardController@edit');
    Route::post('/award/store', 'Admin\AwardController@store');
    Route::post('/award/destroy', 'Admin\AwardController@destroy');
    Route::post('/award/delete_image', 'Admin\AwardController@delete_image');
    Route::post('/award/delete_banner_image', 'Admin\AwardController@delete_banner_image');

    // Teachers Management
    Route::get('/teachers', 'Admin\TeachersController@index');
    Route::get('/teachers/create', 'Admin\TeachersController@create');
    Route::get('/teachers/edit/{id}', 'Admin\TeachersController@edit');
    Route::post('/teachers/store', 'Admin\TeachersController@store');
    Route::post('/teachers/destroy', 'Admin\TeachersController@destroy');
    Route::post('/teachers/delete_image', 'Admin\TeachersController@delete_image');
    Route::post('/teachers/upload_gallery_image', 'Admin\TeachersController@upload_gallery_image');
    Route::post('/teachers/delete_gallery_image', 'Admin\TeachersController@delete_gallery_image');

    // Centers Management
    Route::get('/centers', 'Admin\CentersController@index');
    Route::get('/centers/create', 'Admin\CentersController@create');
    Route::get('/centers/edit/{id}', 'Admin\CentersController@edit');
    Route::post('/centers/store', 'Admin\CentersController@store');
    Route::post('/centers/destroy', 'Admin\CentersController@destroy');
    Route::post('/centers/delete_image', 'Admin\CentersController@delete_image');
    Route::post('/centers/upload_gallery_image', 'Admin\CentersController@upload_gallery_image');
    Route::post('/centers/delete_gallery_image', 'Admin\CentersController@delete_gallery_image');
    Route::post('/centers/get_center_accomodation', 'Admin\CentersController@get_center_accomodation');
    Route::post('/centers/get_center_teachers', 'Admin\CentersController@get_center_teachers');
    Route::post('/centers/delete_accomodation_image', 'Admin\CentersController@delete_accomodation_image');

    // Accomodation Management
    Route::get('/accomodations', 'Admin\AccomodationController@index');
    Route::get('/accomodation/create', 'Admin\AccomodationController@create');
    Route::get('/accomodation/edit/{id}', 'Admin\AccomodationController@edit');
    Route::post('/accomodation/store', 'Admin\AccomodationController@store');
    Route::post('/accomodation/destroy', 'Admin\AccomodationController@destroy');
    Route::post('/accomodation/delete_image', 'Admin\AccomodationController@delete_image');
    Route::post('/accomodation/upload_gallery_image', 'Admin\AccomodationController@upload_gallery_image');
    Route::post('/accomodation/delete_gallery_image', 'Admin\AccomodationController@delete_gallery_image');

    // Commission Management
    Route::get('/commissions', 'Admin\CommissionController@index');
    Route::get('/commission/create', 'Admin\CommissionController@create');
    Route::get('/commission/edit/{id}', 'Admin\CommissionController@edit');
    Route::post('/commission/store', 'Admin\CommissionController@store');
    Route::post('/commission/destroy', 'Admin\CommissionController@destroy');

    // Experiences Management
    Route::get('/listings', 'Admin\ListingsController@index');
    Route::get('/listing/create', 'Admin\ListingsController@create');
    Route::get('/listing/edit/{id}', 'Admin\ListingsController@edit');
    Route::get('/listing/clone/{id}', 'Admin\ListingsController@clone_exp');
    Route::post('/listing/store', 'Admin\ListingsController@store');
    Route::post('/listing/destroy', 'Admin\ListingsController@destroy');
    Route::post('/listing/delete_image', 'Admin\ListingsController@delete_image');
    Route::post('/listing/upload_gallery_image', 'Admin\ListingsController@upload_gallery_image');
    Route::post('/listing/delete_gallery_image', 'Admin\ListingsController@delete_gallery_image');
    Route::post('/listing/delete_accomodation_image', 'Admin\ListingsController@delete_accomodation_image');
    Route::post(
        '/listing/upload_accomodation_gallery_image',
        'Admin\ListingsController@upload_accomodation_gallery_image'
    );
    Route::post(
        '/listing/delete_accomodation_gallery_image',
        'Admin\ListingsController@delete_accomodation_gallery_image'
    );
    Route::post('/listing/delete_food_image', 'Admin\ListingsController@delete_food_image');
    Route::post('/listing/delete_food_gallery_image', 'Admin\ListingsController@delete_food_gallery_image');
    Route::post('/listing/delete_menu_gallery_image', 'Admin\ListingsController@delete_menu_gallery_image');
    Route::get('/listing/getstate', 'Admin\ListingsController@getstate');
    Route::get('/listing/getcity', 'Admin\ListingsController@getcity');

    // Comments Management
    Route::get('/reviews', 'Admin\CommentController@index');
    Route::get('/review/create', 'Admin\CommentController@create');
    Route::get('/review/edit/{id}', 'Admin\CommentController@edit');
    Route::post('/review/store', 'Admin\CommentController@store');
    Route::get('/review/getlistings', 'Admin\CommentController@getlistings');

    // Blog Management
    Route::get('/blogs', 'Admin\BlogController@index');
    Route::get('/blog/create', 'Admin\BlogController@create');
    Route::get('/blog/edit/{id}', 'Admin\BlogController@edit');
    Route::post('/blog/store', 'Admin\BlogController@store');
    Route::post('/blog/destroy', 'Admin\BlogController@destroy');
    Route::post('/blog/delete_image', 'Admin\BlogController@delete_image');
    Route::post('/blog/upload_gallery_image', 'Admin\BlogController@upload_gallery_image');
    Route::post('/blog/delete_gallery_image', 'Admin\BlogController@delete_gallery_image');

    // Blog Management
    Route::get('/bookings', 'Admin\BookingsController@index');
    Route::get('/booking/create', 'Admin\BookingsController@create');
    Route::get('/booking/edit/{id}', 'Admin\BookingsController@edit');
    Route::post('/booking/store', 'Admin\BookingsController@store');
    Route::post('/booking/destroy', 'Admin\BookingsController@destroy');

    Route::resource('permissions', 'Admin\PermissionsController');
    Route::post('permissions_mass_destroy', ['uses' => 'Admin\PermissionsController@massDestroy', 'as' => 'permissions.mass_destroy']);
    Route::resource('roles', 'Admin\RolesController');
    Route::post('roles_mass_destroy', ['uses' => 'Admin\RolesController@massDestroy', 'as' => 'roles.mass_destroy']);
});

Route::group(['prefix' => 'spa-owner-dashboard', 'middleware' => ['auth', 'role:Admin|Owner']], function () {
    Route::get('/dashboard', 'SpaOwnerDashboard\DashboardController@index')->name('spd.dashboard');
    Route::get('/listing', 'SpaOwnerDashboard\ListingController@index')->name('spd.list');
    Route::get('/listing/create', 'SpaOwnerDashboard\ListingController@create')->name('spd.create');
    Route::get('/listing/edit/{id}', 'SpaOwnerDashboard\ListingController@edit')->middleware('isOwnerOfListing')->name('spd.edit');
    Route::get('/listing/clone/{id}', 'SpaOwnerDashboard\ListingController@clone_exp');
    Route::post('/listing/store', 'SpaOwnerDashboard\ListingController@store')->name('spd.store');
    Route::post('/listing/destroy', 'SpaOwnerDashboard\ListingController@destroy');
    Route::post('/listing/delete_image', 'SpaOwnerDashboard\ListingController@delete_image');
    Route::post('/listing/upload_gallery_image', 'SpaOwnerDashboard\ListingController@upload_gallery_image');
    Route::post('/listing/delete_gallery_image', 'SpaOwnerDashboard\ListingController@delete_gallery_image');
    Route::post('/listing/delete_accomodation_image', 'SpaOwnerDashboard\ListingController@delete_accomodation_image');
    Route::post(
        '/listing/upload_accomodation_gallery_image',
        'SpaOwnerDashboard\ListingController@upload_accomodation_gallery_image'
    );
    Route::post(
        '/listing/delete_accomodation_gallery_image',
        'SpaOwnerDashboard\ListingController@delete_accomodation_gallery_image'
    );
    Route::post('/listing/delete_food_image', 'SpaOwnerDashboard\ListingController@delete_food_image');
    Route::post('/listing/delete_food_gallery_image', 'SpaOwnerDashboard\ListingController@delete_food_gallery_image');
    Route::post('/listing/delete_menu_gallery_image', 'SpaOwnerDashboard\ListingController@delete_menu_gallery_image');
    Route::get('/listing/getstate', 'SpaOwnerDashboard\ListingController@getstate');
    Route::get('/listing/getcity', 'SpaOwnerDashboard\ListingController@getcity');

    Route::get('/comments', 'SpaOwnerDashboard\CommentController@index')->name('lstC.index');
    Route::get('/comment/create', 'SpaOwnerDashboard\CommentController@create')->name('lstC.create');
    Route::get('/comment/edit/{id}', 'SpaOwnerDashboard\CommentController@edit')->name('lstC.edit');
    Route::post('/comment/store', 'SpaOwnerDashboard\CommentController@store')->name('lstC.store');
    Route::get('/comment/getlistings', 'SpaOwnerDashboard\CommentController@getlistings')->name('lstC.getlistings');
   // $this->get('spa_admin/myaccount', 'Admin\Auth\LoginController@logout')->name('spa.owner.logout');
    Route::get('/user/myaccount', 'SpaOwnerDashboard\UserController@index')->name('spa.owner.account');
    Route::post('/user/update-profile', 'SpaOwnerDashboard\UserController@update_profile')->name('lstC.updateprofile');
    Route::post('/dashboard/updategraph', 'SpaOwnerDashboard\DashboardController@updategraph');
});
