$(function () {
    "use strict";

    // ============================================================== 
    // List Award
    // ============================================================== 

    $('#tblAward').DataTable({
        "order": false,
        columnDefs: [{orderable: false, targets: [3]}]
    });

    // Delete Award
    $(document).on("click", ".cat-delete", function (e) {
        e.preventDefault();
        $("#id").val($(this).attr("data-rel"));
        //Warning Message
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this award!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
        }, function () {
            $("#frmAward").submit();
            return true;
        });
        return false;
    });
});