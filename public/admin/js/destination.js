$(function () {
    "use strict";

    // ============================================================== 
    // List Destination
    // ============================================================== 

    $('#tblDestination').DataTable({
        "order": false,
        columnDefs: [{orderable: false, targets: [4]}]
    });

    // Delete Destination
    $(document).on("click", ".cat-delete", function (e) {
        e.preventDefault();
        $("#id").val($(this).attr("data-rel"));
        //Warning Message
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this destination!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
        }, function () {
            $("#frmDestination").submit();
            return true;
        });
        return false;
    });
});