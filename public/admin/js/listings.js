$(function () {
    "use strict";

    // ============================================================== 
    // List Listings
    // ============================================================== 

    $('#tblListings').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url" : ADMIN_URL + "/listings",
            "data":{"ajax":true}
        },
        "order": [
            [0, 'desc']
        ],
        columnDefs: [{orderable: false, targets: [4]}]
    });

    // Delete Listings
    $(document).on("click", ".listing-delete", function (e) {
        e.preventDefault();
        $("#id").val($(this).attr("data-rel"));
        //Warning Message
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this Listing!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
        }, function () {
            $("#frmListings").submit();
            return true;
        });
        return false;
    });
});