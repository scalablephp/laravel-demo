var options = {
    enableHighAccuracy: true,
    timeout: 5000,
    maximumAge: 0
};

function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition, error, options);
    } else {
        console.log("Geolocation is not supported by this browser.");
    }
}

function showPosition(position) {
    console.log("Latitude: " + position.coords.latitude + "<br>Longitude: " + position.coords.longitude);
    var container = document.getElementById('discover_form');

    var input2 = document.createElement("input");
    input2.type = "hidden";
    input2.name = "longitude";
    input2.value = position.coords.longitude;
    input2.className = "longitude"; // set the CSS class
    container.insertBefore(input2, container.firstChild); // put it into the DOM

    var input1 = document.createElement("input");
    input1.type = "hidden";
    input1.name = "latitude";
    input1.value = position.coords.latitude;
    input1.className = "latitude";
    container.insertBefore(input1, container.firstChild); // put it into the DOM


}

function error(err) {
    console.warn(err.message);
}

getLocation();