<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ListingMenuTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('listing_menu', function (Blueprint $table) {
            $table->enum('classification', ['Treatments', 'Packages'])->after("menu_description");
            $table->integer('duration')->after("classification");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('listing_menu');
    }

}
