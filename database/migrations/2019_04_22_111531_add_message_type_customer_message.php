<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMessageTypeCustomerMessage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customer_message', function (Blueprint $table)
        {
            $table->integer('message_type')->after('listingid');
            $table->dropColumn('message_to');
            $table->dropColumn('message_from');
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer_message', function (Blueprint $table)
        {
            $table->dropColumn('message_type');
        });
    }
}
