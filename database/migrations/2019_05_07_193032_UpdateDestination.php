<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateDestination extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('destination', function ($table) {
            $table->string('meta_title');
            $table->text('meta_description');
            $table->renameColumn('keywords', 'meta_keywords');
        });
    }
}
