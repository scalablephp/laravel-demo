<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SpaListingEnquiry extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('spa_listing_enquiry', function (Blueprint $table) {
          
          $table->increments('id', 1);
          $table->string('pname');
          $table->string('spaname');
          $table->string('spalocation');
          $table->string('weburl');
          $table->string('email');
          $table->integer('phone')->length(21);
          $table->longtext('message');
          $table->timestamps();
    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
