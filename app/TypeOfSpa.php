<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeOfSpa extends Model
{
    protected $table = "type_of_spa";
    protected $primaryKey = 'id';
    protected $dates = ['created_at'];
    protected $guarded = [
        'id'
    ];

    /**
     * @param  array|null $param
     * @return mixed Fetch  Details of type
     */
    public static function get_data($param = array())
    {
        $orderby = (@$param['orderby']) ? : "name";
        $order = (@$param['order']) ? : "ASC";
        $objTypeOfSpa = TypeOfSpa::query();
        if (@$param['select']) {
            $objTypeOfSpa = $objTypeOfSpa->select($param['select']);
        }
        if (@$param['where']) {
            $objTypeOfSpa = $objTypeOfSpa->where($param['where']);
        }
        if (@$param['limit']) {
            $objTypeOfSpa = $objTypeOfSpa->take($param['limit']);
        }
        $resTypeOfSpa = $objTypeOfSpa->orderBy($orderby, $order)->get();
        return $resTypeOfSpa;
    }

    public function listings()
    {
        return $this->hasMany('App\Listings', 'type_of_institution');
    }
}
