<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Listings extends Model
{

    use FullTextSearch;

    use Geographical;

    protected $table = "listings";
    protected $primaryKey = 'id';
    protected $dates = ['created_at'];
    protected $guarded = [
        'id',
    ];

    /**
     * The columns of the full text index
     */
    protected $searchable = [
        'name',
        'about_us',
    ];

    /**
     * @param  array|null $param
     *
     * @return mixed Fetch  Details of Listing
     */
    public static function get_data($param = [])
    {
        $orderby    = (@$param['orderby']) ?: "name";
        $order      = (@$param['order']) ?: "ASC";
        $objListing = Listings::query();
        if (@$param['select']) {
            $objListing = $objListing->select($param['select']);
        }
        if (@$param['where']) {
            $objListing = $objListing->where($param['where']);
        }
        if (@$param['offset']) {
            $objListing = $objListing->skip($param['offset']);
        }
        if (@$param['limit']) {
            $objListing = $objListing->take($param['limit']);
        }
        $resListing = $objListing->orderBy($orderby, $order)->get();

        return $resListing;
    }

    /**
     * @param  array|null $param
     *
     * @return mixed Fetch  Details of Listing with accommondation price
     */
    public static function get_exp_price_data(
        $cnd = '',
        $orderby = 'e.updated_at',
        $order = 'DESC',
        $limit = 10,
        $offset = 0
    ) {
        $resExp = DB::select("SELECT
                                e.*,
                                e.start_date_time as experience_date,
                                erm.id as erm_id,
                                erm.start_date as recurring,
                                er.recurring_type,
                                (CASE
                                    WHEN
                                        (DATE_SUB(DATE(e.start_date_time),
                                            INTERVAL 2 DAY) >= DATE(NOW())
                                            AND e.is_recurring != 1)
                                    THEN
                                        CONCAT((CASE
                                                    WHEN
                                                        ((MONTHNAME(e.start_date_time) != MONTHNAME(e.end_date_time))
                                                            AND (YEAR(e.start_date_time) = YEAR(e.end_date_time)))
                                                    THEN
                                                        (DATE_FORMAT(e.start_date_time, '%d %b'))
                                                    WHEN (YEAR(e.start_date_time) != YEAR(e.end_date_time)) THEN (DATE_FORMAT(e.start_date_time, '%d %b %Y'))
                                                    ELSE EXTRACT(DAY FROM e.start_date_time)
                                                END),
                                                '-',
                                                DATE_FORMAT(e.end_date_time, '%d %b %Y'))
                                    WHEN
                                        (DATE_SUB(DATE(e.start_date_time),
                                            INTERVAL 2 DAY) >= DATE(NOW())
                                            AND e.is_recurring = 1)
                                    THEN
                                        CONCAT((CONCAT((CASE
                                                            WHEN
                                                                ((MONTHNAME(e.start_date_time) != MONTHNAME(e.end_date_time))
                                                                    AND (YEAR(e.start_date_time) = YEAR(e.end_date_time)))
                                                            THEN
                                                                (DATE_FORMAT(e.start_date_time, '%d %b'))
                                                            WHEN (YEAR(e.start_date_time) != YEAR(e.end_date_time)) THEN (DATE_FORMAT(e.start_date_time, '%d %b %Y'))
                                                            ELSE EXTRACT(DAY FROM e.start_date_time)
                                                        END),
                                                        '-',
                                                        (DATE_FORMAT(e.end_date_time, '%d %b %Y')))),
                                                ' | ',
                                                (CASE  WHEN (er.id IS NOT NULL AND er.recurring_type = 'Weekly') THEN (
                                                  CONCAT('Starts on Every ',DATE_FORMAT(CONCAT('2017-09-1',er.day_of_week),'%W'))
                                                    )
                                                                    ELSE
                                                GROUP_CONCAT(DISTINCT (CONCAT((CASE
                                                                WHEN
                                                                    ((MONTHNAME(erm.start_date) != MONTHNAME(erm.end_date))
                                                                        AND (YEAR(erm.start_date) = YEAR(erm.end_date)))
                                                                THEN
                                                                    (DATE_FORMAT(erm.start_date, '%d %b'))
                                                                WHEN (YEAR(erm.start_date) != YEAR(erm.end_date)) THEN (DATE_FORMAT(erm.start_date, '%d %b %Y'))
                                                                ELSE EXTRACT(DAY FROM erm.start_date)
                                                            END),
                                                            '-',
                                                            DATE_FORMAT(erm.end_date, '%d %b %Y')))
                                                    ORDER BY (erm.start_date)
                                                    SEPARATOR ' | ')
                                                                    END )
                                                )
                                    WHEN
                                        (erm.id IS NOT NULL
                                            AND DATE_SUB(DATE(erm.start_date),
                                            INTERVAL 2 DAY) >= DATE(NOW()))
                                    THEN
                                        GROUP_CONCAT(DISTINCT (CONCAT((CASE
                                                        WHEN (er.id IS NOT NULL AND e.is_recurring = 1) THEN (er.recurring_type)
                                                        WHEN
                                                            ((MONTHNAME(erm.start_date) != MONTHNAME(erm.end_date))
                                                                AND (YEAR(erm.start_date) = YEAR(erm.end_date)))
                                                        THEN
                                                            (DATE_FORMAT(erm.start_date, '%d %b'))
                                                        WHEN (YEAR(erm.start_date) != YEAR(erm.end_date)) THEN (DATE_FORMAT(erm.start_date, '%d %b %Y'))
                                                        ELSE EXTRACT(DAY FROM erm.start_date)
                                                    END),
                                                    '-',
                                                    DATE_FORMAT(erm.end_date, '%d %b %Y')))
                                            ORDER BY (erm.start_date)
                                            SEPARATOR ' | ')
                                            WHEN  (er.id IS NOT NULL) THEN (CASE
                                    WHEN (er.id IS NOT NULL AND er.recurring_type = 'Weekly') THEN (
                                                 CONCAT('Starts on Every ',DATE_FORMAT(CONCAT('2017-09-1',er.day_of_week),'%W'))
                                                    )
                                    END)
                                END) AS available_month,
                                ea.price_per_night_per_guest AS default_price,
                                eap.start_date,
                                eap.end_date,
                                eap.price_per_night_per_guest,
                                (CASE
                                    WHEN
                                        (MONTH(eap.start_date) <= MONTH(NOW())
                                            AND MONTH(eap.end_date) >= MONTH(NOW())
                                            AND EXTRACT(DAY FROM NOW()) >= EXTRACT(DAY FROM eap.start_date)
                                            AND EXTRACT(DAY FROM NOW()) <= EXTRACT(DAY FROM eap.end_date)
                                            AND eap.price_per_night_per_guest IS NOT NULL)
                                    THEN
                                        eap.price_per_night_per_guest
                                    ELSE ea.price_per_night_per_guest
                                END) AS room_price,
                                ea.currency AS accomodation_currency
                            FROM
                                experiences e
                                    LEFT JOIN
                                experience_accomodations ea ON e.id = ea.experience_id
                                    AND ea.accomodation_default = 1
                                    LEFT JOIN
                                experience_accomodation_prices eap ON ea.title = eap.accomodation_id
                                    AND eap.experience_id=ea.experience_id
                                    AND eap.price_per_night_per_guest IS NOT NULL
                                    AND (MONTH(eap.start_date) <= MONTH(NOW())
                                    AND MONTH(eap.end_date) >= MONTH(NOW())
                                    AND EXTRACT(DAY FROM NOW()) >= EXTRACT(DAY FROM eap.start_date)
                                    AND EXTRACT(DAY FROM NOW()) <= EXTRACT(DAY FROM eap.end_date))
                                    LEFT JOIN
                                experience_category ON experience_category.experience_id = e.id
                                    LEFT JOIN
                                experience_recurring er ON er.experience_id = e.id
                                    LEFT JOIN
                                experience_recurring_manually erm ON erm.experience_id = e.id
                            WHERE
                                e.`is_draft` = 0
                                    AND 1 = (CASE
                                    WHEN
                                        (DATE_SUB(DATE(e.start_date_time),
                                            INTERVAL 2 DAY) >= DATE(NOW()))
                                    THEN
                                        1
                                    WHEN
                                        (erm.id IS NOT NULL
                                            AND DATE_SUB(DATE(erm.start_date),
                                            INTERVAL 2 DAY) >= DATE(NOW()))
                                    THEN
                                        1
                                    WHEN
                                        (er.id IS NOT NULL
                                            AND (DATE(er.recurring_end_date) >= DATE(NOW()) OR er.recurring_type='Daily'))
                                    THEN
                                        1
                                    ELSE 0
                                END) $cnd
                            GROUP BY ea.experience_id , accomodation_id
                            ORDER BY $orderby $order
                            LIMIT $offset, $limit");

        return $resExp;
    }

    /**
     * @param  array|null $param
     *
     * @return mixed Fetch  Details of Listing accommondation with price
     */
    public static function get_exp_acm_data($expId = '', $acmId = '', $startdate = "NOW()", $enddate = "NOW()")
    {
        if ($expId) {
            $resExpAcm = DB::select("SELECT
                                a.*,
                                ea.id as experience_accomodations_id,
                                ea.currency,
                                ea.accomodation_default,
                                eap.start_date,
                                eap.end_date,
                                eap.price_per_night_per_guest,
                                (CASE
                                    WHEN
                                        (MONTH(eap.start_date) <= MONTH('$startdate')
                                            AND MONTH(eap.end_date) >= MONTH('$startdate')
                                            AND EXTRACT(DAY FROM '$startdate') >= EXTRACT(DAY FROM eap.start_date)
                                            AND EXTRACT(DAY FROM '$startdate') <= EXTRACT(DAY FROM end_date)
                                            AND eap.price_per_night_per_guest IS NOT NULL)
                                    THEN
                                        eap.price_per_night_per_guest
                                    ELSE ea.price_per_night_per_guest
                                END) AS room_price
                            FROM
                                accomodation a
                                    LEFT JOIN
                                experience_accomodations ea ON a.id = ea.title
                                    LEFT JOIN
                                experience_accomodation_prices eap ON ea.title = eap.accomodation_id
                                    AND eap.experience_id = $expId
                                    AND eap.price_per_night_per_guest IS NOT NULL
                                    AND (MONTH(eap.start_date) <= MONTH('$startdate')
                                    AND MONTH(eap.end_date) >= MONTH('$startdate')
                                    AND EXTRACT(DAY FROM '$startdate') >= EXTRACT(DAY FROM eap.start_date)
                                    AND EXTRACT(DAY FROM '$startdate') <= EXTRACT(DAY FROM end_date))
                            WHERE
                                ea.experience_id = $expId " . (( ! empty($acmId)) ? " and ea.title = $acmId " : ""));
        }

        return $resExpAcm;
    }

    /**
     * @param  array|null $param
     *
     * @return mixed Fetch Listing Category
     */
    public static function get_exp_category(
        $cnd = '',
        $orderby = 'category.id',
        $order = 'ASC',
        $limit = 10,
        $offset = 0
    ) {
        $resExp = DB::select("SELECT
                                    experience_category.category_id, category.id, category.name, count(DISTINCT e.id) as total
                                    from
                                        experiences e
                                            LEFT JOIN
                                        experience_accomodations ea ON e.id = ea.experience_id
                                            AND ea.accomodation_default = 1
                                            LEFT JOIN
                                        experience_accomodation_prices eap ON ea.title = eap.accomodation_id
                                            AND eap.experience_id=ea.experience_id
                                            AND eap.price_per_night_per_guest IS NOT NULL
                                            AND (MONTH(eap.start_date) <= MONTH(NOW())
                                            AND MONTH(eap.end_date) >= MONTH(NOW())
                                            AND EXTRACT(DAY FROM NOW()) >= EXTRACT(DAY FROM eap.start_date)
                                            AND EXTRACT(DAY FROM NOW()) <= EXTRACT(DAY FROM eap.end_date))
                                            LEFT JOIN
                                        experience_category ON experience_category.experience_id = e.id
                                            LEFT JOIN
                                        category ON experience_category.category_id = category.id
                                            LEFT JOIN
                                        experience_recurring er ON er.experience_id = e.id
                                            LEFT JOIN
                                        experience_recurring_manually erm ON erm.experience_id = e.id
                                    WHERE
                                        e.`is_draft` = 0
                                            AND 1 = (CASE
                                            WHEN
                                                (DATE_SUB(DATE(e.start_date_time),
                                                    INTERVAL 2 DAY) >= DATE(NOW()))
                                            THEN
                                                1
                                            WHEN
                                                (erm.id IS NOT NULL
                                                    AND DATE_SUB(DATE(erm.start_date),
                                                    INTERVAL 2 DAY) >= DATE(NOW()))
                                            THEN
                                                1
                                            WHEN
                                                (er.id IS NOT NULL
                                                    AND (DATE(er.recurring_end_date) >= DATE(NOW()) OR er.recurring_type='Daily'))
                                            THEN
                                                1
                                            ELSE 0
                                        END) $cnd
                                    GROUP BY experience_category.category_id
                                    HAVING total > 0
                                ORDER BY $orderby $order
                                LIMIT $offset, $limit");

        return $resExp;
    }

    public function typeOfSpa()
    {
        return $this->belongsTo('App\TypeOfSpa', 'type_of_institution');
    }

    public function services()
    {
        return $this->hasMany('App\ListingService', 'listing_id');
    }

    public function destinations()
    {
        return $this->belongsTo('App\Destination', 'id', 'country');
    }

    public function allCities()
    {
        return $this->belongsTo('App\Destination', 'city', 'id');
    }

    public function getSpaEmailId($listingId = '')
    {
        $result = DB::table('listings')
                    ->select('listings.email')
                    ->where('listings.id', '=', $listingId)
                    ->first();

        return $result;
    }

    public function visitors()
    {
        return $this->hasMany('App\Visitors', 'listing_id');
    }
}
