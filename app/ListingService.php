<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ListingService extends Model
{
    protected $table = "listing_service";
    protected $primaryKey = 'id';
    protected $dates = ['created_at'];
    protected $guarded = [
        'id'
    ];

    /**
     * @param  array|null $param
     * @return mixed Fetch  Details of Listing Category
     */
    public static function get_data($param = array())
    {
        $orderby = (@$param['orderby']) ? : "id";
        $order = (@$param['order']) ? : "DESC";
        $objListingService = ListingService::query();
        if (@$param['select']) {
            $objListingService = $objListingService->select($param['select']);
        }
        if (@$param['where']) {
            $objListingService = $objListingService->where($param['where']);
        }
        if (@$param['limit']) {
            $objListingService = $objListingService->take($param['limit']);
        }
        $resListingService = $objListingService->orderBy($orderby, $order)->get();
        return $resListingService;
    }

    public function service()
    {
        return $this->belongsTo('App\Service', 'service_id');
    }
}
