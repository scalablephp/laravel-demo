<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ListingMenuImageGallery extends Model {

    protected $table = "listing_menu_image_gallery";
    protected $primaryKey = 'id';
    protected $dates = ['created_at'];
    protected $guarded = [
        'id'
    ];

    /**
     * @param  array|null $param
     * @return mixed Fetch  Details of Listing Menu Image Gallery
     */
    public static function get_data($param = array()) {
        $orderby = (@$param['orderby']) ? : "id";
        $order = (@$param['order']) ? : "desc";
        $objMenuImageGallery = ListingMenuImageGallery::query();
        if (@$param['select']) {
            $objMenuImageGallery = $objMenuImageGallery->select($param['select']);
        }
        if (@$param['where']) {
            $objMenuImageGallery = $objMenuImageGallery->where($param['where']);
        }
        if (@$param['limit']) {
            $objMenuImageGallery = $objMenuImageGallery->take($param['limit']);
        }
        $resMenuImageGallery = $objMenuImageGallery->orderBy($orderby, $order)->get();
        return $resMenuImageGallery;
    }

}
