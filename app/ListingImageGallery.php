<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ListingImageGallery extends Model {

    protected $table = "listing_image_gallery";
    protected $primaryKey = 'id';
    protected $dates = ['created_at'];
    protected $guarded = [
        'id'
    ];

    /**
     * @param  array|null $param
     * @return mixed Fetch  Details of Experience Image Gallery
     */
    public static function get_data($param = array()) {
        $orderby = (@$param['orderby']) ? : "id";
        $order = (@$param['order']) ? : "desc";
        $objListingImageGallery = ListingImageGallery::query();
        if (@$param['select']) {
            $objListingImageGallery = $objListingImageGallery->select($param['select']);
        }
        if (@$param['where']) {
            $objListingImageGallery = $objListingImageGallery->where($param['where']);
        }
        if (@$param['limit']) {
            $objListingImageGallery = $objListingImageGallery->take($param['limit']);
        }
        $resListingImageGallery = $objListingImageGallery->orderBy($orderby, $order)->get();
        return $resListingImageGallery;
    }

}
