<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Award extends Model {

    protected $table = "awards";
    protected $primaryKey = 'id';
    protected $dates = ['created_at'];
    protected $guarded = [
        'id'
    ];

    /**
     * @param  array|null $param
     * @return mixed Fetch  Details of destination
     */
    public static function get_data($param = array()) {
        $orderby = (@$param['orderby']) ? : "name";
        $order = (@$param['order']) ? : "ASC";
        $objAward = Award::query();
        if (@$param['select']) {
            $objAward = $objAward->select($param['select']);
        }
        if (@$param['where']) {
            $objAward = $objAward->where($param['where']);
        }
        if (@$param['limit']) {
            $objAward = $objAward->take($param['limit']);
        }
        $resAward = $objAward->orderBy($orderby, $order)->get();
        return $resAward;
    }

}
