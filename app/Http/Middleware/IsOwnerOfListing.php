<?php

namespace App\Http\Middleware;

use App\Listings;
use Closure;
use Illuminate\Support\Facades\Auth;

class IsOwnerOfListing
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $listing = Listings::find($request->id);

        if(empty($listing->id)) {
            return abort(404);
        }

        if ($listing->author_user_id !== Auth::user()->id) {
            return abort(401);
        }

        return $next($request);
    }
}
