<?php

namespace App\Http\Middleware;

use Closure;
use App\Visitors;
use App\Listings;

class VisitorCount
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $routeParameter = $request->route()->parameters();
        $spa            = Listings::where("slug", $routeParameter['slug'])->first();
        $ip             = $request->ip();
        $visited_date   = date("Y-m-d H:i:s");
        $vistor         = Visitors::where('ip_address', '=', $ip)
                                  ->where('listing_id', '=', $spa->id)->orderBy('visited_date', 'desc')->first();
        if ( ! empty($vistor->id)) {
            $firstVisitedDate = strtotime($vistor->visited_date);
            $checkFromDate    = strtotime(date('Y-m-d H:i:s'));
            $timediff         = $checkFromDate - $firstVisitedDate;
            //dd($timediff);
            if ($timediff > 86400) {
               /* $vistor->increment('hits');
                $vistor->update([
                    'visited_date' => $visited_date,
                ]); */
                $vistorCreate = Visitors::create([
                'ip_address'   => $ip,
                'listing_id'   => $spa->id,
                'visited_date' => $visited_date,
                'hits'         => 1,
              ]);
            }
        } else {
            $vistorCreate = Visitors::create([
                'ip_address'   => $ip,
                'listing_id'   => $spa->id,
                'visited_date' => $visited_date,
                'hits'         => 1,
            ]);
        }

        return $next($request);
    }
}
