<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Redirect;
use Auth;

//use App\User;

class ReservationController extends Controller {

    public function __construct() {
        //$this->middleware(['auth', 'isAdmin']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $data = array();
        $experienceId = (@$request["hdn_experience_id"]) ? $request["hdn_experience_id"] : ((!empty(Session('experience_info')['exp_id'])) ? Session('experience_info')['exp_id'] : "");
        $exp_accomodation_id = (@$request["exp_accomodation_id"]) ? $request["exp_accomodation_id"] : ((!empty(Session('experience_info')['exp_acm_id']) && (Session('experience_info')['exp_acm_id'])) ? Session('experience_info')['exp_acm_id'] : "");
        $exp_booking_date = (@$request["booking_date"]) ? $request["booking_date"] : ((!empty(Session('experience_info')['exp_booking_date']) && (Session('experience_info')['exp_booking_date'])) ? Session('experience_info')['exp_booking_date'] : "");
        if ($experienceId) {

            $exParam['where'] = array("id" => $experienceId);
            $experience = \App\Experiences::get_data($exParam);
            $data['experience'] = (@$experience[0]) ? : array();

            $param = array("where" => array("id" => $data['experience']->center_id), "limit" => 1);
            $data['ecenter'] = \App\Centers::get_data($param);
            $data['center'] = (@$data['ecenter'][0]) ? : array();

            $data['experience_accomodation'] = array();
            if ($exp_accomodation_id) {
                $arexp_booking_date = explode(" - ", @$exp_booking_date);
                $exp_booking_start = @$arexp_booking_date[0];
                $exp_booking_end = @$arexp_booking_date[1];
                $experience_accomodations = \App\Experiences::get_exp_acm_data($data['experience']->id, @$exp_accomodation_id, @$exp_booking_start);
                $data['experience_accomodation'] = (!empty($experience_accomodations[0]) ? $experience_accomodations[0] : array());

                // Get Accomodation Gallery Images
                $data['accomodationimagegalleries'] = \App\AccomodationImageGallery::select('id', 'accomodation_id', 'image_url', 'image_title')
                                ->where("accomodation_id", $exp_accomodation_id)->get();
            }


            $pay = @$data['experience_accomodation']->room_price;

            // Early Bird Discount
            if ((!empty(@$data['experience']->eirly_bird_before_days)) && (!empty(@$data['experience']->eirly_bird_discount))) {
                $discount = (@$data['experience_accomodation']->room_price * @$data['experience']->eirly_bird_discount) / 100;
                $pay = $pay - $discount;
            }

            // Calculate Commission
            $commission = ($pay * @$data['experience']->commission) / 100;
            if (!empty(@$data['experience']->deposit_policy)) {
                switch (@$data['experience']->deposit_policy) {
                    case "2" :
                        // If Fixed Amount
                        $pay = @$data['experience']->deposit_amount + $commission;
                        break;
                    case "3" :
                        // If Percentage
                        $deposit_amount = (@$pay * @$data['experience']->deposit_amount) / 100;
                        $pay = $commission + $deposit_amount;
                        break;
                    default:
                        break;
                }
            }
            session(
                    ['experience_info' => [
                            'exp_id' => $experienceId,
                            'exp_acm_id' => $exp_accomodation_id,
                            'exp_acm_price' => @$data['experience_accomodation']->room_price,
                            'exp_cancellation_policy_condition' => @$data['experience']->cancellation_policy_condition,
                            'exp_cancellation_policy_days' => @$data['experience']->cancellation_policy_days,
                            'exp_pay_amount' => @$pay,
                            'exp_discount' => @$discount,
                            'exp_commission' => @$commission,
                            'exp_booking_date' => @$exp_booking_date
                        ]
            ]);

            $data['user_info'] = Auth::user();
            return view('reservation', $data);
        } else {
            return Redirect('experiences');
        }
    }

    /**
     * Store a newly reservation info in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $this->validate($request, [
            'arrival_date' => 'required',
            'firstname' => 'required',
            'lastname' => 'required',
            'phone' => 'required',
            'email' => 'required|email|confirmed'
        ]);

        session(['reservation_info' => [
                "firstname" => $request['firstname'],
                "lastname" => $request['lastname'],
                "email" => $request['email'],
                "phone" => $request['phone'],
                "message" => $request['message'],
                "arrival_date" => $request['arrival_date']
            ]
        ]);
        return Redirect('payment');
    }

}
