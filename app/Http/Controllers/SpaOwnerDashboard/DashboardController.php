<?php

namespace App\Http\Controllers\SpaOwnerDashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ListingComments;
use App\Listings;
use App\Visitors;
use Illuminate\Support\Facades\Auth;
use DB;

class DashboardController extends Controller
{
    public function index()
    {
    	$listings = Listings::where('author_user_id', '=', Auth::user()->id)->count();
    	$ListingComments = ListingComments::join("listings", "listings.id", "=", "listing_comments.listing_id")
                                          ->where('listings.author_user_id', '=', Auth::user()->id)->count();
        $ListingHits = Visitors::join("listings", "listings.id", "=", "visitors.listing_id")
                                          ->where('listings.author_user_id', '=', Auth::user()->id)->sum('hits');

       /* $getLast6Hits = Visitors::select('hits', DB::raw('count(*) as total'))->join("listings", "listings.id", "=", "visitors.listing_id")
                                          ->where('listings.author_user_id', '=', Auth::user()->id)->groupBy('visited_date')->orderBy('visited_date','desc')->take(6)->get();
                                        //  echo $getLast6Hits->toSql();die;
                                          echo "<pre>";dd($getLast6Hits);die; */
        $recentListingComments = ListingComments::select("listing_comments.*", "listings.name", "listings.slug")
                                          ->join("listings", "listings.id", "=", "listing_comments.listing_id")
                                          ->where('listings.author_user_id', '=', Auth::user()->id)->limit(5)->get();

        $getLast6Hits = DB::table('visitors as w')
                ->join("listings", "listings.id", "=", "w.listing_id")
                ->select(array(DB::Raw('sum(w.hits) as Day_count'),DB::Raw('DATE(w.visited_date) day'),'w.listing_id','listings.name'))
                ->where('listings.author_user_id', '=', Auth::user()->id)
                ->groupBy('day')
                ->orderBy('w.visited_date','desc')
                ->limit(7)
                ->get();

		$graphArr  = '[';
		$graphArr2 = '[';
        foreach($getLast6Hits as $lasthits) {        	
                $graphArr .= $lasthits->Day_count.',';
        }
        $graphArr .= ']';

        $getTopListingHits = DB::table('visitors as w')
                ->join("listings", "listings.id", "=", "w.listing_id")
                ->select(array(DB::Raw('sum(w.hits) as Day_count'),DB::Raw('DATE(w.visited_date) day'),'w.listing_id','listings.name'))
                ->where('listings.author_user_id', '=', Auth::user()->id)
                ->groupBy('w.listing_id')
                ->orderBy('w.visited_date','desc')
                ->limit(7)
                ->get();

                //dd($getTopListingHits);
       
        return view('spa-owner-dashboard.dashboard.index',compact('ListingComments','listings','ListingHits','graphArr','getLast6Hits','getTopListingHits','recentListingComments'));
    }

    public function updategraph(Request $request )
    {

    $startDate = date('Y-m-d', strtotime($request->startDate));
    $endDate = date('Y-m-d', strtotime($request->endDate));
    $getHits = DB::table('visitors as w')
                ->join("listings", "listings.id", "=", "w.listing_id")
                ->select(array(DB::Raw('sum(w.hits) as Day_count'),DB::Raw('DATE(w.visited_date) day'),'w.listing_id','listings.name'))
                ->where('listings.author_user_id', '=', Auth::user()->id)
                ->where('w.visited_date', '>=', $startDate)
                ->where('w.visited_date', '<=', $endDate)
               // ->whereBetween('w.visited_date', [$startDate, $endDate])
                ->groupBy('day')
                ->orderBy('w.visited_date','desc')
                ->limit(10)
                ->get();
    $graphArr  = '';
       // $graphArr2 = '[';
    $graphArr2 = '';
    $i =1;
        foreach($getHits as $lasthits) {           
                
                if($i == count($getHits)) {
                    $graphArr2 .= $lasthits->day;
                    $graphArr .= $lasthits->Day_count;
                } else {
                    $graphArr2 .= $lasthits->day.',';
                    $graphArr .= $lasthits->Day_count.',';
                }
                
        $i++;
        }
      //  $graphArr .= ']';
       // $graphArr2 .= ']';

    $data['graphdata'] = $graphArr;
    $data['graphlabel'] = $graphArr2;
    echo json_encode($data);
    exit();
    }
}
