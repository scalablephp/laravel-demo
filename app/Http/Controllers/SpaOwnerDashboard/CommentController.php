<?php

namespace App\Http\Controllers\SpaOwnerDashboard;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\ListingComments;
use App\Listings;

class CommentController extends Controller
{
    public function index()
    {
        $ListingComments = ListingComments::select("listing_comments.*", "listings.name", "listings.slug")
                                          ->join("listings", "listings.id", "=", "listing_comments.listing_id")
                                          ->where('listings.author_user_id', '=', Auth::user()->id)->get();

        //dd($ListingComments);

        return view('spa-owner-dashboard.comments.index', compact('ListingComments'));
    }

    public function create()
    {
        $data             = [];
        $data['listings'] = \App\Listings::orderBy("name", "ASC")->get();

        return view('spa-owner-dashboard.comments.create', $data);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        //dd($request);
        if (! empty($request['id'])) {
            $comment = ListingComments::find($request['id']);
            if ($comment) {
                $comment->comment_approved = ! empty($request['comment_approved']) ? 1 : 0;
                try {
                    $comment->save();
                } catch (Exception $e) {
                    return redirect(route('lstC.index'))
                        ->with('flash_error_message', 'Something went wrong');
                }
            } else {
                return redirect(route('lstC.index'))
                    ->with('flash_error_message', 'Something went wrong');
            }

            return redirect(route('lstC.index'))
                ->with('flash_message', 'Comment updated');
        } else {
            $this->validate($request, [
                'listing_id'      => 'required',
                'comment_content' => 'required',
            ]);
            try {
                $comment                       = new ListingComments();
                $comment->comment_author       = $request['comment_author_email'];
                $comment->comment_author_email = $request['comment_author_email'];
                $comment->listing_id           = $request['listing_id'];
                $comment->comment_content      = $request['comment_content'];
                $comment->comment_approved     = 1;
                $comment->comment_date         = \Carbon\Carbon::now()->format("Y-m-d H:i:s");
                $comment->comment_date_gmt     = \Carbon\Carbon::now()->format("Y-m-d H:i:s");
                $comment->save();
            } catch (\Exception $e) {
                return redirect(route('lstC.index'))
                    ->with('flash_error_message', 'Something went wrong');
            }

            return redirect(route('lstC.index'))
                ->with('flash_message', 'Comment Created');
        }
    }

    /**
     * @param string $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id = '')
    {
        if (! $id) {
            redirect(route('lstC.index'));
        }

        $listingComment = ListingComments::select("listing_comments.*", "listings.name", "listings.slug")
                                         ->join("listings", "listings.id", "=", "listing_comments.listing_id")
                                         ->where("listing_comments.id", $id)
                                         ->where('listings.author_user_id', '=', Auth::user()->id)
                                         ->orderBy("listing_comments.id", "DESC")->first();

        //dd($listingComment);

        return view('spa-owner-dashboard.comments.edit', compact('listingComment'));
    }

    public function getlistings()
    {
        $json_data = [];
        $search    = Request()->input('search');
        if (! empty($search)) {
            $objListings = Listings::select("id", "name")
                                   ->where('author_user_id', '=', Auth::user()->id)
                                   ->where("name", 'like', '%' . $search . '%')->get();
            if (sizeof($objListings) > 0) {
                foreach ($objListings as $objListing) {
                    array_push($json_data, ["id" => $objListing->id, "text" => $objListing->name]);
                }
            }
        }
        echo json_encode($json_data);
    }
}
