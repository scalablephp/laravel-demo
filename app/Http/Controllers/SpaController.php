<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Filters\Spa\SpaFilter;
use DB;
use Storage;
use Mail;
use Carbon\Carbon;
use Redirect;

//use App\Rules\Captcha;
//use Illuminate\Validation\Rule;

class SpaController extends Controller {

    private $spaEmailID;

    public function __construct() {
        //$this->middleware(['auth', 'isAdmin']);
    }

    public function listspa($id) {
        $data['list_by_id'] = \App\Listings::find($id);

        return view('web/frontend/home', $data);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $data = [];

        $order = "DESC";
        $orderby = "total_score";
        if (@$_GET['sort_by']) {
            switch (@$_GET['sort_by']) {
                case "price_asc":
                    $order = "ASC";
                    $orderby = "room_price";
                    break;
                case "price_desc":
                    $order = "DESC";
                    $orderby = "room_price";
                    break;
                case "ranking":
                    $order = "DESC";
                    $orderby = "updated_at";
                    break;
                default:
                    $order = "DESC";
                    $orderby = "updated_at";
                    break;
            }
        }
        $data['querystring'] = request()->all();
        $objListings = \App\Listings::select("listings.*", "cnt.name as countryname", "ct.name as cityname")->leftJoin("destination as cnt", "listings.country", "=", "cnt.id")->leftJoin("destination as ct", "listings.city", "=", "ct.id")->where("is_draft", 0)->orderBy($orderby, $order);
        $objListingsTotal = clone $objListings;
        $data['spas'] = $objListings->paginate(12);
        $data['total_spas'] = $objListingsTotal->get()->count();

        $data['types'] = \App\TypeOfSpa::withCount('listings')->having("listings_count", ">", 0)->orderBy('listings_count', 'desc')->get();
        $data['services'] = \App\Service::select("id", "name")->where('parent', 0)->with('subServices')->get();
        //$data['destinations']   = \App\Destination::select("id", "name")->where("parent", 0)->orderBy("name", "asc")->get();
        $objCities = \App\Destination::select("destination.*", "listings.country", "cnt.name as countryname", DB::raw("COUNT(`listings`.`id`) as total_listings"))
                        ->join("listings", "listings.city", "=", "destination.id")
                        ->leftJoin("destination as cnt", "listings.country", "=", "cnt.id")
                        ->groupBy("listings.city")->orderBy("countryname", "destination.name")->get();
        $data['destinations'] = [];
        foreach ($objCities as $objCity) {
            $data['destinations'][$objCity->country][] = $objCity;
            @$data['destinations'][$objCity->country]['total_listings'] += @$objCity->total_listings;
        }


        $data['searchType'] = "Trending List";

        return view('spa', $data);
    }

    public function detail($slug = '') {
        $data = [];
        if ($slug) {
            $exParam['where'] = ["slug" => $slug];

            if (!isset($_GET['preview']) or ( $_GET['preview'] != 1)) {
                $exParam['where']["is_draft"] = 0;
            }

            $spa = \App\Listings::where("slug", $slug)->first();
            $data['spa'] = (!empty($spa)) ? $spa : [];
            $data['hits_count'] = $spa->visitors()->sum('hits');
            if (!empty($spa)) {
                // Get Listing Gallery Images
                $paramLGI['select'] = ['id', 'listing_id', 'image_url', 'image_title'];
                $paramLGI['where'] = ["listing_id" => @$data['spa']->id];
                $data['imagegalleries'] = \App\ListingImageGallery::get_data($paramLGI);

                // Get Food Gallery Images
                $data['foodimagegalleries'] = \App\ListingFoodImageGallery::select('id', 'image_url', 'image_title')
                                ->where("listing_id", @$data['spa']->id)->get();

                // Get accomodation Gallery Images
                $data['accomodationimagegalleries'] = \App\ListingAccomodationImageGallery::select('id', 'image_url', 'image_title')->where("listing_id", @$data['spa']->id)->get();

                // Get Menu Gallery Images
                $data['menuimagegalleries'] = \App\ListingMenuImageGallery::select('id', 'image_url', 'image_title')
                                ->where("listing_id", @$data['spa']->id)->get();

                $services = \App\ListingService::select("services.*")
                                ->Join('services', function ($join) {
                                    $join->on('services.id', '=', 'listing_service.service_id');
                                })->where("listing_service.listing_id", $data['spa']->id)
                                ->orderBy("name", "ASC")->distinct()->get();

                $arservice = [];
                $parentServices = [];
                if ($services) {
                    foreach ($services as $service) {
                        $arservice[$service->parent][] = $service;
                    }

                    $parentServices = \App\Service::select("id", "name")->whereIn("id", array_keys($arservice))->get();
                }
                $data['services'] = $arservice;
                $data['parentServices'] = $parentServices;

                $objMenus = \App\ListingMenu::where("listing_id", $data['spa']->id)->orderBy("duration", "ASC")->orderBy("menu_price", "ASC")->distinct()->get();

                $data['menus'] = [];
                if (sizeof($objMenus) > 0) {
                    foreach ($objMenus as $objMenu) {
                        $data['menus'][$objMenu->classification][] = $objMenu;
                    }
                }


                // Get Listing Comments
                $paramLC['where'] = ["listing_id" => @$data['spa']->id, "comment_approved" => 1];
                $paramLC['order'] = "DESC";
                $paramLC['orderby'] = "comment_date";
                $data['comments'] = \App\ListingComments::get_data($paramLC);

                if (!empty(@$data['spa']->awards)) {
                    $data['awards'] = \App\Award::whereIn("id", explode("||", @$data['spa']->awards))->get();
                }

                if (!empty(@$data['spa']->country)) {
                    $data['country'] = \App\Destination::where("id", $data['spa']->country)->orderBy("id", "ASC")->first();
                }

                if (!empty(@$data['spa']->city)) {
                    $data['city'] = \App\Destination::where("id", $data['spa']->city)->orderBy("id", "ASC")->first();
                }

                return view('spa_detail', $data);
            } else {
                return redirect("/spa");
            }
        }
    }

    public function listFilter(Request $request) {
        $request->flash();

        // Sorting Start
        $sortBy = "listings.total_score";
        $sort = "DESC";
        $data['sortby'] = "popular";
        if (!empty(request()->input('sortby'))) {
            $data['sortby'] = request()->input('sortby');
            switch (request()->input('sortby')) {
                case "popular":
                    $sortBy = "listings.total_score";
                    $sort = "DESC";
                    break;
                case "new":
                    $sortBy = "listings.created_at";
                    $sort = "DESC";
                    break;
                default :
            }
        }

        $data['querystring'] = request()->all();
        $data['spas'] = SpaFilter::apply($request)->orderBy($sortBy, $sort)->paginate(10);
        $data['types'] = \App\TypeOfSpa::withCount('listings')->having("listings_count", ">", 0)->orderBy('listings_count', 'desc')->get();
        $data['services'] = \App\Service::select("id", "name")->where('parent', 0)->with('subServices')->get();
        //$data['destinations'] = \App\Destination::select("id", "name")->where("parent", 0)->orderBy("name", "asc")->get();

        $objCities = \App\Destination::select("destination.*", "listings.country", "cnt.name as countryname", DB::raw("COUNT(`listings`.`id`) as total_listings"))
                        ->join("listings", "listings.city", "=", "destination.id")
                        ->leftJoin("destination as cnt", "listings.country", "=", "cnt.id")
                        ->groupBy("listings.city")->orderBy("countryname", "destination.name")->get();
        $data['destinations'] = [];
        foreach ($objCities as $objCity) {
            $data['destinations'][$objCity->country][] = $objCity;
            @$data['destinations'][$objCity->country]['total_listings'] += @$objCity->total_listings;
        }

        $data['searchType'] = "Filter:";

        //dd($data); die();
        return view('spa', $data);
    }

    /**
     * Save Listing Comment
     *
     * @param Request $request
     *
     * @return string
     */
    public function savecomment(Request $request) {

        $captcha = request()->input('g-recaptcha-response');
        if ($captcha == '') {
            $response = 'Please check captcha field!';

            return $response;
        }

        $this->validate(
            $request, [
            'name'                 => 'required',
            'email'                => 'required',
            'comment'              => 'required',
            'g-recaptcha-response' => 'required|captcha',
        ], [
                'email.required' => 'Email Id field cannot leave blank',
                'email.unique'   => 'Your record already exist in our database',
            ]
        );

        $comment_author = request()->input('name');
        $comment_author_email = request()->input('email');
        $comment_content = request()->input('comment');
        $comment_author_country = !empty(request()->input('country')) ? request()->input('country') : null ;
        $comment_author_city = !empty(request()->input('city')) ? request()->input('city') : null;
        $rating = request()->input('rating');
        $listing_id = request()->input('listing_id');
        $user_id = 0;

        if (!empty($comment_author_email) && !empty($comment_content)) {
            try {
                // echo "try called";
                $objComments = new \App\ListingComments();
                $objComments->comment_author = $comment_author;
                $objComments->comment_author_email = $comment_author_email;
                $objComments->comment_content = $comment_content;
                $objComments->comment_author_country = $comment_author_country;
                $objComments->comment_author_city = $comment_author_city;
                $objComments->rating = $rating;
                $objComments->user_id = $user_id;
                $objComments->listing_id = $listing_id;
                //$objComments->comment_date = \Carbon\Carbon::now()->format("Y-m-d H:i:s");
                //$objComments->comment_date_gmt = \Carbon\Carbon::now()->format("Y-m-d H:i:s");
                $objComments->save();
                /*   Mail::send('emails.review_comments', ['name' => @$name, 'email' => @$comment_author_email, 'comments' => @$comment_content, 'rating' => @$rating], function ($message) use($email) {
                  $message->subject("Review and Comments");
                  $message->from('sumitkhera87@gmail.com');
                  $message->to('sumitkhera87@gmail.com', 'Balanceboat');
                  }); */
            } catch (Exception $ex) {

            }
        }
    }

    /* Import listing images from azure
     *
     * @param Request Parameters
     * @return \Illuminate\Http\Response
     */

    public function import_azure_listing_images(Request $request) {
        $imagegalleries = [];

        $country = $request['cn'];
        $city = $request['ct'];
        $objCountry = DB::select("select `id` from `destination` where `name` = '$country'");
        $objCity = DB::select("select `id` from `destination` where `name` = '$city'");

        $city_id = $objCity[0]->id;
        $country_id = $objCountry[0]->id;
        $imagedir = Storage::disk('azure')->directories($country . "/" . $city);
        $update = [];
        foreach ($imagedir as $dir) {
            $data = DB::select("select `id` from `listings` where city = '$city_id' and country = '$country_id' and '" . addslashes(basename($dir)) . "' like  CONCAT('%', meta_title, '%')");
            if (sizeof($data) > 0) {
                $listing_id = $data[0]->id;
                array_push($update, $listing_id);

                $banner_image_url = "";
                $tileImages = Storage::disk('azure')->files($country . "/" . $city . "/Tile Images/");
                foreach ($tileImages as $tileimage) {
                    if (pathinfo($tileimage, PATHINFO_FILENAME) == addslashes(basename($dir))) {
                        $banner_image_url = addslashes($tileimage);
                        break;
                    }
                }
                // filter the ones that match the filename.*
                //$banner_image_url =  Storage::disk('azure')->url($country."/".$city."/Tile Images/".addslashes(basename($dir)).".jpg");
                if ($banner_image_url) {
                    DB::select("update listings set banner_image_url = '" . addslashes($banner_image_url) . "' where `id` = '" . $listing_id . "'");
                }
                //DB::select("delete from `listing_image_gallery` where `listing_id` = '".$listing_id."'");
                $ligdata = DB::select("select `id` from `listing_image_gallery` where `listing_id` = '" . $listing_id . "'");
                if (sizeof($ligdata) == 0) {
                    $image_galleries_array = Storage::disk('azure')->files($dir);
                    foreach ($image_galleries_array as $galimage) {
                        $objListingImageGallery = new \App\ListingImageGallery();
                        $objListingImageGallery->listing_id = $listing_id;
                        $objListingImageGallery->image_title = addslashes(basename($galimage));
                        $objListingImageGallery->image_url = addslashes($galimage);
                        $objListingImageGallery->save();
                    }
                }
            }
        }
        echo "<pre/>";
        print_r($update);
    }

    /* Import listing Spa images from azure
     *
     * @param Request Parameters
     * @return \Illuminate\Http\Response
     */

    public function import_azure_listing_menu_images(Request $request) {
        $imagegalleries = [];

        $country = $request['cn'];
        $city = $request['ct'];
        $objCountry = DB::select("select `id` from `destination` where `name` = '$country'");
        $objCity = DB::select("select `id` from `destination` where `name` = '$city'");

        $city_id = $objCity[0]->id;
        $country_id = $objCountry[0]->id;
        $imagedir = Storage::disk('azure')->directories($country . "/" . $city);
        $update = [];
        foreach ($imagedir as $dir) {
            $data = DB::select("select `id` from `listings` where city = '$city_id' and country = '$country_id' and '" . addslashes(basename($dir)) . "' like  CONCAT('%', meta_title, '%')");
            if (sizeof($data) > 0) {
                $listing_id = $data[0]->id;
                array_push($update, $listing_id);
                
                //DB::select("delete from `listing_menu_image_gallery` where `listing_id` = '".$listing_id."'");
                $ligdata = DB::select("select `id` from `listing_menu_image_gallery` where `listing_id` = '" . $listing_id . "'");
                if (sizeof($ligdata) == 0) {
                    $image_galleries_array = Storage::disk('azure')->files($dir."/Spa Menu/");
                    if (sizeof($image_galleries_array) > 0) {
                        foreach ($image_galleries_array as $galimage) {
                            $objListingImageGallery = new \App\ListingMenuImageGallery();
                            $objListingImageGallery->listing_id = $listing_id;
                            $objListingImageGallery->image_title = addslashes(basename($galimage));
                            $objListingImageGallery->image_url = addslashes($galimage);
                            $objListingImageGallery->save();
                        }
                    }
                }
            }
        }
        echo "<pre/>";
        print_r($update);
    }

    /**
     * Save Listing Comment
     *
     * @param Request Parameters
     *
     * @return \Illuminate\Http\Response
     */
    public function storenquiry(Request $request) {
        $captcha = request()->input('g-recaptcha-response');
        if ($captcha == '') {
            $response = 'Please check captcha field!';

            return $response;
        }

        $name = request()->input('name');
        $lastname = request()->input('lastname');
        $email = request()->input('email');
        $phone = request()->input('phone');
        $comment = request()->input('comment');
        $conversationId = 'enq' . rand() . time();
        $spaId = request()->input('spaid');
        $listingObj = new \App\Listings();
        $listingEmail = $listingObj->getSpaEmailId($spaId);
        $spaEmailId = trim($listingEmail->email);
        $this->spaEmailID = $spaEmailId;

        $objComments = new \App\inquiry();
        $objComments->name = $name;
        $objComments->lastname = $lastname;
        $objComments->email = $email;
        $objComments->phone = $phone;
        $objComments->message = $comment;
        $objComments->conversation_id = $conversationId;
        $objComments->save();
        $replyIdforcustomer = $conversationId . '-1';
        $replyIdforcenter = $conversationId . '-2';

        DB::table('customer_message')->insert(
                [
                    'conversation_id' => $conversationId,
                    'listingid' => $spaId,
                    'message_type' => '1',
                    'message' => $comment,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ]
        );


        try {
            Mail::send('emails.spainquiry', [
                'firstname' => @$name,
                'lastname' => @$lastname,
                'email' => @$email,
                'phone' => @$phone,
                'bodymessage' => @$comment,
                'conversation' => @$replyIdforcenter,
                    ], function ($message) use ($email) {
                $message->subject("Inquiry");
                $message->from('support@balanceseek.com');
                $message->to('support@balanceseek.com', 'Balanceseek');
            });
        } catch (Exception $ex) {

        }

        try {
            Mail::send('emails.spainquirycustomer', [
                'firstname' => @$name,
                'lastname' => @$lastname,
                'bodymessage' => @$comment,
                'conversation' => @$replyIdforcustomer,
                    ], function ($message) use ($email) {
                $message->subject("Inquiry");
                $message->from('support@balanceseek.com');
                $message->to(@$email, 'Balanceseek');
            });
        } catch (Exception $ex) {

        }

        try {
            Mail::send('emails.spainquiryorganizer', [
                'firstname' => @$name,
                'lastname' => @$lastname,
                'email' => @$email,
                'phone' => @$phone,
                'bodymessage' => @$comment,
                'conversation' => @$replyIdforcenter,
                    ], function ($message) use ($email) {
                $message->subject("Inquiry");
                $message->from('support@balanceseek.com');
                $message->to(@$this->spaEmailID, 'Balanceseek');
            });
        } catch (Exception $ex) {

        }
    }

    public function add_your_spa() {
        //echo "add your spa";
        return view('spa_add_form');
    }

    public function spa_listing_enquiry_submission(Request $request) {

        $this->validate(
                $request, [
            'pname' => 'required',
            'spaname' => 'required',
            'spalocation' => 'required',
            'phone' => 'required',
            'email' => 'required|unique:spa_listing_enquiry,email',
            'g-recaptcha-response' => 'required|captcha',
            'message' => 'required',
                // 'g-recaptcha-response' => new Captcha(),
                ], [
            'email.required' => 'Email Id field cannot leave blank',
            'email.unique' => 'Your record already exist in our database',
                ]
        );

        $pname = $request->has('pname') ? $request->input('pname') : "";
        $spaname = $request->has('spaname') ? $request->input('spaname') : "";
        $spalocation = $request->has('spalocation') ? $request->input('spalocation') : "";
        $weburl = $request->has('weburl') ? $request->input('weburl') : "";
        $phone = $request->has('phone') ? $request->input('phone') : "";
        $email = $request->has('email') ? $request->input('email') : "";
        $message = $request->has('message') ? $request->input('message') : "";


        if (!empty(@$pname) && !empty(@$spaname) && !empty(@$spalocation) && !empty(@$phone) && !empty(@$email)) {
            try {
                $spalistingenquiry = new \App\SpaListingEnquiry();
                $spalistingenquiry->pname = $pname;
                $spalistingenquiry->spaname = $spaname;
                $spalistingenquiry->spalocation = $spalocation;
                $spalistingenquiry->weburl = $weburl;
                $spalistingenquiry->phone = $phone;
                $spalistingenquiry->email = $email;
                $spalistingenquiry->message = $message;
                $spalistingenquiry->created_at = \Carbon\Carbon::now()->format("Y-m-d H:i:s");
                $spalistingenquiry->updated_at = \Carbon\Carbon::now()->format("Y-m-d H:i:s");

                $spalistingenquiry->save();


                Mail::send('emails.spaenquiryemail2', [
                    'pname' => @$pname,
                    'spaname' => @$spaname,
                    'spalocation' => @$spalocation,
                    'weburl' => @$weburl,
                    'phone' => @$phone,
                    'email' => @$email,
                    'message' => @$message,
                        ], function ($message) use ($email) {
                    $message->subject("Received enquiry for add Spa listing");
                    $message->from("support@balanceseek.com");
                    $message->to('support@balanceseek.com', 'Balanceboat');
                });

                return Redirect::back()->with('flash_message', 'Your Spa listing enquiry has been submitted successfully. We will contact you very soon!');
                // return redirect("/")->with('flash_message', 'Your Spa listing enquiry has been submitted successfully. We will contact you very soon!');
            } catch (Exception $ex) {
                return redirect("/")->with('flash_error_message', 'Something went wrong. Please try again');
            }
        }
    }

    public function searchresult(Request $request) {
        $request->flash();

        // Sorting Start
        $sortBy = "listings.total_score";
        $sort = "DESC";
        $data['sortby'] = "popular";
        if (!empty(request()->input('sortby'))) {
            $data['sortby'] = request()->input('sortby');
            switch (request()->input('sortby')) {
                case "popular":
                    $sortBy = "listings.total_score";
                    $sort = "DESC";
                    break;
                case "new":
                    $sortBy = "listings.created_at";
                    $sort = "DESC";
                    break;
                default :
            }
        }

        $data['querystring'] = request()->all();
        $data['spas'] = SpaFilter::apply($request)->orderBy($sortBy, $sort)->paginate(10);
        $data['types'] = \App\TypeOfSpa::withCount('listings')->having("listings_count", ">", 0)->orderBy('listings_count', 'desc')->get();
        $data['services'] = \App\Service::select("id", "name")->where('parent', 0)->with('subServices')->get();
        //$data['destinations'] = \App\Destination::select("id", "name")->where("parent", 0)->orderBy("name", "asc")->get();

        $objCities = \App\Destination::select("destination.*", "listings.country", "cnt.name as countryname", DB::raw("COUNT(`listings`.`id`) as total_listings"))
                        ->join("listings", "listings.city", "=", "destination.id")
                        ->leftJoin("destination as cnt", "listings.country", "=", "cnt.id")
                        ->groupBy("listings.city")->orderBy("countryname", "destination.name")->get();
        $data['destinations'] = [];
        foreach ($objCities as $objCity) {
            $data['destinations'][$objCity->country][] = $objCity;
            @$data['destinations'][$objCity->country]['total_listings'] += @$objCity->total_listings;
        }

        $data['searchType'] = "Filter:";

        //dd($data); die();
        return view('spa', $data);
    }
    
    public function quicksearch(Request $request) {
        $data = [];

        $order = "DESC";
        $orderby = "total_score";
        if (@$_GET['sort_by']) {
            switch (@$_GET['sort_by']) {
                case "price_asc":
                    $order = "ASC";
                    $orderby = "room_price";
                    break;
                case "price_desc":
                    $order = "DESC";
                    $orderby = "room_price";
                    break;
                case "ranking":
                    $order = "DESC";
                    $orderby = "updated_at";
                    break;
                default:
                    $order = "DESC";
                    $orderby = "updated_at";
                    break;
            }
        }
        $data['querystring'] = request()->all();
        $objListings = \App\Listings::select("listings.*", "cnt.name as countryname", "ct.name as cityname")->leftJoin("destination as cnt", "listings.country", "=", "cnt.id")->leftJoin("destination as ct", "listings.city", "=", "ct.id")->where("is_draft", 0)->orderBy($orderby, $order);
        $objListingsTotal = clone $objListings;
        $data['spas'] = $objListings->paginate(12);
        $data['total_spas'] = $objListingsTotal->get()->count();

        $data['types'] = \App\TypeOfSpa::withCount('listings')->having("listings_count", ">", 0)->orderBy('listings_count', 'desc')->get();
        $data['services'] = \App\Service::select("id", "name")->where('parent', 0)->with('subServices')->get();
        //$data['destinations']   = \App\Destination::select("id", "name")->where("parent", 0)->orderBy("name", "asc")->get();
        $objCities = \App\Destination::select("destination.*", "listings.country", "cnt.name as countryname", DB::raw("COUNT(`listings`.`id`) as total_listings"))
                        ->join("listings", "listings.city", "=", "destination.id")
                        ->leftJoin("destination as cnt", "listings.country", "=", "cnt.id")
                        ->groupBy("listings.city")->orderBy("countryname", "destination.name")->get();
        $data['destinations'] = [];
        foreach ($objCities as $objCity) {
            $data['destinations'][$objCity->country][] = $objCity;
            @$data['destinations'][$objCity->country]['total_listings'] += @$objCity->total_listings;
        }


        


        $data['searchType'] = "Trending List";
        return view('quicksearch', $data);
    }

    function nearspa($to = '', $from = '') {
        $latitude = $from;
        $longitude = $to;
        $distance = 200;
        $circle_radius = 111.045;
        $objListings = \App\Listings::Query();

        $listings = DB::select(
               'SELECT * FROM
                    (SELECT id, name, slug, about_us, latitude, longitude, (' . $circle_radius . ' * DEGREES(ACOS(COS(RADIANS(' . $latitude . ')) * COS(RADIANS(latitude)) * COS(RADIANS(longitude) - RADIANS(' . $longitude . ')) + SIN(RADIANS(' . $latitude . ')) * SIN(RADIANS(latitude)))))
                    AS distance
                    FROM listings) AS distances
                WHERE distance < ' . $distance . '
                ORDER BY distance
                LIMIT 3;
            ');
        $html = '';
        foreach($listings as $listing) {
            $html .= '<div class="place-post list-style"><div class="place-post__gal-box">';
            $html .= '<a href="'.$listing->slug.'"><img class="place-post__image" src="https://balanceseekimages.blob.core.windows.net/assets/India/Goa/Tile Images/69 Happy Ending Classic Spa Calangute Goa.jpg" alt=""></a></div>';
            $html .= '<div class="place-post__content"><h2 class="place-post__title">';
            $html .= '<a href="http://localhost.balanceboat.com/spa/69-happy-ending-classic-spa-calangute-goa">'.$listing->name.'</a></h2>'; 
            $html .= '<p class="place-post__address">
                                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                                    '.$listing->distance.' km
                                </p>';
            $html .= '<p class="place-post__info"><i class="fa fa-clock-o" aria-hidden="true"></i>';
            $html .= '<span class="open">Open</span> until 8:00 PM</p></div></div>';            

        }
        echo $html;
             exit();

    }

}
