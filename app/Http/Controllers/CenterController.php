<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

//use App\User;

class CenterController extends Controller {

    public function __construct() {
        //$this->middleware(['auth', 'isAdmin']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($slug = '') {
        $data = array();
        if ($slug) {
            $cParam['where'] = array("slug" => $slug);
            $center = \App\Centers::get_data($cParam);
            $data['center'] = (@$center[0]) ? : array();

            if (sizeof(@$data['center']) > 0) {

                $paramCRT['order'] = "ASC";
                $paramCRT['orderby'] = "name";
                $data['certificates'] = \App\Certificates::get_data($paramCRT);

                // Get Center Type
                $data['center_type'] = "";
                if ($data['center']->center_type) {
                    $paramCT['order'] = "ASC";
                    $paramCT['orderby'] = "id";
                    $paramCT['where'] = array("id" => $data['center']->center_type);
                    $data['center_type'] = \App\CenterTypes::get_data($paramCT)[0]->name;
                }

                // Get Center Gallery Images
                $paramCGI['select'] = array('id', 'center_id', 'image_url', 'image_title');
                $paramCGI['where'] = array("center_id" => $data['center']->id);
                $data['imagegalleries'] = \App\CenterImageGallery::get_data($paramCGI);

                // Get Center Accomodations
                $data['center_accomodations'] = \App\CenterAccomodations::select("accomodation_id")
                                ->where("center_id", $data['center']->id)->get();

                $data['accomodationimagegalleries'] = array();
                if (sizeof($data['center_accomodations']) > 0) {
                    $acmIds = array();
                    foreach ($data['center_accomodations'] as $ca_acm_image) {
                        array_push($acmIds, $ca_acm_image->accomodation_id);
                    }

                    if ($acmIds) {
                        // Get Accomodation Gallery Images
                        $data['accomodationimagegalleries'] = \App\AccomodationImageGallery::select('id', 'accomodation_id', 'image_url', 'image_title')
                                        ->whereIn("accomodation_id", $acmIds)->get();
                    }
                }

                // Get Center Specialities
                $data['center_specialities'] = array();
                if (!empty($data['center']->speciality_id)) {
                    $data['center_specialities'] = \App\Expertise::select('name')->whereIn("id", explode("||", $data['center']->speciality_id))->get();
                }

                // Get experience Teachers
                $data['experience_teachers'] = \App\Teachers::select("teachers.id", "teachers.name", "teachers.profile_image_url", "teachers.short_description", "teachers.slug", "teachers.certificate_id")
                                ->join("center_teachers", "teachers.id", "=", "center_teachers.teacher_id")
                                ->where("center_teachers.center_id", $data['center']->id)->distinct()->get();

                // Get Center Locations
                $data['center_locations'] = \App\Category::select("category.*")
                                ->join("center_locations", "center_locations.location_id", "=", "category.id")
                                ->where("center_id", $data['center']->id)->get();

                // Get Center Experiences
                $cnd = ' AND e.center_id = "' . @$data['center']->id . '" ';
                $data["center_experiences"] = \App\Experiences::get_exp_price_data($cnd);

                return view('center_detail', $data);
            } else {
                return redirect("/experiences");
            }
        } else {
            $data = array();
            $exParam['order'] = "DESC";
            $exParam['orderby'] = "updated_at";
            $exParam['limit'] = "10";
            $data['experiences'] = \App\Centers::get_data($exParam);
            return view('centers', $data);
        }
    }

}
