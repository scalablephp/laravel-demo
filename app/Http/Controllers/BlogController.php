<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

//use App\User;

class BlogController extends Controller {

    public function __construct() {
        //$this->middleware(['auth', 'isAdmin']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($slug = '') {
        $data = array();
        if ($slug) {
            $exBlog['where'] = array("slug" => $slug);
            if (!isset($_GET['preview']) or ( $_GET['preview'] != 1)) {
                $exBlog['where']["is_draft"] = 0;
            }
            $blog = \App\Blog::get_data($exBlog);
            $data['blog'] = (@$blog[0]) ? @$blog[0] : array();
            if (!empty(@$data['blog'])) {
                
                $order = "DESC";
                $orderby = "updated_at";
                $blogParam['order'] = $order;
                $blogParam['orderby'] = $orderby;
                $blogParam['limit'] = "5";
                $blogParam['where'] = array("is_draft" => 0);
                $data['blogs'] = \App\Blog::get_data($blogParam);
                
                return view('blog_details', $data);
            } else {
                return redirect("/blog");
            }
        } else {
            $data = array();
            $order = "DESC";
            $orderby = "updated_at";
            $blogParam['order'] = $order;
            $blogParam['orderby'] = $orderby;
            $blogParam['limit'] = "10";
            $blogParam['where'] = array("is_draft" => 0,"parent_id" => 0);
            $data['blogs'] = \App\Blog::get_data($blogParam);
            return view('blog', $data);
        }
    }

    /**
     * Display a listing of the resource on Load more.
     *
     * @return \Illuminate\Http\Response
     */
    public function loadDataAjax(Request $request) {
        $data = array();

        $sdest = (!empty($request['sdestination'])) ? $request['sdestination'] : "";
        $scat = (!empty($request['scategory'])) ? $request['scategory'] : "";
        $sedate = (!empty($request['sexp_date'])) ? \Carbon\Carbon::parse($request['sexp_date'])->format("Y-m-d") : "";
        $objDestinations = \App\ExperienceCategory::select("experience_id")->where("category_id", '36')->distinct()->get();
        $objCategories = \App\ExperienceCategory::select("experience_id")->whereIn('experience_id', @$objDestinations)->where("category_id", 1)->distinct()->get();

        $order = "DESC";
        $orderby = "updated_at";
        if (@$_GET['sort_by']) {
            switch (@$_GET['sort_by']) {
                case "price_asc":
                    $order = "ASC";
                    $orderby = "price_per_person";
                    break;
                case "price_desc":
                    $order = "DESC";
                    $orderby = "price_per_person";
                    break;
                case "ranking":
                    $order = "DESC";
                    $orderby = "updated_at";
                    break;
                default :
                    $order = "DESC";
                    $orderby = "updated_at";
                    break;
            }
        }
        $offset = 0;
        $limit = 10;
        if ($request['page']) {
            $offset = ($request['page'] * 10) - 10;
            $limit = $request['page'] * 10;
        }
        $objExp = \App\Experiences::whereIn('id', @$objCategories);
        if ($sedate) {
            $sedateto = \Carbon\Carbon::parse($request['sexp_date'])->addDays(15)->format("Y-m-d");
            $objExp = $objExp->whereBetween("start_date_time", array($sedate, $sedateto));
        }
        $resExp = $objExp->where("is_draft", 0)
                ->skip($offset)
                ->take($limit)
                ->orderBy($orderby, $order)
                ->distinct()
                ->get();
        $data['experiences'] = $resExp;
        $data['sort_by'] = (!empty(@$_GET['sort_by'])) ? @$_GET['sort_by'] : "newest";
        $view = view('content-experience-ajax', $data)->render();
        return response()->json(['html' => $view]);
    }

    /**
     * Get Experience by Request Parameters
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request) {
        $data = array();
        $sdest = (!empty($request['sdestination'])) ? $request['sdestination'] : "";
        $scat = (!empty($request['scategory'])) ? $request['scategory'] : "";
        $sedate = (!empty($request['sexp_date'])) ? \Carbon\Carbon::parse($request['sexp_date'])->format("Y-m-d") : "";
        $objDestinations = \App\ExperienceCategory::select("experience_id")->where("category_id", @$sdest)->distinct()->get();
        if (!empty($scat)) {
            $objCategories = \App\ExperienceCategory::select("experience_id")->whereIn('experience_id', @$objDestinations)->where("category_id", @$scat)->distinct()->get();
        } else {
            $objCategories = $objDestinations;
        }
        $order = "DESC";
        $orderby = "updated_at";
        if (@$_GET['sort_by']) {
            switch (@$_GET['sort_by']) {
                case "price_asc":
                    $order = "ASC";
                    $orderby = "price_per_person";
                    break;
                case "price_desc":
                    $order = "DESC";
                    $orderby = "price_per_person";
                    break;
                case "ranking":
                    $order = "DESC";
                    $orderby = "updated_at";
                    break;
                default :
                    $order = "DESC";
                    $orderby = "updated_at";
                    break;
            }
        }
        $offset = 0;
        $limit = 10;
        if ($request['page']) {
            $offset = ($request['page'] * 10) - 10;
            $limit = $request['page'] * 10;
        }
        $objExp = \App\Experiences::whereIn('id', @$objCategories);
        if ($sedate) {
            $sedateto = \Carbon\Carbon::parse($request['sexp_date'])->addDays(15)->format("Y-m-d");
            $objExp = $objExp->whereBetween("start_date_time", array($sedate, $sedateto));
        }
        $resExp = $objExp->where("is_draft", 0)
                ->skip($offset)
                ->take($limit)
                ->orderBy($orderby, $order)
                ->distinct()
                ->get();
        $data['experiences'] = $resExp;
        $data['sort_by'] = (!empty(@$_GET['sort_by'])) ? @$_GET['sort_by'] : "newest";
        $data['sdest'] = $sdest;
        $data['scat'] = $scat;
        $data['sedate'] = $sedate;
        return view('search', $data);
    }

}
