<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

//use App\User;

class IndexController extends Controller
{
    public function __construct()
    {
        //$this->middleware(['auth', 'isAdmin']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = array();

        // Get Services
        $param['select'] = array("id", "name", "slug");
        $param['order'] = "ASC";
        $param['orderby'] = "parent";
        $param['where'] = array("parent" => "0");
        $param['limit'] = 6;
        $data['services'] = \App\Service::where("display_on_home", "1")->orWhereIn("id",[114,123,111,124])->get();
        $data['serviceListingCount'] = array();
        foreach ($data['services'] as $key => $services) {
            $childServices = \App\Service::Where('parent', $services->id)->orWhere("id", $services->id)->pluck('id')->values();
            $itemCount = \App\ListingService::WhereIn('service_id', $childServices)->distinct('listing_id')->count('listing_id');
            $data['serviceListingCount'][$services->id]['ID'] = $services->id;
            $data['serviceListingCount'][$services->id]['Name'] = $services->name;
            $data['serviceListingCount'][$services->id]['Count'] = $itemCount;
        }

        // Get Listings
        $data['listings'] = \App\Listings::select("listings.*", "type_of_spa.name as type_of_spa", "destination.name as cityname","cnt.name as countryname")->join("destination","listings.city","=","destination.id")->join("destination as cnt","listings.country","=","cnt.id")
                ->leftJoin("type_of_spa", "type_of_spa.id", "=", "listings.type_of_institution")
                ->where("listings.is_draft", "0")
                ->groupBy("listings.id")
                ->orderBy("listings.total_score", "DESC")->limit(10)->get();

        $data['listingcount'] = \App\Listings::select("listings.*", "type_of_spa.name as type_of_spa","destination.name as cityname","cnt.name as countryname")->join("destination","listings.city","=","destination.id")->join("destination as cnt","listings.country","=","cnt.id")
                ->leftJoin("type_of_spa", "type_of_spa.id", "=", "listings.type_of_institution")
                ->where("listings.is_draft", "0")
                ->groupBy("listings.id")
                ->orderBy("listings.total_score", "DESC")->get()->count();

        // Get Destinations
        $data['destinations'] = \App\Destination::With('listings')->where("display_on_home", "1")->orderBy("name", "ASC")->limit(6)->get();

        //$data['searchLocations'] = \App\Destination::With('listings')->with('city')->get();

        $objCities = \App\Destination::select("destination.*","listings.country","cnt.name as countryname",DB::raw("COUNT(`listings`.`id`) as total_listings"))
                                    ->join("listings","listings.city","=","destination.id")
                                    ->leftJoin("destination as cnt","listings.country","=","cnt.id")
                                    ->groupBy("listings.city")->get();

        $data['citycount'] = \App\Destination::join("listings","listings.city","=","destination.id")->groupBy("listings.city")->get()->count();

        $data['searchLocations'] = array();
        foreach ($objCities as $objCity)  {
            $data['searchLocations'][$objCity->country][] = $objCity;
        }


        $data['searchServices'] = \App\Service::where('parent', 0)->get();
        //dd($data);

        return view('index', $data);
    }
}
