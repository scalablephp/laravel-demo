<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Award;

class AwardController extends Controller {

    public function __construct() {
        
    }

    /**
     * Display a listing of the resource - Award.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $data = array();
        $param['order'] = "DESC";
        $param['orderby'] = "id";
        $data['awards'] = Award::get_data($param);
        return view("admin.award.index", $data);
    }

    /**
     * Show the form for creating a new Award.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $data = array();
        return view('admin.award.create', $data);
    }

    /**
     * Store a newly created award in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $this->validate($request, [
            'name' => 'required',
            'slug' => 'required',
        ]);

        $name = $request['name'];
        $slug = $request['slug'];
        $description = $request['description'];
        if ($request->file('award_image')) {
            $imageUrl = $this->upload_image($request);
            $imageTitle = $request->file('award_image')->getClientOriginalName();
        }
        if (!empty($request['id'])) {
            $award = Award::find($request['id']);
            if ($award) {
                $award->name = $name;
                $award->slug = $slug;
                $award->description = $description;
                if ($request['award_image']) {
                    $award->image_title = $imageTitle;
                    $award->image_url = $imageUrl;
                }
                try {
                    $award->save();
                } catch (Exception $e) {
                    return redirect('bbadmin/awards')
                                    ->with('flash_error_message', 'Something went wrong');
                }
            } else {
                return redirect('bbadmin/awards')
                                ->with('flash_error_message', 'Something went wrong');
            }
            return redirect('bbadmin/awards')
                            ->with('flash_message', 'Award ' . $award->name . ' updated');
        } else {
            $objAward = new \App\Award();
            $objAward->name = $name;
            $objAward->slug = $slug;
            $objAward->description = $description;
            if ($request['award_image']) {
                $objAward->image_title = $imageTitle;
                $objAward->image_url = $imageUrl;
            }
            try {
                $objAward->save();
            } catch (Exception $e) {
                return redirect('bbadmin/award')
                                ->with('flash_error_message', 'Something went wrong');
            }

            return redirect('bbadmin/awards')
                            ->with('flash_message', 'Award ' . $objAward->name . ' created');
        }
    }

    /**
     * Show the form for ediing a Award.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id = '') {
        $data = array();
        if (!$id) {
            redirect("bbadmin/awards");
        }
        $param = array("where" => array("id" => $id), "limit" => 1);
        $data['eaward'] = Award::get_data($param);
        $data['eaward'] = $data['eaward'][0];
        return view('admin.award.edit', $data);
    }

    /**
     * Remove the specified award from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request) {
        $id = $request['id'];
        try {
            $award = Award::find($id);
            if (!empty($award)) {
                $award->delete();
            } else {
                return redirect('bbadmin/awards')
                                ->with('flash_error_message', 'Something went wrong.');
            }
        } catch (Exception $e) {
            return redirect('bbadmin/awards')
                            ->with('flash_error_message', 'Something went wrong.');
        }
        return redirect('bbadmin/awards')
                        ->with('flash_message', 'Award deleted successfully');
    }

    public function upload_image($request) {
        $file = $request->file("award_image");
        // check mime type
        if ($file->getClientMimeType() == ( "image/png" ) ||
                $file->getClientMimeType() == ( "image/jpeg" ) ||
                $file->getClientMimeType() == ( "image/gif" ) ||
                $file->getClientMimeType() == ( "image/jpg" )) {
            // feel free to change this logic, that is an example
            $baseFileName = strtolower($file->getClientOriginalName());
            $ext = strtolower($file->getClientOriginalExtension());
            $filenameWithoutExt = preg_replace("~\." . $ext . "$~i", '', $baseFileName);
            $renamefile = $filenameWithoutExt . time() . "." . $ext;
            // folder name in container, could be empty
            $folderName = 'award' . '/' . date("Y") . "/" . date("m") . "/" . date("d");
            // store file on azure blob
            $file->storeAs($folderName, $renamefile, ['disk' => 'azure']);
            // save file name somewhere
            return $saveFileName = $folderName . "/" . $renamefile;
        }
    }

    public function delete_image(Request $request) {
        try {
            $id = $request['id'];
            $objAward = Award::find($id);
            if (!empty($objAward)) {
                \Illuminate\Support\Facades\Storage::disk('azure')->delete($objAward->image_url);
                $objAward->image_title = null;
                $objAward->image_url = null;
                $objAward->save();
                echo true;
            } else {
                echo 'Something went wrong.';
            }
        } catch (Exception $e) {
            echo 'Something went wrong.';
        }
    }

}
