<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\TypeOfSpa;

class TypeController extends Controller {

    public function __construct() {
        
    }

    /**
     * Display a listing of the resource - TypeOfSpa.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $data = array();
        $param['order'] = "ASC";
        $param['orderby'] = "parent";
        $types = TypeOfSpa::get_data($param);
        if ($types) {
            foreach ($types as $type) {
                if ($type->parent > 0) {
                    $data['types'][$type->parent]['subtype'][] = $type->toArray();
                } else {
                    $data['types'][$type->id] = $type->toArray();
                }
            }
        }
        return view("admin.type.index", $data);
    }

    /**
     * Show the form for creating a new TypeOfSpa.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $data = array();
        $param = array();
        $param['where'] = array("parent" => 0);
        $data['types'] = TypeOfSpa::get_data($param);
        return view('admin.type.create', $data);
    }

    /**
     * Store a newly created type in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $this->validate($request, [
            'name' => 'required',
            'slug' => 'required',
        ]);

        $name = $request['name'];
        $slug = $request['slug'];
        $parent = $request['parent'];
        $keywords = $request['keywords'];
        $display_on_home = $request['display_on_home'];
        $description = $request['description'];
        if ($request->file('type_image')) {
            $imageUrl = $this->upload_image($request);
            $imageTitle = $request->file('type_image')->getClientOriginalName();
        }
        if ($request->file('type_banner_image')) {
            $imageBannerUrl = $this->upload_banner_image($request);
            $imageBannerTitle = $request->file('type_banner_image')->getClientOriginalName();
        }
        if (!empty($request['id'])) {
            $type = TypeOfSpa::find($request['id']);
            if ($type) {
                $type->name = $name;
                $type->slug = $slug;
                $type->parent = $parent;
                $type->keywords = $keywords;
                $type->display_on_home = $display_on_home;
                $type->description = $description;
                if ($request['type_image']) {
                    $type->image_title = $imageTitle;
                    $type->image_url = $imageUrl;
                }
                if ($request->file('type_banner_image')) {
                    $type->banner_image_title = $imageBannerTitle;
                    $type->banner_image_url = $imageBannerUrl;
                }
                try {
                    $type->save();
                } catch (Exception $e) {
                    return redirect('bbadmin/type')
                                    ->with('flash_error_message', 'Something went wrong');
                }
            } else {
                return redirect('bbadmin/type')
                                ->with('flash_error_message', 'Something went wrong');
            }
            return redirect('bbadmin/type')
                            ->with('flash_message', 'TypeOfSpa ' . $type->name . ' updated');
        } else {
            $objTypeOfSpa = new \App\TypeOfSpa();
            $objTypeOfSpa->name = $name;
            $objTypeOfSpa->slug = $slug;
            $objTypeOfSpa->parent = $parent;
            $objTypeOfSpa->keywords = $keywords;
            $objTypeOfSpa->display_on_home = $display_on_home;
            $objTypeOfSpa->description = $description;
            if ($request['type_image']) {
                $objTypeOfSpa->image_title = $imageTitle;
                $objTypeOfSpa->image_url = $imageUrl;
            }
            if ($request->file('type_banner_image')) {
                $type->banner_image_title = $imageBannerTitle;
                $type->banner_image_url = $imageBannerUrl;
            }
            try {
                $objTypeOfSpa->save();
            } catch (Exception $e) {
                return redirect('bbadmin/type')
                                ->with('flash_error_message', 'Something went wrong');
            }

            return redirect('bbadmin/type')
                            ->with('flash_message', 'Type ' . $objTypeOfSpa->name . ' created');
        }
    }

    /**
     * Show the form for ediing a TypeOfSpa.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id = '') {
        $data = array();
        $param = array();
        if (!$id) {
            redirect("bbadmin/type");
        }
        $param['where'] = array("parent" => 0);
        $data['types'] = TypeOfSpa::get_data($param);
        $param = array("where" => array("id" => $id), "limit" => 1);
        $data['etype'] = TypeOfSpa::get_data($param);
        $data['etype'] = $data['etype'][0];
        return view('admin.type.edit', $data);
    }

    /**
     * Remove the specified type from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request) {
        $id = $request['id'];
        try {
            $type = TypeOfSpa::find($id);
            if (!empty($type)) {
                $type->delete();
            } else {
                return redirect('bbadmin/type')
                                ->with('flash_error_message', 'Something went wrong.');
            }
        } catch (Exception $e) {
            return redirect('bbadmin/type')
                            ->with('flash_error_message', 'Something went wrong.');
        }
        return redirect('bbadmin/type')
                        ->with('flash_message', 'Type deleted successfully');
    }

    public function upload_image($request) {
        $file = $request->file("type_image");
        // check mime type
        if ($file->getClientMimeType() == ( "image/png" ) ||
                $file->getClientMimeType() == ( "image/jpeg" ) ||
                $file->getClientMimeType() == ( "image/gif" ) ||
                $file->getClientMimeType() == ( "image/jpg" )) {
            // feel free to change this logic, that is an example
            $baseFileName = strtolower($file->getClientOriginalName());
            $ext = strtolower($file->getClientOriginalExtension());
            $filenameWithoutExt = preg_replace("~\." . $ext . "$~i", '', $baseFileName);
            $renamefile = $filenameWithoutExt . time() . "." . $ext;
            // folder name in container, could be empty
            $folderName = 'type' . '/' . date("Y") . "/" . date("m") . "/" . date("d");
            // store file on azure blob
            $file->storeAs($folderName, $renamefile, ['disk' => 'azure']);
            // save file name somewhere
            return $saveFileName = $folderName . "/" . $renamefile;
        }
    }

    public function upload_banner_image($request) {
        $file = $request->file("type_banner_image");
        // check mime type
        if ($file->getClientMimeType() == ( "image/png" ) ||
                $file->getClientMimeType() == ( "image/jpeg" ) ||
                $file->getClientMimeType() == ( "image/gif" ) ||
                $file->getClientMimeType() == ( "image/jpg" )) {
            // feel free to change this logic, that is an example
            $baseFileName = strtolower($file->getClientOriginalName());
            $ext = strtolower($file->getClientOriginalExtension());
            $filenameWithoutExt = preg_replace("~\." . $ext . "$~i", '', $baseFileName);
            $renamefile = $filenameWithoutExt . time() . "." . $ext;
            // folder name in container, could be empty
            $folderName = 'type' . '/' . date("Y") . "/" . date("m") . "/" . date("d");
            // store file on azure blob
            $file->storeAs($folderName, $renamefile, ['disk' => 'azure']);
            // save file name somewhere
            return $saveFileName = $folderName . "/" . $renamefile;
        }
    }

    public function delete_image(Request $request) {
        try {
            $id = $request['id'];
            $objTypeOfSpa = TypeOfSpa::find($id);
            if (!empty($objTypeOfSpa)) {
                \Illuminate\Support\Facades\Storage::disk('azure')->delete($objTypeOfSpa->image_url);
                $objTypeOfSpa->image_title = null;
                $objTypeOfSpa->image_url = null;
                $objTypeOfSpa->save();
                echo true;
            } else {
                echo 'Something went wrong.';
            }
        } catch (Exception $e) {
            echo 'Something went wrong.';
        }
    }

    public function delete_banner_image(Request $request) {
        try {
            $id = $request['id'];
            $objTypeOfSpa = TypeOfSpa::find($id);
            if (!empty($objTypeOfSpa)) {
                \Illuminate\Support\Facades\Storage::disk('azure')->delete($objTypeOfSpa->banner_image_url);
                $objTypeOfSpa->banner_image_title = null;
                $objTypeOfSpa->banner_image_url = null;
                $objTypeOfSpa->save();
                echo true;
            } else {
                echo 'Something went wrong.';
            }
        } catch (Exception $e) {
            echo 'Something went wrong.';
        }
    }

}
