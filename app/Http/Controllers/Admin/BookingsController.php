<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\User;
use App\Bookings;
use Storage;
use DB;

class BookingsController extends Controller {

    public function __construct() {
        
    }

    /**
     * Display a listing of the resource - Bookings.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $data = array();
        $param['order'] = "DESC";
        $param['orderby'] = "id";
        $data['bookings'] = Bookings::select("bookings.id", "bookings.start_date_time", "bookings.end_date_time", "experiences.id as exp_id", "experiences.name", "bookings.created_at", "users.id as user_id", "users.first_name as user_first_name", "users.last_name as user_last_name")
                ->Join("experiences", "experiences.id", "bookings.experience_id")
                ->leftJoin("users", "bookings.user_id", "users.id")
                ->orderBy("bookings.created_at", "DESC")
                ->get();
        return view("admin.bookings.index", $data);
    }

    /**
     * Show the form for creating a new Bookings.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $data = array();
        return view('admin.blog.create', $data);
    }

    /**
     * Store a newly created Bookings in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $this->validate($request, [
            'name' => 'required',
            'slug' => 'required',
        ]);

        $name = $request['name'];
        $slug = $request['slug'];
        $keywords = $request['keywords'];
        $description = $request['description'];
        if ($request->file('banner_image')) {
            $imageUrl = $this->upload_image($request);
            $imageTitle = $request->file('banner_image')->getClientOriginalName();
        }
        $image_galleries = (!empty($request['image_gallery_ids'])) ? $request['image_gallery_ids'] : "";
        if (!empty($request['id'])) {
            $objBookings = Bookings::find($request['id']);
            if ($objBookings) {
                $objBookings->name = $name;
                $objBookings->slug = $slug;
                $objBookings->keywords = $keywords;
                $objBookings->description = $description;
                if ($request['banner_image']) {
                    $objBookings->banner_image_title = $imageTitle;
                    $objBookings->banner_image_url = $imageUrl;
                }
                try {
                    $objBookings->save();
                    $blog_id = $request['id'];

                    // Move Images from tmp to src
                    if (!empty(@$image_galleries)) {
                        $image_galleries_array = explode("|@|@|", @$image_galleries);
                        foreach ($image_galleries_array as $galimage) {
                            $dest = str_replace("tmp/", "", $galimage);
                            Storage::disk('azure')->move($galimage, $dest);
                            $objBookingsImageGallery = new \App\BookingsImageGallery();
                            $objBookingsImageGallery->blog_id = $blog_id;
                            $objBookingsImageGallery->image_title = basename($dest);
                            $objBookingsImageGallery->image_url = $dest;
                            $objBookingsImageGallery->save();
                        }
                    }
                } catch (Exception $e) {
                    return redirect('bbadmin/blogs')
                                    ->with('flash_error_message', 'Something went wrong');
                }
            } else {
                return redirect('bbadmin/blogs')
                                ->with('flash_error_message', 'Something went wrong');
            }
            return redirect('bbadmin/blogs')
                            ->with('flash_message', 'Bookings ' . $objBookings->name . ' updated');
        } else {
            $objBookings = new Bookings();
            $objBookings->name = $name;
            $objBookings->slug = $slug;
            $objBookings->keywords = $keywords;
            $objBookings->description = $description;
            if ($request['banner_image']) {
                $objBookings->banner_image_title = $imageTitle;
                $objBookings->banner_image_url = $imageUrl;
            }
            try {
                $objBookings->save();
                $blog_id = $objBookings->id;

                // Move Images from tmp to src
                if (!empty(@$image_galleries)) {
                    $image_galleries_array = explode("|@|@|", @$image_galleries);
                    foreach ($image_galleries_array as $galimage) {
                        $dest = str_replace("tmp/", "", $galimage);
                        Storage::disk('azure')->move($galimage, $dest);
                        $objBookingsImageGallery = new \App\BookingsImageGallery();
                        $objBookingsImageGallery->blog_id = $blog_id;
                        $objBookingsImageGallery->image_title = basename($dest);
                        $objBookingsImageGallery->image_url = $dest;
                        $objBookingsImageGallery->save();
                    }
                }
            } catch (Exception $e) {
                return redirect('bbadmin/blogs')
                                ->with('flash_error_message', 'Something went wrong');
            }

            return redirect('bbadmin/blogs')
                            ->with('flash_message', 'Bookings ' . $objBookings->name . ' created');
        }
    }

    /**
     * Show the form for ediing a Bookings.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id = '') {
        $data = array();
        $param = array();
        if (!$id) {
            redirect("bbadmin/bookings");
        }

        $param = array("where" => array("id" => $id), "limit" => 1);
        $param['order'] = "DESC";
        $param['orderby'] = "id";
        $objbooking = Bookings::get_data($param);

        $data['ebooking'] = $data['booking_user_info'] = $data['booking_experience'] = $data['booking_center_info'] = $data['center'] = $data['booking_transaction_info'] = array();
        if (sizeof($objbooking) > 0) {
            $data['ebooking'] = $objbooking[0];
            $data['booking_user_info'] = \App\BookingUserInfo::where("booking_id", $data['ebooking']->id)->first();
            
            $data['experience'] = \App\Experiences::where("id", $data['ebooking']->experience_id)->first();  

            $data['center'] = \App\Centers::where("id", $data['experience']->center_id)->first();          
            
            $data['booking_experience'] = \App\BookingExperienceInfo::where("experience_id", $data['ebooking']->experience_id)
                    ->where("booking_id", $data['ebooking']->id)
                    ->first();

            $data['accommodation'] = \App\Accomodation::where("id", $data['ebooking']->experience_accomodation_id)->first();
            
            $data['booking_center_info'] = \App\BookingUserInfo::where("booking_id", $data['ebooking']->id)->first();
            
            $data['booking_transaction_info'] = \App\BookingTransactionInfo::where("booking_id", $data['ebooking']->id)->first();
        }
        return view('admin.bookings.edit', $data);
    }

    /**
     * Remove the specified Bookings from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request) {
        $id = $request['id'];
        try {
            $objBookings = Bookings::find($id);
            if (!empty($objBookings)) {
                $objBookings->delete();
            } else {
                return redirect('bbadmin/blogs')
                                ->with('flash_error_message', 'Something went wrong.');
            }
        } catch (Exception $e) {
            return redirect('bbadmin/blogs')
                            ->with('flash_error_message', 'Something went wrong.');
        }
        return redirect('bbadmin/blogs')
                        ->with('flash_message', 'Bookings deleted successfully');
    }

    public function upload_image($request) {
        $file = $request->file("banner_image");
        // check mime type
        if ($file->getClientMimeType() == ( "image/png" ) ||
                $file->getClientMimeType() == ( "image/jpeg" ) ||
                $file->getClientMimeType() == ( "image/gif" ) ||
                $file->getClientMimeType() == ( "image/jpg" )) {
            // feel free to change this logic, that is an example
            $baseFileName = strtolower($file->getClientOriginalName());
            $ext = strtolower($file->getClientOriginalExtension());
            $filenameWithoutExt = preg_replace("~\." . $ext . "$~i", '', $baseFileName);
            $renamefile = $filenameWithoutExt . time() . "." . $ext;
            // folder name in container, could be empty
            $folderName = 'blogs' . '/' . date("Y") . "/" . date("m") . "/" . date("d");
            // store file on azure blob
            $file->storeAs($folderName, $renamefile, ['disk' => 'azure']);
            // save file name somewhere
            return $saveFileName = $folderName . "/" . $renamefile;
        }
    }

    public function delete_image(Request $request) {
        try {
            $id = $request['id'];
            $objBookings = Bookings::find($id);
            if (!empty($objBookings)) {
                Storage::disk('azure')->delete($objBookings->banner_image_url);
                $objBookings->banner_image_title = null;
                $objBookings->banner_image_url = null;
                $objBookings->save();
                echo true;
            } else {
                echo 'Something went wrong.';
            }
        } catch (Exception $e) {
            echo 'Something went wrong.';
        }
    }

    /**
     * Upload Image Gallery
     *
     * @return \Illuminate\Http\Response
     */
    public function upload_gallery_image(Request $request) {
        $file = $request->file('file');
        if ($file->getClientMimeType() == ( "image/png" ) ||
                $file->getClientMimeType() == ( "image/jpeg" ) ||
                $file->getClientMimeType() == ( "image/gif" ) ||
                $file->getClientMimeType() == ( "image/jpg" )) {
            // feel free to change this logic, that is an example
            $baseFileName = strtolower($file->getClientOriginalName());
            $ext = strtolower($file->getClientOriginalExtension());
            $filenameWithoutExt = preg_replace("~\." . $ext . "$~i", '', $baseFileName);
            $renamefile = $filenameWithoutExt . time() . "." . $ext;
            // folder name in container, could be empty
            $folderName = 'tmp/blogs' . '/' . date("Y") . "/" . date("m") . "/" . date("d");
            // store file on azure blob
            $file->storeAs($folderName, $renamefile, ['disk' => 'azure']);
            // save file name somewhere
            $saveFileName = $folderName . "/" . $renamefile;
            echo (json_encode(array('success' => true, 'filename' => $saveFileName)));
        } else {
            echo (json_encode(array('success' => false, 'message' => 'Either file is not valid or file not found')));
        }
    }

    /**
     * Delete Image Gallery
     *
     * @return \Illuminate\Http\Response
     */
    public function delete_gallery_image(Request $request) {
        try {
            $id = $request['id'];
            $objBookingsImageGallery = \App\BookingsImageGallery::find($id);
            if (!empty($objBookingsImageGallery)) {
                Storage::disk('azure')->delete($objBookingsImageGallery->image_url);
                $objBookingsImageGallery->delete();
                echo true;
            } else {
                echo 'Something went wrong.';
            }
        } catch (Exception $e) {
            echo 'Something went wrong.';
        }
    }

}
