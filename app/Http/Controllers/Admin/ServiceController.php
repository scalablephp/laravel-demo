<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Service;

class ServiceController extends Controller {

    public function __construct() {
        
    }

    /**
     * Display a listing of the resource - Service.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $data = array();
        $param['order'] = "ASC";
        $param['orderby'] = "parent";
        $services = Service::get_data($param);
        if ($services) {
            foreach ($services as $service) {
                if ($service->parent > 0) {
                    $data['services'][$service->parent]['subservice'][] = $service->toArray();
                } else {
                    $data['services'][$service->id] = $service->toArray();
                }
            }
        }
        return view("admin.service.index", $data);
    }

    /**
     * Show the form for creating a new Service.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $data = array();
        $param = array();
        $param['where'] = array("parent" => 0);
        $data['services'] = Service::get_data($param);
        return view('admin.service.create', $data);
    }

    /**
     * Store a newly created Service in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $this->validate($request, [
            'name' => 'required',
            'slug' => 'required',
        ]);

        $name = $request['name'];
        $slug = $request['slug'];
        $parent = $request['parent'];
        $keywords = $request['keywords'];
        $display_on_home = $request['display_on_home'];
        $description = $request['description'];
        if ($request->file('service_image')) {
            $imageUrl = $this->upload_image($request);
            $imageTitle = $request->file('service_image')->getClientOriginalName();
        }
        if ($request->file('service_banner_image')) {
            $imageBannerUrl = $this->upload_banner_image($request);
            $imageBannerTitle = $request->file('service_banner_image')->getClientOriginalName();
        }
        if (!empty($request['id'])) {
            $Service = Service::find($request['id']);
            if ($Service) {
                $Service->name = $name;
                $Service->slug = $slug;
                $Service->parent = $parent;
                $Service->keywords = $keywords;
                $Service->display_on_home = $display_on_home;
                $Service->description = $description;
                if ($request['service_image']) {
                    $Service->image_title = $imageTitle;
                    $Service->image_url = $imageUrl;
                }
                if ($request->file('service_banner_image')) {
                    $Service->banner_image_title = $imageBannerTitle;
                    $Service->banner_image_url = $imageBannerUrl;
                }
                try {
                    $Service->save();
                } catch (Exception $e) {
                    return redirect('bbadmin/services')
                                    ->with('flash_error_message', 'Something went wrong');
                }
            } else {
                return redirect('bbadmin/services')
                                ->with('flash_error_message', 'Something went wrong');
            }
            return redirect('bbadmin/services')
                            ->with('flash_message', 'Service ' . $Service->name . ' updated');
        } else {
            $objService = new \App\Service();
            $objService->name = $name;
            $objService->slug = $slug;
            $objService->parent = $parent;
            $objService->keywords = $keywords;
            $objService->display_on_home = $display_on_home;
            $objService->description = $description;
            if ($request['service_image']) {
                $objService->image_title = $imageTitle;
                $objService->image_url = $imageUrl;
            }
            if ($request->file('service_banner_image')) {
                $Service->banner_image_title = $imageBannerTitle;
                $Service->banner_image_url = $imageBannerUrl;
            }
            try {
                $objService->save();
            } catch (Exception $e) {
                return redirect('bbadmin/services')
                                ->with('flash_error_message', 'Something went wrong');
            }

            return redirect('bbadmin/services')
                            ->with('flash_message', 'Service ' . $objService->name . ' created');
        }
    }

    /**
     * Show the form for ediing a Service.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id = '') {
        $data = array();
        $param = array();
        if (!$id) {
            redirect("bbadmin/services");
        }
        $param['where'] = array("parent" => 0);
        $data['services'] = Service::get_data($param);
        $param = array("where" => array("id" => $id), "limit" => 1);
        $data['eservice'] = Service::get_data($param);
        $data['eservice'] = $data['eservice'][0];
        return view('admin.service.edit', $data);
    }

    /**
     * Remove the specified Service from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request) {
        $id = $request['id'];
        try {
            $Service = Service::find($id);
            if (!empty($Service)) {
                $Service->delete();
            } else {
                return redirect('bbadmin/services')
                                ->with('flash_error_message', 'Something went wrong.');
            }
        } catch (Exception $e) {
            return redirect('bbadmin/services')
                            ->with('flash_error_message', 'Something went wrong.');
        }
        return redirect('bbadmin/services')
                        ->with('flash_message', 'Service deleted successfully');
    }

    public function upload_image($request) {
        $file = $request->file("service_image");
        // check mime type
        if ($file->getClientMimeType() == ( "image/png" ) ||
                $file->getClientMimeType() == ( "image/jpeg" ) ||
                $file->getClientMimeType() == ( "image/gif" ) ||
                $file->getClientMimeType() == ( "image/jpg" )) {
            // feel free to change this logic, that is an example
            $baseFileName = strtolower($file->getClientOriginalName());
            $ext = strtolower($file->getClientOriginalExtension());
            $filenameWithoutExt = preg_replace("~\." . $ext . "$~i", '', $baseFileName);
            $renamefile = $filenameWithoutExt . time() . "." . $ext;
            // folder name in container, could be empty
            $folderName = 'service' . '/' . date("Y") . "/" . date("m") . "/" . date("d");
            // store file on azure blob
            $file->storeAs($folderName, $renamefile, ['disk' => 'azure']);
            // save file name somewhere
            return $saveFileName = $folderName . "/" . $renamefile;
        }
    }

    public function upload_banner_image($request) {
        $file = $request->file("service_banner_image");
        // check mime type
        if ($file->getClientMimeType() == ( "image/png" ) ||
                $file->getClientMimeType() == ( "image/jpeg" ) ||
                $file->getClientMimeType() == ( "image/gif" ) ||
                $file->getClientMimeType() == ( "image/jpg" )) {
            // feel free to change this logic, that is an example
            $baseFileName = strtolower($file->getClientOriginalName());
            $ext = strtolower($file->getClientOriginalExtension());
            $filenameWithoutExt = preg_replace("~\." . $ext . "$~i", '', $baseFileName);
            $renamefile = $filenameWithoutExt . time() . "." . $ext;
            // folder name in container, could be empty
            $folderName = 'service' . '/' . date("Y") . "/" . date("m") . "/" . date("d");
            // store file on azure blob
            $file->storeAs($folderName, $renamefile, ['disk' => 'azure']);
            // save file name somewhere
            return $saveFileName = $folderName . "/" . $renamefile;
        }
    }

    public function delete_image(Request $request) {
        try {
            $id = $request['id'];
            $objService = Service::find($id);
            if (!empty($objService)) {
                \Illuminate\Support\Facades\Storage::disk('azure')->delete($objService->image_url);
                $objService->image_title = null;
                $objService->image_url = null;
                $objService->save();
                echo true;
            } else {
                echo 'Something went wrong.';
            }
        } catch (Exception $e) {
            echo 'Something went wrong.';
        }
    }

    public function delete_banner_image(Request $request) {
        try {
            $id = $request['id'];
            $objService = Service::find($id);
            if (!empty($objService)) {
                \Illuminate\Support\Facades\Storage::disk('azure')->delete($objService->banner_image_url);
                $objService->banner_image_title = null;
                $objService->banner_image_url = null;
                $objService->save();
                echo true;
            } else {
                echo 'Something went wrong.';
            }
        } catch (Exception $e) {
            echo 'Something went wrong.';
        }
    }

}
