<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\ListingComments;

class CommentController extends Controller {

    public function __construct() {
        
    }

    /**
     * Display a listing of the resource - Category.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $data = array();
        $ajax = Request()->input('ajax') ? Request()->input('ajax') : false;
        if ($ajax == true) {
            $page = $_REQUEST['length'];
            $offset = $_REQUEST['start'];
            $search = $_REQUEST['search']['value'];
            
            $sortDir = $_REQUEST['order'][0]['dir'];
            $sortBy = $_REQUEST['order'][0]['column'];
            switch ($sortBy) {
                case "0":
                    $sortBy = "listings.name";
                    break;
                case "1":
                    $sortBy = "comment_author";
                    break;
                case "2":
                    $sortBy = "comment_author_email";
                    break;
                case "3":
                    $sortBy = "comment_date";
                    break;
            }
            
            $ListingComments = ListingComments::select("listing_comments.*", "listings.name", "listings.slug")->join("listings", "listings.id", "=", "listing_comments.listing_id");
            if (!empty($search)) {
                $ListingComments = $ListingComments->where("listings.name", "like", "%" . $search . "%");
            }
            $ListingCommentTotal = clone $ListingComments;
            $ListingComments = $ListingComments->orderBy($sortBy, $sortDir)->skip($offset)->limit($page)->get();
            $listingCommentTotal = $ListingCommentTotal->count();
            $listingCommentData = array();
            if ($ListingComments) {
                foreach ($ListingComments as $objListingComments) {
                    $action = '<a href = "' . url('/bbadmin/review/edit/' . (@$objListingComments->id)) . '" class = "btn btn-info">Detail</a>';
                    $listingCommentData[] = array(
                        '<a href="' . url("/spa/" . $objListingComments->slug) . '" target="_blank" class="' . ((@$objListingComments->comment_approved == 1) ? 'text-info' : 'text-warning') . '">' . $objListingComments->name . '</a>',
                        $objListingComments->comment_author,
                        $objListingComments->comment_author_email,
                        \Carbon\Carbon::parse($objListingComments->comment_date)->format("m/d/Y H:i:s"),
                        $action
                    );
                }
            }

            $json_data = array(
                "draw" => intval($_REQUEST['draw']),
                "recordsTotal" => intval($listingCommentTotal),
                "recordsFiltered" => intval($listingCommentTotal),
                "data" => $listingCommentData
            );
            echo json_encode($json_data);
        } else {
            return view("admin.comments.index", $data);
        }
    }

    /**
     * Show the form for creating a new Comment.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $data = array();
        $data['listings'] = \App\Listings::orderBy("name", "ASC")->get();
        return view('admin.comments.create', $data);
    }

    /**
     * Store a newly created category in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        if (!empty($request['id'])) {
            $comment = ListingComments::find($request['id']);
            if ($comment) {
                $comment->comment_approved = $request['comment_approved'];
                try {
                    $comment->save();
                } catch (Exception $e) {
                    return redirect('bbadmin/reviews')
                                    ->with('flash_error_message', 'Something went wrong');
                }
            } else {
                return redirect('bbadmin/reviews')
                                ->with('flash_error_message', 'Something went wrong');
            }
            return redirect('bbadmin/reviews')
                            ->with('flash_message', 'Comment updated');
        } else {
            $this->validate($request, [
                'listing_id' => 'required',
                'comment_content' => 'required',
            ]);
            try {
                $comment = new ListingComments();
                $comment->comment_author = $request['comment_author_email'];
                $comment->comment_author_email = $request['comment_author_email'];
                $comment->listing_id = $request['listing_id'];
                $comment->comment_content = $request['comment_content'];
                $comment->comment_approved = 1;
                $comment->comment_date = \Carbon\Carbon::now()->format("Y-m-d H:i:s");
                $comment->comment_date_gmt = \Carbon\Carbon::now()->format("Y-m-d H:i:s");
                $comment->save();
            } catch (Exception $e) {
                return redirect('bbadmin/reviews')
                                ->with('flash_error_message', 'Something went wrong');
            }
            return redirect('bbadmin/reviews')
                            ->with('flash_message', 'Comment Created');
        }
    }

    /**
     * Show the form for ediing a Comment.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id = '') {
        $data = array();
        if (!$id) {
            redirect("bbadmin/reviews");
        }
        $data['ecomment'] = ListingComments::select("listing_comments.*", "listings.name", "listings.slug")
                        ->join("listings", "listings.id", "=", "listing_comments.listing_id")
                        ->where("listing_comments.id", $id)
                        ->orderBy("listing_comments.id", "DESC")->first();
        return view('admin.comments.edit', $data);
    }

    public function getlistings(Request $request) {
       
        $json_data = array();
      $search = Request()->input('search');

       
        //    echo "wherere is the ";die;
            $objListings = \App\Listings::select("id", "name")->where("name", 'like', '%ay%')->get();
         //  echo "here"; dd($objListings);exit;
            if ($objListings) {
                foreach ($objListings as $objListing) {
                    array_push($json_data, array("id" => $objListing->id, "text" => $objListing->name));
                }
            }
        
    //    exit;
        echo json_encode($json_data);
    }

}
