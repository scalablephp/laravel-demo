<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Destination;
use DB;

class DestinationController extends Controller {

    public function __construct() {
        
    }

    /**
     * Display a listing of the resource - Destination.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $data = array();
        $param['order'] = "ASC";
        $param['orderby'] = "parent";
        $data['destinations'] = DB::table('destination as a')->leftJoin("destination as b", "a.parent", "=", "b.id")->select("a.*", "b.name as parentname")->orderBy("a.id", "ASC")->get();
        return view("admin.destination.index", $data);
    }

    /**
     * Show the form for creating a new Destination.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $data = array();
        $param['order'] = "ASC";
        $param['orderby'] = "parent";
        $destinations = Destination::get_data($param);
        if ($destinations) {
            foreach ($destinations as $destination) {
                if ($destination->parent > 0) {
                    if (isset($data['destinations'][$destination->parent])) {
                        $data['destinations'][$destination->parent]['subdestination'][$destination->id] = $destination->toArray();
                    }
                } else {
                    $data['destinations'][$destination->id] = $destination->toArray();
                }
            }
        }
        return view('admin.destination.create', $data);
    }

    /**
     * Store a newly created destination in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $this->validate($request, [
            'name' => 'required',
            'slug' => 'required',
        ]);

        $name = $request['name'];
        $slug = $request['slug'];
        $code = $request['code'];
        $parent = $request['parent'];
        $meta_title = $request['meta_title'];
        $meta_keywords = $request['meta_keywords'];
        $meta_description = $request['meta_description'];
        $display_on_home = $request['display_on_home'];
        $description = $request['description'];
        if ($request->file('destination_image')) {
            $imageUrl = $this->upload_image($request);
            $imageTitle = $request->file('destination_image')->getClientOriginalName();
        }
        if ($request->file('destination_banner_image')) {
            $imageBannerUrl = $this->upload_banner_image($request);
            $imageBannerTitle = $request->file('destination_banner_image')->getClientOriginalName();
        }
        if (!empty($request['id'])) {
            $destination = Destination::find($request['id']);
            if ($destination) {
                $destination->name = $name;
                $destination->slug = $slug;
                $destination->code = $code;
                $destination->parent = $parent;
                $destination->meta_title = $meta_title;
                $destination->meta_keywords = $meta_keywords;
                $destination->meta_description = $meta_description;
                $destination->display_on_home = $display_on_home;
                $destination->description = $description;
                if ($request['destination_image']) {
                    $destination->image_title = $imageTitle;
                    $destination->image_url = $imageUrl;
                }
                if ($request->file('destination_banner_image')) {
                    $destination->banner_image_title = $imageBannerTitle;
                    $destination->banner_image_url = $imageBannerUrl;
                }
                try {
                    $destination->save();
                } catch (Exception $e) {
                    return redirect('bbadmin/destination')
                                    ->with('flash_error_message', 'Something went wrong');
                }
            } else {
                return redirect('bbadmin/destination')
                                ->with('flash_error_message', 'Something went wrong');
            }
            return redirect('bbadmin/destination')
                            ->with('flash_message', 'Destination ' . $destination->name . ' updated');
        } else {
            $objDestination = new \App\Destination();
            $objDestination->name = $name;
            $objDestination->slug = $slug;
            $objDestination->code = $code;
            $objDestination->parent = $parent;
            $objDestination->meta_title = $meta_title;
            $objDestination->meta_keywords = $meta_keywords;
            $objDestination->meta_description = $meta_description;
            $objDestination->display_on_home = $display_on_home;
            $objDestination->description = $description;
            if ($request['destination_image']) {
                $objDestination->image_title = $imageTitle;
                $objDestination->image_url = $imageUrl;
            }
            if ($request->file('destination_banner_image')) {
                $destination->banner_image_title = $imageBannerTitle;
                $destination->banner_image_url = $imageBannerUrl;
            }
            try {
                $objDestination->save();
            } catch (Exception $e) {
                return redirect('bbadmin/destination')
                                ->with('flash_error_message', 'Something went wrong');
            }

            return redirect('bbadmin/destination')
                            ->with('flash_message', 'Destination ' . $objDestination->name . ' created');
        }
    }

    /**
     * Show the form for ediing a Destination.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id = '') {
        $data = array();
        if (!$id) {
            redirect("bbadmin/destination");
        }
        $param['order'] = "ASC";
        $param['orderby'] = "parent";
        $destinations = Destination::get_data($param);
        if ($destinations) {
            foreach ($destinations as $destination) {
                if ($destination->parent > 0) {
                    if (isset($data['destinations'][$destination->parent])) {
                        $data['destinations'][$destination->parent]['subdestination'][$destination->id] = $destination->toArray();
                    }
                } else {
                    $data['destinations'][$destination->id] = $destination->toArray();
                }
            }
        }
        $param = array("where" => array("id" => $id), "limit" => 1);
        $data['edestination'] = Destination::get_data($param);
        $data['edestination'] = $data['edestination'][0];
        return view('admin.destination.edit', $data);
    }

    /**
     * Remove the specified destination from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request) {
        $id = $request['id'];
        try {
            $destination = Destination::find($id);
            if (!empty($destination)) {
                $destination->delete();
            } else {
                return redirect('bbadmin/destination')
                                ->with('flash_error_message', 'Something went wrong.');
            }
        } catch (Exception $e) {
            return redirect('bbadmin/destination')
                            ->with('flash_error_message', 'Something went wrong.');
        }
        return redirect('bbadmin/destination')
                        ->with('flash_message', 'Destination deleted successfully');
    }

    public function upload_image($request) {
        $file = $request->file("destination_image");
        // check mime type
        if ($file->getClientMimeType() == ( "image/png" ) ||
                $file->getClientMimeType() == ( "image/jpeg" ) ||
                $file->getClientMimeType() == ( "image/gif" ) ||
                $file->getClientMimeType() == ( "image/jpg" )) {
            // feel free to change this logic, that is an example
            $baseFileName = strtolower($file->getClientOriginalName());
            $ext = strtolower($file->getClientOriginalExtension());
            $filenameWithoutExt = preg_replace("~\." . $ext . "$~i", '', $baseFileName);
            $renamefile = $filenameWithoutExt . time() . "." . $ext;
            // folder name in container, could be empty
            $folderName = 'destination' . '/' . date("Y") . "/" . date("m") . "/" . date("d");
            // store file on azure blob
            $file->storeAs($folderName, $renamefile, ['disk' => 'azure']);
            // save file name somewhere
            return $saveFileName = $folderName . "/" . $renamefile;
        }
    }

    public function upload_banner_image($request) {
        $file = $request->file("destination_banner_image");
        // check mime type
        if ($file->getClientMimeType() == ( "image/png" ) ||
                $file->getClientMimeType() == ( "image/jpeg" ) ||
                $file->getClientMimeType() == ( "image/gif" ) ||
                $file->getClientMimeType() == ( "image/jpg" )) {
            // feel free to change this logic, that is an example
            $baseFileName = strtolower($file->getClientOriginalName());
            $ext = strtolower($file->getClientOriginalExtension());
            $filenameWithoutExt = preg_replace("~\." . $ext . "$~i", '', $baseFileName);
            $renamefile = $filenameWithoutExt . time() . "." . $ext;
            // folder name in container, could be empty
            $folderName = 'destination' . '/' . date("Y") . "/" . date("m") . "/" . date("d");
            // store file on azure blob
            $file->storeAs($folderName, $renamefile, ['disk' => 'azure']);
            // save file name somewhere
            return $saveFileName = $folderName . "/" . $renamefile;
        }
    }

    public function delete_image(Request $request) {
        try {
            $id = $request['id'];
            $objDestination = Destination::find($id);
            if (!empty($objDestination)) {
                \Illuminate\Support\Facades\Storage::disk('azure')->delete($objDestination->image_url);
                $objDestination->image_title = null;
                $objDestination->image_url = null;
                $objDestination->save();
                echo true;
            } else {
                echo 'Something went wrong.';
            }
        } catch (Exception $e) {
            echo 'Something went wrong.';
        }
    }

    public function delete_banner_image(Request $request) {
        try {
            $id = $request['id'];
            $objDestination = Destination::find($id);
            if (!empty($objDestination)) {
                \Illuminate\Support\Facades\Storage::disk('azure')->delete($objDestination->banner_image_url);
                $objDestination->banner_image_title = null;
                $objDestination->banner_image_url = null;
                $objDestination->save();
                echo true;
            } else {
                echo 'Something went wrong.';
            }
        } catch (Exception $e) {
            echo 'Something went wrong.';
        }
    }

}
