<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class SearchController extends Controller {

    public function search() {
        
        // Sorting Start
        $sortBy = "listings.total_score";
        $sort = "DESC";
        $data['sortby'] = "popular";
        if (!empty(request()->input('sortby'))) {
            $data['sortby'] = request()->input('sortby');
            switch (request()->input('sortby')) {
                case "popular":
                    $sortBy = "listings.total_score";
                    $sort = "DESC";
                    break;
                case "new":
                    $sortBy = "listings.created_at";
                    $sort = "DESC";
                    break;
                default :
            }
        }
        // Sorting End
        
        $objListings = \App\Listings::Query();
        $objListings->select('*');
        if (!empty(request()->input('place'))) {
            $place = request()->input('place');
            $objListings->search($place);
            $data['srchplace'] = $place;
        }
        if (!empty(request()->input('destination'))) {
            $srchdestination = request()->input('destination');
            $objSrchDestination = \App\Destination::select('name')->where("id", $srchdestination)->first();
            $data['srchdestination'] = array();
            if (!empty($objSrchDestination)) {
                $data['srchdestination'] = $objSrchDestination;
            }
            $objListings->where('city', request()->input('destination'));
        }
        if (!empty(request()->input('service'))) {
            $srchservice = request()->input('service');
            $objSrchService = \App\Service::select('name')->where("id", $srchservice)->first();
            $data['srchservice'] = array();
            if (!empty($objSrchService)) {
                $data['srchservice'] = $objSrchService;
            }
            $value = \App\Service::Where('parent', request()->input('service'))->pluck('id')->values();
            $objListings->whereIn('id', function ($q) use ($value) {
                $q->select('listing_id')->from('listing_service')->whereIn('service_id', $value);
            });
        }

        if (!empty(request()->input('latitude')) && !empty(request()->input('longitude'))) {
            $latitude = request()->input('latitude');
            $longitude = request()->input('longitude');
            $objListings->select(
                    DB::raw('listings.*, 111.045 * DEGREES(ACOS(COS(RADIANS(' . $latitude . ')) * COS(RADIANS(latitude)) * COS(RADIANS(longitude) - RADIANS(' . $longitude . ')) + SIN(RADIANS(' . $latitude . ')) * SIN(RADIANS(latitude)))) AS distance_in_km'));
            $objListings->orderBy('distance_in_km', 'ASC');
        }

        $objListingsCount = clone $objListings;
        $data['spacount'] = $objListingsCount->count();
        $data['spas'] = $objListings->orderBy($sortBy, $sort)->paginate(10);

        $data['types'] = \App\TypeOfSpa::withCount('listings')->having("listings_count", ">", 0)->orderBy('listings_count', 'desc')->get();
        $data['services'] = \App\Service::select("id", "name")->whereIn('Name', ["Massage", "Facial", "Body Scrubs", "Body Wraps"])->with('subServices')->get();
        //$data['destinations'] = \App\Destination::select("id", "name")->where("parent", 0)->orderBy("name", "asc")->get();
        $objCities = \App\Destination::select("destination.*", "listings.country", "cnt.name as countryname", DB::raw("COUNT(`listings`.`id`) as total_listings"))
                        ->join("listings", "listings.city", "=", "destination.id")
                        ->leftJoin("destination as cnt", "listings.country", "=", "cnt.id")
                        ->groupBy("listings.city")->orderBy("countryname", "destination.name")->get();
        $data['destinations'] = array();
        foreach ($objCities as $objCity) {
            $data['destinations'][$objCity->country][] = $objCity;
            @$data['destinations'][$objCity->country]['total_listings'] += @$objCity->total_listings;
        }

        $data['querystring'] = request()->all();

        //dd($data); die();
        return view('spa', $data);
    }

    public function searchByService($slug = '', $subslug = "") {
        
        // Sorting Start
        $sortBy = "listings.total_score";
        $sort = "DESC";
        $data['sortby'] = "popular";
        if (!empty(request()->input('sortby'))) {
            $data['sortby'] = request()->input('sortby');
            switch (request()->input('sortby')) {
                case "popular":
                    $sortBy = "listings.total_score";
                    $sort = "DESC";
                    break;
                case "new":
                    $sortBy = "listings.created_at";
                    $sort = "DESC";
                    break;
                default :
            }
        }
        // Sorting End
        
        if (!empty($subslug)) {
            $service = \App\Service::Select('id', 'name')->where('slug', $subslug)->firstOrFail();
            $childServices = array($service->id);
        } else {
            $service = \App\Service::Select('id', 'name')->where('slug', $slug)->firstOrFail();

            $data['childServices'] = \App\Service::select("services.*")->join("listing_service", "listing_service.service_id", "=", "services.id")
                            ->where('parent', $service->id)->distinct()->get();
            $childServices = $data['childServices']->pluck('id')->values();

            /* $data['cities'] = \App\Destination::select("destination.*", DB::raw("count(listings.id) as total_listings"))
              ->join("listings", "listings.city", "=", "destination.id")
              ->whereIn('listings.id', function ($q) use ($childServices) {
              $q->select('listing_id')->from('listing_service')->whereIn('service_id', $childServices);
              })->groupBy("listings.city")->orderBy("total_listings", "DESC")->get(); */

            $data['parent'] = $slug;
        }

        $data['spas'] = \App\Listings::select("listings.*", "destination.name as cityname", "cnt.name as countryname")->join("destination", "listings.city", "=", "destination.id")->join("destination as cnt", "listings.country", "=", "cnt.id")->whereIn('listings.id', function ($q) use ($childServices) {
                    $q->select('listing_id')->from('listing_service')->whereIn('service_id', $childServices);
                })->orderBy($sortBy, $sort)->paginate(10);

        $data['spacount'] = \App\Listings::select("listings.*", "destination.name as cityname", "cnt.name as countryname")->join("destination", "listings.city", "=", "destination.id")->join("destination as cnt", "listings.country", "=", "cnt.id")->whereIn('listings.id', function ($q) use ($childServices) {
                    $q->select('listing_id')->from('listing_service')->whereIn('service_id', $childServices);
                })->count();

        $data['types'] = \App\TypeOfSpa::withCount('listings')->having("listings_count", ">", 0)->orderBy('listings_count', 'desc')->get();
        $data['services'] = \App\Service::select("id", "name")->whereIn('Name', ["Massage", "Facial", "Body Scrubs", "Body Wraps"])->with('subServices')->get();
        //$data['destinations'] = \App\Destination::select("id", "name")->where("parent", 0)->orderBy("name", "asc")->get();
        $objCities = \App\Destination::select("destination.*", "listings.country", "cnt.name as countryname", DB::raw("COUNT(`listings`.`id`) as total_listings"))
                        ->join("listings", "listings.city", "=", "destination.id")
                        ->leftJoin("destination as cnt", "listings.country", "=", "cnt.id")
                        ->groupBy("listings.city")->orderBy("countryname", "destination.name")->get();
        $data['destinations'] = array();
        foreach ($objCities as $objCity) {
            $data['destinations'][$objCity->country][] = $objCity;
            @$data['destinations'][$objCity->country]['total_listings'] += @$objCity->total_listings;
        }

        $data['searchType'] = " " . $service->name . "(" . $data['spacount'] . ")";
        
        $data['querystring'] = request()->all();

        return view('spa', $data);
    }

    public function searchByDestination($slug = '', $subslug = '') {

        $data['destinations'] = array();

        // Sorting Start
        $sortBy = "listings.total_score";
        $sort = "DESC";
        $data['sortby'] = "popular";
        if (!empty(request()->input('sortby'))) {
            $data['sortby'] = request()->input('sortby');
            switch (request()->input('sortby')) {
                case "popular":
                    $sortBy = "listings.total_score";
                    $sort = "DESC";
                    break;
                case "new":
                    $sortBy = "listings.created_at";
                    $sort = "DESC";
                    break;
                default :
            }
        }
        // Sorting End
            
        $data['meta_title'] = $data['meta_keywords'] = $data['meta_description'] = "";
        if (!empty($subslug)) {
            $destination = \App\Destination::where('slug', $subslug)->firstOrFail();
            $data['spas'] = \App\Listings::select("listings.*", "destination.name as countryname")->join("destination", "listings.country", "=", "destination.id")->where('city', $destination->id)->orderBy($sortBy, $sort)->paginate(10);
            $data['spacount'] = \App\Listings::select("listings.*", "destination.name as countryname")->join("destination", "listings.country", "=", "destination.id")->where('city', $destination->id)->count();
            $data['cityname'] = $destination->name;
        } else {
            $destination = \App\Destination::where('slug', $slug)->firstOrFail();

            $data['cities'] = \App\Destination::select("destination.*", DB::raw("count(listings.id) as total_listings"))
                            ->join("listings", "listings.city", "=", "destination.id")
                            ->where('country', $destination->id)->groupBy("listings.city")->orderBy("total_listings", "DESC")->get();

            $data['spas'] = \App\Listings::select("listings.*", "destination.name as cityname")->join("destination", "listings.city", "=", "destination.id")->where('country', $destination->id)->orderBy($sortBy, $sort)->paginate(10);
            $data['spacount'] = \App\Listings::select("listings.*", "destination.name as cityname")->join("destination", "listings.city", "=", "destination.id")->where('country', $destination->id)->count();
            $data['countryname'] = $destination->name;
            $data['parent'] = $slug;
            $data['srchcountry'] = "destination";

            $objCities = \App\Destination::select("destination.*", "listings.country", "cnt.name as countryname", DB::raw("COUNT(`listings`.`id`) as total_listings"))
                            ->join("listings", "listings.city", "=", "destination.id")
                            ->leftJoin("destination as cnt", "listings.country", "=", "cnt.id")
                            ->where('listings.country', $destination->id)
                            ->groupBy("listings.city")->orderBy("countryname", "destination.name")->get();
            foreach ($objCities as $objCity) {
                $data['destinations'][$objCity->country][] = $objCity;
                @$data['destinations'][$objCity->country]['total_listings'] += @$objCity->total_listings;
            }
        }

        // Meta Info
        $data['meta_title'] = $destination->meta_title;
        $data['meta_keywords'] = $destination->meta_keywords;
        $data['meta_description'] = $destination->meta_description;
        $data['meta_image_url'] = $destination->image_url;


        $data['types'] = \App\TypeOfSpa::withCount('listings')->having("listings_count", ">", 0)->orderBy('listings_count', 'desc')->get();
        $data['services'] = \App\Service::select("id", "name")->whereIn('Name', ["Massage", "Facial", "Body Scrubs", "Body Wraps"])->with('subServices')->get();
        //$data['destinations'] = \App\Destination::select("id", "name")->where("parent", 0)->orderBy("name", "asc")->get();

        $data['searchType'] = " " . $destination->name . "(" . $data['spacount'] . ")";
        $data['page'] = "destination";
        $data['querystring'] = request()->all();
        return view('spa', $data);
    }

    public function searchByTypeOfSpa($slug = '') {
        $typeOfSpa = \App\TypeOfSpa::Select('id')->where('slug', $slug)->firstOrFail();
        $data['spas'] = \App\Listings::select("listings.*", "destination.name as cityname", "cnt.name as countryname")->join("destination", "listings.city", "=", "destination.id")->join("destination as cnt", "listings.country", "=", "cnt.id")->where('type_of_institution', $typeOfSpa->id)->orderBy("total_score", "DESC")->paginate(10);

        $data['types'] = \App\TypeOfSpa::withCount('listings')->having("listings_count", ">", 0)->orderBy('listings_count', 'desc')->get();
        $data['services'] = \App\Service::select("id", "name")->whereIn('Name', ["Massage", "Facial", "Body Scrubs", "Body Wraps"])->with('subServices')->get();
        //$data['destinations'] = \App\Destination::select("id", "name")->where("parent", 0)->orderBy("name", "asc")->get();
        $objCities = \App\Destination::select("destination.*", "listings.country", "cnt.name as countryname", DB::raw("COUNT(`listings`.`id`) as total_listings"))
                        ->join("listings", "listings.city", "=", "destination.id")
                        ->leftJoin("destination as cnt", "listings.country", "=", "cnt.id")
                        ->groupBy("listings.city")->orderBy("countryname", "destination.name")->get();
        $data['destinations'] = array();
        foreach ($objCities as $objCity) {
            $data['destinations'][$objCity->country][] = $objCity;
            @$data['destinations'][$objCity->country]['total_listings'] += @$objCity->total_listings;
        }

        $data['searchType'] = " " . $slug;

        return view('spa', $data);
    }

}
