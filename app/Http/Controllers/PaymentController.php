<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Softon\Indipay\Facades\Indipay;
use Redirect;
use Auth;

//use App\User;

class PaymentController extends Controller {

    public function __construct() {
        //$this->middleware(['auth', 'isAdmin']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $data = array();
        $experienceId = Session('experience_info')['exp_id'];
        $exp_accomodation_id = Session('experience_info')['exp_acm_id'];
        $exp_booking_date = Session('experience_info')['exp_booking_date'];
        if ($experienceId) {
            Session(['experience_id' => $experienceId, 'exp_accomodation_id' => $exp_accomodation_id]);
            $exParam['where'] = array("id" => $experienceId);
            $experience = \App\Experiences::get_data($exParam);
            $data['experience'] = (@$experience[0]) ? : array();

            $param = array("where" => array("id" => $data['experience']->center_id), "limit" => 1);
            $data['ecenter'] = \App\Centers::get_data($param);
            $data['center'] = (@$data['ecenter'][0]) ? : array();

            $data['experience_accomodation'] = array();
            if ($exp_accomodation_id) {

                $arexp_booking_date = explode(" - ", @$exp_booking_date);
                $exp_booking_start = @$arexp_booking_date[0];
                $exp_booking_end = @$arexp_booking_date[1];

                $experience_accomodations = \App\Experiences::get_exp_acm_data($data['experience']->id, $exp_accomodation_id, @$exp_booking_start);
                $data['experience_accomodation'] = (!empty($experience_accomodations[0]) ? $experience_accomodations[0] : array());

                // Get Accomodation Gallery Images
                $data['accomodationimagegalleries'] = \App\AccomodationImageGallery::select('id', 'accomodation_id', 'image_url', 'image_title')
                                ->where("accomodation_id", $exp_accomodation_id)->get();
            }

            $data['reservation_info'] = Session('reservation_info');
            return view('payment', $data);
        } else {
            return Redirect('payment');
        }
    }

    /**
     * Store a newly reservation info in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function process(Request $request) {
        /* All Required Parameters by your Gateway */

        $experienceId = Session('experience_info')['exp_id'];
        $exp_accomodation_id = Session('experience_info')['exp_acm_id'];
        $exp_booking_date = Session('experience_info')['exp_booking_date'];
        if ($experienceId) {
            Session(['experience_id' => $experienceId, 'exp_accomodation_id' => $exp_accomodation_id]);
            $exParam['where'] = array("id" => $experienceId);
            $experience = \App\Experiences::get_data($exParam);
            $data['experience'] = (@$experience[0]) ? : array();

            $param = array("where" => array("id" => $data['experience']->center_id), "limit" => 1);
            $data['ecenter'] = \App\Centers::get_data($param);
            $data['center'] = (@$data['ecenter'][0]) ? : array();

            if ($exp_accomodation_id) {

                $arexp_booking_date = explode(" - ", @$exp_booking_date);
                $exp_booking_start = @$arexp_booking_date[0];
                $exp_booking_end = @$arexp_booking_date[1];

                $experience_accomodations = \App\Experiences::get_exp_acm_data($data['experience']->id, $exp_accomodation_id, @$exp_booking_start);
                $data['experience_accomodations'] = (!empty($experience_accomodations[0]) ? $experience_accomodations[0] : array());
            }

            $discount = 0;

            $pay = @$data['experience_accomodations']->room_price;
            // Early Bird Discount
            if ((!empty(@$data['experience']->eirly_bird_before_days)) && (!empty(@$data['experience']->eirly_bird_discount)) && (@$data['experience']->eirly_bird_discount > 0)) {
                if (@$data['experience']->eirly_bird_discount_type == "amt") {
                    $discount += @$data['experience']->eirly_bird_discount;
                } else {
                    $discount = (@$pay * @$data['experience']->eirly_bird_discount) / 100;
                }
            }

            if ((!empty(@$data['experience']->offer_start_date)) && (!empty(@$data['experience']->offer_discount)) && (@$data['experience']->offer_discount > 0)) {
                $now = \Carbon\Carbon::now()->format("Y-m-d");
                if ((\Carbon\Carbon::parse(@$data['experience']->offer_start_date)->format("Y-m-d") <= $now) && (\Carbon\Carbon::parse(@$data['experience']->offer_end_date)->format("Y-m-d") >= $now)) {
                    if (@$data['experience']->offer_discount_type == "amt") {
                        $discount += @$data['experience']->offer_discount;
                    } else {
                        $discount += (@$pay * @$data['experience']->offer_discount) / 100;
                    }
                }
            }

            $bookingAmount = $pay - $discount;
            $payAmount = $pay - $discount;

            // Calculate Commission
            $commission = 0;
            //if (!empty(@$data['experience']->commission) && (@$data['experience']->commission > 0)) {
            $commission = ($payAmount * @$data['experience']->commission) / 100;
            if (!empty(@$data['experience']->deposit_policy)) {
                switch (@$data['experience']->deposit_policy) {
                    case "2" :
                        // If Fixed Amount
                        $deposit_amount = @$data['experience']->deposit_amount;
                        $payAmount = @$data['experience']->deposit_amount + $commission;
                        break;
                    case "3" :
                        // If Percentage
                        $deposit_amount = (@$payAmount * @$data['experience']->deposit_amount) / 100;
                        $payAmount = $commission + $deposit_amount;
                        break;
                    default:
                        break;
                }
            }
            //}
        }

        // Insert Booking Info

        $arexp_booking_date = explode(" - ", Session('experience_info')['exp_booking_date']);
        $exp_booking_start = @$arexp_booking_date[0];
        $exp_booking_end = @$arexp_booking_date[1];
        
        $objBooking->experience_id = @$data['experience']->id;
        $objBooking->experience_accomodation_id = @$data['experience_accomodations']->id;
        $objBooking->price_per_person = @$data['experience_accomodations']->room_price;
        $objBooking->booking_amount = @$bookingAmount;
        $objBooking->discount_amount = @$discount;
        $objBooking->commission_amount = @$commission;
        $objBooking->pay_amount = @$deposit_amount;
        $objBooking->arrival_date = (!empty(@Session('reservation_info')['arrival_date'])) ? \Carbon\Carbon::parse(trim(@Session('reservation_info')['arrival_date']))->format("Y-m-d") : "";
        $objBooking->start_date_time = trim(@$exp_booking_start);
        $objBooking->end_date_time = trim(@$exp_booking_end);
        $objBooking->currency = @$data['experience_accomodations']->currency;
        $objBooking->user_id = (!empty(Auth::user()->id)) ? Auth::user()->id : "";
        $objBooking->save();
        $bookingId = $objBooking->id;

        // Insert Booking Exp Info
        $objBookingExperienceInfo = new \App\BookingExperienceInfo();
        $objBookingExperienceInfo->booking_id = @$bookingId;
        $objBookingExperienceInfo->experience_id = @$data['experience']->id;
        $objBookingExperienceInfo->name = @$data['experience']->name;
        $objBookingExperienceInfo->slug = @$data['experience']->slug;
        $objBookingExperienceInfo->center_id = @$data['experience']->center_id;
        $objBookingExperienceInfo->price_per_person = @$data['experience']->price_per_person;
        $objBookingExperienceInfo->currency = @$data['experience']->currency;
        $objBookingExperienceInfo->date_time = @$data['experience']->date_time;
        $objBookingExperienceInfo->start_date_time = @$data['experience']->start_date_time;
        $objBookingExperienceInfo->end_date_time = @$data['experience']->end_date_time;
        $objBookingExperienceInfo->is_full_day_event = @$data['experience']->is_full_day_event;
        $objBookingExperienceInfo->is_recurring = @$data['experience']->is_recurring;
        $objBookingExperienceInfo->deposit_policy = @$data['experience']->deposit_policy;
        $objBookingExperienceInfo->deposit_amount = @$data['experience']->deposit_amount;
        $objBookingExperienceInfo->cancellation_policy_condition = @$data['experience']->cancellation_policy_condition;
        $objBookingExperienceInfo->cancellation_policy_days = @$data['experience']->cancellation_policy_days;
        $objBookingExperienceInfo->rest_of_payment = @$data['experience']->rest_of_payment;
        $objBookingExperienceInfo->rest_of_payment_days = @$data['experience']->rest_of_payment_days;
        $objBookingExperienceInfo->commission = @$data['experience']->commission;
        $objBookingExperienceInfo->tax = @$data['experience']->tax;
        $objBookingExperienceInfo->duration = @$data['experience']->duration;
        $objBookingExperienceInfo->eirly_bird_before_days = @$data['experience']->eirly_bird_before_days;
        $objBookingExperienceInfo->eirly_bird_discount = @$data['experience']->eirly_bird_discount;
        $objBookingExperienceInfo->save();

        // Insert Booking User Info
        $objBookingUserInfo = new \App\BookingUserInfo();
        $objBookingUserInfo->booking_id = @$bookingId;
        $objBookingUserInfo->firstname = @Session('reservation_info')['firstname'];
        $objBookingUserInfo->firstname = @Session('reservation_info')['firstname'];
        $objBookingUserInfo->lastname = @Session('reservation_info')['lastname'];
        $objBookingUserInfo->email = @Session('reservation_info')['email'];
        $objBookingUserInfo->phone = @Session('reservation_info')['phone'];
        $objBookingUserInfo->message = @Session('reservation_info')['message'];
        $objBookingUserInfo->save();

        $parameters = [
            'tid' => $bookingId . time(),
            'order_id' => $bookingId,
            'amount' => \App\Http\Helpers\CommonHelper::get_currency_convertion_rate(@$pay, @$data['experience_accomodations']->currency, 'INR'),
        ];

        $order = Indipay::prepare($parameters);
        return Indipay::process($order);
    }

    public function response(Request $request) {
        Session::forget('reservation_info');
        Session::forget('experience_info');

        // For default Gateway
        $response = Indipay::response($request);
        if (!empty(@$response) || (sizeof(@$response) > 0)) {
            try {
                // Insert Booking Transaction Info
                $objBookingTransactionInfo = new \App\BookingTransactionInfo();
                $objBookingTransactionInfo->booking_id = @$response["order_id"];
                $objBookingTransactionInfo->tracking_id = @$response["tracking_id"];
                $objBookingTransactionInfo->bank_ref_no = @$response["bank_ref_no"];
                $objBookingTransactionInfo->order_status = @$response["order_status"];
                $objBookingTransactionInfo->failure_message = @$response["failure_message"];
                $objBookingTransactionInfo->payment_mode = @$response["payment_mode"];
                $objBookingTransactionInfo->card_name = @$response["card_name"];
                $objBookingTransactionInfo->status_code = @$response["status_code"];
                $objBookingTransactionInfo->status_message = @$response["status_message"];
                $objBookingTransactionInfo->currency = @$response["currency"];
                $objBookingTransactionInfo->amount = @$response["amount"];
                $objBookingTransactionInfo->vault = @$response["vault"];
                $objBookingTransactionInfo->offer_type = @$response["offer_type"];
                $objBookingTransactionInfo->offer_code = @$response["offer_code"];
                $objBookingTransactionInfo->discount_value = @$response["discount_value"];
                $objBookingTransactionInfo->mer_amount = @$response["mer_amount"];
                $objBookingTransactionInfo->eci_value = @$response["eci_value"];
                $objBookingTransactionInfo->retry = @$response["retry"];
                $objBookingTransactionInfo->response_code = @$response["response_code"];
                $objBookingTransactionInfo->billing_notes = @$response["billing_notes"];
                $objBookingTransactionInfo->trans_date = (!empty(@$response["trans_date"])) ? \Carbon\Carbon::parse(@$response["trans_date"])->format("Y-m-d H:i:s") : "";
                $objBookingTransactionInfo->save();

                // Insert Booking Transaction Address Info
                $objBookingTransactionAddressInfo = new \App\BookingTransactionAddressInfo();
                $objBookingTransactionAddressInfo->booking_id = @$response["order_id"];
                $objBookingTransactionAddressInfo->billing_name = @$response["billing_name"];
                $objBookingTransactionAddressInfo->billing_address = @$response["billing_address"];
                $objBookingTransactionAddressInfo->billing_city = @$response["billing_city"];
                $objBookingTransactionAddressInfo->billing_state = @$response["billing_state"];
                $objBookingTransactionAddressInfo->billing_zip = @$response["billing_zip"];
                $objBookingTransactionAddressInfo->billing_country = @$response["billing_country"];
                $objBookingTransactionAddressInfo->billing_tel = @$response["billing_tel"];
                $objBookingTransactionAddressInfo->billing_email = @$response["billing_email"];
                $objBookingTransactionAddressInfo->delivery_name = @$response["delivery_name"];
                $objBookingTransactionAddressInfo->delivery_address = @$response["delivery_address"];
                $objBookingTransactionAddressInfo->delivery_city = @$response["delivery_city"];
                $objBookingTransactionAddressInfo->delivery_state = @$response["delivery_state"];
                $objBookingTransactionAddressInfo->delivery_zip = @$response["delivery_zip"];
                $objBookingTransactionAddressInfo->delivery_country = @$response["delivery_country"];
                $objBookingTransactionAddressInfo->delivery_tel = @$response["delivery_tel"];
                $objBookingTransactionAddressInfo->save();

                $objBooking = \App\Bookings::find(@$response["order_id"]);
                if (!empty($objExperience) || sizeof($objBooking) > 0) {
                    $objBooking->order_status = @$response["order_status"];
                    $objBooking->transaction_id = @$response["tracking_id"];
                    $objBooking->payment_status = @$response["status_code"];
                    $objBooking->save();
                }

                if (@$response["order_status"] != "Success") {
                    return redirect('payment/cancel')
                                    ->with('flash_error_message', 'Booking Failed');
                }
            } catch (Exception $ex) {
                return redirect('payment/cancel')
                                ->with('flash_error_message', 'Booking Failed');
            }

            return redirect('payment/success')
                            ->with('flash_message', 'Your experience has been booked Successfully!');
        } else {
            return redirect('payment/cancel')
                            ->with('flash_error_message', 'Booking Failed');
        }
    }

    public function success(Request $request) {
        $data = array();
        return view('payment-success', $data);
    }

    public function cancel(Request $request) {
        $data = array();
        return view('payment-cancel', $data);
    }

}
