<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Award;
use App\Listings;
use Carbon\Carbon;
use Storage;
use Auth;
use App\User;
use Spatie\OpeningHours\OpeningHours;
use Mail;

class SubscriptionController extends Controller {

    public function __construct() {
        
    }

    /**
     * Show the form for creating a new Listing.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $data = array();
        $objListing = Listings::select("id")->where("user_id", Auth::user()->id)->first();
        if (!empty($objListing)) {
            $param = array();
            $id = $objListing->id;
            $param['order'] = "ASC";
            $param['orderby'] = "name";
            $data['certificates'] = \App\Certificates::get_data($param);
            $data['detinations'] = \App\Destination::select("id", "name")->where("parent", 0)->orderBy("name", "asc")->get();
            $data['types'] = \App\TypeOfSpa::get_data($param);
            $data['awards'] = \App\Award::get_data($param);

            $param['order'] = "ASC";
            $param['orderby'] = "parent";
            $services = \App\Service::get_data($param);
            $arServices = array();
            foreach ($services as $service) {
                if ($service->parent > 0) {
                    $arServices[$service->parent]['subservice'][$service->id] = $service->name;
                } else {
                    $arServices[$service->id] = array("name" => $service->name);
                }
            }
            $data['services'] = $arServices;

            $param = array("where" => array("id" => $id), "limit" => 1);
            $data['elisting'] = Listings::get_data($param);
            $data['elisting'] = (@$data['elisting'][0]) ? : array();

            if ($data['elisting']->state) {
                $data['state'] = \App\Destination::select("name")->where("id", $data['elisting']->state)->orderBy("name", "asc")->first();
            }
            if ($data['elisting']->city) {
                $data['city'] = \App\Destination::select("name")->where("id", $data['elisting']->city)->orderBy("name", "asc")->first();
            }

            // Get Listing Service
            $paramLS['select'] = array('id', 'service_id', 'listing_id');
            $paramLS['where'] = array("listing_id" => $id);
            $listing_services = \App\ListingService::get_data($paramLS);
            $data['listing_services'] = array();
            if (!empty(@$listing_services)) {
                foreach (@$listing_services as $listing_service) {
                    $data['listing_services'][$listing_service->id] = $listing_service->service_id;
                }
            }

            // Get Listing Menu
            $paramLM['where'] = array("listing_id" => $id);
            $paramLM['order'] = "ASC";
            $paramLM['orderby'] = "id";
            $data['listing_menu'] = \App\ListingMenu::get_data($paramLM);

            // Get Listing Destinations
            $paramLD['select'] = array('id', 'destination_id', 'listing_id');
            $paramLD['where'] = array("listing_id" => $id);
            $listing_destinations = \App\ListingDestination::get_data($paramLD);
            $data['listing_destinations'] = array();
            if (!empty(@$listing_destinations)) {
                foreach (@$listing_destinations as $listing_destination) {
                    $data['listing_destinations'][$listing_destination->id] = $listing_destination->destination_id;
                }
            }

            // Get Listing Gallery Images
            $paramEGI['select'] = array('id', 'listing_id', 'image_url', 'image_title');
            $paramEGI['where'] = array("listing_id" => $id);
            $data['imagegalleries'] = \App\ListingImageGallery::get_data($paramEGI);

            // Get Listing Food Gallery Images
            $paramEFGI['select'] = array('id', 'listing_id', 'image_url', 'image_title');
            $paramEFGI['where'] = array("listing_id" => $id);
            $data['foodimagegalleries'] = \App\ListingFoodImageGallery::get_data($paramEFGI);

            // Get Listing Menu Gallery Images
            $paramEFGI['select'] = array('id', 'listing_id', 'image_url', 'image_title');
            $paramEFGI['where'] = array("listing_id" => $id);
            $data['menuimagegalleries'] = \App\ListingMenuImageGallery::get_data($paramEFGI);

            // Get Listing Accomodation Gallery Images
            $paramLAGI['select'] = array('id', 'listing_id', 'image_url', 'image_title');
            $paramLAGI['where'] = array("listing_id" => $id);
            $data['accomodationgalleries'] = \App\ListingAccomodationImageGallery::get_data($paramLAGI);
            return view('subscription', $data);
        }
        return view('subscription', $data);
    }

    /**
     * Store a newly created Listing in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $this->validate($request, [
            'name' => 'required',
            'slug' => 'required',
        ]);
        $name = $request['name'];
        $slug = $request['slug'];

        $meta_title = $request['meta_title'];
        $meta_keywords = $request['meta_keywords'];
        $meta_description = $request['meta_description'];
        
        $city = $request['city'];
        $state = $request['state'];
        $country = $request['country'];
        
        $objCountry = \App\Destination::select("id", "name")->where("id", $country)->orderBy("id", "asc")->first();
        $countryName = (!empty(@$objCountry->name)) ? @$objCountry->name : "Country";
        $objCity = \App\Destination::select("id", "name")->where("id", $city)->orderBy("id", "asc")->first();
        $cityName = (!empty(@$objCity->name)) ? @$objCity->name : "City";
        if ($request->file('banner_image')) {
            $path = $countryName. '/' . $cityName . "/Tile Images";
            $imageUrl = $this->upload_image($request->file('banner_image'), $path, $meta_title);
        }

        $type_of_institution = $request['type_of_institution'];
        $listing_service = ($request['service']) ? $request['service'] : array();

        $zipcode = $request['zipcode'];
        $address = $request['address'];
        $area = $request['area'];
        $landmark = $request['landmark'];
        $nearest_public_transport = $request['nearest_public_transport'];
        $latitude = $request['latitude'];
        $longitude = $request['longitude'];
        $how_to_get_there = $request['how_to_get_there'];
        $email = $request['email'];
        $country_code = $request['country_code'];
        $std_code = $request['std_code'];
        $landline = $request['landline'];
        $mobile = $request['mobile'];
        $website = $request['website'];
        $facebook_url = $request['facebook_url'];
        $instagram_url = $request['instagram_url'];
        $youtube_channel = $request['youtube_channel'];
        $about_us = $request['about_us'];
        $parking = $request['parking'];
        $spa_area = $request['spa_area'];
        $gender = $request['gender'];
        $video = (is_array($request['video'])) ? implode("||", $request['video']) : NULL;
        $awards = (is_array($request['awards'])) ? implode("||", $request['awards']) : NULL;
        $accommodation = $request['accommodation'];
        if ($request->file('accommodation_banner_image')) {
            $path = $countryName. '/' . $cityName . "/".$meta_title."/Accommodation";
            $acmimageUrl = $this->upload_image($request->file('accommodation_banner_image'),$path, $meta_title);
        }

        $food = $request['food'];
        if ($request->file('food_banner_image')) {
            $path = $countryName. '/' . $cityName . "/".$meta_title."/Food";
            $foodimageUrl = $this->upload_image($request->file('food_banner_image'),$path, $meta_title);
            $foodimageTitle = $request->file('food_banner_image')->getClientOriginalName();
        }

        $currency = $request['currency'];
        $menu_ids = ($request['menu_id']) ? $request['menu_id'] : array();
        $menu_title = $request['menu_title'];

        $is_draft = 1;
        $display_on_home = 0;
        $is_featured = 0;

        // Opening Hours
        $opening_hours = $request['opening_hours'];
        $objHours = array();
        if (!empty($opening_hours)) {
            foreach ($opening_hours as $key => $oh) {
                if (!empty(@$oh['opentime']) && !empty(@$oh['closetime'])) {
                    $objHours[$key] = array($oh['opentime'] . "-" . $oh['closetime']);
                }
            }
        }
        $opening_hours = serialize(OpeningHours::create($objHours));

        $image_galleries = (!empty($request['image_gallery_ids'])) ? $request['image_gallery_ids'] : "";
        $food_image_galleries = (!empty($request['food_image_gallery_ids'])) ? $request['food_image_gallery_ids'] : "";
        $menu_image_galleries = (!empty($request['menu_image_gallery_ids'])) ? $request['menu_image_gallery_ids'] : "";
        $accommodation_image_galleries = (!empty($request['accommodation_image_gallery_ids'])) ? $request['accommodation_image_gallery_ids'] : "";

        if (!empty($request['id'])) {
            $objListing = Listings::find($request['id']);
            if ($objListing) {
                $objListing->name = $name;
                $objListing->slug = $slug;
                $objListing->type_of_institution = $type_of_institution;
                $objListing->meta_title = $meta_title;
                $objListing->meta_keywords = $meta_keywords;
                $objListing->meta_description = $meta_description;
                $objListing->address = $address;
                $objListing->area = $area;
                $objListing->landmark = $landmark;
                $objListing->country = $country;
                $objListing->city = $city;
                $objListing->state = $state;
                $objListing->zipcode = $zipcode;
                $objListing->latitude = $latitude;
                $objListing->longitude = $longitude;
                $objListing->longitude = $longitude;
                $objListing->nearest_public_transport = $nearest_public_transport;
                $objListing->email = $email;
                $objListing->landline = $landline;
                $objListing->mobile = $mobile;
                $objListing->website = $website;
                $objListing->facebook_url = $facebook_url;
                $objListing->instagram_url = $instagram_url;
                $objListing->youtube_channel = $youtube_channel;
                $objListing->opening_hours = $opening_hours;
                $objListing->about_us = $about_us;
                $objListing->parking = $parking;
                $objListing->spa_area = $spa_area;
                $objListing->gender = $gender;
                $objListing->video = $video;
                $objListing->awards = $awards;
                $objListing->how_to_get_there = $how_to_get_there;
                $objListing->accommodation = $accommodation;
                $objListing->food = $food;
                $objListing->is_draft = $is_draft;
                $objListing->display_on_home = $display_on_home;
                $objListing->is_featured = $is_featured;
                if ($request['banner_image']) {
                    $objListing->banner_image_url = $imageUrl;
                }
                if ($request['food_banner_image']) {
                    $objListing->food_banner_image_url = $foodimageUrl;
                }
                if ($request['accommodation_banner_image']) {
                    $objListing->accommodation_banner_image_url = $acmimageUrl;
                }
                $objListing->currency = $currency;
                try {
                    $objListing->save();
                    $listing_id = $request['id'];

                    // Move Images from tmp to src
                    if (!empty(@$image_galleries)) {
                        $image_galleries_array = explode("|@|@|", @$image_galleries);
                        foreach ($image_galleries_array as $galimage) {
                            $path = $countryName. '/' . $cityName . "/".$meta_title;
                            $renamefile =  basename($galimage);
                            $dest = $path."/".$renamefile;
                            Storage::disk('azure')->move($galimage, $dest);
                            $objListingImageGallery = new \App\ListingImageGallery();
                            $objListingImageGallery->listing_id = $listing_id;
                            $objListingImageGallery->image_title = basename($dest);
                            $objListingImageGallery->image_url = $dest;
                            $objListingImageGallery->save();
                        }
                    }

                    // Move Images from tmp to src
                    if (!empty(@$food_image_galleries)) {
                        $food_image_galleries_array = explode("|@|@|", @$food_image_galleries);
                        foreach ($food_image_galleries_array as $galimage) {                            
                            $path = $countryName. '/' . $cityName . "/".$meta_title."/Food";
                            $renamefile =  basename($galimage);
                            $dest = $path."/".$renamefile;
                            Storage::disk('azure')->move($galimage, $dest);
                            $objListingImageGallery = new \App\ListingFoodImageGallery();
                            $objListingImageGallery->listing_id = $listing_id;
                            $objListingImageGallery->image_title = basename($dest);
                            $objListingImageGallery->image_url = $dest;
                            $objListingImageGallery->save();
                        }
                    }
                    
                    // Accommodation Move Images from tmp to src
                    if (!empty(@$accommodation_image_galleries)) {
                        $accommodation_image_galleries_array = explode("|@|@|", @$accommodation_image_galleries);
                        foreach ($accommodation_image_galleries_array as $galimage) {                            
                            $path = $countryName. '/' . $cityName . "/".$meta_title."/Accommodation";
                            $renamefile =  basename($galimage);
                            $dest = $path."/".$renamefile;
                            Storage::disk('azure')->move($galimage, $dest);
                            $objListingImageGallery = new \App\ListingAccomodationImageGallery();
                            $objListingImageGallery->listing_id = $listing_id;
                            $objListingImageGallery->image_title = basename($dest);
                            $objListingImageGallery->image_url = $dest;
                            $objListingImageGallery->save();
                        }
                    }

                    // Move Images from tmp to src
                    if (!empty(@$menu_image_galleries)) {
                        $menu_image_galleries_array = explode("|@|@|", @$menu_image_galleries);
                        foreach ($menu_image_galleries_array as $galimage) {
                            $path = $countryName. '/' . $cityName . " Spa Menu/".$meta_title;
                            $renamefile =  basename($galimage);
                            $dest = $path."/".$renamefile;
                            Storage::disk('azure')->move($galimage, $dest);
                            $objListingImageGallery = new \App\ListingMenuImageGallery();
                            $objListingImageGallery->listing_id = $listing_id;
                            $objListingImageGallery->image_title = basename($dest);
                            $objListingImageGallery->image_url = $dest;
                            $objListingImageGallery->save();
                        }
                    }

                    // Listing Service Mapping
                    $existlstserIds = (@$request['hdn_lst_ser_id']) ? explode("||", $request['hdn_lst_ser_id']) : array();
                    if (sizeof(@$existlstserIds) > 0) {
                        foreach (@$existlstserIds as $hdn_lst_ser_id) {
                            if (!in_array($hdn_lst_ser_id, @$listing_service) or empty(@$listing_service)) {
                                $objListingService = \App\ListingService::select("id")->where(array("listing_id" => $listing_id, "service_id" => $hdn_lst_ser_id))->first();
                                if (!empty($objListingService)) {
                                    $objListingService->delete();
                                }
                            }
                        }
                    }

                    if (sizeof(@$listing_service) > 0) {
                        foreach (@$listing_service as $listing_service_id) {
                            if (!in_array($listing_service_id, @$existlstserIds)) {
                                $objListingService = new \App\ListingService();
                                $objListingService->listing_id = $listing_id;
                                $objListingService->service_id = $listing_service_id;
                                $objListingService->save();
                            }
                        }
                    }

                    // Listing Destination Mapping
                    $existlstdstIds = (@$request['hdn_lst_dst_id']) ? explode("||", $request['hdn_lst_dst_id']) : array();
                    if (sizeof(@$existlstdstIds) > 0) {
                        foreach (@$existlstdstIds as $hdn_lst_dst_id) {
                            if (!in_array($hdn_lst_dst_id, @$listing_destination_ids) or empty(@$listing_destination_ids)) {
                                $objListingDestination = \App\ListingDestination::select("id")->where(array("listing_id" => $listing_id, "destination_id" => $hdn_lst_dst_id))->first();
                                if (!empty($objListingDestination)) {
                                    $objListingDestination->delete();
                                }
                            }
                        }
                    }

                    if (!empty(@$listing_destination_ids)) {
                        foreach (@$listing_destination_ids as $listing_destination_id) {
                            if (!in_array($listing_destination_id, @$existlstdstIds)) {
                                $objListingDestination = new \App\ListingDestination();
                                $objListingDestination->listing_id = $listing_id;
                                $objListingDestination->destination_id = $listing_destination_id;
                                $objListingDestination->save();
                            }
                        }
                    }

                    // Listing Menu Mapping
                    if (sizeof(@$menu_title) > 0) {
                        foreach (@$menu_title as $mkey => $menu) {
                            if (!empty(@$menu)) {
                                if (!empty(@$request['menu_id'][$mkey])) {
                                    $objListingMenu = \App\ListingMenu::where("id", @$request['menu_id'][$mkey])->first();
                                } else {
                                    $objListingMenu = new \App\ListingMenu();
                                }
                                $objListingMenu->listing_id = $listing_id;
                                $objListingMenu->menu_title = $menu;
                                $objListingMenu->menu_price = @$request['menu_price'][$mkey];
                                $objListingMenu->menu_description = @$request['menu_description'][$mkey];
                                $objListingMenu->duration = @$request['duration'][$mkey];
                                $objListingMenu->classification = @$request['classification'][$mkey];
                                $objListingMenu->save();
                            } else if (empty(@$menu) && !empty(@$request['menu_id'][$mkey])) {
                                $objListingMenu = \App\ListingMenu::where("id", @$request['menu_id'][$mkey])->first();
                                $objListingMenu->delete();
                            }
                        }
                    }
                    
                    // Email Notifications to Spa owner
                    Mail::send('emails.spa.owner', ['objListing' => @$objListing,'firstname'=>Auth::user()->first_name, 'lastname'=>Auth::user()->last_name], function ($message) {
                        $message->subject("Acknowledgement");
                        $message->from('zen@balanceboat.com', 'Balanceboat');
                        $message->to(Auth::user()->email,Auth::user()->first_name." ".Auth::user()->last_name);
                    });
                    
                    // Email Notifications to admin
                    Mail::send('emails.spa.admin', ['objListing' => @$objListing,'firstname'=>Auth::user()->first_name, 'lastname'=>Auth::user()->last_name], function ($message) {
                        $message->subject("Acknowledgement");
                        $message->from('zen@balanceboat.com', 'Balanceboat');
                        $message->to('zen@balanceboat.com', 'Balanceboat');
                    });
                    
                    
                } catch (Exception $e) {
                    return redirect('subscription')
                                    ->with('flash_error_message', 'Something went wrong');
                }
            } else {
                return redirect('subscription')
                                ->with('flash_error_message', 'Something went wrong');
            }
            return redirect('subscription')
                            ->with('flash_message', 'Listing ' . $objListing->name . ' updated');
        }
    }

    public function upload_image($file, $path, $filename) {
        // check mime type
        if ($file->getClientMimeType() == ( "image/png" ) ||
                $file->getClientMimeType() == ( "image/jpeg" ) ||
                $file->getClientMimeType() == ( "image/gif" ) ||
                $file->getClientMimeType() == ( "image/jpg" )) {
            // feel free to change this logic, that is an example
            $baseFileName = strtolower($file->getClientOriginalName());
            $ext = strtolower($file->getClientOriginalExtension());
            $filenameWithoutExt = preg_replace("~\." . $ext . "$~i", '', $baseFileName);
            $renamefile = $filename . "." . $ext;
            // folder name in container, could be empty
            $folderName = $path;
            // store file on azure blob
            $file->storeAs($folderName, $renamefile, ['disk' => 'azure']);
            // save file name somewhere
            return $saveFileName = $folderName . "/" . $renamefile;
        }
    }

    public function delete_image(Request $request) {
        try {
            $id = $request['id'];
            $objListing = Listings::find($id);
            if (!empty($objListing)) {
                Storage::disk('azure')->delete($objListing->banner_image_url);
                $objListing->banner_image_url = null;
                $objListing->save();
                echo true;
            } else {
                echo 'Something went wrong.';
            }
        } catch (Exception $e) {
            echo 'Something went wrong.';
        }
    }

    public function delete_food_image(Request $request) {
        try {
            $id = $request['id'];
            $objListing = Listings::find($id);
            if (!empty($objListing)) {
                Storage::disk('azure')->delete($objListing->food_banner_image_url);
                $objListing->food_banner_image_url = null;
                $objListing->save();
                echo true;
            } else {
                echo 'Something went wrong.';
            }
        } catch (Exception $e) {
            echo 'Something went wrong.';
        }
    }

    /**
     * Delete Food Image Gallery
     *
     * @return \Illuminate\Http\Response
     */
    public function delete_food_gallery_image(Request $request) {
        try {
            $id = $request['id'];
            $objListingFoodImageGallery = \App\ListingFoodImageGallery::find($id);
            if (!empty($objListingFoodImageGallery)) {
                Storage::disk('azure')->delete($objListingFoodImageGallery->image_url);
                $objListingFoodImageGallery->delete();
                echo true;
            } else {
                echo 'Something went wrong.';
            }
        } catch (Exception $e) {
            echo 'Something went wrong.';
        }
    }

    /**
     * Delete Menu Image Gallery
     *
     * @return \Illuminate\Http\Response
     */
    public function delete_menu_gallery_image(Request $request) {
        try {
            $id = $request['id'];
            $objListingMenuImageGallery = \App\ListingMenuImageGallery::find($id);
            if (!empty($objListingMenuImageGallery)) {
                Storage::disk('azure')->delete($objListingMenuImageGallery->image_url);
                $objListingMenuImageGallery->delete();
                echo true;
            } else {
                echo 'Something went wrong.';
            }
        } catch (Exception $e) {
            echo 'Something went wrong.';
        }
    }

    /**
     * Upload Image Gallery
     *
     * @return \Illuminate\Http\Response
     */
    public function upload_gallery_image(Request $request) {
        $file = $request->file('file');
        if ($file->getClientMimeType() == ( "image/png" ) ||
                $file->getClientMimeType() == ( "image/jpeg" ) ||
                $file->getClientMimeType() == ( "image/gif" ) ||
                $file->getClientMimeType() == ( "image/jpg" )) {
            // feel free to change this logic, that is an example
            $baseFileName = strtolower($file->getClientOriginalName());
            $ext = strtolower($file->getClientOriginalExtension());
            $filenameWithoutExt = preg_replace("~\." . $ext . "$~i", '', $baseFileName);
            $renamefile = $filenameWithoutExt . time() . "." . $ext;
            // folder name in container, could be empty
            $folderName = 'tmp/listings' . '/' . date("Y") . "/" . date("m") . "/" . date("d");
            // store file on azure blob
            $file->storeAs($folderName, $renamefile, ['disk' => 'azure']);
            // save file name somewhere
            $saveFileName = $folderName . "/" . $renamefile;
            echo (json_encode(array('success' => true, 'filename' => $saveFileName)));
        } else {
            echo (json_encode(array('success' => false, 'message' => 'Either file is not valid or file not found')));
        }
    }

    /**
     * Delete Image Gallery
     *
     * @return \Illuminate\Http\Response
     */
    public function delete_gallery_image(Request $request) {
        try {
            $id = $request['id'];
            $objListingImageGallery = \App\ListingImageGallery::find($id);
            if (!empty($objListingImageGallery)) {
                Storage::disk('azure')->delete($objListingImageGallery->image_url);
                $objListingImageGallery->delete();
                echo true;
            } else {
                echo 'Something went wrong.';
            }
        } catch (Exception $e) {
            echo 'Something went wrong.';
        }
    }

    /**
     * Upload ACM Image Gallery
     *
     * @return \Illuminate\Http\Response
     */
    public function upload_accomodation_gallery_image(Request $request) {
        $file = $request->file('file');
        if ($file->getClientMimeType() == ( "image/png" ) ||
                $file->getClientMimeType() == ( "image/jpeg" ) ||
                $file->getClientMimeType() == ( "image/gif" ) ||
                $file->getClientMimeType() == ( "image/jpg" )) {
            // feel free to change this logic, that is an example
            $baseFileName = strtolower($file->getClientOriginalName());
            $ext = strtolower($file->getClientOriginalExtension());
            $filenameWithoutExt = preg_replace("~\." . $ext . "$~i", '', $baseFileName);
            $renamefile = $filenameWithoutExt . time() . "." . $ext;
            // folder name in container, could be empty
            $folderName = 'tmp/listings/accomodations' . '/' . date("Y") . "/" . date("m") . "/" . date("d");
            // store file on azure blob
            $file->storeAs($folderName, $renamefile, ['disk' => 'azure']);
            // save file name somewhere
            $saveFileName = $folderName . "/" . $renamefile;
            echo (json_encode(array('success' => true, 'filename' => $saveFileName)));
        } else {
            echo (json_encode(array('success' => false, 'message' => 'Either file is not valid or file not found')));
        }
    }

    public function delete_accomodation_image(Request $request) {
        try {
            $id = $request['id'];
            $objListing = Listings::find($id);
            if (!empty($objListing)) {
                Storage::disk('azure')->delete($objListing->accommodation_banner_image_url);
                $objListing->accommodation_banner_image_url = null;
                $objListing->save();
                echo true;
            } else {
                echo 'Something went wrong.';
            }
        } catch (Exception $e) {
            echo 'Something went wrong.';
        }
    }

    /**
     * Delete Accomodation Image Gallery
     *
     * @return \Illuminate\Http\Response
     */
    public function delete_accomodation_gallery_image(Request $request) {
        try {
            $id = $request['id'];
            $objExperienceImageGallery = \App\ExperienceImageGallery::find($id);
            if (!empty($objExperienceImageGallery)) {
                Storage::disk('azure')->delete($objExperienceImageGallery->image_url);
                $objExperienceImageGallery->delete();
                echo true;
            } else {
                echo 'Something went wrong.';
            }
        } catch (Exception $e) {
            echo 'Something went wrong.';
        }
    }

    public function getstate() {
        $json_data = array();
        $country = Request()->input('country');
        $search = Request()->input('search');
        if (!empty($country)) {
            $objStates = \App\Destination::select("id", "name")->where("name", 'like', '%' . $search . '%')->where("parent", $country)->get();
            if (sizeof($objStates) > 0) {
                foreach ($objStates as $objState) {
                    array_push($json_data, array("id" => $objState->id, "text" => $objState->name));
                }
            }
        }
        echo json_encode($json_data);
    }

    public function getcity() {
        $json_data = array();
        $state = Request()->input('state');
        $search = Request()->input('search');
        if (!empty($state)) {
            $objCities = \App\Destination::select("id", "name")->where("name", 'like', '%' . $search . '%')->where("parent", $state)->get();
            if (sizeof($objCities) > 0) {
                foreach ($objCities as $objCity) {
                    array_push($json_data, array("id" => $objCity->id, "text" => $objCity->name));
                }
            }
        }
        echo json_encode($json_data);
    }

}
