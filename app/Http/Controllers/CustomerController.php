<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\customer;
use Mail;

class CustomerController extends Controller
{
    
    public $customerMessage = 1;
    public $organizerMessage = 2;

	public function inquiry($conversationid = '')

	{
		$customer = new customer();
		$mixConverseId = explode('-',$conversationid);
		$guestType = $mixConverseId[1];
		$conversationid = $mixConverseId[0];
		$listingId = $customer->getListingID($conversationid);
		$customerMessage = $customer->getMessage($conversationid);
		

		//echo "<pre>";print_r($messageCustomer);die;
		//$customerMessage=json_decode($customerMessages);
		//$customerName = $customerMessage->customer;
// dd($customerMessage);
	//	return view('customer')->with('customerMessage',$customerMessage);
		//return view('customer')->with('customerMessage', $customerMessages); 
		return view('customer', compact('customerMessage','guestType','listingId','conversationid')); 
	}

	public function savemessage()

	{
		$data['message'] = request()->input('message');
        $data['listingid'] = request()->input('listingid');
        $data['message_type'] = request()->input('message_type');
        $data['conversationId'] = request()->input('conversationId');
        
        $objComments = new \App\customer();
        $objComments->message = $data['message'];
        $objComments->conversation_id = $data['conversationId'];
        $objComments->listingid = $data['listingid'];
        $objComments->message_type = $data['message_type'];
        $objComments->save(); 
        $this->sendNotifications($data);
        return redirect()->back()->with('flash_message', 'Message sent!');
	}

	public function sendNotifications($data) {
        
        $customer = new customer();
        $listingInfo = $customer->getListingID($data['conversationId']);
        $customerInfo = $customer->getCustomerInfo($data['conversationId']);
        
        

        if($data['message_type'] == $this->organizerMessage) {
            $name = $listingInfo->name;
            $replyIdforcustomer = $data['conversationId'].'-1';
            
           $email = $customerInfo->email;//die;
             try {
                Mail::send('emails.inquiryreply', ['name' => @$name, 'bodymessage' => @$data['message'], 'conversation' => @$replyIdforcustomer], function ($message) use($email) {
                    $message->subject("Inquiry");
                    $message->from('support@balanceseek.com');
                    $message->to(@$email, 'Balanceseek');
                });
            } catch (Exception $ex) {
           
            }

        } else {

           $name = $customerInfo->name.' '.$customerInfo->lastname;
           $replyIdforcenter = $data['conversationId'].'-2';
           $email = $listingInfo->email;
            try {
                Mail::send('emails.inquiryreply', ['name' => @$name,'bodymessage' => @$data['message'], 'conversation' => @$replyIdforcenter], function ($message) use($email) {
                    $message->subject("Inquiry");
                    $message->from('support@balanceseek.com');
                    $message->to(@$email, 'Balanceseek');
                });
            } catch (Exception $ex) {
            	
            
            }
        }

        try {
                Mail::send('emails.inquiryreply', ['name' => @$name,'bodymessage' => @$data['message'], 'conversation' => @$replyIdforcenter], function ($message) use($email) {
                    $message->subject("Inquiry");
                    $message->from('support@balanceseek.com');
                    $message->to('support@balanceseek.com', 'Balanceseek');
                });
            } catch (Exception $ex) {
            	
            
            }

	}

}
