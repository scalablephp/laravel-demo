<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use Auth;
use Session;
use Mail;
use App\ContactUsEnquiry;

class EmailController extends Controller {

    public function __construct() {
        //$this->middleware(['auth', 'clearance'])->except('index', 'show');
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //return redirect("/");
        //return view('posts.index', compact('posts'));
    }

    /**
     * Send Register your Business Email
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function send_register_your_business_email(Request $request) {
        $this->validate($request, [
            'business_name' => 'required',
            'business_email' => 'required|email',
            'business_website' => 'required|url'
        ]);

        $business_name = $request->has('business_name') ? $request->input('business_name') : "";
        $business_email = $request->has('business_email') ? $request->input('business_email') : "";
        $business_website = $request->has('business_website') ? $request->input('business_website') : "";

        if (!empty(@$business_email)) {
            try {
                Mail::send('emails.register_your_business', ['business_name' => @$business_name, 'business_email' => @$business_email, 'business_website' => @$business_website], function ($message) use($business_email) {
                    $message->subject("Register your Business");
                    $message->from(@$business_email);
                    $message->to('zen@balanceboat.com', 'Balanceboat');
                });
                
                Mail::send('emails.register_your_business_response', ['business_name' => @$business_name, 'business_email' => @$business_email, 'business_website' => @$business_website], function ($message) use($business_email) {
                    $message->subject("Register your Business");
                    $message->from('zen@balanceboat.com', 'Balanceboat');                    
                    $message->to(@$business_email);
                });
            } catch (Exception $ex) {
                return redirect("/contact-us")->with('flash_business_error_message', 'Something went wrong');
            }
        }
        return redirect("/contact-us")->with('flash_business_message', 'Your request has been successfully sent. We will contact you very soon!');
        exit;
    }

    /**
     * Send Contact Us Email
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function send_contact_us_email(Request $request) {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|unique:contactus_enquiry,email',
        ],
        [
            'email.required' => 'Email Id field cannot leave blank',
            'email.unique' => 'Your record already exist in our database',
        ]);

        $name = $request->has('name') ? $request->input('name') : "";
        $email = $request->has('email') ? $request->input('email') : "";
        $phone = $request->has('phone') ? $request->input('phone') : "";
        $content = $request->has('message') ? $request->input('message') : "";

        if (!empty($email)) {
            try {
                    $contactusenquiry = new  ContactUsEnquiry();
                    $contactusenquiry->name = $name;
                    $contactusenquiry->email = $email;
                    $contactusenquiry->phone = $phone;
                    $contactusenquiry->message = $content;
                    $contactusenquiry->save();

                Mail::send('emails.contactus', ['name' => @$name, 'email' => @$email, 'phone' => @$phone, 'bodymessage' => @$content], function ($message) use($email) {
                    $message->subject("Contact Us Email");
                    $message->from('support@balanceseek.com');
                    $message->to('support@balanceseek.com', 'Balanceboat');
                });
            } catch (Exception $ex) {
                return redirect("/contact-us#contact_us")->with('flash_error_message', 'Something went wrong');
            }
        }
        return redirect("/contact-us#contact_us")->with('flash_message', 'Your message has been successfully sent. We will contact you very soon!');
        exit;
    }

    /**
     * Send Inquiry Email
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function send_inquiry_email(Request $request) {
        $this->validate($request, [
            'firstname' => 'required',
            'lastname' => 'required',
            'email' => 'required|email|confirmed'
        ]);

        $firstname = $request->has('firstname') ? $request->input('firstname') : "";
        $lastname = $request->has('lastname') ? $request->input('lastname') : "";
        $email = $request->has('email') ? $request->input('email') : "";
        $phone = $request->has('phone') ? $request->input('phone') : "";
        $content = $request->has('message') ? $request->input('message') : "";
        $exp_id = $request->has('exp_id') ? $request->input('exp_id') : "";

        $experience = \App\Experiences::select("id", "name", "slug")->where("id", @$exp_id)->first();
        if (!empty($email) && !empty($experience) && sizeof($experience) > 0) {
            try {
                Mail::send('emails.inquiry', ['firstname' => @$firstname, 'lastname' => @$lastname, 'email' => @$email, 'phone' => @$phone, 'bodymessage' => @$content, 'experience' => @$experience], function ($message) use($email) {
                    $message->subject("Inquiry");
                    $message->from(@$email);
                    $message->to('zen@balanceboat.com', 'Balanceboat');
                });
            } catch (Exception $ex) {
                return redirect("/experience-inquiry/" . $experience->slug)->with('flash_error_message', 'Something went wrong');
            }
        }
        return redirect("/experience-inquiry/" . $experience->slug)->with('flash_message', 'Thanks for your inquiry. Our team shall get back to you soon!');
        exit;
    }

}
