<?php

namespace App\Http\Helpers;

use Swap;
use DB;

class CommonHelper {

    /**
     * @param null
     * 
     * @return Currency Array
     */
    public static function get_currency() {
        $objCurrency = \App\Currency::select("name")->get()->toArray();
        $currency = array();
        foreach ($objCurrency as $cur) {
            array_push($currency, $cur['name']);
        } // array('INR', 'USD', 'EUR', 'IDR', 'AUD', 'CAD', 'THB');
        return $currency;
    }

    /**
     * @param $currency
     * 
     * @return Currency Symbol
     */
    public static function get_currency_symbol($currency) {
        $currency_symbol = "";
        if ($currency) {
            $objCurrency = \App\Currency::select("symbol")->where("name", $currency)->first();
            $currency_symbol = $objCurrency->symbol;
            //$arCurrency = array('INR' => '&#8377;', 'USD' => '$', 'EUR' => '€', 'IDR' => 'Rp', 'AUD' => '$', 'CAD' => '$', 'THB' => '฿');
            //$currency_symbol = $arCurrency[$currency];
        }
        return $currency_symbol;
    }

    /**
     * @param $currency
     * 
     * @return Currency Symbol
     */
    public static function get_currency_rate($amount = 0, $fromCurrency = "INR", $withsymbol = true) {
        $fromCurrency = (!empty($fromCurrency)) ? $fromCurrency : "INR";
        $toCurrency = CommonHelper::get_site_currency();
        if (!in_array($toCurrency, CommonHelper::get_currency())) {
            session(['site_currency' => "USD"]);
            $toCurrency = "USD";
        }
        $amount = (!empty($amount)) ? $amount : 0;
        if ($fromCurrency != $toCurrency) {
            //$objRate = Swap::latest($fromCurrency . "/" . $toCurrency);
            //$rate = $objRate->getValue();
            $fromCurrencyRate = 0;
            $toCurrencyRate = 0;
            $objCurrency = \App\Currency::select("name", "rate")->whereIn("name", [$fromCurrency, $toCurrency])->get();
            foreach ($objCurrency as $objCur) {
                if ($objCur->name == $fromCurrency) {
                    $fromCurrencyRate = $objCur->rate;
                }
                if ($objCur->name == $toCurrency) {
                    $toCurrencyRate = $objCur->rate;
                }
            }
            $cur_rate = ($toCurrencyRate * $amount) / $fromCurrencyRate;
        } else {
            $rate = 1;
            $cur_rate = ($rate * $amount);
        }
        if ($withsymbol) {
            return CommonHelper::get_currency_symbol($toCurrency) . round($cur_rate);
        } else {
            return round($cur_rate);
        }
    }

    /**
     * 
     * @return Currency Symbol
     */
    public static function get_site_currency() {
        $siteCur = "USD";
        if (@$_GET['global_site_currency']) {
            session(['site_currency' => $_GET['global_site_currency']]);
        } else if (!session()->has('site_currency')) {
            $arCur = CommonHelper::getLocationInfoByIp();
            if (isset($arCur['name']) && !empty($arCur['name'])) {
                $siteCur = $arCur['name'];
            }
        }
        return session('site_currency', $siteCur);
    }

    /**
     * 
     * @return get Destinations
     */
    public static function get_site_destinations() {
        $destinations = \App\Experiences::get_exp_category(" and category.type=1 ");
        /* \App\ExperienceCategory::select('category.id', 'category.name', DB::raw('count(experience_category.experience_id) as total'))
          ->join("category", function($join) {
          $join->on("category_id", "=", "category.id");
          $join->where('type', 1);
          })
          ->join("experiences", function($join) {
          $join->on("experiences.id", "=", "experience_category.experience_id");
          //$join->where("experiences.start_date_time", ">", \Carbon\Carbon::now());
          $join->where('is_draft', 0);
          })->orderBy('name', 'asc')->groupBy('name')->get(); */
        return $destinations;
    }

    /**
     * 
     * @return get Categories
     */
    public static function get_site_categories() {
        $categories = \App\Experiences::get_exp_category(" and category.type=0 ");
        /* \App\ExperienceCategory::select('category.id', 'category.name', DB::raw('count(experience_category.experience_id) as total'))
          ->join("category", function($join) {
          $join->on("category_id", "=", "category.id");
          $join->where('type', 0);
          })
          ->join("experiences", function($join) {
          $join->on("experiences.id", "=", "experience_category.experience_id");
          $join->where('is_draft', 0);
          })->orderBy('name', 'asc')->groupBy('name')->get(); */
        return $categories;
    }

    public static function excerpt($text, $max_length = 140, $cut_off = '...', $keep_word = false) {
        if (strlen($text) <= $max_length) {
            return $text;
        }

        if (strlen($text) > $max_length) {
            if ($keep_word) {
                $text = substr($text, 0, $max_length + 1);

                if ($last_space = strrpos($text, ' ')) {
                    $text = substr($text, 0, $last_space);
                    $text = rtrim($text);
                    $text .= $cut_off;
                }
            } else {
                $text = substr($text, 0, $max_length);
                $text = rtrim($text);
                $text .= $cut_off;
            }
        }

        return $text;
    }

    /**
     * @param $currency
     * 
     * @return currency convertion rate
     */
    public static function get_currency_convertion_rate($amount = 0, $fromCurrency = "USD", $toCurrency = "INR") {
        $fromCurrency = (!empty($fromCurrency)) ? $fromCurrency : "USD";
        $toCurrency = (!empty($toCurrency)) ? $toCurrency : "INR";
        if (!in_array($toCurrency, CommonHelper::get_currency())) {
            session(['site_currency' => "USD"]);
            $toCurrency = "USD";
        }
        $amount = (!empty($amount)) ? $amount : 0;
        if ($fromCurrency != $toCurrency) {
            $objRate = Swap::latest($fromCurrency . "/" . $toCurrency);
            $rate = $objRate->getValue();
        } else {
            $rate = 1;
        }
        $cur_rate = ($rate * $amount);
        return number_format($cur_rate, 2, '.', '');
    }

    /**
     * @param $currency
     * 
     * @return currency convertion rate
     */
    public static function getLocationInfoByIp() {
        $client = @$_SERVER['HTTP_CLIENT_IP'];
        $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
        $remote = @$_SERVER['REMOTE_ADDR'];
        $result = array('country' => '', 'city' => '');
        if (filter_var($client, FILTER_VALIDATE_IP)) {
            $ip = $client;
        } else if (filter_var($forward, FILTER_VALIDATE_IP)) {
            $ip = $forward;
        } else {
            $ip = $remote;
        }
        $ip_data = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ip));
        if ($ip_data && $ip_data->geoplugin_countryName != null) {
            $result['country'] = $ip_data->geoplugin_countryCode;
            $result['city'] = $ip_data->geoplugin_city;
            $result['symbol'] = $ip_data->geoplugin_currencySymbol_UTF8;
            $result['name'] = $ip_data->geoplugin_currencyCode;
        }
        return $result;
    }

    /**
     * @param $url
     * 
     * @return http $url
     */
    public static function addhttp($url) {
        if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
            $url = "http://" . $url;
        }
        return $url;
    }

    /**
     * @param $url
     * 
     * @return http $url
     */
    public static function get_http_response_code($url) {
        $headers = get_headers($url);
        if (substr($headers[0], 9, 3) != "200") {
            return substr($headers[0], 9, 3);
        } else {
            return "";
        }
    }

}

?>