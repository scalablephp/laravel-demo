<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ListingMenu extends Model {

    protected $table = "listing_menu";
    protected $primaryKey = 'id';
    protected $dates = ['created_at'];
    protected $guarded = [
        'id'
    ];

    /**
     * @param  array|null $param
     * @return mixed Fetch  Details of Listing Category
     */
    public static function get_data($param = array()) {
        $orderby = (@$param['orderby']) ? : "id";
        $order = (@$param['order']) ? : "DESC";
        $objListingMenu = ListingMenu::query();
        if (@$param['select']) {
            $objListingMenu = $objListingMenu->select($param['select']);
        }
        if (@$param['where']) {
            $objListingMenu = $objListingMenu->where($param['where']);
        }
        if (@$param['limit']) {
            $objListingMenu = $objListingMenu->take($param['limit']);
        }
        $resListingMenu = $objListingMenu->orderBy($orderby, $order)->get();
        return $resListingMenu;
    }

}
