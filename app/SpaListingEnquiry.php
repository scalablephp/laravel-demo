<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SpaListingEnquiry extends Model
{
    protected $table = "spa_listing_enquiry";
}
