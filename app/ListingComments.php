<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ListingComments extends Model {

    public $timestamps = false;
    protected $table = "listing_comments";
    protected $primaryKey = 'id';
    protected $dates = ['comment_date'];
    protected $guarded = [
        'id'
    ];

    /**
     * @param  array|null $param
     * @return mixed Fetch  Details of category
     */
    public static function get_data($param = array()) {
        $orderby = (@$param['orderby']) ? : "comment_date";
        $order = (@$param['order']) ? : "DESC";
        $objComment = ListingComments::query();
        if (@$param['select']) {
            $objComment = $objComment->select($param['select']);
        }
        if (@$param['where']) {
            $objComment = $objComment->where($param['where']);
        }
        if (@$param['limit']) {
            $objComment = $objComment->take($param['limit']);
        }
        $resComment = $objComment->orderBy($orderby, $order)->get();
        return $resComment;
    }

}
