<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class cleanAboutus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clean:aboutus';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clean about us of listing';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $listing = DB::select('select about_us,id from listings where 1');
         $i=1;
         foreach($listing as $listings) {
            $string =  $listings->about_us;
            $html = 0;
            
            if($string != strip_tags($string)) {
               $html = 1; // contains HTML
            }

            $string2 = explode(',',strip_tags($string));
            foreach($string2 as $key => $value)  {
                if(empty(trim($value))|| trim($value) == '')
                {
                //echo $key;
                unset($string2[$key]);
                } 
            } 
            $newString = implode(',',$string2);
            if($html == 1) {
                $newString = '<p>'.$newString.'</p>';
            }
           $i++;
           // $newString = 'pppppppppptestttttttttt';
            $affected = DB::update('update listings set about_us = "'.addslashes($newString).'" where id = ?', [$listings->id]);
            echo "Listing id ".$listings->id." record has been updated\n";
            

         }
    }
}
