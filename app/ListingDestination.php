<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ListingDestination extends Model {

    protected $table = "listing_destination";
    protected $primaryKey = 'id';
    protected $dates = ['created_at'];
    protected $guarded = [
        'id'
    ];

    /**
     * @param  array|null $param
     * @return mixed Fetch  Details of Listing Destination
     */
    public static function get_data($param = array()) {
        $orderby = (@$param['orderby']) ? : "id";
        $order = (@$param['order']) ? : "DESC";
        $objListingDestination = ListingDestination::query();
        if (@$param['select']) {
            $objListingDestination = $objListingDestination->select($param['select']);
        }
        if (@$param['where']) {
            $objListingDestination = $objListingDestination->where($param['where']);
        }
        if (@$param['limit']) {
            $objListingDestination = $objListingDestination->take($param['limit']);
        }
        $resListingDestination = $objListingDestination->orderBy($orderby, $order)->get();
        return $resListingDestination;
    }

}
