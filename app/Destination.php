<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Destination extends Model
{
    protected $table = "destination";
    protected $primaryKey = 'id';
    protected $dates = ['created_at'];
    protected $guarded = ['id'];

    /**
     * @param  array|null $param
     * @return mixed Fetch  Details of destination
     */
    public static function get_data($param = array())
    {
        $orderby = (@$param['orderby']) ? : "name";
        $order = (@$param['order']) ? : "ASC";
        $objDestination = Destination::query();
        if (@$param['select']) {
            $objDestination = $objDestination->select($param['select']);
        }
        if (@$param['where']) {
            $objDestination = $objDestination->where($param['where']);
        }
        if (@$param['limit']) {
            $objDestination = $objDestination->take($param['limit']);
        }
        $resDestination = $objDestination->orderBy($orderby, $order)->get();
        return $resDestination;
    }

    public function country()
    {
        return $this->belongsTo('App\Destination', 'parent');
    }

    public function state()
    {
        return $this->hasMany('App\Destination', 'parent');
    }

    public function city()
    {
        return $this->state()->with('city');
    }

    public function listings()
    {
        return $this->hasMany('App\Listings', 'country', 'id');
    }

    public function citylistings()
    {
        return $this->hasMany('App\Listings', 'city', 'id');
    }

}
