<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $table = "services";
    protected $primaryKey = 'id';
    protected $dates = ['created_at'];
    protected $guarded = [
        'id'
    ];

    /**
     * @param  array|null $param
     * @return mixed Fetch  Details of destination
     */
    public static function get_data($param = array())
    {
        $orderby = (@$param['orderby']) ? : "name";
        $order = (@$param['order']) ? : "ASC";
        $objService = Service::query();
        if (@$param['select']) {
            $objService = $objService->select($param['select']);
        }
        if (@$param['where']) {
            $objService = $objService->where($param['where']);
        }
        if (@$param['limit']) {
            $objService = $objService->take($param['limit']);
        }
        $resService = $objService->orderBy($orderby, $order)->get();
        return $resService;
    }

    public function subServices()
    {
        return $this->hasMany('App\Service', 'parent', 'id');
    }

    public function parentServices()
    {
        return $this->belongsTo('App\Service', 'parent', 'id');
    }

    public function listings()
    {
        return $this->belongsToMany('App\Listings', 'listing_service', 'service_id', 'listing_id');
    }
}
