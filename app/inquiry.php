<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class inquiry extends Model
{
	protected $fillable = [
		'name', 'lastname','email','phone','message','conversation_id',
	];
}
