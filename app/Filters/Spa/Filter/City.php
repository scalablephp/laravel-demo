<?php

namespace App\Filters\Spa\Filter;

use Illuminate\Database\Eloquent\Builder;

class City implements Filter
{
    /**
     * Apply a given search value to the builder instance.
     *
     * @param Builder $builder
     * @param mixed $value
     * @return Builder $builder
     */
    public static function apply(Builder $builder, $value)
    {
        //return $builder->join('destination', 'listings.city', 'destination.id')
        //->whereRaw('lower(destination.name) like (?)', ["%{$value}%"]);

        return $builder->where('city', $value);
    }
}
