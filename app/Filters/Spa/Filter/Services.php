<?php

namespace App\Filters\Spa\Filter;

use Illuminate\Database\Eloquent\Builder;

class Services implements Filter
{
    /**
     * Apply a given search value to the builder instance.
     *
     * @param Builder $builder
     * @param mixed $value
     * @return Builder $builder
     */
    public static function apply(Builder $builder, $value)
    {
        return $builder->whereIn('listings.id', function ($q) use ($value) {
            $q->select('listing_id')->from('listing_service')->whereIn('service_id', $value);
        });
    }
}
