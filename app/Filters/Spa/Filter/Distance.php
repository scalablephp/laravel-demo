<?php

namespace App\Filters\Spa\Filter;

use Illuminate\Database\Eloquent\Builder;

class Distance implements Filter
{
    /**
     * Apply a given search value to the builder instance.
     *
     * @param Builder $builder
     * @param mixed $value
     * @return Builder $builder
     */
    public static function apply(Builder $builder, $value)
    {
        if ($result = self::getPositionFromCity()) {
            return $builder->geofence($result[0], $result[1], $value);
        }
        return $builder;
    }

    private static function getPositionFromCity()
    {
        $geocodeData = array();
        $request = \Request::all();
        if (!empty($request['latitude']) && !empty($request['longitude'])) {
            //$geocodeData = explode(',', $request['position']);
            $geocodeData[] = $request['latitude'];
            $geocodeData[] = $request['longitude'];
            //dd($geocodeData);
            return $geocodeData;
        }

        return false;
    }
}
