<?php

namespace App\Filters\Spa;

use App\Listings;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;

class SpaFilter
{
    public static function apply(Request $filters)
    {
        $query = static::applyDecoratorsFromRequest($filters, (new Listings)->newQuery());
        return $query;
    }

    private static function applyDecoratorsFromRequest(Request $request, Builder $query)
    {
        $query->select('listings.*');
        foreach ($request->all() as $filterName => $value) {
            $decorator = static::createFilterDecorator($filterName);
            if (static::isValidDecorator($decorator) && !empty($value)) {
                $query = $decorator::apply($query, $value);
            }
        }
        return $query;
    }

    private static function createFilterDecorator($name)
    {
        return __NAMESPACE__ . '\\Filter\\' . studly_case($name);
    }

    private static function isValidDecorator($decorator)
    {
        return class_exists($decorator);
    }

    private static function getResults(Builder $query)
    {
        return $query->get();
    }
}
