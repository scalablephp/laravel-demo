<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Visitors extends Model
{
    protected $fillable = ['ip_address', 'listing_id', 'visited_date', 'hits'];

    protected $table = 'visitors';

    public $timestamps = false;
}
